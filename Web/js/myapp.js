angular.module('myApp',['ngRoute','hoController','imbController','iumController','domisiliUsahaController'])
	.config(['$routeProvider',function($routeProvider) {
		$routeProvider
		.when('/',{templateUrl:'view/home.html'})
		.when('/401',{templateUrl:'view/404.html'})
		.when('/news',{templateUrl:'view/news.html'})
		.when('/read',{templateUrl:'newsdetail.html'})
		.when('/portal',{templateUrl:'portal.html'})
		.when('/layanan',{templateUrl:'layanan.html'})

	}])