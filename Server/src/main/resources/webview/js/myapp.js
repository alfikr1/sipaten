angular.module('myApp',['ngRoute','hoController','imbController','iumController','domisiliUsahaController'])
	.config(['$routeProvider',function($routeProvider) {
		$routeProvider
		.when('/',{templateUrl:'webview/pages/home.html'})
		.when('/401',{templateUrl:'webview/pages/404.html'})
		.when('/news',{templateUrl:'webview/pages/news.html'})
		.when('/read',{templateUrl:'webview/pages/newsdetail.html'})
		.when('/portal',{templateUrl:'webview/pages/portal.html'})
		.when('/layanan',{templateUrl:'webview/pages/layanan.html'})
                .when('/login',{templateUrl:'login.html'});

	}]);