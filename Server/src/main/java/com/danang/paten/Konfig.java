// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten;

import com.danang.paten.domain.MessageFormat;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.danang.paten.konfigurasi.SpringConfig;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.danang.paten.domain.Petugas;
import com.danang.paten.util.CallerNama;
import com.danang.paten.util.PlayAudio;
import com.danang.paten.util.ProsesFile;
import com.danang.paten.view.VideoMain;
import com.danang.paten.view.AdminFrame;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

public class Konfig {

    private static ApplicationContext context;
    private static Konfig provider;
    private AdminFrame frame;
    private VideoMain videoMain;
    private Petugas petugas;
    private final Gson gson;
    private LinkedBlockingQueue queue;
    private HazelcastInstance antrian;
    private PlayAudio pa;
    private CallerNama cn;
    private ProsesFile pf;
    private WebService wsService;
    private Channel pemeriksaanChannel;
    private Channel penetapanChannel;
    private Channel loketChannel;
    private Logger log = Logger.getLogger(Konfig.class);

    public Konfig() {
        gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        context = new AnnotationConfigApplicationContext(SpringConfig.class);
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            pemeriksaanChannel = connection.createChannel();
            pemeriksaanChannel.queueDeclare("PEMERISAAN", false, false, false, new HashMap<>());
            penetapanChannel = connection.createChannel();
            penetapanChannel.queueDeclare("PENETAPAN", false, false, false, null);
            loketChannel = connection.createChannel();
            loketChannel.queueDeclare("LOKET", false, false, false, null);
        } catch (IOException | TimeoutException e) {
            log.error("Error create channel", e);
        }
        init();
    }

    public Channel getPemeriksaanChannel() {
        return pemeriksaanChannel;
    }

    public Channel getPenetapanChannel() {
        return penetapanChannel;
    }

    private void init() {
        pa = new PlayAudio();
        pf = new ProsesFile();
        cn = new CallerNama();
        cn.start();
        pa.start();
        pf.start();
    }

    public void sendNotifikasi() {
        if (wsService != null) {
            Map<String, Object> map = new HashMap<>();
            MessageFormat res = new MessageFormat();
            String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
            HazelcastInstance hi = antrian;
            BlockingQueue<HashMap<String, Object>> antrianPerijinan = hi.getQueue("antrianPERIJINAN" + date);
            IAtomicLong nomorAntrianPerijinan = hi.getAtomicLong("nomor-antrianPERIJINAN" + date);
            map.put("sisaPerijinan", antrianPerijinan.size());
            map.put("totalPerijinan", nomorAntrianPerijinan.get());
            BlockingQueue<HashMap<String, Object>> antrianNonPerijinan = hi.getQueue("antrianNONPERIJINAN" + date);
            IAtomicLong nomorAntrianNonPerijinan = hi.getAtomicLong("nomor-antrianNONPERIJINAN" + date);
            map.put("sisaNonPerijinan", antrianNonPerijinan.size());
            map.put("totalNonPerijinan", nomorAntrianNonPerijinan.get());
            BlockingQueue<HashMap<String, Object>> antrianHelpdesk = hi.getQueue("antrianHELPDESK" + date);
            IAtomicLong nomorAntrianHelpdesk = hi.getAtomicLong("nomor-antrianHELPDESK" + date);
            map.put("sisaHelpDesk", antrianHelpdesk.size());
            map.put("totalHelpdesk", nomorAntrianHelpdesk.get());
            res = new MessageFormat();
            res.setCommand("antrian-reply");
            res.setError("");
            res.setMsg(gson.toJson(map));
            wsService.sendNotifikasiAll(gson.toJson(res));
        }
    }

    public CallerNama getNama() {
        return cn;
    }

    public void setWsService(WebService wsService) {
        this.wsService = wsService;
    }

    public ProsesFile getPf() {
        return pf;
    }

    public PlayAudio getPa() {
        return pa;
    }

    public void setAntrian(HazelcastInstance antrian) {
        this.antrian = antrian;
    }

    public HazelcastInstance getAntrian() {
        return antrian;
    }

    public LinkedBlockingQueue getQueue() {
        return queue;
    }

    public Gson getGson() {
        return gson;
    }

    public Petugas getPetugas() {
        return petugas;
    }

    public void setPetugas(Petugas petugas) {
        this.petugas = petugas;
    }

    public static synchronized Konfig getInstance() {
        Konfig temp;
        if (Konfig.provider == null) {
            Konfig.provider = new Konfig();
            temp = Konfig.provider;
        } else if (Konfig.context == null) {
            Konfig.provider = new Konfig();
            temp = Konfig.provider;
        } else {
            temp = Konfig.provider;
        }
        return temp;
    }

    public ApplicationContext getContext() {
        return Konfig.context;
    }

    public AdminFrame getFrame() {
        return this.frame;
    }

    public void setFrame(final AdminFrame frame) {
        this.frame = frame;
    }

    public VideoMain getVideoMain() {
        return this.videoMain;
    }

    public void setVideoMain(final VideoMain videoMain) {
        this.videoMain = videoMain;
    }
}
