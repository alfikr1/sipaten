/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten;

import com.danang.paten.controller.FtpUserManager;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;

/**
 *
 * @author danang
 */
public class FtpServer {
    public static void bukaFTP(int port) throws FtpException{
        FtpServerFactory serverFactory=new FtpServerFactory();        
        ListenerFactory factory =new ListenerFactory();        
        factory.setPort(port);
        serverFactory.setUserManager(new FtpUserManager());
        serverFactory.addListener("default", factory.createListener());
        org.apache.ftpserver.FtpServer server = serverFactory.createServer();        
        server.start();
    }
    public static void main(String[] args) throws FtpException {
        FtpServer.bukaFTP(9871);
    }
}
