// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import java.io.File;

public interface FolderListener
{
    void fileAdded(final File p0);
    
    void fileChanged(final File p0);
    
    void fileRemoved(final String p0);
    
    void notivy();
}
