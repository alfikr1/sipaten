// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageImpl;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer
{
    private final Gson gson;
    
    public JsonTransformer() {
        this.gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
    }
    
    @Override
    public String render(final Object model) throws Exception {
        if (model instanceof PageImpl) {
            final Map<String, Object> map = new HashMap<>();
            final PageImpl od = (PageImpl)model;
            map.put("nomor", od.getNumber());
            map.put("", od.getNumberOfElements());
            map.put("data", od.getContent());
            map.put("size", od.getSize());
            map.put("totalPage", od.getTotalPages());
            return this.gson.toJson(map);
        }
        return this.gson.toJson(model);
    }
}
