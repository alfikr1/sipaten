// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JComponent;

public class CleanUpText
{
    public static void clean(final JComponent comp) {
        if (comp instanceof JPanel) {
            for (final Component c : comp.getComponents()) {
                bersih(c);
            }
        }
        else {
            bersih(comp);
        }
    }
    
    private static void bersih(final Component c) {
        if (c instanceof JTextField) {
            ((JTextField)c).setText("");
        }
    }
}
