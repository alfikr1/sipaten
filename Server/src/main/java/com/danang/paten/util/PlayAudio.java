/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import com.danang.paten.Konfig;
import jaco.mp3.player.MP3Player;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author danang
 */
public class PlayAudio extends Thread {

    private List<File> items;
    private boolean running = false;
    private final MP3Player player;
    private BlockingQueue<Map<String, Object>> listData = new LinkedBlockingQueue<>();

    public PlayAudio() {
        player = new MP3Player();
    }

    public PlayAudio(List<File> items) {
        player = new MP3Player();
        this.items = items;
    }

    @Override
    public void run() {
        while (true) {
            if (!listData.isEmpty()) {
                Map<String, Object> map = listData.poll();
                String model = (String) map.get("jenis");
                if (model == null) {
                    items = MyStt.bunyi((String) map.get("loket"), (int) map.get("antri"), (int) map.get("urut"));
                    try {
                        Konfig.getInstance().getVideoMain().setAntrean(map.get("loket").toString() + map.get("antri").toString(), map.get("urut").toString());
                    } catch (Exception e) {
                    }
                    playData();
                } else {
                    try {
                        String fname = "capil.mp3";
                        ambilData((String) map.get("nama"), fname);
                    } catch (Exception e) {
                        e.printStackTrace(System.err);
                    }
                }
            }
        }
    }

    private SourceDataLine getLine(AudioFormat audioFormat) throws LineUnavailableException {
        SourceDataLine res = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
        res = (SourceDataLine) AudioSystem.getLine(info);
        res.open(audioFormat);
        return res;
    }

    private void rawPlay(AudioFormat targetFormat, AudioInputStream din) throws LineUnavailableException, IOException {
        byte[] data = new byte[4096];
        SourceDataLine line = getLine(targetFormat);
        if (line != null) {
            line.start();
            int nBytesRead = 0, nBytesWritten = 0;
            while (nBytesRead != -1) {
                nBytesRead = din.read(data, 0, data.length);
                if (nBytesRead != -1) {
                    nBytesWritten = line.write(data, 0, nBytesRead);
                }
            }
            line.drain();
            line.stop();
            line.close();
            din.close();
        }
    }

    public void playData() {
        running = true;
        items.stream().forEach((f) -> {
            try {
                AudioInputStream in = AudioSystem.getAudioInputStream(f);
                AudioInputStream din = null;
                AudioFormat baseFormat = in.getFormat();
                AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                        baseFormat.getSampleRate(), 16, baseFormat.getChannels(),
                        baseFormat.getChannels() * 2, baseFormat.getSampleRate(), false);
                din = AudioSystem.getAudioInputStream(decodedFormat, in);
                rawPlay(decodedFormat, din);
                //din.reset();
                in.close();
                din.close();
            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
                e.printStackTrace(System.err);
                running = false;
            }
        });
        items.clear();
        interrupt();
        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public synchronized void setData(String th, int a, int b) throws InterruptedException {
        Map<String, Object> map = new HashMap<>();
        map.put("jenis",null);
        map.put("loket", th);
        map.put("antri", a);
        map.put("urut", b);
        listData.add(map);

    }

    public synchronized void setNama(String nama) {
        Map<String,Object> map = new HashMap<>();
        map.put("jenis","ektp");
        map.put("nama", "permohonan atas nama "+nama+" mohon memasuki ruang rekam e k t p");
        listData.add(map);
    }

    private void ambilData(String data, String filename) throws UnsupportedEncodingException, URISyntaxException, IOException, LineUnavailableException, UnsupportedAudioFileException {
        HttpClient client = HttpClientBuilder.create().setUserAgent("Mozilla").build();
        StringBuilder sb = new StringBuilder();
        sb.append("http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob");
        sb.append("&q=");
        sb.append(URLEncoder.encode(data, "UTF-8"));
        sb.append("&tl=id");
        System.out.println(sb.toString());
        HttpGet get = new HttpGet(new URI(sb.toString()));
        HttpResponse response = client.execute(get);
        int code = response.getStatusLine().getStatusCode();
        if (code >= 200 && code < 300) {
            File fff = new File("./Sounds/" + filename);
            BufferedInputStream bis = new BufferedInputStream(response.getEntity().getContent());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fff));
            int inByte;
            while ((inByte = bis.read()) != -1) {
                bos.write(inByte);
            }
            bis.close();
            bos.close();
            AudioInputStream in = AudioSystem.getAudioInputStream(fff);
            AudioInputStream din = null;
            AudioFormat baseFormat = in.getFormat();
            AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    baseFormat.getSampleRate(), 16, baseFormat.getChannels(),
                    baseFormat.getChannels() * 2, baseFormat.getSampleRate(), false);
            din = AudioSystem.getAudioInputStream(decodedFormat, in);
            rawPlay(decodedFormat, din);
            //din.reset();
            in.close();
            din.close();
        }
    }

}
