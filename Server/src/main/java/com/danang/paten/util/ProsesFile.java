/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import com.hazelcast.core.HazelcastInstance;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.servlet.MultipartConfigElement;
import spark.Request;

/**
 *
 * @author danang
 */
public class ProsesFile extends Thread {

    private BlockingQueue<Map<String, Object>> listData = new LinkedBlockingQueue<>();
    
    public ProsesFile(){
        
    }

    @Override
    public void run() {
        while(true){
            if(!listData.isEmpty()){
                Map<String, Object> map = listData.poll();
                receiveData((Request)map.get("request"),(String)map.get("jenis"),
                        (long)map.get("nomor"),(BlockingQueue<HashMap<String,Object>>)map.get("antrian"));
            }
        }        
    }
    public void receiveData(Request request,String jenis,long nomor,BlockingQueue<HashMap<String,Object>> antrian){
        String namaFile="./img/"+new SimpleDateFormat("ddMMyyyyhh:mm:ss").format(new Date())+jenis+"nomor"+nomor+".jpg";
        File file = new File(namaFile);
        request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
        try (InputStream is = request.raw().getPart("file").getInputStream()) {
            // Use the input stream to create a file
            byte[] buffer=new byte[is.available()];
            is.read(buffer);
            OutputStream os = new FileOutputStream(file);
            os.write(buffer);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        HashMap<String,Object> map = new HashMap<>();
        map.put("file", file.getName());
        map.put("nomor", nomor);
        antrian.add(map);
    }
    public synchronized void setData(BlockingQueue<HashMap<String, Object>> antrian,Request request,long nomor,String jenis){
        Map<String,Object> map = new HashMap<>();
        map.put("jenis", jenis);
        map.put("antrian", antrian);
        map.put("request", request);
        map.put("nomor", nomor);
        listData.add(map);
    }
}
