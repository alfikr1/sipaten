/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.smslib.AGateway;
import org.smslib.GatewayException;
import org.smslib.IOutboundMessageNotification;
import org.smslib.OutboundMessage;
import org.smslib.TimeoutException;

/**
 *
 * @author Danang
 */
public class OutboundMessageNotifi implements IOutboundMessageNotification {

    private String msg;
    private Logger log = Logger.getLogger(OutboundMessageNotifi.class);

    @Override
    public void process(AGateway ag, OutboundMessage om) {

        try {
            ag.sendMessage(om);
        } catch (TimeoutException | GatewayException | IOException | InterruptedException e) {
            log.error("kirim sms gagal", e);
            throw new IllegalStateException("pengiriman sms gagal");
        }
    }

}
