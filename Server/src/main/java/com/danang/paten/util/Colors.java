// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.awt.Color;
import java.awt.Paint;
import java.awt.GradientPaint;
import java.util.Map;

public class Colors
{
    private Map<Integer, GradientPaint> colorMap;
    private static Colors colorUtils;
    private GradientPaint glossyTopBtnColor;
    private Paint bgColor;
    private Color textColor;
    
    public Colors() {
        this.colorMap = new HashMap<Integer, GradientPaint>();
        this.textColor = Color.WHITE;
    }
    
    public static Colors getInStance() {
        if (Colors.colorUtils == null) {
            Colors.colorUtils = new Colors();
        }
        return Colors.colorUtils;
    }
    
    public List getStandardColor(final int theme, final int startPoint, final int endPoint) {
        final List colors = new ArrayList();
        GradientPaint bgColor = null;
        switch (theme) {
            case 202: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(0, 140, 0), 0.0f, endPoint, new Color(0, 85, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 203: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(31, 175, 114), 0.0f, endPoint, new Color(20, 113, 74));
                this.textColor = Color.WHITE;
                break;
            }
            case 204: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(102, 223, 36), 0.0f, endPoint, new Color(68, 154, 23));
                this.textColor = Color.WHITE;
                break;
            }
            case 205: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(121, 232, 98), 0.0f, 3 * endPoint / 4, new Color(61, 208, 31));
                this.textColor = Color.WHITE;
                break;
            }
            case 206: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(117, 198, 6), 0.0f, endPoint, new Color(68, 116, 4));
                this.textColor = Color.WHITE;
                break;
            }
            case 207: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(181, 223, 38), 0.0f, 3 * endPoint / 4, new Color(137, 170, 26));
                this.textColor = Color.WHITE;
                break;
            }
            case 208: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 100, 100), 0.0f, endPoint, new Color(255, 0, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 209: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 0, 6), 0.0f, endPoint, new Color(181, 0, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 210: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(251, 139, 62), 0.0f, endPoint, new Color(255, 102, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 211: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(247, 174, 24), 0.0f, endPoint, new Color(255, 133, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 212: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(185, 181, 0), 0.0f, endPoint, new Color(123, 120, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 213: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(253, 247, 11), 0.0f, endPoint, new Color(211, 204, 2));
                this.textColor = Color.BLACK;
                break;
            }
            case 214: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 233, 18), 0.0f, endPoint, new Color(255, 213, 0));
                this.textColor = Color.BLACK;
                break;
            }
            case 215: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 166), 0.0f, endPoint, new Color(255, 255, 56));
                this.textColor = Color.BLACK;
                break;
            }
            case 216: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(202, 62, 2), 0.0f, 3 * endPoint / 4, new Color(118, 35, 1));
                this.textColor = Color.WHITE;
                break;
            }
            case 217: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(232, 194, 125), 0.0f, 3 * endPoint / 4, new Color(212, 151, 37));
                this.textColor = Color.WHITE;
                break;
            }
            case 218: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(248, 234, 203), 0.0f, 3 * endPoint / 4, new Color(236, 205, 132));
                this.textColor = Color.BLACK;
                break;
            }
            case 224: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(44, 105, 180), 0.0f, endPoint, new Color(5, 25, 114));
                this.textColor = Color.WHITE;
                break;
            }
            case 225: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(49, 120, 206), 0.0f, endPoint, new Color(35, 84, 146));
                this.textColor = Color.WHITE;
                break;
            }
            case 226: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(58, 92, 252), 0.0f, endPoint, new Color(3, 37, 188));
                this.textColor = Color.WHITE;
                break;
            }
            case 227: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(17, 136, 255), 0.0f, endPoint, new Color(0, 96, 194));
                this.textColor = Color.WHITE;
                break;
            }
            case 228: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(51, 191, 238), 0.0f, endPoint, new Color(17, 160, 208));
                this.textColor = Color.WHITE;
                break;
            }
            case 229: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(82, 0, 164), 0.0f, endPoint, new Color(44, 0, 89));
                this.textColor = Color.WHITE;
                break;
            }
            case 230: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(203, 64, 239), 0.0f, endPoint, new Color(186, 0, 255));
                this.textColor = Color.WHITE;
                break;
            }
            case 231: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(165, 117, 239), 0.0f, endPoint, new Color(107, 60, 173));
                this.textColor = Color.WHITE;
                break;
            }
            case 232: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(170, 0, 128), 0.0f, endPoint, new Color(115, 0, 85));
                this.textColor = Color.WHITE;
                break;
            }
            case 233: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(238, 83, 133), 0.0f, endPoint, new Color(220, 22, 86));
                this.textColor = Color.WHITE;
                break;
            }
            case 234: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 174, 235), 0.0f, endPoint, new Color(255, 128, 223));
                this.textColor = Color.WHITE;
                break;
            }
            case 219: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(90, 90, 90), 0.0f, endPoint, new Color(0, 0, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 220: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(90, 90, 90), 0.0f, endPoint, new Color(70, 70, 70));
                this.textColor = Color.WHITE;
                break;
            }
            case 221: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(163, 163, 163), 0.0f, endPoint, new Color(128, 128, 128));
                this.textColor = Color.WHITE;
                break;
            }
            case 222: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(151, 164, 170), 0.0f, 3 * endPoint / 4, new Color(120, 137, 145));
                this.textColor = Color.WHITE;
                break;
            }
            case 223: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(68, 113, 153), 0.0f, endPoint, new Color(32, 53, 72));
                this.textColor = Color.WHITE;
                break;
            }
            case 235: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(148, 148, 255), 0.0f, endPoint, new Color(98, 98, 255));
                this.textColor = Color.WHITE;
                break;
            }
            case 201: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(236, 241, 242), 0.0f, endPoint, new Color(206, 220, 223));
                this.textColor = Color.BLACK;
                break;
            }
            default: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(149, 159, 207), 0.0f, endPoint, new Color(85, 134, 194));
                this.textColor = Color.BLACK;
                break;
            }
        }
        colors.add(bgColor);
        colors.add(this.textColor);
        return colors;
    }
    
    public List getGradientColor(final int theme, final int startPoint, final int endPoint) {
        final List colors = new ArrayList();
        GradientPaint bgColor = null;
        switch (theme) {
            case 101: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(136, 255, 136), 0.0f, endPoint, new Color(1, 54, 2));
                this.textColor = Color.WHITE;
                break;
            }
            case 102: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(170, 240, 210), 0.0f, endPoint, new Color(12, 69, 45));
                this.textColor = Color.WHITE;
                break;
            }
            case 103: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(73, 252, 7), 0.0f, endPoint, new Color(0, 64, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 104: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(185, 234, 36), 0.0f, endPoint, new Color(68, 116, 4));
                this.textColor = Color.WHITE;
                break;
            }
            case 106: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(217, 242, 138), 0.0f, endPoint, new Color(168, 216, 24));
                this.textColor = Color.BLACK;
                break;
            }
            case 105: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(159, 255, 159), 0.0f, endPoint, new Color(61, 208, 31));
                this.textColor = Color.BLACK;
                break;
            }
            case 107: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(249, 200, 0), 0.0f, endPoint, new Color(242, 40, 30));
                this.textColor = Color.WHITE;
                break;
            }
            case 108: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(249, 200, 0), 0.0f, endPoint, new Color(181, 0, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 109: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 197, 63), 0.0f, endPoint, new Color(255, 102, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 110: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(255, 133, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 111: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(123, 120, 0));
                this.textColor = Color.BLACK;
                break;
            }
            case 112: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(211, 204, 2));
                this.textColor = Color.BLACK;
                break;
            }
            case 113: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(255, 201, 14));
                this.textColor = Color.BLACK;
                break;
            }
            case 114: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(255, 255, 56));
                this.textColor = Color.BLACK;
                break;
            }
            case 123: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(71, 232, 252), 0.0f, endPoint, new Color(5, 25, 114));
                this.textColor = Color.WHITE;
                break;
            }
            case 124: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(34, 85, 146));
                this.textColor = Color.WHITE;
                break;
            }
            case 125: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(71, 232, 252), 0.0f, endPoint, new Color(3, 37, 188));
                this.textColor = Color.WHITE;
                break;
            }
            case 126: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(71, 232, 252), 0.0f, endPoint, new Color(6, 113, 196));
                this.textColor = Color.WHITE;
                break;
            }
            case 127: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(71, 232, 252), 0.0f, endPoint, new Color(17, 160, 208));
                this.textColor = Color.WHITE;
                break;
            }
            case 128: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(186, 0, 255), 0.0f, endPoint, new Color(44, 0, 89));
                this.textColor = Color.WHITE;
                break;
            }
            case 131: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(170, 170, 255), 0.0f, endPoint, new Color(98, 98, 255));
                this.textColor = Color.WHITE;
                break;
            }
            case 129: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(186, 60, 255));
                this.textColor = Color.WHITE;
                break;
            }
            case 130: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(192, 128, 255));
                this.textColor = Color.WHITE;
                break;
            }
            case 132: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(115, 0, 85));
                this.textColor = Color.WHITE;
                break;
            }
            case 133: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(220, 22, 86));
                this.textColor = Color.WHITE;
                break;
            }
            case 134: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endPoint, new Color(255, 128, 223));
                this.textColor = Color.WHITE;
                break;
            }
            case 118: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(150, 150, 150), 0.0f, endPoint, new Color(0, 0, 0));
                this.textColor = Color.WHITE;
                break;
            }
            case 135: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(218, 228, 231), 0.0f, endPoint, new Color(255, 0, 0));
                this.textColor = Color.BLACK;
                break;
            }
            case 115: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(202, 62, 2), 0.0f, endPoint, new Color(118, 35, 1));
                this.textColor = Color.WHITE;
                break;
            }
            case 116: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(232, 194, 125), 0.0f, endPoint, new Color(212, 151, 37));
                this.textColor = Color.WHITE;
                break;
            }
            case 117: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(248, 234, 203), 0.0f, endPoint, new Color(236, 205, 132));
                this.textColor = Color.BLACK;
                break;
            }
            case 119: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(200, 200, 200), 0.0f, endPoint, new Color(70, 70, 70));
                this.textColor = Color.WHITE;
                break;
            }
            case 120: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(183, 183, 183), 0.0f, endPoint, new Color(128, 128, 128));
                this.textColor = Color.WHITE;
                break;
            }
            case 122: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(205, 210, 214), 0.0f, 3 * endPoint / 4, new Color(120, 137, 145));
                this.textColor = Color.WHITE;
                break;
            }
            case 121: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(141, 175, 205), 0.0f, endPoint, new Color(32, 53, 72));
                this.textColor = Color.WHITE;
                break;
            }
            default: {
                bgColor = new GradientPaint(0.0f, startPoint, new Color(149, 159, 207), 0.0f, endPoint, new Color(85, 134, 194));
                this.textColor = Color.BLACK;
                break;
            }
        }
        colors.add(bgColor);
        colors.add(this.textColor);
        return colors;
    }
    
    public List getGlossyColor(final int theme, final int startPoint, final int endpoint) {
        final List glossyColors = new ArrayList();
        switch (theme) {
            case 2: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(3, 167, 7), 0.0f, endpoint, new Color(2, 117, 5, 150));
                this.bgColor = new Color(1, 54, 2);
                this.textColor = Color.WHITE;
                break;
            }
            case 3: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(72, 223, 159), 0.0f, endpoint, new Color(41, 218, 142, 100));
                this.bgColor = new Color(20, 113, 74);
                this.textColor = Color.WHITE;
                break;
            }
            case 5: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(219, 255, 202), 0.0f, endpoint, new Color(219, 255, 187, 100));
                this.bgColor = new Color(97, 204, 0);
                this.textColor = Color.BLACK;
                break;
            }
            case 4: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(211, 237, 194), 0.0f, endpoint, new Color(109, 176, 71));
                this.bgColor = new Color(68, 154, 23);
                this.textColor = Color.WHITE;
                break;
            }
            case 6: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(231, 247, 183), 0.0f, endpoint, new Color(192, 234, 68));
                this.bgColor = new Color(168, 216, 24);
                this.textColor = Color.BLACK;
                break;
            }
            case 7: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(138, 234, 9), 0.0f, endpoint, new Color(128, 216, 7, 100));
                this.bgColor = new Color(68, 116, 4);
                this.textColor = Color.WHITE;
                break;
            }
            case 8: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 233, 232), 0.0f, endpoint, new Color(255, 160, 160));
                this.bgColor = new Color(255, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 9: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 191, 191), 0.0f, endpoint, new Color(255, 174, 174, 150));
                this.bgColor = new Color(181, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 10: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(240, 240, 240), 0.0f, endpoint, new Color(246, 147, 90, 200));
                this.bgColor = new Color(255, 102, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 11: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(250, 250, 250), 0.0f, endpoint, new Color(255, 216, 176, 150));
                this.bgColor = new Color(255, 153, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 13: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(253, 247, 15), 0.0f, endpoint, new Color(253, 247, 15, 150));
                this.bgColor = new Color(211, 204, 2);
                this.textColor = Color.BLACK;
                break;
            }
            case 12: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(221, 216, 0), 0.0f, endpoint, new Color(187, 183, 0, 150));
                this.bgColor = new Color(123, 120, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 14: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endpoint, new Color(255, 230, 108));
                this.bgColor = new Color(255, 213, 0);
                this.textColor = Color.BLACK;
                break;
            }
            case 15: {
                this.bgColor = new Color(254, 188, 16);
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(254, 239, 192), 0.0f, startPoint, new Color(254, 227, 147, 150));
                this.textColor = Color.BLACK;
                break;
            }
            case 16: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(254, 173, 139), 0.0f, endpoint, new Color(253, 115, 55, 100));
                this.bgColor = new Color(118, 35, 1);
                this.textColor = Color.WHITE;
                break;
            }
            case 17: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(240, 215, 166), 0.0f, endpoint, new Color(226, 179, 88));
                this.bgColor = new Color(212, 151, 37);
                this.textColor = Color.WHITE;
                break;
            }
            case 18: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(253, 250, 242), 0.0f, endpoint, new Color(242, 221, 170));
                this.bgColor = new Color(236, 205, 132);
                this.textColor = Color.BLACK;
                break;
            }
            case 24: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(188, 200, 252), 0.0f, endpoint, new Color(188, 200, 252, 100));
                this.bgColor = new Color(5, 25, 114);
                this.textColor = Color.WHITE;
                break;
            }
            case 26: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(121, 145, 223), 0.0f, endpoint, new Color(121, 145, 223, 150));
                this.bgColor = new Color(3, 37, 188);
                this.textColor = Color.WHITE;
                break;
            }
            case 25: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(150, 177, 211), 0.0f, endpoint, new Color(40, 91, 149));
                this.bgColor = new Color(0, 59, 127);
                this.textColor = Color.WHITE;
                break;
            }
            case 27: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(206, 231, 255), 0.0f, endpoint, new Color(206, 231, 255, 100));
                this.bgColor = new Color(0, 96, 194);
                this.textColor = Color.WHITE;
                break;
            }
            case 28: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(167, 227, 248), 0.0f, endpoint, new Color(167, 227, 248, 100));
                this.bgColor = new Color(17, 160, 208);
                this.textColor = Color.WHITE;
                break;
            }
            case 37: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(206, 206, 255), 0.0f, endpoint, new Color(170, 170, 255, 100));
                this.bgColor = new Color(108, 108, 255);
                this.textColor = Color.WHITE;
                break;
            }
            case 29: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(202, 149, 255), 0.0f, endpoint, new Color(135, 15, 255, 100));
                this.bgColor = new Color(44, 0, 89);
                this.textColor = Color.WHITE;
                break;
            }
            case 30: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(238, 200, 224), 0.0f, endpoint, new Color(222, 152, 198, 150));
                this.bgColor = new Color(186, 0, 255);
                this.textColor = Color.WHITE;
                break;
            }
            case 31: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(208, 190, 233), 0.0f, endpoint, new Color(147, 105, 203, 150));
                this.bgColor = new Color(107, 60, 173);
                this.textColor = Color.WHITE;
                break;
            }
            case 32: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 191, 239), 0.0f, endpoint, new Color(255, 191, 239, 100));
                this.bgColor = new Color(115, 0, 85);
                this.textColor = Color.WHITE;
                break;
            }
            case 33: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(251, 215, 226), 0.0f, endpoint, new Color(251, 215, 226, 100));
                this.bgColor = new Color(220, 22, 86);
                this.textColor = Color.WHITE;
                break;
            }
            case 34: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 236, 251), 0.0f, endpoint, new Color(255, 236, 251, 100));
                this.bgColor = new Color(255, 128, 223);
                this.textColor = Color.WHITE;
                break;
            }
            case 1: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(250, 251, 253), 0.0f, endpoint, new Color(238, 243, 248));
                this.bgColor = new Color(191, 210, 228);
                this.textColor = Color.BLACK;
                break;
            }
            case 19: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(170, 170, 170), 0.0f, endpoint, new Color(150, 130, 130, 130));
                this.bgColor = new Color(0, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 20: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(197, 197, 197), 0.0f, endpoint, new Color(128, 128, 128, 150));
                this.bgColor = new Color(91, 91, 91);
                this.textColor = Color.WHITE;
                break;
            }
            case 21: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(215, 215, 215), 0.0f, endpoint, new Color(215, 215, 215, 100));
                this.bgColor = new Color(159, 159, 159);
                this.textColor = Color.WHITE;
                break;
            }
            case 35: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(200, 205, 209), 0.0f, endpoint, new Color(120, 137, 145, 100));
                this.bgColor = new Color(73, 92, 105);
                this.textColor = Color.WHITE;
                break;
            }
            case 23: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(200, 205, 209), 0.0f, endpoint, new Color(120, 137, 145, 100));
                this.bgColor = new Color(32, 53, 72);
                this.textColor = Color.WHITE;
                break;
            }
            case 36: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endpoint, new Color(85, 134, 194));
                this.bgColor = new Color(1, 31, 99);
                this.bgColor = new GradientPaint(0.0f, startPoint, new Color(1, 31, 99), 0.0f, endpoint, new Color(17, 213, 255));
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endpoint, new Color(85, 134, 194));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(1, 31, 99), 0.0f, 40.0f, new Color(137, 255, 255));
                this.textColor = Color.WHITE;
                break;
            }
            case 38: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endpoint, new Color(85, 134, 194));
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 197, 63), 0.0f, endpoint, new Color(255, 197, 63, 100));
                this.bgColor = new Color(255, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 42: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 197, 63), 0.0f, endpoint, new Color(255, 0, 0, 100));
                this.bgColor = new Color(0, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 39: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(251, 139, 62), 0.0f, endpoint, new Color(255, 102, 0, 100));
                this.bgColor = new Color(0, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 40: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(192, 234, 68), 0.0f, endpoint, new Color(168, 216, 24, 100));
                this.bgColor = new Color(0, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 41: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 213, 0), 0.0f, endpoint, new Color(255, 213, 0, 100));
                this.bgColor = new Color(0, 0, 0);
                this.textColor = Color.WHITE;
                break;
            }
            case 43: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(71, 232, 252), 0.0f, endpoint, new Color(71, 232, 252, 50));
                this.bgColor = new Color(3, 37, 188);
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(3, 37, 188), 0.0f, 2 * endpoint - startPoint, new Color(71, 232, 252));
                this.textColor = Color.WHITE;
                break;
            }
            case 51: {
                System.out.println("endpoint : " + endpoint);
                System.out.println("startPoint : " + startPoint);
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 150, 150), 0.0f, endpoint, new Color(255, 53, 53));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(255, 0, 0), 0.0f, 2 * endpoint - startPoint, new Color(255, 233, 232));
                this.textColor = Color.WHITE;
                break;
            }
            case 52: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 191, 191), 0.0f, endpoint, new Color(255, 174, 174, 150));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(181, 0, 0), 0.0f, 2 * endpoint - startPoint, new Color(255, 191, 191));
                this.textColor = Color.WHITE;
                break;
            }
            case 47: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(184, 226, 156), 0.0f, endpoint, new Color(109, 176, 71));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(68, 154, 23), 0.0f, 2 * endpoint - startPoint, new Color(211, 237, 194));
                this.textColor = Color.WHITE;
                break;
            }
            case 45: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(3, 167, 7), 0.0f, endpoint, new Color(2, 117, 5, 150));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(1, 54, 2), 0.0f, 2 * endpoint - startPoint, new Color(3, 167, 7));
                break;
            }
            case 46: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(72, 223, 159), 0.0f, endpoint, new Color(41, 218, 142, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(20, 113, 74), 0.0f, 2 * endpoint - startPoint, new Color(72, 223, 159));
                break;
            }
            case 48: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(72, 223, 159), 0.0f, endpoint, new Color(41, 218, 142, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(20, 113, 74), 0.0f, 2 * endpoint - startPoint, new Color(72, 223, 159));
                break;
            }
            case 50: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(138, 234, 9), 0.0f, endpoint, new Color(128, 216, 7, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(68, 116, 4), 0.0f, 2 * endpoint - startPoint, new Color(138, 234, 9));
                break;
            }
            case 53: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(250, 194, 160), 0.0f, endpoint, new Color(244, 147, 90, 200));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(255, 102, 0), 0.0f, 2 * endpoint - startPoint, new Color(240, 240, 240));
                break;
            }
            case 57: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 248, 223), 0.0f, endpoint, new Color(255, 233, 166));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(255, 211, 4), 0.0f, 2 * endpoint - startPoint, new Color(240, 240, 240));
                break;
            }
            case 67: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(188, 200, 252), 0.0f, endpoint, new Color(188, 200, 252, 130));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(5, 25, 114), 0.0f, 2 * endpoint - startPoint, new Color(188, 200, 252));
                break;
            }
            case 68: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(150, 177, 211), 0.0f, endpoint, new Color(40, 91, 149));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(0, 59, 127), 0.0f, 2 * endpoint - startPoint, new Color(150, 177, 211));
                break;
            }
            case 69: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(206, 231, 255), 0.0f, endpoint, new Color(206, 231, 255, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(0, 96, 194), 0.0f, 2 * endpoint - startPoint, new Color(206, 231, 255));
                break;
            }
            case 70: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(167, 227, 248), 0.0f, endpoint, new Color(167, 227, 248, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(17, 160, 208), 0.0f, 2 * endpoint - startPoint, new Color(255, 255, 255));
                break;
            }
            case 71: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(202, 149, 255), 0.0f, endpoint, new Color(135, 15, 255, 150));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(44, 0, 89), 0.0f, 2 * endpoint - startPoint, new Color(202, 149, 255));
                this.textColor = Color.WHITE;
            }
            case 72: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(238, 200, 224), 0.0f, endpoint, new Color(222, 152, 198, 150));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(186, 0, 255), 0.0f, 2 * endpoint - startPoint, new Color(238, 200, 224));
                break;
            }
            case 73: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(208, 190, 233), 0.0f, endpoint, new Color(147, 105, 203, 200));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(107, 60, 173), 0.0f, 2 * endpoint - startPoint, new Color(208, 190, 233));
                break;
            }
            case 77: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(206, 206, 255), 0.0f, endpoint, new Color(170, 170, 255, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(108, 108, 255), 0.0f, 2 * endpoint - startPoint, new Color(206, 206, 255));
                break;
            }
            case 74: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 191, 239), 0.0f, endpoint, new Color(255, 191, 239, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(115, 0, 85), 0.0f, 2 * endpoint - startPoint, new Color(255, 191, 239));
                break;
            }
            case 75: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(251, 215, 226), 0.0f, endpoint, new Color(251, 215, 226, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(220, 22, 86), 0.0f, 2 * endpoint - startPoint, new Color(251, 215, 226));
                break;
            }
            case 76: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 236, 251), 0.0f, endpoint, new Color(255, 236, 251, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(255, 128, 223), 0.0f, 2 * endpoint - startPoint, new Color(255, 236, 251));
                break;
            }
            case 59: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(254, 173, 139), 0.0f, endpoint, new Color(253, 115, 55, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(118, 35, 1), 0.0f, 2 * endpoint - startPoint, new Color(254, 173, 139));
                break;
            }
            case 60: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(240, 215, 166), 0.0f, endpoint, new Color(226, 179, 88));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(212, 151, 37), 0.0f, 2 * endpoint - startPoint, new Color(240, 215, 166));
                break;
            }
            case 66: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(200, 205, 209), 0.0f, endpoint, new Color(120, 137, 145, 150));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(32, 53, 72), 0.0f, 2 * endpoint - startPoint, new Color(200, 205, 209));
                break;
            }
            case 63: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(197, 197, 197), 0.0f, endpoint, new Color(128, 128, 128, 150));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(91, 91, 91), 0.0f, 2 * endpoint - startPoint, new Color(197, 197, 197));
                break;
            }
            case 64: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(215, 215, 215), 0.0f, endpoint, new Color(215, 215, 215, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(159, 159, 159), 0.0f, 2 * endpoint - startPoint, new Color(215, 215, 215));
                break;
            }
            case 62: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(130, 130, 130), 0.0f, endpoint, new Color(100, 100, 100, 100));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(0, 0, 0), 0.0f, 2 * endpoint - startPoint, new Color(170, 170, 170));
                break;
            }
            case 44: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(34, 144, 255), 0.0f, endpoint, new Color(0, 101, 202));
                this.bgColor = new GradientPaint(0.0f, endpoint, new Color(0, 82, 164), 0.0f, 2 * endpoint - startPoint, new Color(206, 231, 255));
                this.textColor = Color.WHITE;
                break;
            }
            default: {
                this.glossyTopBtnColor = new GradientPaint(0.0f, startPoint, new Color(255, 255, 255), 0.0f, endpoint, new Color(85, 134, 194));
                this.bgColor = new GradientPaint(0.0f, 0.0f, new Color(1, 31, 99), 0.0f, 2 * endpoint - startPoint, new Color(137, 255, 255));
                this.textColor = Color.WHITE;
                break;
            }
        }
        glossyColors.add(this.glossyTopBtnColor);
        glossyColors.add(this.bgColor);
        glossyColors.add(this.textColor);
        return glossyColors;
    }
}
