/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import com.danang.paten.socket.Responder;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author danang
 */
public class ServerChat implements Runnable {
    private final Responder responder;
    private final Socket connectionSocket;

    public ServerChat(Responder responder, Socket connectionSocket) {
        this.responder = responder;
        this.connectionSocket = connectionSocket;
    }
    
    
    @Override
    public void run() {
        tunggu();
        try {
            connectionSocket.close();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
    public synchronized void tunggu(){
        while(responder.responderMethod(connectionSocket)){
            try {
                wait(5000);
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
            }
        }
    }
}
