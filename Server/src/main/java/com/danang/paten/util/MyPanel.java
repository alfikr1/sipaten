// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
    private String id;
    private String antrean;
    
    public MyPanel() {
        this.antrean = "0";
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public void setAntrean(final String antrean) {
        this.antrean = antrean;
        this.repaint();
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        final Graphics2D g2 = (Graphics2D)g.create();
        final String stringTime = String.valueOf(this.antrean);
        final FontMetrics fm = g2.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(stringTime, g2);
        final int x = (this.getWidth() - (int)r.getWidth()) / 2;
        final int y = (this.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent();
        g.setFont(new Font("DejaVu Sans", 1, 48));
        g.drawString(stringTime, x, y);
    }
}
