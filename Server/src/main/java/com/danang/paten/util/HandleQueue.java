/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import com.danang.paten.Konfig;
import com.hazelcast.core.HazelcastInstance;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author danang
 */
public class HandleQueue extends Thread {

    private HazelcastInstance hi;

    public HandleQueue() {
        hi = Konfig.getInstance().getAntrian();
    }

    @Override
    public void run() {
        BlockingQueue<PlayAudio> bq = hi.getQueue("antrianPayer");
        while (bq.size() > 0) {
            PlayAudio pa = bq.poll();
            if (pa != null) {
                pa.playData();
            }
        }

    }

}
