/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import com.danang.paten.InitServer;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import org.apache.log4j.Logger;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 *
 * @author Danang
 */
public abstract class WebDafuk implements Route {

    private final Logger log = Logger.getLogger(WebDafuk.class);
    final Template template;
    private Configuration cfg;

    public WebDafuk(String template) throws IOException {
        cfg = new Configuration();
        cfg.setClassForTemplateLoading(InitServer.class, "/webview");
        this.template = cfg.getTemplate(template);
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        StringWriter writer = new StringWriter();
        try {
            doHandle(request, response, writer);
        } catch (IOException | TemplateException e) {
            log.error("response error " + request.url(), e);
            response.redirect("/401");
        }
        return writer;
    }

    protected abstract void doHandle(final Request request, final Response response, final Writer writer)
            throws IOException, TemplateException;
}
