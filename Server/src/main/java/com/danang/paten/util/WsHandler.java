// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import com.danang.paten.domain.MessageFormat;

public interface WsHandler
{
    MessageFormat handle(final String p0);
}
