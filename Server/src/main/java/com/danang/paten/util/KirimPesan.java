/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.log4j.Logger;

/**
 *
 * @author danang
 */
public class KirimPesan implements Runnable {

    private final String nomor;
    private final String pesan;
    private final String TAG = KirimPesan.class.getSimpleName();
    private final Logger log = Logger.getLogger(KirimPesan.class);

    public KirimPesan(String nomor, String pesan) {
        this.nomor = nomor;
        this.pesan = pesan;
    }

    @Override
    public void run() {
        ProcessBuilder pb = new ProcessBuilder("gammu", "--sendsms", "TEXT", nomor, "-text", pesan);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        try {
            Process p = pb.start();
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            log.debug("Log Message : " + sb.toString());
        } catch (IOException ex) {
            log.debug("pesan error " + nomor + " pesan : " + pesan);
            log.error(ex);
        }
    }

}
