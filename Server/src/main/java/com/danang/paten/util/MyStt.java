/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import static com.danang.paten.util.MyStt.bunyi;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author danang
 */
public class MyStt {

    private static List<File> playlist = new ArrayList<>();
    private static File[] sebel = {null, new File("./Sounds/1.mp3"), new File("./Sounds/2.mp3"),
        new File("./Sounds/3.mp3"), new File("./Sounds/4.mp3"),
        new File("./Sounds/5.mp3"), new File("./Sounds/6.mp3"),
        new File("./Sounds/7.mp3"), new File("./Sounds/8.mp3"),
        new File("./Sounds/9.mp3"), new File("./Sounds/10.mp3"),
        new File("./Sounds/11.mp3")};

    public static List<File> bunyi(String abj, int urut, int loket) {
        playlist.add(new File("./Sounds/notif.mp3"));
        playlist.add(new File("./Sounds/antrian.mp3"));
        if (abj != null) {
            switch (abj.toUpperCase()) {
                case "A":
                    playlist.add(new File("./Sounds/A.mp3"));
                    break;
                case "B":
                    playlist.add(new File("./Sounds/B.mp3"));
                    break;
                case "C":
                    playlist.add(new File("./Sounds/C.mp3"));
                    break;
                case "D":
                    playlist.add(new File("./Sounds/D.mp3"));
                    break;
                case "E":
                    playlist.add(new File("./Sounds/E.mp3"));
                    break;
                case "F":
                    playlist.add(new File("./Sounds/F.mp3"));
                    break;
                case "G":
                    playlist.add(new File("./Sounds/G.mp3"));
                    break;
                case "H":
                    playlist.add(new File("./Sounds/H.mp3"));
                    break;
                case "I":
                    playlist.add(new File("./Sounds/I.mp3"));
                    break;
                case "J":
                    playlist.add(new File("./Sounds/J.mp3"));
                    break;
                case "K":
                    playlist.add(new File("./Sounds/K.mp3"));
                    break;
                case "L":
                    playlist.add(new File("./Sounds/L.mp3"));
                    break;
                case "M":
                    playlist.add(new File("./Sounds/M.mp3"));
                    break;
                case "N":
                    playlist.add(new File("./Sounds/N.mp3"));
                    break;
                case "O":
                    playlist.add(new File("./Sounds/O.mp3"));
                    break;
                case "P":
                    playlist.add(new File("./Sounds/P.mp3"));
                    break;
                case "Q":
                    playlist.add(new File("./Sounds/Q.mp3"));
                    break;
                case "R":
                    playlist.add(new File("./Sounds/R.mp3"));
                    break;
                case "S":
                    playlist.add(new File("./Sounds/S.mp3"));
                    break;
                case "T":
                    playlist.add(new File("./Sounds/T.mp3"));
                    break;
                case "U":
                    playlist.add(new File("./Sounds/U.mp3"));
                    break;
                case "V":
                    playlist.add(new File("./Sounds/V.mp3"));
                    break;
                case "W":
                    playlist.add(new File("./Sounds/W.mp3"));
                    break;
                case "X":
                    playlist.add(new File("./Sounds/X.mp3"));
                    break;
                case "Y":
                    playlist.add(new File("./Sounds/Y.mp3"));
                    break;
                case "Z":
                    playlist.add(new File("./Sounds/Z.mp3"));
                    break;
            }
        }
        bunyiAngka(urut).forEach((x) -> {
            if (x != null) {
                playlist.add(x);
            }
        });
        playlist.add(new File("./Sounds/tujuan.mp3"));
        bunyiAngka(loket).stream().forEach((x) -> {
            if (x != null) {
                playlist.add(x);
            }
        });
        playlist.add(new File("./Sounds/thanks.mp3"));
        return playlist;
    }

    private static List<File> bunyiAngka(int urut) {
        List<File> result = new ArrayList<>();
        if (urut < 12) {
            result.add(sebel[urut]);
        }
        if (urut >= 12 && urut <= 19) {
            result.add(sebel[(int) urut % 10]);
            result.add(new File("./Sounds/belas.mp3"));
        }
        if (urut >= 20 && urut <= 99) {
            result.add(sebel[urut / 10]);
            result.add(new File("./Sounds/puluh.mp3"));
            result.add(sebel[(int) urut % 10]);
        }
        if (urut >= 100 && urut <= 199) {
            result.add(new File("./Sounds/100.mp3"));
            bunyiAngka(urut % 100).stream().forEach((x) -> {
                result.add(x);
            });
        }
        if (urut >= 200 && urut <= 999) {
            result.add(sebel[(int) urut / 100]);
            result.add(new File("./Sounds/ratus.mp3"));
            bunyiAngka(urut % 100).stream().forEach((x) -> {
                result.add(x);
            });
        }
        if (urut >= 1000 && urut <= 1999) {
            result.add(new File("./Sounds/1000.mp3"));
            bunyiAngka(urut % 1000).stream().forEach((x) -> {
                result.add(x);
            });
        }
        if (urut >= 2000 && urut <= 999999) {
            bunyiAngka((int) urut / 1000).stream().forEach((x) -> {
                result.add(x);
            });
            result.add(new File("./Sounds/ribu.mp3"));
            bunyiAngka(urut % 1000).stream().forEach((x) -> {
                result.add(x);
            });
        }
        if (urut >= 1000000 && urut <= 99999999) {
            bunyiAngka((int) urut / 1000000).stream().forEach((x) -> {
                result.add(x);
            });
            result.add(new File("./Sounds/juta.mp3"));
            bunyiAngka(urut % 1000000).stream().forEach((x) -> {
                result.add(x);
            });
        }
        return result;
    }
}
