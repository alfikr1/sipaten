/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.util;

import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author danang
 */
public class ChatTcpServer {
    private static ServerSocket serverSocket=null;
    private static Socket clientSocket =null;
    
    private clientThread[] thread;
    public void bukaServer(int port){
        try {
            serverSocket = new ServerSocket(port);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        while(true){
            try {
                clientSocket=serverSocket.accept();
                int i=0;
                for(i=0;i<thread.length;i++){
                    if(thread[i]==null){
                        (thread[i]=new clientThread(clientSocket,thread)).start();
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
