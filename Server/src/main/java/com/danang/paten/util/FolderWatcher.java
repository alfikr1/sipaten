// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.util;

import java.io.File;
import java.nio.file.LinkOption;
import java.util.HashMap;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.SimpleFileVisitor;
import java.io.IOException;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.Path;
import java.nio.file.WatchKey;
import java.util.Map;
import java.nio.file.WatchService;

public class FolderWatcher {

    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private final boolean recursive;
    private boolean trace;
    private FolderListener listener;
    private Path path;

    static <T> WatchEvent<T> cast(final WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    private void register(final Path dir) throws IOException {
        final WatchKey key = dir.register(this.watcher, (WatchEvent.Kind<?>[]) new WatchEvent.Kind[]{StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY});
        if (this.trace) {
            final Path prev = this.keys.get(key);
            if (prev == null) {
                System.out.format("register : %s\n", dir);
            } else if (!dir.equals(prev)) {
                System.out.format("update : %s -> %s \n", prev, dir);
            }
        }
        this.keys.put(key, dir);
    }

    private void registerAll(final Path start) throws IOException {
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attr) throws IOException {
                FolderWatcher.this.register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public FolderWatcher(final Path dir, final boolean recursive) throws IOException {
        this.trace = false;
        this.path = dir;
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<>();
        this.recursive = recursive;
        if (recursive) {
            System.out.format("Scanning %s ...\n", dir);
            this.registerAll(dir);
            System.out.println("Selesai");
        } else {
            this.register(dir);
        }
        this.trace = true;
    }

    public void addEventListener(final FolderListener listener) {
        this.listener = listener;
    }

    public void processEvents() {
        while (true) {
            WatchKey key;
            try {
                key = this.watcher.take();
            } catch (InterruptedException e2) {
                return;
            }
            final Path dir = this.keys.get(key);
            if (dir == null) {
                System.err.println("WatchKey not recognized");
            } else {
                key.pollEvents().stream().forEach(event -> {
                    WatchEvent.Kind kind = event.kind();
                    if (kind != StandardWatchEventKinds.OVERFLOW) {
                        WatchEvent<Object> ev = cast(event);
                        Path nme = (Path) ev.context();
                        Path ch = path.resolve(nme);
                        if (this.listener != null) {
                            this.listener.notivy();
                        }
                        System.out.println("hello " + event.kind().name());
                        System.out.format("%s : %s\n", event.kind().name(), ch);
                        if (this.recursive) {
                            if (kind != StandardWatchEventKinds.ENTRY_CREATE) {
                                if (kind != StandardWatchEventKinds.ENTRY_MODIFY) {
                                    return;
                                }
                            }
                            try {
                                if (Files.isDirectory(ch, LinkOption.NOFOLLOW_LINKS)) {
                                    this.registerAll(ch);
                                    if (this.listener != null) {
                                        File[] fs = ch.toFile().listFiles();
                                        for (int i = 0; i < fs.length; ++i) {
                                            File f = fs[i];
                                            this.listener.fileAdded(f);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace(System.err);
                            }
                        }
                    }
                });
                final boolean valid = key.reset();
                if (valid) {
                    continue;
                }
                this.keys.remove(key);
                if (this.keys.isEmpty()) {
                    break;
                }
            }
        }
    }

    public synchronized File[] getFiles() {
        final File file = this.path.toFile();
        return file.listFiles();
    }

    public static void usage() {
        System.err.println("usage: java WatchDir [-r] dir");
        System.exit(-1);
    }
}
