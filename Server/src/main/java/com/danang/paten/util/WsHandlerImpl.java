// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.util;

import com.danang.paten.domain.MessageFormat;
import com.danang.paten.Konfig;
import com.danang.paten.controller.TrxController;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.controller.LaporanController;
import com.danang.paten.controller.LoginController;
import com.danang.paten.controller.PenyerahanBerkasController;
import com.danang.paten.controller.TerimaBerkasController;
import com.danang.paten.controller.PendudukController;
import com.google.gson.Gson;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

public class WsHandlerImpl implements WsHandler {

    private final Gson gson;
    private final PendudukController pk;
    private final TerimaBerkasController tbc;
    private final PenyerahanBerkasController pbc;
    private final LoginController lc;
    private final LaporanController lapc;
    private final PemeriksaanController pc;
    private final TrxController trxc;
    private final Logger log = Logger.getLogger(WsHandlerImpl.class);

    public WsHandlerImpl() {
        this.gson = Konfig.getInstance().getGson();
        this.pk = Konfig.getInstance().getContext().getBean(PendudukController.class);
        this.tbc = Konfig.getInstance().getContext().getBean(TerimaBerkasController.class);
        this.pbc = Konfig.getInstance().getContext().getBean(PenyerahanBerkasController.class);
        this.lc = Konfig.getInstance().getContext().getBean(LoginController.class);
        this.lapc = Konfig.getInstance().getContext().getBean(LaporanController.class);
        this.pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
        this.trxc = Konfig.getInstance().getContext().getBean(TrxController.class);
    }

    @Override
    public MessageFormat handle(final String msg) {
        log.debug("Message Request : " + msg);
        final MessageFormat mf = this.gson.fromJson(msg, MessageFormat.class);

        MessageFormat res = new MessageFormat();
        if (mf.getCommand().equalsIgnoreCase("LOGIN")) {
            res = this.lc.handle(mf);
            return res;
        }
        if (!lc.validasiToken(mf.getToken())) {
            res.setError("Token tidak valid");
            return res;
        }
        final String command = mf.getCommand();
        switch (command) {
            case "Penduduk": {
                res = this.pk.handlePenduduk(mf);
                break;
            }
            case "Registrasi": {
                res = this.tbc.handler(mf);
                break;
            }
            case "TRX": {
                try {
                    res = this.trxc.handle(mf);
                    break;
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            }
            case "Penyerahan": {
                res = this.pbc.handle(mf);
                break;
            }
            case "Pemeriksaan": {
                res = this.pc.handle(mf);
                break;
            }
            case "Laporan": {
                res = this.lapc.handler(mf);
                break;
            }
            case "Antrian": {
                Map<String, Object> map = new HashMap<>();
                String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
                HazelcastInstance hi = Konfig.getInstance().getAntrian();
                BlockingQueue<HashMap<String, Object>> antrianPerijinan = hi.getQueue("antrianPERIJINAN" + date);
                IAtomicLong nomorAntrianPerijinan = hi.getAtomicLong("nomor-antrianPERIJINAN" + date);
                map.put("sisaPerijinan", antrianPerijinan.size());
                map.put("totalPerijinan", nomorAntrianPerijinan.get());
                BlockingQueue<HashMap<String, Object>> antrianNonPerijinan = hi.getQueue("antrianNONPERIJINAN" + date);
                IAtomicLong nomorAntrianNonPerijinan = hi.getAtomicLong("nomor-antrianNONPERIJINAN" + date);
                map.put("sisaNonPerijinan", antrianNonPerijinan.size());
                map.put("totalNonPerijinan", nomorAntrianNonPerijinan.get());
                BlockingQueue<HashMap<String, Object>> antrianHelpdesk = hi.getQueue("antrianHELPDESK" + date);
                IAtomicLong nomorAntrianHelpdesk = hi.getAtomicLong("nomor-antrianHELPDESK" + date);
                map.put("sisaHelpDesk", antrianHelpdesk.size());
                map.put("totalHelpdesk", nomorAntrianHelpdesk.get());
                res = new MessageFormat();
                res.setCommand("antrian-reply");
                res.setError("");
                res.setMsg(gson.toJson(map));
            }
        }
        return res;
    }

}
