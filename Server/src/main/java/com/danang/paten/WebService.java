/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten;

import com.danang.paten.domain.MessageFormat;
import com.danang.paten.util.WsHandler;
import com.danang.paten.util.WsHandlerImpl;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

/**
 *
 * @author Danang
 */
@WebSocket
public class WebService {

    private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();
    private WsHandler handler = new WsHandlerImpl();
    private final Logger log = Logger.getLogger(WebService.class);

    public WebService() {
        Konfig.getInstance().setWsService(this);
    }
    
    @OnWebSocketConnect
    public void connected(Session session) {

        sessions.add(session);
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        sessions.remove(session);
    }
    public synchronized void sendNotifikasiAll(String msg){
        sessions.stream().forEach((x)->{
            try {
                x.getRemote().sendString(msg);
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        });
    }
    @OnWebSocketMessage
    public void message(Session session, String message) throws IOException {
        log.debug("request from " + session.getRemoteAddress().getHostName());
        log.debug("data " + message);
        System.out.println(message);
        MessageFormat rest;
        try {
            rest = handler.handle(message);
        } catch (Exception e) {
            rest=new MessageFormat();
            rest.setError(e.getMessage());
        }
        session.getRemote().sendString(Konfig.getInstance().getGson().toJson(rest));
    }
}
