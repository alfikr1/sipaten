package com.danang.paten;

import com.danang.paten.controller.AdminController;
import com.danang.paten.controller.AntrianController;
import java.util.concurrent.BlockingQueue;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.HazelcastInstance;
import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.danang.paten.route.FilePostRoute;
import com.danang.paten.domain.KartuKendaliSuratKeluar;
import com.danang.paten.domain.KartuKendaliSuratMasuk;
import com.danang.paten.controller.PenyerahanBerkasController;
import com.hazelcast.config.Config;
import java.util.concurrent.TimeUnit;
import java.util.Map;
import com.hazelcast.core.Hazelcast;
import com.danang.paten.route.ReportRoute;
import com.danang.paten.route.DomisiliUsahaRoute;
import com.danang.paten.route.TransaksiRoute;
import com.danang.paten.route.PenelitianRoute;
import com.danang.paten.route.IUMRoute;
import com.danang.paten.route.DispensasiRoute;
import com.danang.paten.route.IMBRoute;
import com.danang.paten.route.HORoute;
import java.util.Date;
import com.danang.paten.domain.BerkasPeriksa;
import com.danang.paten.domain.Layanan;
import java.util.ArrayList;
import com.danang.paten.domain.Berkas;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.danang.paten.domain.MessageFormat;
import java.text.SimpleDateFormat;
import com.danang.paten.repository.PetugasRepository;
import com.danang.paten.domain.Petugas;
import java.util.HashMap;
import com.danang.paten.domain.DataPenduduk;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import com.danang.paten.util.JsonTransformer;
import com.google.gson.GsonBuilder;
import com.danang.paten.controller.PenetapanController;
import com.danang.paten.controller.SuratKeluarController;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.controller.SuratMasukController;
import com.danang.paten.controller.LayananController;
import com.danang.paten.controller.TerimaBerkasController;
import com.danang.paten.controller.TrxController;
import com.danang.paten.controller.PetugasController;
import com.danang.paten.controller.PendudukController;
import com.danang.paten.controller.LoginController;
import com.danang.paten.controller.LoketController;
import com.danang.paten.domain.Kecamatan;
import com.danang.paten.domain.Loket;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.util.KodeSurat;
import com.danang.paten.konfigurasi.Konfigurasi;
import com.danang.paten.repository.LoketRepository;
import com.danang.paten.route.SpplRoute;
import com.danang.paten.service.UpdateService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import javax.imageio.ImageIO;
import javax.servlet.MultipartConfigElement;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.data.domain.Page;
import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.webSocket;

public class InitServer {

    private final LoginController loginController;
    private final PendudukController pendudukController;
    private final PetugasController petugasController;
    private final TrxController trxController;
    private final LoketController loketController;
    private final TerimaBerkasController terimaBerkasController;
    private final LayananController layananController;
    private final SuratMasukController suratMasukController;
    private final PemeriksaanController pemeriksaanController;
    private final SuratKeluarController suratKeluarController;
    private final PenetapanController penetapanController;
    private final PenyerahanBerkasController penyerahanBerkasController;
    private final UpdateService updateService;
    private final Logger log = Logger.getLogger(InitServer.class);
    private final AdminController adminController;
    private final AntrianController antrianController;

    public InitServer() {
        pemeriksaanController = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
        loginController = Konfig.getInstance().getContext().getBean(LoginController.class);
        pendudukController = Konfig.getInstance().getContext().getBean(PendudukController.class);
        petugasController = Konfig.getInstance().getContext().getBean(PetugasController.class);
        trxController = Konfig.getInstance().getContext().getBean(TrxController.class);
        terimaBerkasController = Konfig.getInstance().getContext().getBean(TerimaBerkasController.class);
        layananController = Konfig.getInstance().getContext().getBean(LayananController.class);
        loketController = Konfig.getInstance().getContext().getBean(LoketController.class);
        suratMasukController = Konfig.getInstance().getContext().getBean(SuratMasukController.class);
        suratKeluarController = Konfig.getInstance().getContext().getBean(SuratKeluarController.class);
        penetapanController = Konfig.getInstance().getContext().getBean(PenetapanController.class);
        penyerahanBerkasController = Konfig.getInstance().getContext().getBean(PenyerahanBerkasController.class);
        updateService = Konfig.getInstance().getContext().getBean(UpdateService.class);
        adminController = Konfig.getInstance().getContext().getBean(AdminController.class);
        antrianController = Konfig.getInstance().getContext().getBean(AntrianController.class);
    }

    private void initializeLayanan() {
        log.debug("initialize layanan start");
        LayananRepository dao = Konfig.getInstance().getContext().getBean(LayananRepository.class);
        Layanan l = dao.findOne(KodeSurat.DOMISILI);
        LoketRepository loketDao = Konfig.getInstance().getContext().getBean(LoketRepository.class);
        if (loketDao.findAll().isEmpty()) {
            Loket loket = new Loket();
            loket.setKode("1");
            loket.setDesk("Loket 1");
            loketDao.save(loket);
        }
        //user default
        PetugasRepository userDao = Konfig.getInstance().getContext().getBean(PetugasRepository.class);
        if (userDao.findAll().isEmpty()) {
            Petugas p = new Petugas();
            p.setAdministrator(true);
            p.setIsLogged(false);
            p.setKode("admin");
            p.setNama("Administrator");
            p.setNip("001");
            p.setUser("admin");
            String salt = BCrypt.gensalt();
            p.setSalt(salt);
            String psw = BCrypt.hashpw("admin", salt);
            p.setPsw(psw);
            userDao.save(p);
        }
    }

    private void initializeUpdate() {
        Thread t = new Thread() {
            @Override
            public void run() {
                updateService.jalankanCounterChecker();
                updateService.jalankanUpdateKodeLayanan();
            }
        };
        t.start();
    }

    public void init() {
        Config ijinConfig = new Config();
        ijinConfig.setInstanceName("Antrian");
        HazelcastInstance antri = Hazelcast.getOrCreateHazelcastInstance(ijinConfig);
        Konfig.getInstance().setAntrian(antri);
        initializeLayanan();
        initializeUpdate();
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        webSocket("/data/chat", WebService.class);
        //Spark.init();
        Konfigurasi konfigurasi = new Konfigurasi();
        port(konfigurasi.getPort());
        //Spark.secure("keystoreFile", "keystorePassword", "truststoreFile", "truststorePassword");
        //after((req,res)->res.header("Content-Encoding", "gzip"));
        //after((request, response) -> response.header("Content-Type", "application/json"));
        post("/login", (req, res) -> {
            String username = req.queryParams("username");
            String password = req.queryParams("password");
            String ip = req.ip();
            Map<String, Object> map = this.loginController.getToken(username, password, ip);
            return map;
        }, new JsonTransformer());
        before("/user/*", (req, res) -> {
            String keypair = req.headers("keypair");
            log.debug("keypair" + keypair);
            if (keypair == null) {
                halt(401, "please login");
            }
            /*else if (!this.loginController.validasiToken(keypair)) {
                halt(401, "please login first");
            }*/
            boolean val = loginController.validasiToken(keypair);
            if (!val) {
                halt(401, "Sesi anda telah berakhir, Silahkan login kembali");
            }
        });
        get("/user/penduduk", (req, res) -> {
            log.debug("cari data");
            String cari = req.queryMap().get("key").value();
            String keypair = req.headers("keypair");
            int page = Integer.valueOf(req.queryMap().get("page").value() != null ? req.queryMap().get("page").value() : "0");
            int size = Integer.valueOf(req.queryMap().get("size").value() != null ? req.queryMap().get("size").value() : "25");

            return this.pendudukController.findDataPenduduk(cari, keypair, new PageRequest(page, size));
        }, new JsonTransformer());
        get("/user/penduduk/f", (req, res) -> {
            log.debug("find one");
            String id = req.queryParams("nik");
            String keypair = req.headers("keypair");
            return this.pendudukController.findOne(id, keypair);
        }, new JsonTransformer());
        post("/user/penduduk", (req, res) -> {
            String data = req.queryParams("data");
            DataPenduduk dp = gson.fromJson(data, DataPenduduk.class);
            dp.setDesa(dp.getDesa().toUpperCase());
            dp.setKecamatan((dp.getKecamatan() != null) ? dp.getKecamatan().toUpperCase() : "BERGAS");
            dp.setLingk(dp.getLingk().toUpperCase());
            dp.setKabupaten((dp.getKabupaten() != null) ? dp.getKabupaten().toUpperCase() : "SEMARANG");
            return this.pendudukController.simpanPenduduk(dp);
        }, new JsonTransformer());
        put("/user/penduduk", (req, res) -> {
            String data = req.queryParams("data");
            DataPenduduk dp = gson.fromJson(data, DataPenduduk.class);
            return this.pendudukController.updatePenduduk(dp);
        }, new JsonTransformer());
        delete("/user/penduduk", (req, res) -> {
            Map<String, String> map = new HashMap<>();
            log.debug(req.queryParams("nik"));
            try {
                pendudukController.hapus(req.queryParams("nik"));
                map.put("error", "");
                map.put("status", "done");
            } catch (Exception e2) {
                map.put("error", e2.getMessage());
                map.put("status", "failed");
            }
            return map;
        }, new JsonTransformer());
        post("/user/matikan", (req, res) -> {
            Runtime runtime = Runtime.getRuntime();
            Process p = runtime.exec("/usr/bin/dbus-send --system --print-reply --dest=\"org.freedesktop.ConsoleKit\" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop");
            p.waitFor();
            Map<String, String> map = new HashMap<>();
            map.put("error", "hello");
            return map;
        });
        post("/user/marquee", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            String request = req.queryParams("marquee");
            if (request == null) {
                map.put("error", "text tidak boleh kosong");
                return map;
            } else {
                Konfig.getInstance().getVideoMain().setTextMarquee(request);
                map.put("error", "");
                return map;
            }
        }, new JsonTransformer());
        post("/user/pegawai", (req, res) -> {
            Petugas p2 = gson.fromJson(req.queryParams("data"), Petugas.class);
            return this.petugasController.save(p2);
        }, new JsonTransformer());
        get("/user/pegawai", (req, res) -> {
            String tipe = req.queryMap().get("tipe").value();
            String keypair = req.headers("keypair");
            log.debug("tipe " + tipe);
            if (keypair == null) {
                throw new IllegalStateException("Maaf anda tidak diperkenankan untuk mengambil data");
            }
            if (tipe.equalsIgnoreCase("all")) {
                return Konfig.getInstance().getContext().getBean(PetugasRepository.class).findAll();
            } else {
                String cari = req.queryMap().get("cari").value();
                int page = Integer.parseInt(req.queryMap().get("page").value() != null ? req.queryMap().get("page").value() : "0");
                int size = Integer.parseInt(req.queryMap().get("size").value() != null ? req.queryMap().get("size").value() : "25");
                return this.petugasController.findAll(cari, keypair, new PageRequest(page, size));
            }
        }, new JsonTransformer());
        post("/user/analisis", (req, res) -> {
            return null;
        }, new JsonTransformer());
        get("/user/pegawai/:id", (req, res) -> {
            String id = req.params("id");
            String keypair = req.headers("keypair");
            return this.petugasController.findOne(id, keypair);
        }, new JsonTransformer());
        put("/user/pegawai", (req, res) -> {
            Petugas p = gson.fromJson(req.queryParams("data"), Petugas.class);
            return this.petugasController.update(p);
        });
        delete("/user/pegawai", (req, res) -> {
            String kode = req.queryParams("kode");
            String keypair = req.headers("keypair");
            Map<String, Object> map = new HashMap<>();
            Petugas p = this.petugasController.findOne(kode, keypair);
            if (p == null) {
                map.put("error", "data tidak ditemukan");
                this.petugasController.hapusData(kode);
            } else {
                map.put("error", "");
            }
            return map;
        });
        get("/user/dashboard", (req, res) -> {
            return trxController.getDashboardCounter();
        }, new JsonTransformer());
        get("/user/dashboard/all", (req, res) -> {
            return trxController.getDashboardCount();
        }, new JsonTransformer());
        post("/user/findTransaksi", (req, res) -> {
            String cari = req.queryParams("cari");
            int ukuran = Integer.parseInt(req.queryParams("ukuran"));
            int page3 = Integer.parseInt(req.queryParams("page"));
            String awal = req.queryParams("awal");
            String akhir = req.queryParams("akhir");
            Map<String, Object> map = new HashMap<>();
            map.put("cari", cari);
            map.put("ukuran", ukuran);
            map.put("page", page3);
            if (awal != null && akhir != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                Date aw = sdf.parse(awal);
                Date ak = sdf.parse(akhir);
                return this.trxController.findCariData(cari, aw, ak, new PageRequest(page3, ukuran));
            } else {
                return this.trxController.findCariData(cari, new PageRequest(page3, ukuran));
            }
        }, new JsonTransformer());
        post("/user/caridata", (req, res) -> {
            String data = req.queryParams("data");
            MessageFormat mf = Konfig.getInstance().getGson().fromJson(data, MessageFormat.class);
            MessageFormat respon = this.trxController.handle(mf);
            return respon;
        }, new JsonTransformer());
        post("/user/terimaberkas", (req, res) -> {
            String data = req.queryParams("data");
            String pemohon = req.queryParams("pemohon");
            String pengaju = req.queryParams("pengaju");
            String dispo = req.queryParams("disposisi");
            String token = req.headers("keypair");
            String layanan = req.queryParams("layanan");
            String suratDesa = req.queryParams("surat");
            String catatan = req.queryParams("catatan");
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            Date tlgPengantar = sdf.parse(req.queryParams("tglSurat"));
            Type tps = new TypeToken<List<Berkas>>() {
            }.getType();
            List<Berkas> items = Konfig.getInstance().getGson().fromJson(data, tps);
            Map<String, Object> replay = terimaBerkasController.simpanTerimaBerkas(pemohon, pengaju, token, dispo, layanan, suratDesa, tlgPengantar, items, catatan);
            //log.debug(replay);
            return replay;
        }, new JsonTransformer());
        post("/user/layanan/child", (req, res) -> {
            String kode = req.queryParams("kode");
            if (kode != null) {
                return this.layananController.findByParent(kode);
            } else {
                return new ArrayList<>();
            }
        }, new JsonTransformer());
        get("/user/layanan/:kode", (req, res) -> {
            String kode = req.params("kode");
            return layananController.findByKode(kode);
        }, new JsonTransformer());
        get("/user/layanan", (req, res) -> {
            String tipe = req.queryMap().get("tipe").value();
            String keypair = req.headers("keypair");
            if (keypair == null) {
                throw new IllegalStateException("Maaf anda tidak diperkenankan untuk mengambil data");
            }
            if (tipe.equalsIgnoreCase("all")) {
                return this.layananController.getSemua();
            } else {
                String cari = req.queryMap().get("key").value();
                int page = Integer.parseInt(req.queryMap().get("page").value() != null ? req.queryMap().get("page").value() : "0");
                int size = Integer.parseInt(req.queryMap().get("size").value() != null ? req.queryMap().get("size").value() : "25");

                return this.layananController.cariData(cari, new PageRequest(page, size));
            }
        }, new JsonTransformer());
        get("/user/loket", (req, res) -> {
            return loketController.findAll();
        }, new JsonTransformer());
        post("/user/layanan", (req, res) -> {
            Layanan l = new Layanan();
            String kode = req.queryParams("kode");
            boolean langsung = req.queryParams("tetapkan").equalsIgnoreCase("TRUE");
            boolean masaberlaku = req.queryParams("masaberlaku").equalsIgnoreCase("TRUE");
            String nama = req.queryParams("nama");
            int mulai = Integer.valueOf(req.queryParams("mulai"));
            Type tps = new TypeToken<List<Berkas>>() {
            }.getType();
            List<Berkas> items = gson.fromJson(req.queryParams("items"), tps);
            String parent = req.queryParams("parent");
            return layananController.buatLayanan(kode, nama, parent, items, langsung, masaberlaku, mulai);
        }, new JsonTransformer());
        get("/user/penetapan", (req, res) -> {
            String cari = req.queryMap("cari").value();
            if (cari == null) {
                cari = "";
            }
            int halaman = Integer.valueOf(req.queryMap("page").value() != null ? req.queryMap("page").value() : "0");
            int size = Integer.valueOf(req.queryMap("ukuran").value() != null ? req.queryMap("ukuran").value() : "25");
            String keypair = req.headers("keypair");
            Pageable p = new PageRequest(halaman, size);
            return penetapanController.cariPenetapan(cari, keypair, p);
        }, new JsonTransformer());
        post("/user/penetapan", (req, res) -> {
            String nomor = req.queryParams("nomor");
            String command = req.queryParams("command");
            String token = req.headers("keypair");
            return this.penetapanController.simpanPenetapan(nomor, command, token);
        }, new JsonTransformer());
        get("/user/periksa", (req, res) -> {
            String cari = req.queryMap("cari").value();
            if (cari == null) {
                cari = "";
            }
            int halaman = Integer.valueOf(req.queryMap("page").value() != null ? req.queryMap("page").value() : "0");
            int size = Integer.valueOf(req.queryMap("ukuran").value() != null ? req.queryMap("ukuran").value() : "25");
            String keypair = req.headers("keypair");
            return this.pemeriksaanController.cariPeriksa(cari, keypair, new PageRequest(halaman, size));
        }, new JsonTransformer());
        post("/user/periksa", (req, res) -> {
            String token = req.queryParams("petugas");
            boolean lanjut = req.queryParams("lanjut").equalsIgnoreCase("TRUE");
            String nomor = req.queryParams("nomor");
            String data = req.queryParams("data");
            Type tps = new TypeToken<List<BerkasPeriksa>>() {
            }.getType();
            List<BerkasPeriksa> items = gson.fromJson(data, tps);
            String catatan = req.queryParams("catatan");
            return pemeriksaanController.simpanPeriksa(new Date(), catatan, lanjut, nomor, token, items);
        }, new JsonTransformer());
        post("/user/imb/guna", (req, res) -> {

            return null;
        }, new JsonTransformer());
        get("/user/cetak", (req, res) -> {
            String keypair = req.headers("keypair");
            return null;
        }, new JsonTransformer());
        post("/user/HO", new HORoute(), new JsonTransformer());
        post("/user/IMB", new IMBRoute(), new JsonTransformer());
        post("/user/dispensasinikah", new DispensasiRoute(), new JsonTransformer());
        post("/user/ium", new IUMRoute(), new JsonTransformer());
        post("/user/penelitian", new PenelitianRoute(), new JsonTransformer());
        post("/user/trx", new TransaksiRoute(), new JsonTransformer());
        post("/user/domisiliusaha", new DomisiliUsahaRoute(), new JsonTransformer());
        post("/user/sppl", new SpplRoute(), new JsonTransformer());
        post("/user/report", (req, res) -> new ReportRoute().handle(req, res), new JsonTransformer());
        get("/user/expired", (req, res) -> {
            String keypair = req.headers("keypair");

            return null;
        }, new JsonTransformer());
        get("/user/periksaBerkas", (req, res) -> {
            String nomor = req.queryMap().get("nomor").value();
            String keypair = req.headers("keypair");
            Map<String, Object> map = new HashMap<>();
            if (keypair == null) {
                map.put("error", "Data tidak valid");
                return map;
            }
            return this.pemeriksaanController.getPemeriksaan(nomor);
        }, new JsonTransformer());
        get("/user/arsip", (req, res) -> {
            String cari = req.queryParams("cari");
            int page = Integer.valueOf(req.queryParams("page"));
            int size = Integer.valueOf(req.queryParams("size"));
            return this.trxController.getArsip(cari, new PageRequest(page, size));
        }, new JsonTransformer());
        get("/user/loket/antrian", (req, res) -> {
            log.debug(gson.toJson(req.params()));
            String jenis = req.queryParams("jenis");
            String loket = req.queryParams("loket");
            if (jenis == null) {
                throw new IllegalStateException("Parameter kurang");
            }
            if (loket == null) {
                throw new IllegalStateException("Paramenter request kurang");
            }
            String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
            HazelcastInstance hi = Konfig.getInstance().getAntrian();
            BlockingQueue<HashMap<String, Object>> antrian = hi.getQueue("antrian" + jenis + date);
            if (antrian.isEmpty()) {
                throw new IllegalStateException("Antrian sudah habis");
            }
            HashMap<String, Object> antre = antrian.poll(2, TimeUnit.SECONDS);
            String k = "";
            long nomor = 0;
            String filename;
            Map<String, Object> result = new HashMap<>();
            nomor = (long) antre.get("nomor");
            filename = String.valueOf(antre.get("file"));
            result.put("nomor", nomor);
            result.put("file", filename);
            String th;
            int lk = Integer.valueOf(loket);
            switch (jenis) {
                case "PERIJINAN":
                    th = "A";
                    break;
                case "NONPERIJINAN":
                    th = "B";
                    break;
                default:
                    th = "C";
                    break;
            }
            Konfig.getInstance().getPa().setData(th, (int) nomor, lk);
            result.put("error", "");
            antrianController.simpanAntrian(jenis, th + nomor, lk, new Date(), filename);
            return result;
        }, new JsonTransformer());

        delete("/user/loket/antrian", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            String loket = req.queryParams("loket");
            String nomor = req.queryParams("nomor");
            String jenis = req.queryParams("jenis");
            if (loket == null || nomor == null || jenis == null) {
                throw new IllegalStateException("Data tidak lengkap");
            }
            String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
            HazelcastInstance hi = Konfig.getInstance().getAntrian();
            BlockingQueue<HashMap<String, Object>> antrian = hi.getQueue("antrian" + jenis + date);
            if (antrian.isEmpty()) {
                throw new IllegalStateException("Tidak ada antrian untuk dihapus");
            }
            antrian.stream().forEach((x) -> {
                if (Integer.valueOf(nomor) == (int) x.get("nomor")) {
                    antrian.remove(x);
                }
            });
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        post("/user/loket/antrian/skip", (req, res) -> {
            long nomor = Long.valueOf(req.queryParams("nomor"));
            Map<String, Object> result = new HashMap<>();
            if (nomor == -1) {
                result.put("error", "data tidak valid");
            }
            String jenis = req.queryParams("jenis");
            String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
            HashMap<String, Object> map = new HashMap<>();
            HazelcastInstance hi = Konfig.getInstance().getAntrian();
            BlockingQueue<HashMap<String, Object>> antrian = hi.getQueue("antrian" + jenis + date);
            map.put("nomor", nomor);
            map.put("file", req.queryParams("file"));
            antrian.put(map);
            HashMap<String, Object> mp = antrian.poll();
            result.put("nomor", mp.get("nomor"));
            result.put("file", mp.get("file"));
            result.put("error", "");
            return result;
        }, new JsonTransformer());
        post("/loket/callnama", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            Konfig.getInstance().getPa().setNama(req.queryParams("nama"));
            map.put("error", "");
            map.put("data", req.queryParams("data"));
            return map;
        }, new JsonTransformer());
        get("/loket/antrian", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
            HazelcastInstance hi = Konfig.getInstance().getAntrian();
            BlockingQueue<HashMap<String, Object>> antrianPerijinan = hi.getQueue("antrianPERIJINAN" + date);
            IAtomicLong nomorAntrianPerijinan = hi.getAtomicLong("nomor-antrianPERIJINAN" + date);
            map.put("sisaPerijinan", antrianPerijinan.size());
            map.put("totalPerijinan", nomorAntrianPerijinan.get());
            BlockingQueue<HashMap<String, Object>> antrianNonPerijinan = hi.getQueue("antrianNONPERIJINAN" + date);
            IAtomicLong nomorAntrianNonPerijinan = hi.getAtomicLong("nomor-antrianNONPERIJINAN" + date);
            map.put("sisaNonPerijinan", antrianNonPerijinan.size());
            map.put("totalNonPerijinan", nomorAntrianNonPerijinan.get());
            BlockingQueue<HashMap<String, Object>> antrianHelpdesk = hi.getQueue("antrianHELPDESK" + date);
            IAtomicLong nomorAntrianHelpdesk = hi.getAtomicLong("nomor-antrianHELPDESK" + date);
            map.put("sisaHelpDesk", antrianHelpdesk.size());
            map.put("totalHelpdesk", nomorAntrianHelpdesk.get());
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        put("/loket/coba", new Route() {
            @Override
            public Object handle(Request request, Response response) throws Exception {
                Map<String, Object> map = new HashMap<>();
                request.params().entrySet().stream().forEach((x) -> {
                    System.out.println(x.getKey() + "," + x.getValue());
                });
                return map;
            }
        }, new JsonTransformer());
        put("/loket/recall", (req, res) -> {
            Map<String, Object> result = new HashMap<>();
            String jenis = req.queryParams("jenis");
            String antr = req.queryParams("antrian");
            int antrian = Integer.valueOf(req.queryParams("antrian"));
            int loket = Integer.valueOf(req.queryParams("loket"));
            String th;
            switch (jenis) {
                case "PERIJINAN":
                    th = "A";
                    break;
                case "NONPERIJINAN":
                    th = "B";
                    break;
                default:
                    th = "C";
                    break;
            }
            log.debug("request accepted " + jenis + " " + antrian);
            Konfig.getInstance().getPa().setData(th, antrian, loket);
            result.put("error", "");
            return result;
        }, new JsonTransformer());
        post("/loket/antrian", (req, res) -> {
            Map<String, Object> result = new HashMap<>();
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            String jenis = req.queryParams("jenis");
            log.debug(jenis);
            String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
            HazelcastInstance hi = Konfig.getInstance().getAntrian();
            BlockingQueue<HashMap<String, Object>> antrian = hi.getQueue("antrian" + jenis + date);
            IAtomicLong nomorAntrian = hi.getAtomicLong("nomor-antrian" + jenis + date);
            long nomor = nomorAntrian.incrementAndGet();
            String namaFile = "./img/" + new SimpleDateFormat("ddMMyyyyhh:mm:ss").format(new Date()) + jenis + "nomor" + nomor + ".jpg";
            File file = new File(namaFile);
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            try (InputStream is = req.raw().getPart("file").getInputStream()) {
                // Use the input stream to create a file
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                OutputStream os = new FileOutputStream(file);
                os.write(buffer);
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
            }
            HashMap<String, Object> map = new HashMap<>();
            map.put("file", file.getName());
            map.put("nomor", nomor);
            antrian.add(map);
            result.put("nomor", nomor);
            String th;
            int loket;
            switch (jenis) {
                case "PERIJINAN":
                    loket = 1;
                    th = "A";
                    break;
                case "NONPERIJINAN":
                    loket = 2;
                    th = "B";
                    break;
                default:
                    loket = 3;
                    th = "C";
                    break;
            }
            result.put("seri", th);
            result.put("loket", loket);
            result.put("sisaperijinan", hi.getQueue("antrianPERIJINAN" + date).size());
            result.put("sisanonperijinan", hi.getQueue("antrianNONPERIJINAN" + date).size());
            result.put("sisahelpDesk", hi.getQueue("antrianHELPDESK" + date).size());
            result.put("totalperijinan", hi.getAtomicLong("antrianPERIJINAN" + date).get());
            result.put("totalnonperijinan", hi.getAtomicLong("antrianNONPERIJINAN" + date).get());
            result.put("totalhelpDesk", hi.getAtomicLong("antrianHELPDESK" + date).get());
            log.debug(gson.toJson(map));
            Konfig.getInstance().sendNotifikasi();
            return result;
        }, new JsonTransformer());
        get("/loket/layanan", (req, res) -> {
            return layananController.findSemuaLayanan();
        }, new JsonTransformer());
        get("/loket/layanan/:id", (req, res) -> {
            String kode = req.params("id");
            return layananController.findByKode(kode);
        }, new JsonTransformer());
        get("/user/image/:filename", (req, res) -> {
            File file = new File("./img/" + req.params("filename"));
            if (!file.exists()) {
                throw new IllegalStateException("File tidak tersedia");
            }
            res.raw().setContentType("image/jpeg");
            try (OutputStream out = res.raw().getOutputStream()) {
                ImageIO.write(ImageIO.read(file), "jpg", out);
            }
            return res.raw();
        });
        get("/antrian/artikel", (req, res) -> {

            return null;
        }, new JsonTransformer());
        get("/user/trx", (req, res) -> {
            log.debug(req.queryMap());
            String keypair = req.headers("keypair");
            String nomor = req.queryMap().get("nomor").value();
            Map<String, Object> map = new HashMap<>();
            if (keypair == null) {
                map.put("error", "Go away !! Restricted area");
                return map;
            }
            map.put("error", "");
            map.put("data", trxController.findOne(nomor));
            return map;
        }, new JsonTransformer());
        get("/user/trx/kembali", (req, res) -> {
            String keypair = req.headers("keypair");
            if (keypair == null) {
                throw new IllegalStateException("Go Away!! Restriction area");
            }
            int size = Integer.valueOf(req.queryMap().get("size").value() != null ? req.queryMap().get("size").value() : "25");
            int page = Integer.valueOf(req.queryMap().get("page").value() != null ? req.queryMap().get("page").value() : "0");
            String cari = req.queryMap().get("cari").value();
            return this.trxController.getKembalikanBerkas(cari, new PageRequest(page, size));
        }, new JsonTransformer());
        post("/user/trx/kembali", (req, res) -> {
            String nomor = req.queryParams("nomor");
            String penerima = req.queryParams("penerima");
            String alamat = req.queryParams("alamat");
            String hp = req.queryParams("hp");
            String token = req.headers("keypair");
            String item = req.queryParams("item");
            Type tps = new TypeToken<List<String>>() {
            }.getType();
            List<String> items = gson.fromJson(item, tps);
            return penyerahanBerkasController.simpan(nomor, penerima, token, alamat, hp, new Date(), items);
        }, new JsonTransformer());
        post("/user/disposisi", (req, res) -> {
            String kode = req.queryParams("kode");
            String dispo = req.queryParams("disposisi");
            String catatan = req.queryParams("catatan");
            String tugas = req.queryParams("tugas");
            return suratMasukController.simpanDisposisiSurat(kode, dispo, catatan, tugas);
        }, new JsonTransformer());
        post("/user/suratMasuk", (req, res) -> {
            String data = req.queryParams("data");
            KartuKendaliSuratMasuk kkm = gson.fromJson(data, KartuKendaliSuratMasuk.class);
            log.debug("data masuk " + data);
            return this.suratMasukController.simpanSuratMasuk(kkm);
        }, new JsonTransformer());
        put("/user/suratMasuk", (req, res) -> {
            String data = req.queryParams("data");
            KartuKendaliSuratMasuk kkm = gson.fromJson(data, KartuKendaliSuratMasuk.class);
            return suratMasukController.updateKartuKendali(kkm);
        }, new JsonTransformer());
        delete("/user/suratMasuk", (req, res) -> {
            String id = req.queryMap().get("id").value();
            return suratMasukController.deleteKartuKendali(id);
        }, new JsonTransformer());
        get("/user/suratMasuk/:id", (req, res) -> {
            String keypair = req.headers("keypair");
            if (keypair == null) {
                Map<String, Object> map = new HashMap<>();
                map.put("error", "Go Away!! Restricted area");
                return map;
            }
            return suratMasukController.temukanSatu(req.params("id"));
        }, new JsonTransformer());
        get("/user/suratMasuk", (req, res) -> {
            String keypair = req.headers("keypair");
            if (keypair == null) {
                Map<String, Object> map = new HashMap<>();
                map.put("error", "Go Away!! Restricted area");
                return map;
            }
            String cari = req.queryMap().get("cari").value();
            if (cari == null) {
                cari = "";
            }

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            Date awal = sdf.parse(req.queryMap().get("awal").value());
            Date akhir = sdf.parse(req.queryMap().get("akhir").value());

            int page = Integer.parseInt(req.queryMap().get("page").value() != null ? req.queryMap().get("page").value() : "0");
            int size = Integer.parseInt(req.queryMap().get("size").value() != null ? req.queryMap().get("size").value() : "25");
            return suratMasukController.findSurat(cari, awal, akhir, new PageRequest(page, size));
        }, new JsonTransformer());
        post("/user/suratMasuk/find", (req, res) -> {
            String cari = req.queryParams("cari");
            if (cari == null) {
                cari = "";
            }
            int page = Integer.valueOf((req.queryParams("page") == null) ? "0" : req.queryParams("page"));
            int size = Integer.valueOf((req.queryParams("size") == null) ? "25" : req.queryParams("size"));
            return suratMasukController.findSuratMasuk(cari, new PageRequest(page, size));
        }, new JsonTransformer());
        get("/user/suratKeluar", (req, res) -> {
            String keypair = req.headers("keypair");
            if (keypair == null) {
                Map<String, Object> map = new HashMap<>();
                map.put("error", "Go Away!! Restricted area");
                return map;
            }
            String cari = req.queryMap().get("cari").value();
            if (cari == null) {
                cari = "";
            }

            int page = Integer.valueOf((req.queryParams("page") == null) ? "0" : req.queryParams("page"));
            int size = Integer.valueOf((req.queryParams("size") == null) ? "25" : req.queryParams("size"));
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            Date awal = req.queryMap().get("awal").value() != null ? sdf.parse(req.queryMap().get("awal").value()) : new Date();
            Date akhir = req.queryMap().get("akhir").value() != null ? sdf.parse(req.queryMap().get("akhir").value()) : new Date();
            return this.suratKeluarController.findCariData(cari, awal, akhir, new PageRequest(page, size));
        }, new JsonTransformer());
        put("/user/suratkeluar", (req, res) -> {
            String data = req.queryParams("data");
            Map<String, Object> map = new HashMap<>();
            if (data == null) {
                map.put("error", "Data tidak valid");
                return map;
            } else {
                KartuKendaliSuratKeluar kks = Konfig.getInstance().getGson().fromJson(data, KartuKendaliSuratKeluar.class);
                return suratKeluarController.simpanSuratKeluar(kks);
            }
        }, new JsonTransformer());
        delete("/user/suratkeluar", (req, res) -> {
            return null;
        }, new JsonTransformer());
        post("/user/suratkeluar", (req, res) -> {
            String data = req.queryParams("data");
            Map<String, Object> map = new HashMap<>();
            if (data == null) {
                throw new IllegalStateException("Data tidak valid");
            } else {
                KartuKendaliSuratKeluar kks = Konfig.getInstance().getGson().fromJson(data, KartuKendaliSuratKeluar.class);
                return suratKeluarController.simpanSuratKeluar(kks);
            }
        }, new JsonTransformer());
        post("/user/upload", new FilePostRoute(), new JsonTransformer());
        get("/home", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("Test Data", "Ok");
            return map;
        }, new JsonTransformer());
        //admin frame
        get("/user/setting/kecamatan", (req, res) -> {
            return adminController.getKecamatan();
        }, new JsonTransformer());
        post("/user/setting/kecamatan", (req, res) -> {
            Kecamatan kcp = gson.fromJson(req.queryParams("data"), Kecamatan.class);
            return adminController.simpanKecamatan(kcp);
        }, new JsonTransformer());
        get("/user/setting/petugas", (req, res) -> {
            int halaman = Integer.valueOf(req.queryParams("halaman"));
            int size = Integer.valueOf(req.queryParams("size"));
            String cari = req.queryParams("cari");
            return adminController.findAllPetugas(cari, new PageRequest(halaman, size));
        }, new JsonTransformer());

        post("/user/setting/petugas", (req, res) -> {
            //tambah username
            Petugas p = gson.fromJson(req.queryParams("data"), Petugas.class);
            Petugas x = petugasController.save(p);
            Map<String, Object> map = new HashMap<>();
            map.put("data", p);
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        delete("/user/setting/petugas", (req, res) -> {
            //delete petugas
            String kode = req.queryParams("kode");
            Map<String, Object> map = new HashMap<>();
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        put("/user/setting/petugas", (req, res) -> {
            //update petugas
            Petugas p = gson.fromJson(req.queryParams("data"), Petugas.class);
            Petugas x = petugasController.update(p);
            Map<String, Object> map = new HashMap<>();
            map.put("data", p);
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        get("/user/setting/layanan", (req, res) -> {
            //get all layanan
            int page = Integer.valueOf(req.queryParams("page") == null ? "0" : req.queryParams("page"));
            int size = Integer.valueOf(req.queryParams("size") == null ? "25" : req.queryParams("size"));
            String cari = req.queryParams("cari");
            Map<String, Object> map = new HashMap<>();
            Page<Layanan> pl;
            if (cari == null) {
                pl = layananController.getData(new PageRequest(page, size));
            } else {
                pl = layananController.cariData(cari, new PageRequest(page, size));
            }
            map.put("data", pl.getContent());
            map.put("pageTotal", pl.getTotalPages());
            map.put("error", "");
            return map;
        }, new JsonTransformer());

        post("/user/setting/layanan", (req, res) -> {
            //tambah layanan
            Type tps = new TypeToken<List<Berkas>>() {
            }.getType();
            List<Berkas> items = gson.fromJson(req.queryParams("items"), tps);
            return layananController.buatLayanan(req.queryParams("kode"), req.queryParams("nama"),
                    req.queryParams("parent"), items,
                    req.queryParams("langsung").equalsIgnoreCase("TRUE"),
                    req.queryParams("masaberlaku").equalsIgnoreCase("TRUE"),
                    Integer.valueOf(req.queryParams("startingNumber")));
        }, new JsonTransformer());
        delete("/user/setting/layanan", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            String kode = req.queryParams("kode");
            layananController.hapusLayanan(kode);
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        put("/user/setting/layanan", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            //update data layanan
            Layanan l = gson.fromJson(req.queryParams("data"), Layanan.class);
            map.put("data", l);
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        get("/user/setting/loket", (req, res) -> {
            int page = Integer.valueOf(req.queryParams("page") == null ? "0" : req.queryParams("page"));
            int size = Integer.valueOf(req.queryParams("size") == null ? "25" : req.queryParams("size"));
            String cari = req.queryParams("cari");
            Page<Loket> pg;
            if (cari == null) {
                pg = loketController.findAllData(new PageRequest(page, size));
            } else {
                pg = loketController.findAllData(cari, new PageRequest(page, size));
            }
            Map<String, Object> map = new HashMap<>();
            map.put("data", pg.getContent());
            map.put("totalPage", pg.getTotalPages());
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        post("/user/setting/loket", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            Loket l = gson.fromJson(req.queryParams("data"), Loket.class);
            map.put("data", loketController.simpan(l));
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        delete("/user/setting/loket", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            String kode = req.queryParams("kode");
            loketController.delete(kode);
            map.put("error", "");
            return map;
        }, new JsonTransformer());
        put("/user/setting/loket", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("error", "");
            map.put("data", loketController.updateLoket(gson.fromJson(req.queryParams("data"), Loket.class)));
            return map;
        }, new JsonTransformer());
        exception(IllegalStateException.class, (e, req, res) -> {
            e.printStackTrace(System.err);
            res.status(400);
            Map<String, Object> map = new HashMap<>();
            map.put("error", e.getMessage());
            map.put("errorDescription", e);
            res.body(gson.toJson(map));
        });
        exception(IllegalArgumentException.class, (e, req, res) -> {
            e.printStackTrace(System.err);
            e.printStackTrace(System.err);
            res.status(406);
            Map<String, Object> map = new HashMap<>();
            map.put("error", e.getMessage());
            res.body(gson.toJson(map));
        });
        exception(NullPointerException.class, (e, req, res) -> {
            e.printStackTrace(System.err);
            res.status(404);
            Map<String, Object> map = new HashMap<>();
            map.put("error", "Maaf data yang anda minta tidak kami temukan");
            map.put("errorDescription", e);
            res.body(gson.toJson(map));
        });
        exception(NumberFormatException.class, (e, req, res) -> {
            e.printStackTrace(System.err);
            res.status(406);
            Map<String, Object> map = new HashMap<>();
            map.put("error", "Maaf data anda tidak valid");
            map.put("errorDescription", e);
            map.put("request", req.queryParams());
            res.body(gson.toJson(map));
        });
        exception(ParseException.class, (e, req, res) -> {
            e.printStackTrace(System.err);
            res.status(406);
            Map<String, Object> map = new HashMap<>();
            map.put("error", "Maaf data anda tidak valid");
            map.put("errorDescription", e);
            map.put("request", req.queryParams());
            res.body(gson.toJson(map));
        });
    }

    public static void main(final String[] args) {
        new InitServer().init();        
    }
}
