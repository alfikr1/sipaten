// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten;

import com.danang.paten.konfigurasi.Konfigurasi;
import com.danang.paten.util.InboundMessageNotifi;
import com.danang.paten.util.OutboundMessageNotifi;
import com.danang.paten.util.PlayAudio;
import java.awt.GraphicsDevice;
import com.danang.paten.view.VideoMain;
import javax.swing.SwingUtilities;
import com.danang.paten.view.MyLogin;
import com.sun.jna.NativeLibrary;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.UIManager;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXLoginPane.JXLoginFrame;
import org.smslib.SMSLibException;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;

public class MainClass {

    private static final Logger log = Logger.getLogger(MainClass.class);

    public static void main(final String[] args) {

        NativeLibrary.addSearchPath("libvlc", "C:\\Program Files\\VideoLAN\\VLC\\plugins");
        Konfig.getInstance();
        final Thread t = new Thread(() -> {
            NativeLibrary.getInstance("libvlc");
            try {
                for (final UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                    if ("Metal".equals(info.getName())) {
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {

                ex.printStackTrace(System.err);
            }
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            if (gs.length > 1) {
                JXLoginFrame loginFrame = new JXLoginFrame(new MyLogin().init());
                SwingUtilities.invokeLater(() -> loginFrame.setVisible(true));
                VideoMain videoMain = new VideoMain(gs[1].getDefaultConfiguration());
                Konfig.getInstance().setVideoMain(videoMain);
                videoMain.setExtendedState(6);
                videoMain.setVisible(true);
            } else {
                System.out.println("monitor 1");
                VideoMain videoMain = new VideoMain();
                Konfig.getInstance().setVideoMain(videoMain);
                videoMain.setLocationRelativeTo(null);
                videoMain.setExtendedState(6);
                videoMain.setVisible(true);
            }
        });
        final Thread t3 = new Thread(() -> {
            try {
                Konfigurasi k = new Konfigurasi();
                SerialModemGateway usb = new SerialModemGateway("modem", k.getComPORT(), k.getBaudRate(), k.getMerkModem(), k.getSeriModem());
                Service ss = Service.getInstance();
                ss.addGateway(usb);
                ss.setOutboundMessageNotification(new OutboundMessageNotifi());
                ss.setInboundMessageNotification(new InboundMessageNotifi());
                ss.startService();
            } catch (SMSLibException | IOException | InterruptedException e) {
                log.error("Failed to create sms service", e);
            } catch (Exception e) {
                log.error("Failed to create sms service", e);
            }
        });
        final Thread t4 = new Thread(() -> {
            try {
                FtpServer.bukaFTP(9877);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        });
        final Thread t2 = new Thread(() -> new InitServer().init());
        SwingUtilities.invokeLater(() -> {
            t.start();
            //JXLoginFrame loginFrame = new JXLoginFrame(new MyLogin().init());
            //SwingUtilities.invokeLater(() -> loginFrame.setVisible(true));
            t2.start();
            t3.start();
            t4.start();
        });
    }
}
