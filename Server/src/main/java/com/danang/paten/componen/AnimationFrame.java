// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import java.awt.BorderLayout;
import java.text.NumberFormat;
import java.awt.Label;
import javax.swing.JFrame;

class AnimationFrame extends JFrame
{
    private Label mStatusLabel;
    private NumberFormat mFormat;
    
    public AnimationFrame(TextBouncer ac) {
        setLayout(new BorderLayout());
        add(ac, "Center");
        add(this.mStatusLabel = new Label(), "South");
        (this.mFormat = NumberFormat.getInstance()).setMaximumFractionDigits(1);
        ac.setRateListener(this);
        final Thread t = new Thread(ac);
        t.start();
    }
    
    public void rateChanged(final double frameRate) {
        this.mStatusLabel.setText(this.mFormat.format(frameRate) + " fps");
    }
}
