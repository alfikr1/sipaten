// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import javax.swing.JFrame;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.font.FontRenderContext;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.font.TextLayout;
import java.awt.Font;
import java.util.Map;
import java.awt.RenderingHints;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;

public class ScrollText extends JComponent
{
    private BufferedImage image;
    private Dimension imageSize;
    private volatile int currOffset;
    private Thread internalThread;
    private volatile boolean noStopRequested;
    
    public ScrollText(final String text) {
        this.currOffset = 0;
        this.buildImage(text);
        this.setMinimumSize(this.imageSize);
        this.setPreferredSize(this.imageSize);
        this.setMaximumSize(this.imageSize);
        this.setSize(this.imageSize);
        this.noStopRequested = true;
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    ScrollText.this.runWork();
                }
                catch (Exception x) {
                    x.printStackTrace();
                }
            }
        };
        (this.internalThread = new Thread(r, "ScrollText")).start();
    }
    
    private void buildImage(final String text) {
        final RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        final BufferedImage scratchImage = new BufferedImage(1, 1, 1);
        final Graphics2D scratchG2 = scratchImage.createGraphics();
        scratchG2.setRenderingHints(renderHints);
        final Font font = new Font("Arial", 3, 48);
        final FontRenderContext frc = scratchG2.getFontRenderContext();
        final TextLayout tl = new TextLayout(text, font, frc);
        final Rectangle2D textBounds = tl.getBounds();
        final int textWidth = (int)Math.ceil(textBounds.getWidth());
        final int textHeight = (int)Math.ceil(textBounds.getHeight());
        final int horizontalPad = 10;
        final int verticalPad = 6;
        this.imageSize = new Dimension(textWidth + horizontalPad, textHeight + verticalPad);
        this.image = new BufferedImage(this.imageSize.width, this.imageSize.height, 1);
        final Graphics2D g2 = this.image.createGraphics();
        g2.setRenderingHints(renderHints);
        final int baselineOffset = verticalPad / 2 - (int)textBounds.getY();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, this.imageSize.width, this.imageSize.height);
        g2.setColor(Color.blue);
        tl.draw(g2, 0.0f, baselineOffset);
        scratchG2.dispose();
        scratchImage.flush();
        g2.dispose();
    }
    
    @Override
    public void paint(final Graphics g) {
        g.setClip(0, 0, this.imageSize.width, this.imageSize.height);
        final int localOffset = this.currOffset;
        g.drawImage(this.image, -localOffset, 0, this);
        g.drawImage(this.image, this.imageSize.width - localOffset, 0, this);
        g.setColor(Color.black);
        g.drawRect(0, 0, this.imageSize.width - 1, this.imageSize.height - 1);
    }
    
    private void runWork() {
        while (this.noStopRequested) {
            try {
                Thread.sleep(100L);
                this.currOffset = (this.currOffset + 1) % this.imageSize.width;
                this.repaint();
            }
            catch (InterruptedException x) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    public void stopRequest() {
        this.noStopRequested = false;
        this.internalThread.interrupt();
    }
    
    public boolean isAlive() {
        return this.internalThread.isAlive();
    }
    
    public static void main(final String[] args) {
        final ScrollText st = new ScrollText("Java can do animation!");
        final JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, 0));
        p.add(st);
        final JFrame f = new JFrame("ScrollText Demo");
        f.setContentPane(p);
        f.setDefaultCloseOperation(3);
        f.setSize(400, 100);
        f.setVisible(true);
    }
}
