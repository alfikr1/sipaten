// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.GroupLayout;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelAntrean extends JPanel
{
    private String id;
    private long antrean;
    private JLabel jLabel1;
    
    public PanelAntrean() {
        this.antrean = 0L;
        this.initComponents();
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public void setAntrean(final long antrean) {
        this.antrean = antrean;
        this.jLabel1.setText(String.valueOf(antrean));
    }
    
    private void initComponents() {
        (this.jLabel1 = new JLabel()).setFont(new Font("DejaVu Sans", 1, 48));
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText("0");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1, -1, 178, 32767).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1, -1, 78, 32767).addContainerGap()));
    }
}
