// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JFrame;
import java.awt.event.WindowEvent;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Graphics;
import javax.swing.BoxLayout;
import javax.swing.Timer;
import java.awt.event.WindowListener;
import javax.swing.event.AncestorListener;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

public class MarqueePanel extends JPanel implements ActionListener, AncestorListener, WindowListener
{
    protected boolean paintChildren;
    protected boolean scrollingPaused;
    protected int scrollOffset;
    protected int wrapOffset;
    private int preferredWidth;
    private int scrollAmount;
    private int scrollFrequency;
    private boolean wrap;
    private int wrapAmount;
    private boolean scrollWhenFocused;
    private Timer timer;
    
    public MarqueePanel() {
        this(5, 5);
    }
    
    public MarqueePanel(final int scrollFrequency, final int scrollAmount) {
        this.preferredWidth = -1;
        this.wrap = false;
        this.wrapAmount = 50;
        this.scrollWhenFocused = true;
        this.timer = new Timer(1000, this);
        this.setScrollFrequency(scrollFrequency);
        this.setScrollAmount(scrollAmount);
        this.setLayout(new BoxLayout(this, 0));
        this.addAncestorListener(this);
    }
    
    public void paintChildren(final Graphics g) {
        if (!this.paintChildren) {
            return;
        }
        final Graphics2D g2d = (Graphics2D)g;
        g2d.translate(-this.scrollOffset, 0);
        super.paintChildren(g);
        g2d.translate(this.scrollOffset, 0);
        if (this.isWrap()) {
            this.wrapOffset = this.scrollOffset - super.getPreferredSize().width - this.wrapAmount;
            g2d.translate(-this.wrapOffset, 0);
            super.paintChildren(g);
            g2d.translate(this.wrapOffset, 0);
        }
    }
    
    @Override
    public Dimension getPreferredSize() {
        final Dimension d = super.getPreferredSize();
        d.width = ((this.preferredWidth == -1) ? (d.width / 2) : this.preferredWidth);
        return d;
    }
    
    @Override
    public Dimension getMinimumSize() {
        return this.getPreferredSize();
    }
    
    public int getPreferredWidth() {
        return this.preferredWidth;
    }
    
    public void setPreferredWidth(final int preferredWidth) {
        this.preferredWidth = preferredWidth;
        this.revalidate();
    }
    
    public int getScrollAmount() {
        return this.scrollAmount;
    }
    
    public void setScrollAmount(final int scrollAmount) {
        this.scrollAmount = scrollAmount;
    }
    
    public int getScrollFrequency() {
        return this.scrollFrequency;
    }
    
    public void setScrollFrequency(final int scrollFrequency) {
        this.scrollFrequency = scrollFrequency;
        final int delay = 1000 / scrollFrequency;
        this.timer.setInitialDelay(delay);
        this.timer.setDelay(delay);
    }
    
    public boolean isScrollWhenFocused() {
        return this.scrollWhenFocused;
    }
    
    public void setScrollWhenFocused(final boolean scrollWhenFocused) {
        this.scrollWhenFocused = scrollWhenFocused;
    }
    
    public boolean isWrap() {
        return this.wrap;
    }
    
    public void setWrap(final boolean wrap) {
        this.wrap = wrap;
    }
    
    public int getWrapAmount() {
        return this.wrapAmount;
    }
    
    public void setWrapAmount(final int wrapAmount) {
        this.wrapAmount = wrapAmount;
    }
    
    public void startScrolling() {
        this.paintChildren = true;
        this.scrollOffset = -this.getSize().width;
        this.timer.start();
    }
    
    public void stopScrolling() {
        this.timer.stop();
        this.paintChildren = false;
        this.repaint();
    }
    
    public void pauseScrolling() {
        if (this.timer.isRunning()) {
            this.timer.stop();
            this.scrollingPaused = true;
        }
    }
    
    public void resumeScrolling() {
        if (this.scrollingPaused) {
            this.timer.restart();
            this.scrollingPaused = false;
        }
    }
    
    @Override
    public void actionPerformed(final ActionEvent ae) {
        this.scrollOffset += this.scrollAmount;
        final int width = super.getPreferredSize().width;
        if (this.scrollOffset > width) {
            this.scrollOffset = (this.isWrap() ? (this.wrapOffset + this.scrollAmount) : (-this.getSize().width));
        }
        this.repaint();
    }
    
    @Override
    public void ancestorAdded(final AncestorEvent e) {
        SwingUtilities.windowForComponent(this).addWindowListener(this);
    }
    
    @Override
    public void ancestorMoved(final AncestorEvent e) {
    }
    
    @Override
    public void ancestorRemoved(final AncestorEvent e) {
    }
    
    @Override
    public void windowActivated(final WindowEvent e) {
        if (this.isScrollWhenFocused()) {
            this.resumeScrolling();
        }
    }
    
    @Override
    public void windowClosed(final WindowEvent e) {
        this.stopScrolling();
    }
    
    @Override
    public void windowClosing(final WindowEvent e) {
        this.stopScrolling();
    }
    
    @Override
    public void windowDeactivated(final WindowEvent e) {
        if (this.isScrollWhenFocused()) {
            this.pauseScrolling();
        }
    }
    
    @Override
    public void windowDeiconified(final WindowEvent e) {
        this.resumeScrolling();
    }
    
    @Override
    public void windowIconified(final WindowEvent e) {
        this.pauseScrolling();
    }
    
    @Override
    public void windowOpened(final WindowEvent e) {
        this.startScrolling();
    }
    
    public static void main(final String[] args) {
        final JFrame f = new JFrame();
        final MarqueePanel panel = new MarqueePanel();
        panel.setWrap(true);
        final JLabel lbl = new JLabel("Hello world this is new world");
        lbl.setFont(new Font("SansSerif", 1, 48));
        panel.add(lbl);
        f.getContentPane().setLayout(new BoxLayout(f.getContentPane(), 0));
        f.getContentPane().add(panel);
        f.setDefaultCloseOperation(3);
        f.setExtendedState(6);
        f.setLocationRelativeTo(null);
        SwingUtilities.invokeLater(() -> f.setVisible(true));
    }
}
