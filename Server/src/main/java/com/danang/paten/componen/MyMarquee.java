// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.font.FontRenderContext;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.font.TextLayout;
import java.awt.Font;
import java.awt.RenderingHints;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;

public class MyMarquee extends JComponent
{
    private BufferedImage image;
    private Dimension imageSize;
    private volatile int currOffset;
    private Thread internalThread;
    private volatile boolean noStopRequested;
    
    public MyMarquee(final String text) {
        this.currOffset = 0;
        this.buildImage(text);
        this.noStopRequested = true;
        final Runnable r = () -> {
            try {
                this.runWork();
            }
            catch (Exception e) {
                e.printStackTrace(System.err);
            }
            return;
        };
        (this.internalThread = new Thread(r, "sc")).start();
    }
    
    public void setText(final String text) {
        this.noStopRequested = false;
        if (this.internalThread.isAlive() || this.internalThread.isDaemon()) {
            this.internalThread.interrupt();
        }
        this.currOffset = 0;
        this.buildImage(text);
        this.noStopRequested = true;
        final Runnable r = () -> {
            try {
                this.runWork();
            }
            catch (Exception e) {
                e.printStackTrace(System.err);
            }
            return;
        };
        (this.internalThread = new Thread(r, "sc")).start();
    }
    
    private void buildImage(final String text) {
        final RenderingHints rend = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rend.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        final BufferedImage si = new BufferedImage(1, 1, 1);
        final Graphics2D g2 = si.createGraphics();
        g2.setRenderingHints(rend);
        final Font font = new Font("Serif", 3, 59);
        final FontRenderContext frc = g2.getFontRenderContext();
        final TextLayout t1 = new TextLayout(text, font, frc);
        final Rectangle2D tb = t1.getBounds();
        final int textWidth = (int)Math.ceil(tb.getWidth());
        final int textHeight = (int)Math.ceil(tb.getHeight());
        final int horizontalPad = 600;
        final int verticalPad = 10;
        this.imageSize = new Dimension(textWidth + horizontalPad, textHeight + verticalPad);
        this.image = new BufferedImage(this.imageSize.width, this.imageSize.height, 1);
        final Graphics2D gg2 = this.image.createGraphics();
        gg2.setRenderingHints(rend);
        final int baselineOffset = verticalPad / 2 - (int)tb.getY();
        gg2.setColor(Color.BLACK);
        gg2.fillRect(0, 0, this.imageSize.width, this.imageSize.height);
        gg2.setColor(Color.GREEN);
        t1.draw(gg2, 0.0f, baselineOffset);
        g2.dispose();
        si.flush();
        gg2.dispose();
    }
    
    @Override
    public void paint(final Graphics g) {
        g.setClip(0, 0, this.imageSize.width, this.imageSize.height);
        final int localOffset = this.currOffset;
        g.drawImage(this.image, -localOffset, 0, this);
        g.drawImage(this.image, this.imageSize.width - localOffset, 0, this);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, this.imageSize.width - 1, this.imageSize.height - 1);
    }
    
    private void runWork() {
        while (this.noStopRequested) {
            try {
                Thread.sleep(10L);
                this.currOffset = (this.currOffset + 1) % this.imageSize.width;
                this.repaint();
            }
            catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    public void stopRequest() {
        this.noStopRequested = false;
        this.internalThread.interrupt();
    }
    
    public static void main(final String[] args) {
        final JFrame f = new JFrame();
        f.setLayout(new BorderLayout());
        f.setDefaultCloseOperation(3);
        f.setSize(600, 400);
        f.getContentPane().add(new MyMarquee("Hello world"));
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
