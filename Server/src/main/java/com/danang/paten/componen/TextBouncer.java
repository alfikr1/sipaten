// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Paint;
import java.awt.GradientPaint;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.Checkbox;
import java.awt.geom.Rectangle2D;
import java.awt.event.ComponentListener;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import java.awt.geom.AffineTransform;
import java.awt.font.FontRenderContext;
import java.util.Random;
import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.LayoutManager;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Choice;
import java.awt.Panel;
import java.awt.MediaTracker;
import java.awt.Component;
import java.awt.Image;
import javax.swing.JPanel;

public class TextBouncer extends JPanel implements Runnable
{
    private final boolean trucking = true;
    private final long[] previousTimes;
    private int previousIndex;
    private boolean previousFilled;
    private double frameRate;
    private Image image;
    private boolean mAntialiasing;
    private boolean mGradient;
    private boolean mShear;
    private boolean mRotate;
    private boolean mAxes;
    public static final int ANTIALIASING = 0;
    public static final int GRADIENT = 1;
    public static final int SHEAR = 2;
    public static final int ROTATE = 3;
    public static final int AXES = 5;
    private float mDeltaX;
    private float mDeltaY;
    private float mX;
    private float mY;
    private float mWidth;
    private float mHeight;
    private float mTheta;
    private float mShearX;
    private float mShearY;
    private float mShearDeltaX;
    private float mShearDeltaY;
    private String mString;
    private transient AnimationFrame mRateListener;
    private static Component sComponent;
    private static final MediaTracker sTracker;
    private static int sID;
    
    public static void main(final String[] args) {
        String s = "Java Source and Support";
        final int size = 64;
        if (args.length > 0) {
            s = args[0];
        }
        final Panel controls = new Panel();
        final Choice choice = new Choice();
        final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        final Font[] allFonts2;
        final Font[] allFonts = allFonts2 = ge.getAllFonts();
        for (final Font allFont : allFonts2) {
            choice.addItem(allFont.getName());
        }
        final Font defaultFont = new Font(allFonts[0].getName(), 0, 64);
        final TextBouncer bouncer = new TextBouncer(s, defaultFont);
        final JFrame f = new AnimationFrame(bouncer);
        f.setFont(new Font("Serif", 0, 12));
        controls.add(bouncer.createCheckbox("Antialiasing", 0));
        controls.add(bouncer.createCheckbox("Gradient", 1));
        controls.add(bouncer.createCheckbox("Shear", 2));
        controls.add(bouncer.createCheckbox("Rotate", 3));
        controls.add(bouncer.createCheckbox("Axes", 5));
        final Panel fontControls = new Panel();
        
        //final Font font1;
        final JComponent component;
        /*choice.addItemListener(ie -> {
           Choice choice2;
            Font font1 = new Font(choice2.getSelectedItem(), 0, 64);
            component.setFont(font1);
            return;
        });*/
        fontControls.add(choice);
        final Panel allControls = new Panel(new GridLayout(2, 1));
        allControls.add(controls);
        allControls.add(fontControls);
        f.setDefaultCloseOperation(3);
        f.add(allControls, "North");
        f.setSize(300, 300);
        f.setVisible(true);
    }
    
    public TextBouncer(final String s, final Font f) {
        this.image = null;
        this.mAntialiasing = false;
        this.mGradient = false;
        this.mShear = false;
        this.mRotate = false;
        this.mAxes = false;
        (this.previousTimes = new long[128])[0] = System.currentTimeMillis();
        this.previousIndex = 1;
        this.previousFilled = false;
        this.mString = s;
        this.setFont(f);
        final Random random = new Random();
        this.mX = random.nextFloat() * 500.0f;
        this.mY = random.nextFloat() * 500.0f;
        this.mDeltaX = random.nextFloat() * 3.0f;
        this.mDeltaY = random.nextFloat() * 3.0f;
        this.mShearX = random.nextFloat() / 2.0f;
        this.mShearY = random.nextFloat() / 2.0f;
        final float n = 0.05f;
        this.mShearDeltaY = n;
        this.mShearDeltaX = n;
        final FontRenderContext frc = new FontRenderContext(null, true, false);
        final Rectangle2D bounds = this.getFont().getStringBounds(this.mString, frc);
        this.mWidth = (float)bounds.getWidth();
        this.mHeight = (float)bounds.getHeight();
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(final ComponentEvent ce) {
                final Dimension d = TextBouncer.this.getSize();
                if (TextBouncer.this.mX < 0.0f) {
                    TextBouncer.this.mX = 0.0f;
                }
                else if (TextBouncer.this.mX + TextBouncer.this.mWidth >= d.width) {
                    TextBouncer.this.mX = d.width - TextBouncer.this.mWidth - 1.0f;
                }
                if (TextBouncer.this.mY < 0.0f) {
                    TextBouncer.this.mY = 0.0f;
                }
                else if (TextBouncer.this.mY + TextBouncer.this.mHeight >= d.height) {
                    TextBouncer.this.mY = d.height - TextBouncer.this.mHeight - 1.0f;
                }
            }
        });
    }
    
    public void setSwitch(final int item, final boolean value) {
        switch (item) {
            case 0: {
                this.mAntialiasing = value;
                break;
            }
            case 1: {
                this.mGradient = value;
                break;
            }
            case 2: {
                this.mShear = value;
                break;
            }
            case 3: {
                this.mRotate = value;
                break;
            }
            case 5: {
                this.mAxes = value;
                break;
            }
        }
    }
    
    protected Checkbox createCheckbox(final String label, final int item) {
        final Checkbox check = new Checkbox(label);
        check.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent ie) {
                TextBouncer.this.setSwitch(item, ie.getStateChange() == 1);
            }
        });
        return check;
    }
    
    public void timeStep() {
        final Dimension d = this.getSize();
        if (this.mX + this.mDeltaX < 0.0f) {
            this.mDeltaX = -this.mDeltaX;
        }
        else if (this.mX + this.mWidth + this.mDeltaX >= d.width) {
            this.mDeltaX = -this.mDeltaX;
        }
        if (this.mY + this.mDeltaY < 0.0f) {
            this.mDeltaY = -this.mDeltaY;
        }
        else if (this.mY + this.mHeight + this.mDeltaY >= d.height) {
            this.mDeltaY = -this.mDeltaY;
        }
        this.mX += this.mDeltaX;
        this.mY += this.mDeltaY;
        this.mTheta += 0.016362461737446838;
        if (this.mTheta > 6.283185307179586) {
            this.mTheta -= 6.283185307179586;
        }
        if (this.mShearX + this.mShearDeltaX > 0.5) {
            this.mShearDeltaX = -this.mShearDeltaX;
        }
        else if (this.mShearX + this.mShearDeltaX < -0.5) {
            this.mShearDeltaX = -this.mShearDeltaX;
        }
        if (this.mShearY + this.mShearDeltaY > 0.5) {
            this.mShearDeltaY = -this.mShearDeltaY;
        }
        else if (this.mShearY + this.mShearDeltaY < -0.5) {
            this.mShearDeltaY = -this.mShearDeltaY;
        }
        this.mShearX += this.mShearDeltaX;
        this.mShearY += this.mShearDeltaY;
    }
    
    @Override
    public void paint(final Graphics g) {
        final Graphics2D g2 = (Graphics2D)g;
        this.setAntialiasing(g2);
        this.setTransform(g2);
        this.setPaint(g2);
        g2.setFont(this.getFont());
        g2.drawString(this.mString, this.mX, this.mY + this.mHeight);
        this.drawAxes(g2);
    }
    
    protected void setAntialiasing(final Graphics2D g2) {
        if (!this.mAntialiasing) {
            return;
        }
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }
    
    protected void setTransform(final Graphics2D g2) {
        final Dimension d = this.getSize();
        final int cx = d.width / 2;
        final int cy = d.height / 2;
        g2.translate(cx, cy);
        if (this.mShear) {
            g2.shear(this.mShearX, this.mShearY);
        }
        if (this.mRotate) {
            g2.rotate(this.mTheta);
        }
        g2.translate(-cx, -cy);
    }
    
    protected void setPaint(final Graphics2D g2) {
        if (this.mGradient) {
            final GradientPaint gp = new GradientPaint(0.0f, 0.0f, Color.blue, 50.0f, 25.0f, Color.green, true);
            g2.setPaint(gp);
        }
        else {
            g2.setPaint(Color.orange);
        }
    }
    
    protected void drawAxes(final Graphics2D g2) {
        if (!this.mAxes) {
            return;
        }
        g2.setPaint(this.getForeground());
        g2.setStroke(new BasicStroke());
        final Dimension d = this.getSize();
        final int side = 20;
        final int arrow = 4;
        final int w = d.width / 2;
        final int h = d.height / 2;
        g2.drawLine(w - side, h, w + side, h);
        g2.drawLine(w + side - arrow, h - arrow, w + side, h);
        g2.drawLine(w, h - side, w, h + side);
        g2.drawLine(w + arrow, h + side - arrow, w, h + side);
    }
    
    @Override
    public void run() {
        while (true) {
            this.render();
            this.timeStep();
            this.calculateFrameRate();
        }
    }
    
    protected void render() {
        final Graphics g = this.getGraphics();
        if (g != null) {
            final Dimension d = this.getSize();
            if (this.checkImage(d)) {
                final Graphics imageGraphics = this.image.getGraphics();
                imageGraphics.setColor(this.getBackground());
                imageGraphics.fillRect(0, 0, d.width, d.height);
                imageGraphics.setColor(this.getForeground());
                this.paint(imageGraphics);
                g.drawImage(this.image, 0, 0, null);
                imageGraphics.dispose();
            }
            g.dispose();
        }
    }
    
    protected boolean checkImage(final Dimension d) {
        if (d.width == 0 || d.height == 0) {
            return false;
        }
        if (this.image == null || this.image.getWidth(null) != d.width || this.image.getHeight(null) != d.height) {
            this.image = this.createImage(d.width, d.height);
        }
        return true;
    }
    
    protected void calculateFrameRate() {
        final long now = System.currentTimeMillis();
        final int numberOfFrames = this.previousTimes.length;
        double newRate;
        if (this.previousFilled) {
            newRate = numberOfFrames / (now - this.previousTimes[this.previousIndex]) * 1000.0;
        }
        else {
            newRate = 1000.0 / (now - this.previousTimes[numberOfFrames - 1]);
        }
        this.firePropertyChange("frameRate", this.frameRate, newRate);
        this.frameRate = newRate;
        this.previousTimes[this.previousIndex] = now;
        ++this.previousIndex;
        if (this.previousIndex >= numberOfFrames) {
            this.previousIndex = 0;
            this.previousFilled = true;
        }
    }
    
    public double getFrameRate() {
        return this.frameRate;
    }
    
    public void setRateListener(final AnimationFrame af) {
        this.mRateListener = af;
    }
    
    @Override
    public void firePropertyChange(final String name, final double oldValue, final double newValue) {
        this.mRateListener.rateChanged(newValue);
    }
    
    public static boolean waitForImage(final Image image) {
        final int id;
        synchronized (TextBouncer.sComponent) {
            id = TextBouncer.sID++;
        }
        TextBouncer.sTracker.addImage(image, id);
        try {
            TextBouncer.sTracker.waitForID(id);
        }
        catch (InterruptedException ie) {
            return false;
        }
        return !TextBouncer.sTracker.isErrorID(id);
    }
    
    public Image blockingLoad(final String path) {
        final Image image = Toolkit.getDefaultToolkit().getImage(path);
        if (!waitForImage(image)) {
            return null;
        }
        return image;
    }
    
    public static Image blockingLoad(final URL url) {
        final Image image = Toolkit.getDefaultToolkit().getImage(url);
        if (!waitForImage(image)) {
            return null;
        }
        return image;
    }
    
    public BufferedImage makeBufferedImage(final Image image) {
        return this.makeBufferedImage(image, 1);
    }
    
    public BufferedImage makeBufferedImage(final Image image, final int imageType) {
        if (!waitForImage(image)) {
            return null;
        }
        final BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), imageType);
        final Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, null, null);
        return bufferedImage;
    }
    
    static {
        TextBouncer.sComponent = new Component() {};
        sTracker = new MediaTracker(TextBouncer.sComponent);
        TextBouncer.sID = 0;
    }
}
