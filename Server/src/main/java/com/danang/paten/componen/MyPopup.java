// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.JXSearchField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;

public class MyPopup extends JPanel
{
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    private JButton jButton4;
    private JLabel jLabel1;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;
    private JTable jTable1;
    private JXSearchField jXSearchField1;
    
    public MyPopup() {
        this.initComponents();
    }
    
    public JTable getTableView() {
        return this.jTable1;
    }
    
    public void setHalaman(final int halaman) {
        this.jLabel1.setText(String.valueOf(halaman));
    }
    
    public JButton btnLast() {
        return this.jButton4;
    }
    
    public JButton btnPref() {
        return this.jButton2;
    }
    
    public JButton btnFirst() {
        return this.jButton1;
    }
    
    public JButton btnNext() {
        return this.jButton3;
    }
    
    public JXSearchField getTxtCari() {
        return this.jXSearchField1;
    }
    
    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.jPanel3 = new JPanel();
        this.jXSearchField1 = new JXSearchField();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jLabel1 = new JLabel();
        this.jButton3 = new JButton();
        this.jButton4 = new JButton();
        this.jTable1.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
        this.jScrollPane1.setViewportView(this.jTable1);
        this.jPanel3.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        this.jXSearchField1.setText("jXSearchField1");
        this.jButton1.setText("|<");
        this.jButton2.setText("<");
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText("0");
        this.jButton3.setText(">");
        this.jButton4.setText(">|");
        final GroupLayout jPanel3Layout = new GroupLayout(this.jPanel3);
        this.jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addContainerGap().addComponent(this.jXSearchField1, -2, 185, -2).addGap(18, 18, 18).addComponent(this.jButton1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel1, -2, 43, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton4).addContainerGap(-1, 32767)));
        jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup().addContainerGap(-1, 32767).addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jXSearchField1, -2, -1, -2).addComponent(this.jButton1).addComponent(this.jButton2).addComponent(this.jLabel1).addComponent(this.jButton3).addComponent(this.jButton4)).addContainerGap()));
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 542, 32767).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jPanel3, -1, -1, 32767).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jScrollPane1, -1, 398, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel3, -2, -1, -2).addContainerGap()));
    }
}
