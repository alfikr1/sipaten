// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.componen;

import javax.swing.text.ViewFactory;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import java.awt.Graphics;
import javax.swing.BoundedRangeModel;
import java.awt.Rectangle;
import javax.swing.JTextField;
import java.awt.Shape;
import javax.swing.text.Element;
import javax.swing.text.ParagraphView;
import java.awt.Component;
import javax.swing.plaf.ComponentUI;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicTextFieldUI;

public class MyTextUI extends BasicTextFieldUI
{
    public static ComponentUI createUI(final JComponent c) {
        return new MyTextUI();
    }
    
    @Override
    protected String getPropertyPrefix() {
        return "myTxt";
    }
    
    public int getResizeWeight(final int axis) {
        if (axis == 0) {
            return 1;
        }
        return 0;
    }
    
    @Override
    public Component.BaselineResizeBehavior getBaselineResizeBehavior(final JComponent c) {
        super.getBaselineResizeBehavior(c);
        return Component.BaselineResizeBehavior.CENTER_OFFSET;
    }
    
    static class I18nFieldView extends ParagraphView
    {
        I18nFieldView(final Element elem) {
            super(elem);
        }
        
        @Override
        public int getFlowSpan(final int index) {
            return Integer.MAX_VALUE;
        }
        
        @Override
        protected void setJustification(final int j) {
        }
        
        static boolean isLeftToRight(final Component c) {
            return c.getComponentOrientation().isLeftToRight();
        }
        
        Shape adjustAllocation(final Shape a) {
            if (a != null) {
                final Rectangle bounds = a.getBounds();
                final int vspan = (int)this.getPreferredSpan(1);
                final int hspan = (int)this.getPreferredSpan(0);
                if (bounds.height != vspan) {
                    final int slop = bounds.height - vspan;
                    final Rectangle rectangle = bounds;
                    rectangle.y += slop / 2;
                    final Rectangle rectangle2 = bounds;
                    rectangle2.height -= slop;
                }
                final Component c = this.getContainer();
                if (c instanceof JTextField) {
                    final JTextField field = (JTextField)c;
                    final BoundedRangeModel vis = field.getHorizontalVisibility();
                    final int max = Math.max(hspan, bounds.width);
                    int value = vis.getValue();
                    final int extent = Math.min(max, bounds.width - 1);
                    if (value + extent > max) {
                        value = max - extent;
                    }
                    vis.setRangeProperties(value, extent, vis.getMinimum(), max, false);
                    if (hspan < bounds.width) {
                        final int slop2 = bounds.width - 1 - hspan;
                        int align = ((JTextField)c).getHorizontalAlignment();
                        if (isLeftToRight(c)) {
                            if (align == 10) {
                                align = 2;
                            }
                            else if (align == 11) {
                                align = 4;
                            }
                        }
                        else if (align == 10) {
                            align = 4;
                        }
                        else if (align == 11) {
                            align = 2;
                        }
                        switch (align) {
                            case 0: {
                                final Rectangle rectangle3 = bounds;
                                rectangle3.x += slop2 / 2;
                                final Rectangle rectangle4 = bounds;
                                rectangle4.width -= slop2;
                                break;
                            }
                            case 4: {
                                final Rectangle rectangle5 = bounds;
                                rectangle5.x += slop2;
                                final Rectangle rectangle6 = bounds;
                                rectangle6.width -= slop2;
                                break;
                            }
                        }
                    }
                    else {
                        bounds.width = hspan;
                        final Rectangle rectangle7 = bounds;
                        rectangle7.x -= vis.getValue();
                    }
                }
                return bounds;
            }
            return null;
        }
        
        void updateVisibilityModel() {
            final Component c = this.getContainer();
            if (c instanceof JTextField) {
                final JTextField field = (JTextField)c;
                final BoundedRangeModel vis = field.getHorizontalVisibility();
                final int hspan = (int)this.getPreferredSpan(0);
                int extent = vis.getExtent();
                final int maximum = Math.max(hspan, extent);
                extent = ((extent == 0) ? maximum : extent);
                int value = maximum - extent;
                int oldValue = vis.getValue();
                if (oldValue + extent > maximum) {
                    oldValue = maximum - extent;
                }
                value = Math.max(0, Math.min(value, oldValue));
                vis.setRangeProperties(value, extent, 0, maximum, false);
            }
        }
        
        @Override
        public void paint(final Graphics g, final Shape a) {
            final Rectangle r = (Rectangle)a;
            g.clipRect(r.x, r.y, r.width, r.height);
            super.paint(g, this.adjustAllocation(a));
        }
        
        @Override
        public int getResizeWeight(final int axis) {
            if (axis == 0) {
                return 1;
            }
            return 0;
        }
        
        @Override
        public Shape modelToView(final int pos, final Shape a, final Position.Bias b) throws BadLocationException {
            return super.modelToView(pos, this.adjustAllocation(a), b);
        }
        
        @Override
        public Shape modelToView(final int p0, final Position.Bias b0, final int p1, final Position.Bias b1, final Shape a) throws BadLocationException {
            return super.modelToView(p0, b0, p1, b1, this.adjustAllocation(a));
        }
        
        @Override
        public int viewToModel(final float fx, final float fy, final Shape a, final Position.Bias[] bias) {
            return super.viewToModel(fx, fy, this.adjustAllocation(a), bias);
        }
        
        @Override
        public void insertUpdate(final DocumentEvent changes, final Shape a, final ViewFactory f) {
            super.insertUpdate(changes, this.adjustAllocation(a), f);
            this.updateVisibilityModel();
        }
        
        @Override
        public void removeUpdate(final DocumentEvent changes, final Shape a, final ViewFactory f) {
            super.removeUpdate(changes, this.adjustAllocation(a), f);
            this.updateVisibilityModel();
        }
    }
}
