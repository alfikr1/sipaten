// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.tablemodel;

import javax.swing.SwingUtilities;
import java.util.ArrayList;
import com.danang.paten.domain.Layanan;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class LayananTableModel extends AbstractTableModel
{
    private List<Layanan> items;
    private final String[] header;
    
    public LayananTableModel() {
        this(new ArrayList<Layanan>());
    }
    
    public LayananTableModel(final List<Layanan> items) {
        this.header = new String[] { "Kode", "Layanan" };
        this.items = items;
    }
    
    public void add(final Layanan layanan) {
        SwingUtilities.invokeLater(() -> this.tambah(layanan));
    }
    
    private void tambah(final Layanan layanan) {
        this.items.add(layanan);
        this.fireTableRowsInserted(this.items.size() - 1, this.items.size() - 1);
    }
    
    @Override
    public String getColumnName(final int col) {
        return this.header[col];
    }
    
    public Layanan getItem(final int pos) {
        return this.items.get(pos);
    }
    
    @Override
    public int getRowCount() {
        return this.items.size();
    }
    
    @Override
    public int getColumnCount() {
        return this.header.length;
    }
    
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return this.items.get(rowIndex).getKode();
            }
            case 1: {
                return this.items.get(rowIndex).getNamaLayanan();
            }
            default: {
                return null;
            }
        }
    }
}
