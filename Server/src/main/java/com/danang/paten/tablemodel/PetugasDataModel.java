// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.tablemodel;

import javax.swing.SwingUtilities;
import java.util.ArrayList;
import com.danang.paten.domain.Petugas;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class PetugasDataModel extends AbstractTableModel
{
    private List<Petugas> items;
    private final String[] header;
    
    public PetugasDataModel() {
        this(new ArrayList<Petugas>());
    }
    
    public PetugasDataModel(final List<Petugas> items) {
        this.header = new String[] { "NIP", "Nama" };
        this.items = items;
    }
    
    public void add(final Petugas p) {
        SwingUtilities.invokeLater(() -> this.tambah(p));
    }
    
    public Petugas getItem(final int row) {
        return this.items.get(row);
    }
    
    private void tambah(final Petugas p) {
        this.items.add(p);
        this.fireTableRowsInserted(this.items.size() - 1, this.items.size() - 1);
    }
    
    @Override
    public String getColumnName(final int col) {
        return this.header[col];
    }
    
    @Override
    public int getRowCount() {
        return this.items.size();
    }
    
    @Override
    public int getColumnCount() {
        return this.header.length;
    }
    
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return this.items.get(rowIndex).getNip();
            }
            case 1: {
                return this.items.get(rowIndex).getNama();
            }
            default: {
                return null;
            }
        }
    }
}
