// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.tablemodel;

import java.util.Iterator;
import java.util.ArrayList;
import com.danang.paten.domain.Berkas;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class SyaratTableModel extends AbstractTableModel
{
    private List<Berkas> items;
    private final String[] header;
    
    public SyaratTableModel() {
        this(new ArrayList<>());
    }
    
    public SyaratTableModel(final List<Berkas> items) {
        this.header = new String[] { "Dokumen", "Optional" };
        (this.items = items).add(new Berkas());
    }
    
    public void hapusBerkas(final int row) {
        final Berkas b = this.items.get(row);
        if (b != null) {
            this.items.remove(b);
        }
        this.fireTableRowsDeleted(row, row);
    }
    
    public Berkas getItem(final int row) {
        return this.items.get(row);
    }
    
    public void cekItem() {
        final Iterator it = this.items.iterator();
        while (it.hasNext()) {
            Berkas b = (Berkas) it.next();
            if (b == null || b.getDokumen().equals("")) {
                it.remove();
            }
        }
        this.items.add(new Berkas());
        this.fireTableDataChanged();
    }
    
    public void addItem(final Berkas berkas) {
        this.items.add(berkas);
        this.fireTableRowsInserted(this.items.size() - 1, this.items.size() - 1);
    }
    
    @Override
    public boolean isCellEditable(final int row, final int col) {
        return true;
    }
    
    @Override
    public Class<?> getColumnClass(final int col) {
        switch (col) {
            case 0: {
                return String.class;
            }
            default: {
                return Boolean.class;
            }
        }
    }
    
    @Override
    public String getColumnName(final int col) {
        return this.header[col];
    }
    
    public List<Berkas> getItems() {
        final Iterator it = this.items.iterator();
        while (it.hasNext()) {
            Berkas b = (Berkas) it.next();
            if (b.getDokumen() == null) {
                it.remove();
            }
        }
        return this.items;
    }
    
    @Override
    public int getRowCount() {
        return this.items.size();
    }
    
    @Override
    public int getColumnCount() {
        return this.header.length;
    }
    
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return this.items.get(rowIndex).getDokumen();
            }
            case 1: {
                return this.items.get(rowIndex).isOptional();
            }
            default: {
                return null;
            }
        }
    }
}
