// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.tablemodel;

import javax.swing.SwingUtilities;
import java.util.ArrayList;
import com.danang.paten.domain.Loket;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class LoketTableModel extends AbstractTableModel
{
    private List<Loket> items;
    private final String[] header;
    
    public LoketTableModel() {
        this(new ArrayList<Loket>());
    }
    
    public LoketTableModel(final List<Loket> items) {
        this.header = new String[] { "Kode", "Keterangan" };
        this.items = items;
    }
    
    @Override
    public String getColumnName(final int cl) {
        return this.header[cl];
    }
    
    public void add(final Loket loket) {
        SwingUtilities.invokeLater(() -> this.tambah(loket));
    }
    
    private void tambah(final Loket loket) {
        this.items.add(loket);
        this.fireTableRowsInserted(this.items.size() - 1, this.items.size() - 1);
    }
    
    public Loket getItem(final int row) {
        return this.items.get(row);
    }
    
    @Override
    public int getRowCount() {
        return this.items.size();
    }
    
    @Override
    public int getColumnCount() {
        return this.header.length;
    }
    
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return this.items.get(rowIndex).getKode();
            }
            case 1: {
                return this.items.get(rowIndex).getDesk();
            }
            default: {
                return null;
            }
        }
    }
}
