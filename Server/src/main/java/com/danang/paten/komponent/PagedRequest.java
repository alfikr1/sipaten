// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.komponent;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.query.MongoQueryMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class PagedRequest
{
    private final Pageable pageable;
    @Autowired
    private MongoOperations operation;
    private MongoQueryMethod method;
    
    public PagedRequest(final Pageable pageable) {
        this.pageable = pageable;
    }
    
    public Object execute(final Query query) {
        final int overallLimit = query.getLimit();
        return null;
    }
}
