// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.route;

import java.lang.reflect.Type;
import java.util.Map;
import com.danang.paten.controller.PenetapanController;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.danang.paten.domain.BerkasPeriksa;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.danang.paten.form.IMB;
import com.danang.paten.Konfig;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.repository.PendudukRepository;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class IMBRoute implements Route {

    PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
    PenetapanController pen = Konfig.getInstance().getContext().getBean(PenetapanController.class);
    private PendudukRepository pDao = Konfig.getInstance().getContext().getBean(PendudukRepository.class);

    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final String command = request.queryParams("command");
        final Map<String, Object> map = new HashMap<>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        final String s = command;
        switch (s) {
            case "periksa": {

                /*IMB imb = new IMB();
                JsonObject obj
                        = Konfig.getInstance().getGson().fromJson(request.queryParams("imb"), JsonObject.class);
                imb.setAtap(obj.get("atap").getAsString());
                imb.setAtasNamaTanah(obj.get("atasNamaTanah").getAsString());
                imb.setDinding(obj.get("dinding").getAsString());
                imb.setGsp(obj.get("gsp").getAsDouble());
                imb.setGunaBangunan(obj.get("atasNamaTanah").getAsString());
                DataPenduduk ijin = pDao.findOne(obj.get("ijin").getAsString());
                DataPenduduk pemohon=pDao.findOne(obj.get("pemohon").getAsString());
                imb.setIjin(ijin);
                imb.setJalan(obj.get("jalan").getAsString());
                imb.setJarakJalan(obj.get("jarakJalan").getAsDouble());
                imb.setKab("Semarang");
                imb.setKecamatan("Bergas");
                imb.setKelas(obj.get("kelas").getAsString());
                imb.setKelurahan(obj.get("kelurahan").getAsString());
                imb.setKerangka(obj.get("kerangka").getAsString());
                imb.setKerangkaAtap(obj.get("kerangkaAtap").getAsString());
                imb.setLantai(obj.get("lantai").getAsString());
                imb.setLingk(obj.get("lingk").getAsString());
                imb.setLuasBangunan(obj.get("luasBangunan").getAsDouble());
                imb.setLuasTanah(obj.get("luasTanah").getAsDouble());
                imb.setNomorHM(obj.get("nomorHM").getAsString());
                imb.setPemohon(pemohon);
                imb.setNomorPersil(obj.get("nomorPersil").getAsString());   */
                IMB imb = Konfig.getInstance().getGson().fromJson(request.queryParams("imb"), IMB.class);
                final boolean lanjut = request.queryParams("lanjut").equalsIgnoreCase("TRUE");
                final String nomor = request.queryParams("nomor");
                final String data = request.queryParams("data");
                final String catatan = request.queryParams("catatan");
                final String token = request.headers("keypair");
                final Type tps = new TypeToken<List<BerkasPeriksa>>() {
                }.getType();
                final List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                return pc.simpanIMB(new Date(), catatan, lanjut, nomor, token, items, imb);
            }
            case "tetapkan": {
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

                final Date mulai = sdf.parse(request.queryParams("mulai"));
                final Date selesai = sdf.parse(request.queryParams("selesai"));
                final String token2 = request.headers("keypair");
                final String nomor2 = request.queryParams("nomor");
                return pen.simpanPenetapanIMB(selesai, mulai, nomor2, token2);
            }
            case "periksakembali": {
                final PenetapanController pen2 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                final String nomor3 = request.queryParams("nomor");
                return pen2.kembaliPeriksa(nomor3);
            }
            default: {
                return null;
            }
        }
    }
}
