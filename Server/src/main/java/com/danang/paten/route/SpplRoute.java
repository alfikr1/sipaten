/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.route;

import com.danang.paten.Konfig;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.controller.PenetapanController;
import com.danang.paten.domain.BerkasPeriksa;
import com.danang.paten.form.SPPL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 *
 * @author danang
 */
public class SpplRoute implements Route {

    private final PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
    private final PenetapanController pen = Konfig.getInstance().getContext().getBean(PenetapanController.class);
    private final Logger log =Logger.getLogger(SpplRoute.class);
    private final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy").create();
    @Override
    public Object handle(Request rqst, Response rspns) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        final String command = rqst.queryParams("command");
        final Map<String, Object> map = new HashMap<>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        switch (command) {
            case "periksa":
                
                SPPL sppl = gson.fromJson(rqst.queryParams("sppl"), SPPL.class);                
                String token = rqst.headers("keypair");
                String nomor = rqst.queryParams("nomor");
                String data = rqst.queryParams("data");
                boolean lanjut = rqst.queryParams("lanjut").equalsIgnoreCase("TRUE");
                String catatan = rqst.queryParams("catatan");
                Type tps = new TypeToken<List<BerkasPeriksa>>() {
                }.getType();
                List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                return pc.simpanSPPL(token, nomor, sppl, items, lanjut, catatan);
            case "tetapkan":
                String tkn = rqst.headers("keypair");
                String nomorBerkas = rqst.queryParams("nomor");
                Map<String,String> mp=rqst.params();
                log.addAppender(new ConsoleAppender());
                log.debug(gson.toJson(mp));
                if(nomorBerkas==null){
                    throw new IllegalStateException("param nomor tidak ditemukan");
                }
                return pen.simpanPenetapanSPPL(nomorBerkas, tkn);
            case "periksakembali":
                String r = rqst.queryParams("nomor");
                return pen.kembaliPeriksa(r);
            default:
                return null;
        }
    }

}
