// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.route;

import java.util.Date;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import com.danang.paten.Konfig;
import com.danang.paten.controller.SuratMasukController;
import com.danang.paten.controller.SuratKeluarController;
import com.danang.paten.controller.LaporanController;
import spark.Route;

public class ReportRoute implements Route
{
    private final LaporanController laporanController;
    private final SuratKeluarController skc;
    private final SuratMasukController smc;
    
    public ReportRoute() {
        this.laporanController = Konfig.getInstance().getContext().getBean(LaporanController.class);
        this.skc = Konfig.getInstance().getContext().getBean(SuratKeluarController.class);
        this.smc = Konfig.getInstance().getContext().getBean(SuratMasukController.class);
    }
    
    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        Map<String, Object> map = new HashMap<>();
        final String command = request.queryParams("command");
        if (command.equalsIgnoreCase("layanan")) {
            final String kode = request.queryParams("kode");
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            final Date awal = sdf.parse(request.queryParams("awal"));
            final Date akhir = sdf.parse(request.queryParams("akhir"));
            map = this.laporanController.getLaporanLayanan(kode, awal, akhir);
            System.out.println(Konfig.getInstance().getGson().toJson(map));
            return map;
        }
        if(command.equalsIgnoreCase("ml")){
            final String kode = request.queryParams("kode");
            
        }
        if (command.equalsIgnoreCase("surat masuk")) {
            SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");
            final Date awal2 = sdf2.parse(request.queryParams("awal"));
            final Date akhir2 = sdf2.parse(request.queryParams("akhir"));
            map = smc.report(awal2, akhir2);
            return map;
        }
        if (command.equalsIgnoreCase("surat keluar")) {
            final SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");
            final Date awal2 = sdf2.parse(request.queryParams("awal"));
            final Date akhir2 = sdf2.parse(request.queryParams("akhir"));
            map = this.skc.report(awal2, akhir2);
            return map;
        }
        return map;
    }
}
