// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.route;

import java.lang.reflect.Type;
import java.util.Map;
import com.danang.paten.controller.PenetapanController;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.danang.paten.domain.BerkasPeriksa;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.danang.paten.Konfig;
import com.danang.paten.controller.PemeriksaanController;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class DispensasiRoute implements Route
{
    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final String command = request.queryParams("command");
        final Map<String, Object> map = new HashMap<String, Object>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        final String s = command;
        switch (s) {
            case "periksa": {
                final PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
                final boolean lanjut = request.queryParams("lanjut").equalsIgnoreCase("TRUE");
                final String nomor = request.queryParams("nomor");
                final String data = request.queryParams("data");
                final String catatan = request.queryParams("catatan");
                final String token = request.headers("keypair");
                final Type tps = new TypeToken<List<BerkasPeriksa>>() {}.getType();
                final List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                final String pria = request.queryParams("pria");
                final String wanita = request.queryParams("wanita");
                final String tempat = request.queryParams("tempat");
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy hh:mm:ss");
                final Date tanggal = sdf.parse(request.queryParams("tanggal"));
                System.out.println(request.queryParams("tanggal"));
                return pc.simpanDispensasiMenikah(new Date(), catatan, lanjut, nomor, token, items, pria, wanita, tanggal, tempat);
            }
            case "tetapkan": {
                final PenetapanController pen = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                final String token2 = request.headers("keypair");
                final String nomor = request.queryParams("nomor");
                return pen.simpanPenetapanDispensasiMenikah(new Date(), nomor, token2);
            }
            case "periksakembali": {
                final PenetapanController pen = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                final String nomor2 = request.queryParams("nomor");
                return pen.kembaliPeriksa(nomor2);
            }
            default: {
                return null;
            }
        }
    }
}
