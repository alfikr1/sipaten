// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.route;

import java.lang.reflect.Type;
import java.util.Map;
import com.danang.paten.controller.PenetapanController;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.Konfig;
import com.danang.paten.domain.BerkasPeriksa;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class HORoute implements Route
{
    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final String command = request.queryParams("command");
        final Map<String, Object> map = new HashMap<>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        final String s = command;
        switch (s) {
            case "periksa": {
                final String token = request.queryParams("petugas");
                final Boolean lanjut = request.queryParams("lanjut").equalsIgnoreCase("TRUE");
                final String nomor = request.queryParams("nomor");
                final String data = request.queryParams("data");
                final Type tps = new TypeToken<List<BerkasPeriksa>>() {}.getType();
                final List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                final PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
                final String catatan = request.queryParams("catatan");
                final String namaPerusahaan = request.queryParams("nama perusahaan");
                final String alamatPerusahaan = request.queryParams("alamat perusahaan");
                final String luas = request.queryParams("luas");
                final String bangunan = request.queryParams("bangunan");
                final String jenisUsaha = request.queryParams("jenis usaha");
                final String investasi = request.queryParams("investasi");
                final String index = request.queryParams("index");
                final String retribusi = request.queryParams("retribusi");
                return pc.simpanHO(new Date(), catatan, lanjut, nomor, token, items, index, namaPerusahaan, alamatPerusahaan, jenisUsaha, Double.parseDouble(retribusi), Double.parseDouble(luas), Double.parseDouble(bangunan), Double.parseDouble(investasi));
            }
            case "tetapkan": {
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                final PenetapanController pen = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                final Date mulai = sdf.parse(request.queryParams("mulai"));
                final Date selesai = sdf.parse(request.queryParams("selesai"));
                final String token2 = request.headers("keypair");
                final String nomor2 = request.queryParams("nomor");
                return pen.simpanPenetapanHO(new Date(), mulai, selesai, nomor2, token2);
            }
            case "periksakembali": {
                final PenetapanController pen2 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                final String nomor3 = request.queryParams("nomor");
                return pen2.kembaliPeriksa(nomor3);
            }
            default: {
                return map;
            }
        }
    }
}
