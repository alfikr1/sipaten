// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.route;

import java.lang.reflect.Type;
import java.util.Map;
import com.danang.paten.controller.PenetapanController;
import java.util.Date;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.domain.BerkasPeriksa;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.danang.paten.Konfig;
import com.danang.paten.form.RekomendasiPenelitian;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class PenelitianRoute implements Route
{
    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final String command = request.queryParams("command");
        final Map<String, Object> map = new HashMap<String, Object>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        final String s = command;
        switch (s) {
            case "periksa": {
                final String token = request.headers("keypair");
                final boolean lanjut = request.queryParams("lanjut").equalsIgnoreCase("TRUE");
                final String nomor = request.queryParams("nomor");
                final String data = request.queryParams("data");
                final String catatan = request.queryParams("catatan");
                final RekomendasiPenelitian rp = Konfig.getInstance().getGson().fromJson(request.queryParams("rekomendasi"), RekomendasiPenelitian.class);
                final Type tps = new TypeToken<List<BerkasPeriksa>>() {}.getType();
                final List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                final PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
                return pc.simpanPenelitian(new Date(), catatan, lanjut, nomor, token, items, rp);
            }
            case "penetapan": {
                final String token = request.headers("keypair");
                final String nomor2 = request.queryParams("nomor");
                final PenetapanController pc2 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                return pc2.simpanPenetapanPenelitian(new Date(), nomor2, token);
            }
            case "kembaliperiksa": {
                final String nomor3 = request.queryParams("nomor");
                final PenetapanController pc3 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                return pc3.kembaliPeriksa(nomor3);
            }
            default: {
                return null;
            }
        }
    }
}
