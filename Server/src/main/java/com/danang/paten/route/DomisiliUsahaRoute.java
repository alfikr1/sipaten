// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.route;

import java.lang.reflect.Type;
import java.util.Map;
import java.text.SimpleDateFormat;
import com.danang.paten.controller.PenetapanController;
import java.util.Date;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.domain.BerkasPeriksa;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.danang.paten.Konfig;
import com.danang.paten.form.DomisiliUsaha;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class DomisiliUsahaRoute implements Route
{
    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final String command = request.queryParams("command");
        final Map<String, Object> map = new HashMap<String, Object>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        final String s = command;
        switch (s) {
            case "periksa": {
                final String token = request.headers("keypair");
                final boolean lanjut = request.queryParams("lanjut").equalsIgnoreCase("TRUE");
                final String nomor = request.queryParams("nomor");
                final String data = request.queryParams("data");
                final String catatan = request.queryParams("catatan");
                final String dus = request.queryParams("domisiliUsaha");
                final DomisiliUsaha du = Konfig.getInstance().getGson().fromJson(dus, DomisiliUsaha.class);
                final Type tps = new TypeToken<List<BerkasPeriksa>>() {}.getType();
                final List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                final PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
                return pc.simpanDomisiliUsaha(new Date(), catatan, lanjut, nomor, token, items, du);
            }
            case "penetapan": {
                final String token = request.headers("keypair");
                final String nomor2 = request.queryParams("nomor");
                final PenetapanController pc2 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                final Date berlaku = sdf.parse(request.queryParams("mulai"));
                final Date akhir = sdf.parse(request.queryParams("akhir"));
                return pc2.simpanPenetapanDomisiliUsaha(new Date(), berlaku, akhir, token, nomor2);
            }
            case "kembaliperiksa": {
                final String nomor3 = request.queryParams("nomor");
                final PenetapanController pc3 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                return pc3.kembaliPeriksa(nomor3);
            }
            default: {
                map.put("error", "data tidak lengkap");
                return map;
            }
        }
    }
}
