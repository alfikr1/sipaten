// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.route;

import java.io.File;
import java.nio.file.Path;
import javax.servlet.http.Part;
import java.util.Map;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.servlet.MultipartConfigElement;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class FilePostRoute implements Route
{
    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final Map<String, Object> map = new HashMap<>();
        File folder = new File("img");
        if(!folder.exists()){
            folder.mkdir();
        }
        if (request.raw().getAttribute("org.eclipse.jetty.multipartConfig") == null) {
            MultipartConfigElement mce = new MultipartConfigElement("img");
            //request.raw().setAttribute("org.eclipse.multipartConfig", mce);
            request.raw().setAttribute("org.eclipse.jetty.multipartConfig", mce);
        }
        Part part = request.raw().getPart("file");
        String filename = part.getSubmittedFileName();
        File f = new File("img/"+filename);
        if(f.exists()){
            f.delete();
        }
        Path filePath = Paths.get("img/", filename);        
        Files.copy(part.getInputStream(), filePath);
        map.put("url", filePath.toFile().getAbsolutePath());
        return map;
    }
}
