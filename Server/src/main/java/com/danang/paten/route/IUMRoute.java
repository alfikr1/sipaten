// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.route;

import java.lang.reflect.Type;
import java.util.Map;
import com.danang.paten.controller.PenetapanController;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.danang.paten.controller.PemeriksaanController;
import com.danang.paten.domain.BerkasPeriksa;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.danang.paten.Konfig;
import com.danang.paten.form.IUM;
import java.util.HashMap;
import spark.Response;
import spark.Request;
import spark.Route;

public class IUMRoute implements Route {

    @Override
    public Object handle(final Request request, final Response response) throws Exception {
        final String command = request.queryParams("command");
        final Map<String, Object> map = new HashMap<String, Object>();
        if (command == null) {
            map.put("error", "data tidak valid");
            return map;
        }
        final String s = command;
        switch (s) {
            case "periksa": {
                String token = request.headers("keypair");
                boolean lanjut = request.queryParams("lanjut").equalsIgnoreCase("TRUE");
                String nomor = request.queryParams("nomor");
                String data = request.queryParams("data");
                String catatan = request.queryParams("catatan");
                IUM ium = Konfig.getInstance().getGson().fromJson(request.queryParams("ium"), IUM.class);
                Type tps = new TypeToken<List<BerkasPeriksa>>() {
                }.getType();
                List<BerkasPeriksa> items = Konfig.getInstance().getGson().fromJson(data, tps);
                PemeriksaanController pc = Konfig.getInstance().getContext().getBean(PemeriksaanController.class);
                return pc.simpanIUM(new Date(), catatan, lanjut, nomor, token, items, ium);
            }
            case "penetapan": {
                final String token = request.headers("keypair");
                final String nomor2 = request.queryParams("nomor");
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                final Date berlaku = sdf.parse(request.queryParams("mulai"));
                final Date akhir = sdf.parse(request.queryParams("akhir"));
                final PenetapanController pc2 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                return pc2.simpanPenetapanIUM(new Date(), token, nomor2, berlaku, akhir);
            }
            case "kembaliperiksa": {
                final String nomor3 = request.queryParams("nomor");
                final PenetapanController pc3 = Konfig.getInstance().getContext().getBean(PenetapanController.class);
                return pc3.kembaliPeriksa(nomor3);
            }
            default: {
                return null;
            }
        }
    }
}
