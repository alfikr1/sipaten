// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.view;

import java.util.Iterator;
import javax.swing.table.AbstractTableModel;
import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.CellEditorListener;
import javax.swing.DefaultCellEditor;
import com.danang.paten.domain.Roles;
import java.util.ArrayList;
import org.mindrot.jbcrypt.BCrypt;
import java.awt.Component;
import javax.swing.JOptionPane;
import com.danang.paten.Konfig;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import com.danang.paten.componen.UserDialogListener;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.PetugasRepository;
import com.danang.paten.domain.Petugas;
import com.google.gson.Gson;
import javax.swing.JPanel;

public class CrudUser extends JPanel {

    private Petugas petugas;
    private final PetugasRepository petugasDao;
    private RoleModel model;
    private final LayananRepository layananDao;
    private boolean isNew;
    private UserDialogListener listener;
    private JButton jButton1;
    private JButton jButton2;
    private JCheckBox jCheckBox1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JTable jTable1;
    private JTextField txtNama;
    private JTextField txtNip;
    private JPasswordField txtPassword;
    private JPasswordField txtRPassword;
    private JTextField txtUsername;

    public UserDialogListener getListener() {
        return this.listener;
    }

    public void setListener(final UserDialogListener listener) {
        this.listener = listener;
    }

    public CrudUser() {
        this(new Petugas());
    }

    public CrudUser(final Petugas petugas) {
        this.initComponents();
        this.petugas = petugas;
        this.petugasDao = Konfig.getInstance().getContext().getBean(PetugasRepository.class);
        this.layananDao = Konfig.getInstance().getContext().getBean(LayananRepository.class);
        this.isNew = (petugas.getKode() == null);
        this.init();
    }

    public JButton getBtnSimpan() {
        return this.jButton1;
    }

    private void init() {
        this.jButton2.addActionListener(x -> bersih());
        this.jButton1.addActionListener(x -> {
            if (this.validasi()) {
                Petugas y = getPengguna(petugas);
                if (this.isNew) {
                    if (petugas != null) {
                        petugas = this.petugasDao.findOne(y.getKode());
                        if (petugas == null) {
                            petugas = this.petugasDao.findByUser(y.getUser());
                            if (petugas == null) {
                                this.petugasDao.save(y);
                                this.bersih();
                                this.petugas = new Petugas();
                                if (listener != null) {
                                    listener.reload();
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Username sudah digunakan", "Insert data gagal", 0);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Kode nip sudah digunakan", "Insert data gagal", 0);
                        }
                    }
                } else {
                    this.petugasDao.save(y);
                    this.bersih();
                    this.petugas = new Petugas();
                }
                if (this.listener != null) {
                    this.listener.reload();
                }
            } else {
                JOptionPane.showMessageDialog(this, "Input data salah", "Insert data gagal", 0);
            }
        });
        this.bersih();
        if (!this.isNew) {
            this.txtNip.setText((this.petugas.getKode() != null) ? this.petugas.getKode() : "");
            this.txtNama.setText((this.petugas.getNama() != null) ? this.petugas.getNama() : "");
            this.txtUsername.setText((this.petugas.getUser() != null) ? this.petugas.getUser() : "");
            this.txtPassword.setText("");
            this.jCheckBox1.setSelected(this.petugas.isAdministrator());
            this.model.setItems(this.petugas.getRoles());
        }
    }

    public Petugas getPengguna(final Petugas p) {
        if (this.validasi()) {
            p.setNip(this.txtNip.getText());
            p.setNama(this.txtNama.getText());
            p.setUser(this.txtUsername.getText());
            if (this.isNew) {
                String salt = BCrypt.gensalt();
                p.setSalt(salt);
                String psw = BCrypt.hashpw(new String(this.txtPassword.getPassword()), salt);
                p.setPsw(psw);
            } else if (this.txtPassword.getPassword().length > 0) {
                final String salt = BCrypt.gensalt();
                p.setSalt(salt);
                final String psw = BCrypt.hashpw(new String(this.txtPassword.getPassword()), salt);
                p.setPsw(psw);
            }
            p.setAdministrator(this.jCheckBox1.isSelected());
            p.setKode(this.txtNip.getText().toLowerCase());
            p.setRoles(this.model.getItems());
            return p;
        }
        return null;
    }

    private void bersih() {
        for (final Component c : this.getComponents()) {
            if (c instanceof JTextField) {
                ((JTextField) c).setText("");
            } else if (c instanceof JPasswordField) {
                ((JPasswordField) c).setText("");
            }
        }
        final List<Roles> items = new ArrayList<>();
        this.layananDao.findAll().forEach(x -> {
            Roles role = new Roles();
            role.setKode(x.getKode());
            role.setLayanan(x);
            role.setDeskripsi(x.getNamaLayanan());
            role.setBaca(false);
            role.setHapus(false);
            role.setTulis(false);
            items.add(role);
        });
        this.model = new RoleModel(items);
        this.jTable1.setModel(this.model);
        final DefaultCellEditor tulisEditor = new DefaultCellEditor(new JCheckBox());
        final DefaultCellEditor bacaEditor = new DefaultCellEditor(new JCheckBox());
        final DefaultCellEditor hapusEditor = new DefaultCellEditor(new JCheckBox());
        tulisEditor.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(final ChangeEvent e) {
                final int row = CrudUser.this.jTable1.getSelectedRow();
                if (row == -1) {
                    return;
                }
                final Roles r = CrudUser.this.model.getItem(row);
                r.setTulis(((JCheckBox) tulisEditor.getComponent()).isSelected());
                CrudUser.this.model.fireTableRowsUpdated(row, row);
            }

            @Override
            public void editingCanceled(final ChangeEvent e) {
            }
        });
        bacaEditor.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(final ChangeEvent e) {
                final int row = CrudUser.this.jTable1.getSelectedRow();
                if (row == -1) {
                    return;
                }
                final Roles r = CrudUser.this.model.getItem(row);
                r.setBaca(((JCheckBox) bacaEditor.getComponent()).isSelected());
                CrudUser.this.model.fireTableRowsUpdated(row, row);
            }

            @Override
            public void editingCanceled(final ChangeEvent e) {
            }
        });
        hapusEditor.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(final ChangeEvent e) {
                final int row = CrudUser.this.jTable1.getSelectedRow();
                if (row == -1) {
                    return;
                }
                final Roles r = CrudUser.this.model.getItem(row);
                r.setHapus(((JCheckBox) hapusEditor.getComponent()).isSelected());
                CrudUser.this.model.fireTableRowsUpdated(row, row);
            }

            @Override
            public void editingCanceled(final ChangeEvent e) {
            }
        });
        this.jTable1.getColumnModel().getColumn(1).setCellEditor(bacaEditor);
        this.jTable1.getColumnModel().getColumn(2).setCellEditor(tulisEditor);
        this.jTable1.getColumnModel().getColumn(3).setCellEditor(hapusEditor);
    }

    private boolean validasi() {
        if (this.txtNip.getText().equals("") || this.txtNama.getText().equals("") || this.txtUsername.getText().equals("")) {
            return false;
        }
        if (this.isNew) {
            final String ps = new String(this.txtPassword.getPassword());
            final String ps2 = new String(this.txtRPassword.getPassword());
            return !ps.equals("") && !ps2.equals("") && ps.equals(ps2);
        }
        return true;
    }

    public interface UserListener {

        public void simpan();

        public void batal();
    }

    public static void main(final String[] args) {
        Konfig.getInstance();
        final JFrame f = new JFrame();
        f.setSize(600, 400);
        f.setDefaultCloseOperation(3);
        f.getContentPane().setLayout(new BorderLayout());
        f.getContentPane().add(new CrudUser());
        f.setLocationRelativeTo(null);
        SwingUtilities.invokeLater(() -> f.setVisible(true));
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.txtNip = new JTextField();
        this.txtNama = new JTextField();
        this.txtUsername = new JTextField();
        this.txtPassword = new JPasswordField();
        this.txtRPassword = new JPasswordField();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.jCheckBox1 = new JCheckBox();
        this.jLabel1.setText("NIP");
        this.jLabel2.setText("Nama");
        this.jLabel3.setText("Username");
        this.jLabel4.setText("Password");
        this.jLabel5.setText("Retype Password");
        this.txtNip.setText("jTextField1");
        this.txtNama.setText("jTextField2");
        this.txtUsername.setText("jTextField3");
        this.txtPassword.setText("jPasswordField1");
        this.txtRPassword.setText("jPasswordField2");
        this.jButton1.setText("Simpan");
        this.jButton2.setText("Reset");
        this.jPanel1.setBorder(BorderFactory.createTitledBorder("Role"));
        this.jPanel1.setLayout(new BorderLayout());
        this.jTable1.setModel(new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this.jScrollPane1.setViewportView(this.jTable1);
        this.jPanel1.add(this.jScrollPane1, "Center");
        this.jCheckBox1.setText("Administrator");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, 536, 32767).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.jButton1, -1, -1, 32767).addComponent(this.jLabel1, GroupLayout.Alignment.LEADING).addComponent(this.jLabel2, GroupLayout.Alignment.LEADING).addComponent(this.jLabel3, GroupLayout.Alignment.LEADING).addComponent(this.jLabel4, GroupLayout.Alignment.LEADING).addComponent(this.jLabel5, -1, -1, 32767)).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton2, -2, 98, -2)).addGroup(layout.createSequentialGroup().addGap(57, 57, 57).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.txtNip).addComponent(this.txtNama).addComponent(this.txtUsername).addComponent(this.txtPassword, -1, 205, 32767).addComponent(this.txtRPassword)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jCheckBox1))).addGap(0, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.txtNip, -2, -1, -2).addComponent(this.jCheckBox1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.txtNama, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.txtUsername, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this.txtPassword, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this.txtRPassword, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -1, 162, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jButton1).addComponent(this.jButton2)).addContainerGap()));
    }

    class RoleModel extends AbstractTableModel {

        private List<Roles> items;
        private final String[] header;

        public RoleModel(final CrudUser this$0) {
            this(new ArrayList<Roles>());
        }

        public void setItems(final List<Roles> item) {
            item.stream().forEach(x -> this.items.forEach(d -> {
                try {
                    if (d.getLayanan().getKode().equals(x.getLayanan().getKode())) {
                        d.setBaca(x.isBaca());
                        d.setHapus(x.isHapus());
                        d.setTulis(x.isTulis());
                    }
                } catch (Exception e) {
                    Gson gson =Konfig.getInstance().getGson();
                    System.out.println(gson.toJson(d));
                    System.out.println(gson.toJson(x));
                }
            }));
        }

        public List<Roles> getItems() {
            final Iterator itr = this.items.iterator();
            while (itr.hasNext()) {
                final Roles r = (Roles) itr.next();
                if (!r.isBaca() || !r.isHapus() || !r.isTulis()) {
                    itr.remove();
                }
            }
            return this.items;
        }

        @Override
        public boolean isCellEditable(final int row, final int col) {
            switch (col) {
                case 0: {
                    return false;
                }
                default: {
                    return true;
                }
            }
        }

        public RoleModel(final List<Roles> items) {
            this.header = new String[]{"Layanan", "Baca", "Tulis", "Hapus"};
            this.items = items;
        }

        @Override
        public Class<?> getColumnClass(final int col) {
            switch (col) {
                case 0: {
                    return String.class;
                }
                default: {
                    return Boolean.class;
                }
            }
        }

        public void setEnable(final List<Roles> it) {
            this.items.stream().forEach(x -> {
            });
        }

        @Override
        public String getColumnName(final int col) {
            return this.header[col];
        }

        @Override
        public int getRowCount() {
            return this.items.size();
        }

        public Roles getItem(final int row) {
            return this.items.get(row);
        }

        @Override
        public int getColumnCount() {
            return this.header.length;
        }

        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
            switch (columnIndex) {
                case 0: {
                    return this.items.get(rowIndex).getDeskripsi();
                }
                case 1: {
                    return this.items.get(rowIndex).isBaca();
                }
                case 2: {
                    return this.items.get(rowIndex).isTulis();
                }
                case 3: {
                    return this.items.get(rowIndex).isHapus();
                }
                default: {
                    return null;
                }
            }
        }
    }
}
