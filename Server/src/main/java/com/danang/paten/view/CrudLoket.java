// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import java.util.Iterator;
import javax.swing.table.AbstractTableModel;
import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.CellEditorListener;
import javax.swing.DefaultCellEditor;
import com.danang.paten.util.CleanUpText;
import java.util.List;
import com.danang.paten.domain.Layanan;
import java.util.ArrayList;
import com.danang.paten.Konfig;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.LoketRepository;
import com.danang.paten.domain.Loket;
import javax.swing.JPanel;

public class CrudLoket extends JPanel
{
    private Loket loket;
    private final LoketRepository loketDao;
    private final LayananRepository layananDao;
    private JButton btnRefresh;
    private JButton btnReset;
    private JButton btnSimpan;
    private JCheckBox jCheckBox1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JScrollPane jScrollPane1;
    private JTable jTable1;
    private JTextField jTextField1;
    private JTextField jTextField2;
    
    public CrudLoket() {
        this(new Loket());
    }
    
    public CrudLoket(final Loket loket) {
        this.initComponents();
        this.loket = loket;
        this.loketDao = Konfig.getInstance().getContext().getBean(LoketRepository.class);
        this.layananDao = Konfig.getInstance().getContext().getBean(LayananRepository.class);
        this.bersih();
        this.init(loket.getKode() == null);
    }
    
    public Loket getLoket() {
        this.loket.setKode(this.jTextField1.getText());
        this.loket.setDesk(this.jTextField2.getText());
        this.loket.setBuka(this.jCheckBox1.isSelected());
        final List<Layanan> items = new ArrayList<>();
        final List<LayananHolder> xx = ((LayananModel)this.jTable1.getModel()).getItems();        
        final List<Layanan> list;
        xx.stream().forEach(xi -> {
            Layanan xxx = xi.getLayanan();
            items.add(xxx);
            return;
        });
        this.loket.setLayanans(items);
        return this.loket;
    }
    
    private void init(final boolean isNew) {
        this.btnReset.addActionListener(x -> {
            this.bersih();
            this.loket = new Loket();
            return;
        });
        if (!isNew) {
            this.jTextField1.setText(this.loket.getKode());
            this.jTextField2.setText(this.loket.getDesk());
            this.jCheckBox1.setSelected(this.loket.isBuka());
            ((LayananModel)this.jTable1.getModel()).setEnabled(this.loket.getLayanans());
        }
    }
    
    public JButton getBtnSimpan() {
        return this.btnSimpan;
    }
    
    private List<LayananHolder> getAllLayanan() {
        final List<LayananHolder> items = new ArrayList<LayananHolder>();
        this.layananDao.findAll().forEach(x -> items.add(new LayananHolder(x, false)));
        return items;
    }
    
    public void bersih() {
        CleanUpText.clean(this);
        this.jTable1.setModel(new LayananModel(this.getAllLayanan()));
        this.initTable();
    }
    
    private void initTable() {
        final DefaultCellEditor ly = new DefaultCellEditor(new JCheckBox());
        ly.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(final ChangeEvent e) {
                final int row = CrudLoket.this.jTable1.getSelectedRow();
                if (row == -1) {
                    return;
                }
                final LayananHolder it = ((LayananModel)CrudLoket.this.jTable1.getModel()).getItem(row);
                it.setEnable(((JCheckBox)ly.getComponent()).isSelected());
            }
            
            @Override
            public void editingCanceled(final ChangeEvent e) {
            }
        });
        this.jTable1.getColumnModel().getColumn(1).setCellEditor(ly);
    }
    
    public static void main(final String[] args) {
        Konfig.getInstance();
        final JFrame f = new JFrame();
        f.setSize(600, 400);
        f.setDefaultCloseOperation(3);
        f.getContentPane().setLayout(new BorderLayout());
        final Loket l = Konfig.getInstance().getContext().getBean(LoketRepository.class).findOne("001");
        f.getContentPane().add(new CrudLoket(l));
        f.setLocationRelativeTo(null);
        SwingUtilities.invokeLater(() -> f.setVisible(true));
    }
    
    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.btnSimpan = new JButton();
        this.btnReset = new JButton();
        this.btnRefresh = new JButton();
        this.jTextField1 = new JTextField();
        this.jTextField2 = new JTextField();
        this.jCheckBox1 = new JCheckBox();
        this.jLabel1.setText("Kode");
        this.jLabel2.setText("Deskripsi");
        this.jTable1.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
        this.jScrollPane1.setViewportView(this.jTable1);
        this.btnSimpan.setText("Simpan");
        this.btnReset.setText("Reset");
        this.btnRefresh.setText("Refresh Data Table");
        this.jTextField1.setText("jTextField1");
        this.jTextField2.setText("jTextField2");
        this.jCheckBox1.setText("Selalu Buka");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 511, 32767).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.btnSimpan, -2, 94, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnReset, -2, 94, -2)).addComponent(this.btnRefresh, -2, 143, -2).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1, -2, 73, -2).addComponent(this.jLabel2)).addGap(36, 36, 36).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jTextField1, -2, 141, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jCheckBox1)).addComponent(this.jTextField2, -2, 311, -2)))).addGap(0, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.jTextField1, -2, -1, -2).addComponent(this.jCheckBox1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.jTextField2, -2, -1, -2)).addGap(11, 11, 11).addComponent(this.btnRefresh).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -2, 287, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnSimpan).addComponent(this.btnReset)).addContainerGap(27, 32767)));
    }
    
    class LayananHolder
    {
        private Layanan layanan;
        private boolean enable;
        
        public LayananHolder() {
        }
        
        public LayananHolder(final Layanan layanan, final boolean enable) {
            this.layanan = layanan;
            this.enable = enable;
        }
        
        public Layanan getLayanan() {
            return this.layanan;
        }
        
        public void setLayanan(final Layanan layanan) {
            this.layanan = layanan;
        }
        
        public boolean isEnable() {
            return this.enable;
        }
        
        public void setEnable(final boolean enable) {
            this.enable = enable;
        }
    }
    
    class LayananModel extends AbstractTableModel
    {
        private List<LayananHolder> items;
        private final String[] header;
        
        public LayananModel(final CrudLoket this$0) {
            this(new ArrayList<>());
        }
        
        public LayananModel(final List<LayananHolder> items) {
            this.header = new String[] { "Layanan", "Enable" };
            this.items = items;
        }
        
        public void setEnabled(final List<Layanan> l) {
            this.items.stream().forEach(lh -> l.stream().forEach(x -> {
                if (lh.getLayanan().getKode().equals(x.getKode())) {
                    lh.setEnable(true);
                }
            }));
        }
        
        @Override
        public boolean isCellEditable(final int row, final int col) {
            return col == 1;
        }
        
        public List<LayananHolder> getItems() {
            final Iterator itr = this.items.iterator();
            while (itr.hasNext()) {
                final LayananHolder lh = (LayananHolder) itr.next();
                if (!lh.isEnable()) {
                    itr.remove();
                }
            }
            return this.items;
        }
        
        @Override
        public String getColumnName(final int col) {
            return this.header[col];
        }
        
        @Override
        public Class<?> getColumnClass(final int col) {
            switch (col) {
                case 0: {
                    return String.class;
                }
                default: {
                    return Boolean.class;
                }
            }
        }
        
        public LayananHolder getItem(final int row) {
            return this.items.get(row);
        }
        
        @Override
        public int getRowCount() {
            return this.items.size();
        }
        
        @Override
        public int getColumnCount() {
            return this.header.length;
        }
        
        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
            switch (columnIndex) {
                case 0: {
                    return this.items.get(rowIndex).getLayanan().getNamaLayanan();
                }
                case 1: {
                    return this.items.get(rowIndex).isEnable();
                }
                default: {
                    return null;
                }
            }
        }
    }
}
