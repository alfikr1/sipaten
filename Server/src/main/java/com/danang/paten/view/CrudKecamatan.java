// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import com.danang.paten.util.CleanUpText;
import java.util.List;
import com.danang.paten.Konfig;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import com.danang.paten.repository.KecamatanRepository;
import com.danang.paten.domain.Kecamatan;
import javax.swing.JPanel;

public class CrudKecamatan extends JPanel
{
    private Kecamatan kecamatan;
    private final KecamatanRepository kecamatanDao;
    private JButton jButton1;
    private JButton jButton2;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JTextField jTextField4;
    private JTextField jTextField5;
    private JTextField jTextField6;
    private JTextField jTextField7;
    private JTextField txtKodeKabupaten;
    private JTextField txtKodeKecamatan;
    private JTextField txtNamaKabupaten;
    private JTextField txtNamaKecamatan;
    private JTextField txtSerial;
    
    public CrudKecamatan() {
        this.initComponents();
        this.kecamatanDao = Konfig.getInstance().getContext().getBean(KecamatanRepository.class);
        final List<Kecamatan> kl = (this.kecamatanDao).findAll();
        if (kl.isEmpty()) {
            this.kecamatan = new Kecamatan();
        }
        else {
            this.kecamatan = kl.get(0);
        }
        System.out.println(this.kecamatan.toString());
        this.init(this.kecamatan == null);
    }
    
    private void init(final boolean isNew) {
        this.bersih();
        this.jButton2.addActionListener(x -> this.bersih());
        this.jButton1.addActionListener(x -> {
            System.out.println("simpan");
            this.kecamatanDao.deleteAll();
            Kecamatan k = this.kecamatanDao.save(this.getKecamatan());
            if (k == null) {
                System.out.println("gagal");
            }
            return;
        });
        if (!isNew) {
            this.txtKodeKecamatan.setText(this.kecamatan.getKodeKecamatan());
            this.txtNamaKecamatan.setText(this.kecamatan.getNamaKecamatan());
            this.txtKodeKabupaten.setText(this.kecamatan.getKodeKabupaten());
            this.txtNamaKabupaten.setText(this.kecamatan.getNamaKabupaten());
            this.jTextField4.setText(this.kecamatan.getAlamat());
            this.jTextField5.setText(this.kecamatan.getAlamat2());
            this.jTextField6.setText(this.kecamatan.getTelp());
            this.jTextField7.setText(this.kecamatan.getKodepos());
            this.txtSerial.setText(this.kecamatan.getSerial());
            this.jTextField1.setText(this.kecamatan.getCamat());
            this.jTextField2.setText(this.kecamatan.getJabatan());
            this.jTextField3.setText(this.kecamatan.getNip());
        }
    }
    
    public void bersih() {
        CleanUpText.clean(this);
    }
    
    public Kecamatan getKecamatan() {
        this.kecamatan.setKodeKecamatan(this.txtKodeKecamatan.getText());
        this.kecamatan.setNamaKecamatan(this.txtNamaKecamatan.getText());
        this.kecamatan.setKodeKabupaten(this.txtKodeKabupaten.getText());
        this.kecamatan.setNamaKabupaten(this.txtNamaKabupaten.getText());
        this.kecamatan.setAlamat(this.jTextField4.getText());
        this.kecamatan.setAlamat2(this.jTextField5.getText());
        this.kecamatan.setTelp(this.jTextField6.getText());
        this.kecamatan.setKodepos(this.jTextField7.getText());
        this.kecamatan.setSerial(this.txtSerial.getText());
        this.kecamatan.setCamat(this.jTextField1.getText());
        this.kecamatan.setJabatan(this.jTextField2.getText());
        this.kecamatan.setNip(this.jTextField3.getText());
        return this.kecamatan;
    }
    
    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.txtKodeKecamatan = new JTextField();
        this.txtNamaKecamatan = new JTextField();
        this.txtKodeKabupaten = new JTextField();
        this.txtNamaKabupaten = new JTextField();
        this.jTextField4 = new JTextField();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jTextField5 = new JTextField();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.jTextField6 = new JTextField();
        this.jTextField7 = new JTextField();
        this.jLabel6 = new JLabel();
        this.txtSerial = new JTextField();
        this.jLabel7 = new JLabel();
        this.jTextField1 = new JTextField();
        this.jLabel8 = new JLabel();
        this.jTextField2 = new JTextField();
        this.jLabel9 = new JLabel();
        this.jTextField3 = new JTextField();
        this.jLabel1.setText("Kecamatan");
        this.jLabel2.setText("Kabupaten");
        this.jLabel3.setText("Alamat Kecamatan");
        this.txtNamaKecamatan.setText("jTextField1");
        this.txtNamaKabupaten.setText("jTextField3");
        this.jTextField4.setText("jTextField4");
        this.jButton1.setText("Simpan");
        this.jButton2.setText("Reset");
        this.jTextField5.setText("jTextField5");
        this.jLabel4.setText("Telp");
        this.jLabel5.setText("Kode Pos");
        this.jTextField6.setText("jTextField6");
        this.jTextField7.setText("jTextField7");
        this.jLabel6.setText("Serial");
        this.txtSerial.setText("jTextField1");
        this.jLabel7.setText("Camat");
        this.jTextField1.setText("jTextField1");
        this.jLabel8.setText("Pangkat");
        this.jTextField2.setText("jTextField2");
        this.jLabel9.setText("NIP");
        this.jTextField3.setText("jTextField3");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jButton1, -2, 113, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton2, -2, 129, -2)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.jLabel2).addComponent(this.jLabel3).addComponent(this.jLabel4).addComponent(this.jLabel5).addComponent(this.jLabel6).addComponent(this.jLabel7).addComponent(this.jLabel8).addComponent(this.jLabel9)).addGap(39, 39, 39).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jTextField4).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.txtKodeKecamatan, -1, 61, 32767).addComponent(this.txtKodeKabupaten)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.txtNamaKecamatan, -1, 188, 32767).addComponent(this.txtNamaKabupaten)).addGap(0, 29, 32767)).addComponent(this.jTextField5).addComponent(this.jTextField6).addComponent(this.jTextField7).addComponent(this.txtSerial).addComponent(this.jTextField1).addComponent(this.jTextField2).addComponent(this.jTextField3)))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.txtKodeKecamatan, -2, -1, -2).addComponent(this.txtNamaKecamatan, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.txtKodeKabupaten, -2, -1, -2).addComponent(this.txtNamaKabupaten, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.jTextField4, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jTextField5, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jTextField6, -2, -1, -2).addComponent(this.jLabel4)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jTextField7, -2, -1, -2).addComponent(this.jLabel5)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel6).addComponent(this.txtSerial, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel7).addComponent(this.jTextField1, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel8).addComponent(this.jTextField2, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel9).addComponent(this.jTextField3, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 55, 32767).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jButton1).addComponent(this.jButton2)).addContainerGap()));
    }
}
