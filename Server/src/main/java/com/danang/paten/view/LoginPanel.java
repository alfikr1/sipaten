// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import javax.swing.LayoutStyle;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import com.danang.paten.util.CleanUpText;
import com.danang.paten.Konfig;
import com.danang.paten.controller.PetugasController;
import com.danang.paten.domain.Petugas;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;

public class LoginPanel extends JPanel
{
    private JButton jButton1;
    private JButton jButton2;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPasswordField jPasswordField1;
    private JTextField jTextField1;
    
    public LoginPanel() {
        this.initComponents();
        this.init();
    }
    
    private void init() {
        this.bersih();
    }
    
    public Petugas getPetugas() {
        final PetugasController pDao = Konfig.getInstance().getContext().getBean(PetugasController.class);
        final Petugas dl = pDao.login(this.jTextField1.getText(), new String(this.jPasswordField1.getPassword()));
        return dl;
    }
    
    public JButton btnSimpan() {
        return this.jButton1;
    }
    
    public JButton btnBatal() {
        return this.jButton2;
    }
    
    private void bersih() {
        CleanUpText.clean(this);
    }
    
    public boolean validForm() {
        return !this.jTextField1.getText().equals("") || !new String(this.jPasswordField1.getPassword()).equals("");
    }
    
    private void initComponents() {
        this.jTextField1 = new JTextField();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jPasswordField1 = new JPasswordField();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jTextField1.setText("jTextField1");
        this.jLabel1.setText("Username");
        this.jLabel2.setText("Password");
        this.jPasswordField1.setText("jPasswordField1");
        this.jButton1.setText("Login");
        this.jButton2.setText("Batal");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(45, 45, 45).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jButton1, -2, 105, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton2, -2, 114, -2)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2).addComponent(this.jLabel1)).addGap(45, 45, 45).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jTextField1).addComponent(this.jPasswordField1, -1, 181, 32767)))).addContainerGap(65, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(87, 87, 87).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.jTextField1, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2).addComponent(this.jPasswordField1, -2, -1, -2)).addGap(27, 27, 27).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jButton1).addComponent(this.jButton2)).addContainerGap(99, 32767)));
    }
}
