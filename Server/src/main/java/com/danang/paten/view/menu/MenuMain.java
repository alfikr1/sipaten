// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view.menu;

import javax.swing.GroupLayout;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import com.danang.paten.Konfig;
import com.danang.paten.view.CrudKecamatan;
import com.danang.paten.view.controller.ViewLoketController;
import com.danang.paten.view.controller.ViewPetugasController;
import com.danang.paten.view.controller.ViewLayananController;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.jdesktop.swingx.JXHyperlink;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import org.jdesktop.swingx.JXTaskPane;
import javax.swing.JPanel;

public class MenuMain extends JPanel
{
    public MenuMain() {
        this.initMenu();
    }
    
    private void initMenu() {
        final String[] master = { "Layanan", "User", "Loket", "Kecamatan" };
        final JXTaskPane pane = new JXTaskPane();
        
        pane.setTitle("Master");
        this.setLayout(new GridBagLayout());
        final GridBagConstraints f = new GridBagConstraints();
        f.weightx = 1.0;
        f.gridx = 0;
        f.fill = 2;
        f.anchor = 18;
        f.gridy = 0;
        for (final String s : master) {
            final JXHyperlink l = new JXHyperlink();
            l.setText(s);
            l.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent evt) {
                    MenuMain.this.isiData(s);
                }
            });
            pane.add(l);
        }
        this.add(pane, f);
        this.repaint();
    }
    
    private void isiData(final String s) {
        System.out.println("klik " + s);
        final String lowerCase = s.toLowerCase();
        switch (lowerCase) {
            case "layanan": {
                final ViewLayananController vl = new ViewLayananController();
                vl.init();
                break;
            }
            case "user": {
                final ViewPetugasController vp = new ViewPetugasController();
                vp.init();
                break;
            }
            case "loket": {
                final ViewLoketController vlc = new ViewLoketController();
                vlc.init();
                break;
            }
            case "kecamatan": {
                final CrudKecamatan kc = new CrudKecamatan();
                Konfig.getInstance().getFrame().setKontent(kc);
                break;
            }
        }
    }
    
    public static void main(final String[] args) {
        Konfig.getInstance();
        final JFrame f = new JFrame();
        f.setSize(600, 400);
        f.setDefaultCloseOperation(3);
        f.getContentPane().setLayout(new BorderLayout());
        f.getContentPane().add(new MenuMain());
        f.setLocationRelativeTo(null);
        SwingUtilities.invokeLater(() -> f.setVisible(true));
    }
    
    private void initComponents() {
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }
}
