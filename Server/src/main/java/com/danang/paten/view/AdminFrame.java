// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import java.awt.EventQueue;
import com.danang.paten.Konfig;
import javax.swing.UnsupportedLookAndFeelException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.JXLoginPane;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import com.danang.paten.view.menu.MenuMain;
import javax.swing.JToolBar;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class AdminFrame extends JFrame
{
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;
    private JToolBar jToolBar2;
    
    public AdminFrame() {
        this.initComponents();
        this.init();
        this.setExtendedState(6);
        this.setLocationRelativeTo(null);
    }
    
    private void init() {
        this.jPanel1.add(new MenuMain(), "North");
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(final WindowEvent e) {
            }
            
            @Override
            public void windowClosing(final WindowEvent e) {
            }
            
            @Override
            public void windowClosed(final WindowEvent e) {
                final JXLoginPane.JXLoginFrame frame = new JXLoginPane.JXLoginFrame(new MyLogin().init());
                SwingUtilities.invokeLater(() -> frame.setVisible(true));
            }
            
            @Override
            public void windowIconified(final WindowEvent e) {
            }
            
            @Override
            public void windowDeiconified(final WindowEvent e) {
            }
            
            @Override
            public void windowActivated(final WindowEvent e) {
            }
            
            @Override
            public void windowDeactivated(final WindowEvent e) {
            }
        });
    }
    
    public void setKontent(final JComponent c) {
        this.jPanel2.removeAll();
        this.jPanel2.add(c);
        this.jPanel2.revalidate();
        this.jPanel2.repaint();
    }
    
    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jPanel2 = new JPanel();
        this.jToolBar2 = new JToolBar();
        this.setDefaultCloseOperation(2);
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel2.setLayout(new BoxLayout(this.jPanel2, 2));
        this.jScrollPane1.setViewportView(this.jPanel2);
        this.jToolBar2.setRollover(true);
        final GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jPanel1, -2, 180, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 381, 32767)).addComponent(this.jToolBar2, -1, -1, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this.jScrollPane1, -1, 377, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jToolBar2, -2, 25, -2)));
        this.pack();
    }
    
    public static void main(final String[] args) {
        try {
            for (final UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex2) {
            Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex2);
        }
        catch (IllegalAccessException ex3) {
            Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex3);
        }
        catch (UnsupportedLookAndFeelException ex4) {
            Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex4);
        }
        EventQueue.invokeLater(() -> {
            AdminFrame af = new AdminFrame();
            Konfig.getInstance().setFrame(af);
            af.setVisible(true);
        });
    }
}
