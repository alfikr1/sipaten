// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JLabel;

public class RunningText
{
    private final JLabel lblText;
    private final Timer timer;
    
    public RunningText(final JLabel lbl) {
        this.lblText = lbl;
        this.timer = new Timer(100, new LabelListener(this.lblText));
        this.lblText.addMouseListener(new LabelMouseListener(this.timer));
    }
    
    public static void main(final String[] args) {
        final JFrame f = new JFrame();
        final JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        final JLabel lbl = new JLabel();
        lbl.setText("Hello world");
        p.add(lbl, "Center");
        final RunningText text = new RunningText(lbl);
        f.getContentPane().setLayout(new BorderLayout());
        f.add(p, "Center");
        f.setSize(600, 400);
        f.setDefaultCloseOperation(3);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
    
    class LabelListener implements ActionListener
    {
        private JLabel lbl;
        
        public LabelListener(final JLabel lbl) {
            this.lbl = lbl;
        }
        
        @Override
        public void actionPerformed(final ActionEvent e) {
            final String text = this.lbl.getText();
            final String text2 = text.substring(1) + text.substring(0, 1);
            this.lbl.setText(text2);
        }
    }
    
    class LabelMouseListener extends MouseAdapter
    {
        Timer waktu;
        
        public LabelMouseListener(final Timer waktu) {
            this.waktu = waktu;
        }
        
        @Override
        public void mouseEntered(final MouseEvent evt) {
            this.waktu.stop();
        }
        
        @Override
        public void mouseExited(final MouseEvent evt) {
            this.waktu.start();
        }
    }
}
