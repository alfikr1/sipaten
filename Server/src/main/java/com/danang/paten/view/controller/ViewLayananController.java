// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.view.controller;

import com.danang.paten.domain.Roles;
import com.danang.paten.domain.Counter;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import com.danang.paten.view.CrudLayanan;
import javax.swing.JDialog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import javax.swing.JButton;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.Action;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import com.danang.paten.Konfig;
import com.danang.paten.controller.LayananController;
import com.danang.paten.domain.Layanan;
import org.springframework.data.domain.Page;
import com.danang.paten.tablemodel.LayananTableModel;
import com.danang.paten.view.PanelUmum;
import com.danang.paten.repository.CounterRepository;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.service.LayananService;

public class ViewLayananController {

    private final LayananService service;
    private final LayananRepository layananDao;
    private final CounterRepository counterDao;
    private final LayananController layananController;
    private PanelUmum panel;
    private int halaman;
    private LayananTableModel model;
    private Page<Layanan> page;
    private Layanan fx;

    public ViewLayananController() {
        this.halaman = 0;
        this.service = Konfig.getInstance().getContext().getBean(LayananService.class);
        this.layananDao = Konfig.getInstance().getContext().getBean(LayananRepository.class);
        this.counterDao = Konfig.getInstance().getContext().getBean(CounterRepository.class);
        layananController = Konfig.getInstance().getContext().getBean(LayananController.class);
    }

    public void init() {
        this.panel = new PanelUmum("Layanan");
        this.panel.getTxtCari().setText("");
        this.panel.getTxtCari().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(final KeyEvent evt) {
                if (evt.getKeyCode() == 10) {
                    ViewLayananController.this.refresh();
                }
            }
        });
        this.panel.getButtonTambah().addActionListener(x -> this.tambah());
        this.panel.getInputMap(2).put(KeyStroke.getKeyStroke(78, 128), "tambah");
        this.panel.getInputMap(2).put(KeyStroke.getKeyStroke(82, 128), "refresh");
        this.panel.getInputMap(2).put(KeyStroke.getKeyStroke(70, 128), "cari");
        this.panel.getActionMap().put("tambah", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLayananController.this.tambah();
            }
        });
        this.panel.getActionMap().put("refresh", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLayananController.this.refresh();
            }
        });
        this.panel.getActionMap().put("cari", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLayananController.this.panel.getPanelCari().setVisible(!ViewLayananController.this.panel.getPanelCari().isVisible());
            }
        });
        this.panel.getTableView().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent evt) {
                if (evt.getClickCount() == 2 && ViewLayananController.this.panel.getTableView().getSelectedRow() != -1) {
                    ViewLayananController.this.edit();
                }
            }
        });
        this.panel.getTableView().registerKeyboardAction(new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLayananController.this.hapus();
            }
        }, "hapus", KeyStroke.getKeyStroke(127, 0), 0);
        this.refresh();
        Konfig.getInstance().getFrame().setKontent(this.panel);
    }

    private void loadData() {
        final Callable cal = () -> {
            this.page.getContent().stream().forEach(x -> this.model.add(x));
            return this.page.getTotalElements();
        };
        final FutureTask ft = new FutureTask(cal);
        final ExecutorService ex = Executors.newFixedThreadPool(1);
        ex.execute(ft);
    }

    private void loadPagination() {
        this.panel.getPagination().removeAll();
        for (int i = 0; i < this.page.getTotalPages(); ++i) {
            final JButton btn = new JButton(i + 1 + "");
            btn.addActionListener(x -> {
                this.halaman = Integer.parseInt(btn.getText()) - 1;
                this.refresh();
                return;
            });
            this.panel.getPagination().add(btn);
        }
        this.panel.getPagination().revalidate();
        this.panel.getPagination().repaint();
    }

    private void refresh() {
        this.model = new LayananTableModel();
        final Pageable p = new PageRequest(this.halaman, 20);
        if (this.panel.getTxtCari().getText().equals("")) {
            this.page = layananDao.findAll(p);
        } else {
            this.page = this.service.cariData(this.panel.getTxtCari().getText(), p);
        }
        this.panel.getTableView().setModel(this.model);
        this.loadData();
        this.loadPagination();
    }

    private void edit() {
        final JDialog dlg = new JDialog(Konfig.getInstance().getFrame(), true);
        dlg.setSize(600, 400);
        final int row = this.panel.getTableView().getSelectedRow();
        this.fx = this.model.getItem(row);
        final CrudLayanan crd = new CrudLayanan(this.fx);
        final Action simpan = new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLayananController.this.fx = crd.getLayanan();
                ViewLayananController.this.layananDao.save(ViewLayananController.this.fx);
                dlg.dispose();
                ViewLayananController.this.refresh();
            }
        };
        crd.getBtnSimpan().addActionListener(simpan);
        dlg.setLayout(new BorderLayout());
        dlg.add(crd);
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
    }

    private void hapus() {
        int ret = JOptionPane.showConfirmDialog(this.panel, "Hapus data ini", "Hapus dat", 2, 3);
        if (ret == 0) {
            final int row = panel.getTableView().getSelectedRow();
            Layanan la = model.getItem(row);
            layananController.hapusLayanan(la.getKode());
            refresh();
        }
    }

    private void tambah() {
        final JDialog dlg = new JDialog(Konfig.getInstance().getFrame(), true);
        dlg.setSize(600, 400);
        final CrudLayanan crd = new CrudLayanan();
        crd.getBtnSimpan().addActionListener(x -> {
            this.fx = crd.getLayanan();
            this.layananDao.save(this.fx);
            dlg.dispose();
            this.refresh();
        });
        dlg.setLayout(new BorderLayout());
        dlg.add(crd);
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
    }
    
    public void simpanLayanan(final Layanan layanan) {
        this.service.simpan(layanan);
        Counter c = this.counterDao.findOne(layanan.getKode());
        if (c == null) {
            c = new Counter();
            c.setKode(layanan.getKode());
            c.setSeri(layanan.getMulaiSeri());
        }
        this.counterDao.save(c);
        final Roles role = new Roles();
        role.setDeskripsi(layanan.getNamaLayanan());
        role.setBaca(false);
        role.setHapus(false);
        role.setTulis(false);
    }
}
