// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.awt.Shape;
import java.awt.Font;
import java.awt.BasicStroke;
import java.awt.geom.GeneralPath;
import java.awt.GradientPaint;
import java.awt.Color;
import java.awt.geom.RoundRectangle2D;
import java.awt.Paint;
import com.danang.paten.util.Colors;
import com.danang.paten.util.Theme;
import java.awt.AlphaComposite;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.net.URL;
import javax.swing.Action;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import org.jdesktop.swingx.JXSearchField;
import javax.swing.JTable;
import javax.swing.JSeparator;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelUmum extends JPanel
{
    private String lbl;
    private JButton jButton1;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;
    private JTable jTable1;
    private JXSearchField jXSearchField1;
    private JPanel panelPagination;
    
    public PanelUmum(final String label) {
        initComponents();
        this.lbl = label;
        jPanel1.setVisible(false);
        try {
            URL url = this.getClass().getClassLoader().getResource("images/tambah.png");
            System.out.println(getClass().getClassLoader().getResource("images/tambah.png").getFile());
            //final Icon icon = new ImageIcon(ImageIO.read(url));
            //this.jButton1.setIcon(icon);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
        }
        this.getInputMap(2).put(KeyStroke.getKeyStroke(27, 0), "tutupcari");
        this.getActionMap().put("tutupcari", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                PanelUmum.this.getPanelCari().setVisible(false);
            }
        });
        this.getInputMap(2).put(KeyStroke.getKeyStroke(70, 128), "cari");
        this.getActionMap().put("cari", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                PanelUmum.this.getPanelCari().setVisible(!PanelUmum.this.jPanel1.isVisible());
                if (PanelUmum.this.jPanel1.isVisible()) {
                    PanelUmum.this.getTxtCari().requestFocus();
                }
            }
        });
        this.getInputMap(2).put(KeyStroke.getKeyStroke(KeyEvent.VK_P, 128), "cetak");
        getActionMap().put("cetak", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    cetak();
                } catch (Exception ex) {
                }
            }
            
        });
    }
    
    public void setLbl(final String lbl) {
        this.lbl = lbl;
        this.repaint();
    }
    
    public JButton getButtonTambah() {
        return this.jButton1;
    }
    
    public void aksiButton(final Action action) {
        this.jButton1.addActionListener(action);
    }
    
    public JPanel getPagination() {
        return this.panelPagination;
    }
    
    public JTable getTableView() {
        return this.jTable1;
    }
    
    public JPanel getPanelCari() {
        return this.jPanel1;
    }
    
    public JXSearchField getTxtCari() {
        return this.jXSearchField1;
    }
    public void cetak() throws PrinterException{
        jTable1.print();
    }
    
    @Override
    protected void paintComponent(Graphics g2) {
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        Shape shape = null;
        Paint paint = null;
        int width = getWidth();
        int height = getHeight();
//Set Transparency
        AlphaComposite newComposite = AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER, .9f);
        g.setComposite(newComposite);
// Background shape
        List colors = Colors.getInStance().getStandardColor(Theme.GLOSSY_BLUEGRAY_THEME, 0, height);
        paint = (Paint) colors.get(0);
        shape = new RoundRectangle2D.Double(0, 0, width, height, 26, 26);
        g.setPaint(paint);
        g.fill(shape);
//Set Transparency
        newComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f);
        g.setComposite(newComposite);
        paint = new GradientPaint(0, 0, new Color(255, 255, 255), 0, 50,
                ((GradientPaint) colors.get(0)).getColor1());
// Curved shape
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(15.1308, 1.8172914);
        ((GeneralPath) shape).curveTo(9.820799, 1.8172914, 2.1307993, 4.507291,
                2.1307993, 13.817291);
        ((GeneralPath) shape).lineTo(2.1307993, 31.78604);
        ((GeneralPath) shape).curveTo(39.09004, 37.30802, 86.78241, 40.69229,
                width / 2, 40.69229);
        ((GeneralPath) shape).curveTo(width / 2, 40.69229, width / 2 + 30,
                38.130333, width - 1, 33.84854);
        ((GeneralPath) shape).lineTo(width - 1, 11.817291);
        ((GeneralPath) shape).curveTo(width - 1, 6.507291, width - 5,
                2.8172914, width - 13, 1.8172914);
        ((GeneralPath) shape).lineTo(18.1308, 1.8172914);
        ((GeneralPath) shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
// Drawing outer Shape
        shape = new RoundRectangle2D.Double(1, 1, width - 3, height - 3, 18, 18);
        g.setStroke(new BasicStroke(3));
        g.setPaint(new Color(255, 255, 255));
        g.draw(shape);
        g.setFont(new Font("Microsoft Sanserif", Font.BOLD, 20));
        g.setColor(new Color(0, 0, 0));
        g.drawString(lbl, 30, 40);
        g.setColor(new Color(0, 255, 0));
        g.drawString(lbl, 30, 40);
    }
    
    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.panelPagination = new JPanel();
        this.jPanel1 = new JPanel();
        this.jXSearchField1 = new JXSearchField();
        this.jButton1 = new JButton();
        this.jSeparator1 = new JSeparator();
        this.jTable1.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
        this.jScrollPane1.setViewportView(this.jTable1);
        this.jXSearchField1.setText("jXSearchField1");
        final GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(23, 23, 23).addComponent(this.jXSearchField1, -2, 342, -2).addContainerGap(-1, 32767)));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(this.jXSearchField1, -2, -1, -2).addContainerGap(-1, 32767)));
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 601, 32767).addComponent(this.panelPagination, -1, -1, 32767).addComponent(this.jPanel1, -1, -1, 32767).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGap(0, 0, 32767).addComponent(this.jButton1, -2, 84, -2))).addContainerGap()).addComponent(this.jSeparator1, GroupLayout.Alignment.TRAILING));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addComponent(this.jButton1, -2, 61, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 243, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.panelPagination, -2, 52, -2).addContainerGap()));
    }
    
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame f = new JFrame();
            f.setSize(400, 500);
            f.setLayout(new BorderLayout());
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.getContentPane().add(new PanelUmum("jjj"),BorderLayout.CENTER);
            f.setLocationRelativeTo(null);
            f.setVisible(true);
        });
    }
}
