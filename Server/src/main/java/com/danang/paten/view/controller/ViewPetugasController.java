// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.view.controller;

import com.danang.paten.view.CrudUser;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import javax.swing.JOptionPane;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import com.danang.paten.Konfig;
import com.danang.paten.view.PanelUmum;
import com.danang.paten.tablemodel.PetugasDataModel;
import com.danang.paten.domain.Petugas;
import org.springframework.data.domain.Page;
import com.danang.paten.service.PetugasService;
import com.danang.paten.repository.PetugasRepository;
import com.danang.paten.repository.RolesRepository;

public class ViewPetugasController {

    private final PetugasRepository petugasDao;
    private final PetugasService service;
    private Page<Petugas> page;
    private int halaman;
    private PetugasDataModel model;
    private PanelUmum panel;
    private final RolesRepository rolesDao;

    public ViewPetugasController() {
        this.halaman = 0;
        this.petugasDao = Konfig.getInstance().getContext().getBean(PetugasRepository.class);
        this.service = Konfig.getInstance().getContext().getBean(PetugasService.class);
        this.rolesDao = Konfig.getInstance().getContext().getBean(RolesRepository.class);
    }

    public void init() {
        this.panel = new PanelUmum("Petugas");
        this.panel.getTxtCari().setText("");
        this.panel.getButtonTambah().addActionListener(x -> this.tambah());
        this.panel.getInputMap(0).put(KeyStroke.getKeyStroke(82, 128), "refresh");
        this.panel.getInputMap(0).put(KeyStroke.getKeyStroke(78, 128), "tambah");
        this.panel.getTableView().registerKeyboardAction(new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewPetugasController.this.hapus();
            }
        }, "hapus", KeyStroke.getKeyStroke(127, 0), 0);
        this.panel.getActionMap().put("refresh", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewPetugasController.this.refresh();
            }
        });
        this.panel.getTableView().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent evt) {
                final int row = ViewPetugasController.this.panel.getTableView().getSelectedRow();
                if (evt.getClickCount() == 2 && row != -1) {
                    ViewPetugasController.this.edit();
                }
            }
        });
        this.panel.getActionMap().put("tambah", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewPetugasController.this.tambah();
            }
        });
        this.refresh();
        Konfig.getInstance().getFrame().setKontent(this.panel);
    }

    private void hapus() {
        final int row = this.panel.getTableView().getSelectedRow();
        if (row == -1) {
            return;
        }
        final Petugas p = this.model.getItem(row);
        final int ret = JOptionPane.showConfirmDialog(this.panel, "Hapus petugas ini", "Hapus data", 2, 3);
        if (ret == 0) {
            (this.petugasDao).delete(p);
        }
    }

    private void refresh() {
        this.model = new PetugasDataModel();
        this.panel.getTableView().removeAll();
        this.panel.getTableView().setModel(this.model);
        final Pageable p = new PageRequest(this.halaman, 20);
        if (this.panel.getTxtCari().getText().equals("")) {
            this.page = (this.petugasDao).findAll(p);
        } else {
            this.page = this.service.cariData(this.panel.getTxtCari().getText(), p);
        }
        this.loadData();
        this.pagination();
    }

    private void loadData() {
        final Callable cal = () -> {
            this.page.getContent().stream().forEach(x -> this.model.add(x));
            return null;
        };
        final FutureTask ft = new FutureTask(cal);
        final ExecutorService exe = Executors.newFixedThreadPool(1);
        exe.execute(ft);
    }

    private void pagination() {
        this.panel.getPagination().removeAll();
        for (int i = 0; i < this.page.getTotalPages(); ++i) {
            final JButton btn = new JButton(String.valueOf(i + 1));
            btn.addActionListener(x -> {
                this.halaman = Integer.valueOf(btn.getText()) - 1;
                this.refresh();
                return;
            });
        }
        this.panel.getPagination().repaint();
    }

    private void tambah() {
        final JDialog dlg = new JDialog(Konfig.getInstance().getFrame(), true);
        dlg.setSize(600, 400);
        dlg.setLayout(new BoxLayout(dlg.getContentPane(), 0));
        final CrudUser cd = new CrudUser();
        cd.setListener(() -> dlg.dispose());
        dlg.add(cd);
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
        this.refresh();
    }

    private void edit() {
        final int row = this.panel.getTableView().getSelectedRow();
        final Petugas p = this.model.getItem(row);
        CrudUser cu = new CrudUser(p);
        final JDialog dlg = new JDialog(Konfig.getInstance().getFrame(), true);
        dlg.setSize(600, 400);
        dlg.setLayout(new BoxLayout(dlg.getContentPane(), 0));
        dlg.add(cu);
        cu.getBtnSimpan().addActionListener((x) -> {
            Petugas p1 = cu.getPengguna(p);
            p1.getRoles().stream().forEach((r) -> {
                rolesDao.save(r);
            });
            petugasDao.save(p1);

            dlg.dispose();
        });
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
        this.refresh();
    }
}
