// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view.video;

import org.joda.time.DateTime;

public class TestDate
{
    public static void main(final String[] args) {
        final DateTime dt = new DateTime();
        System.out.println(dt.getMonthOfYear());
        System.out.println(getRomawi(dt.getMonthOfYear()));
    }
    
    private static String getRomawi(final int i) {
        switch (i) {
            case 1: {
                return "I";
            }
            case 2: {
                return "II";
            }
            case 3: {
                return "III";
            }
            case 4: {
                return "IV";
            }
            case 5: {
                return "V";
            }
            case 6: {
                return "VI";
            }
            case 7: {
                return "VII";
            }
            case 8: {
                return "VIII";
            }
            case 9: {
                return "IX";
            }
            case 10: {
                return "X";
            }
            case 11: {
                return "XI";
            }
            default: {
                return "XII";
            }
        }
    }
}
