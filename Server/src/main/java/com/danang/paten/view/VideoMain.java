// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.view;

import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import java.awt.EventQueue;
import javax.swing.UnsupportedLookAndFeelException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import org.jdesktop.swingx.VerticalLayout;
import javax.swing.border.LineBorder;
import java.awt.BorderLayout;
import com.danang.paten.domain.Petugas;
import com.danang.paten.util.MyPanel;
import javax.swing.BorderFactory;
import com.danang.paten.componen.PanelAntrean;
import com.danang.paten.Konfig;
import com.danang.paten.repository.LoketRepository;
import javax.swing.InputMap;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.JXLoginPane;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;
import com.danang.paten.util.FolderWatcher;
import java.awt.Color;
import java.awt.Canvas;
import com.danang.paten.konfigurasi.Konfigurasi;
import uk.co.caprica.vlcj.medialist.MediaList;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.list.MediaListPlayerMode;
import java.nio.file.Files;
import java.io.File;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsDevice;
import java.awt.Component;
import java.awt.GraphicsConfiguration;
import javax.swing.JPanel;
import com.danang.paten.componen.MyMarquee;
import uk.co.caprica.vlcj.player.list.MediaListPlayer;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import com.danang.paten.util.FolderListener;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;
import javax.swing.JFrame;

public class VideoMain extends JFrame implements MediaPlayerEventListener, FolderListener {

    private MediaPlayerFactory mediaFactory;
    private EmbeddedMediaPlayer mediaPlayer;
    private MediaListPlayer listPlayer;
    private MyMarquee marquee;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;

    public VideoMain() {
        this.initComponents();
        
        this.init();
        this.initKey();
    }

    public VideoMain(final GraphicsConfiguration gc) {
        super(gc);
        System.out.println("gc " + gc.getDevice().getIDstring());
        this.initComponents();
        this.init();
        this.initKey();
    }

    private GraphicsDevice getOtherScreen(final Component component) {
        final GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (graphicsEnvironment.getScreenDevices().length == 1) {
            return graphicsEnvironment.getScreenDevices()[0];
        }
        final GraphicsDevice theWrongOne = component.getGraphicsConfiguration().getDevice();
        for (final GraphicsDevice dev : graphicsEnvironment.getScreenDevices()) {
            if (dev != theWrongOne) {
                return dev;
            }
        }
        return null;
    }

    public synchronized void loadData() {
        System.out.println("starting load video");
        final File[] files = new File("video").listFiles();
        this.listPlayer = this.mediaFactory.newMediaListPlayer();
        MediaList ml = this.listPlayer.getMediaList();
        if (ml == null) {
            ml = this.mediaFactory.newMediaList();
        }
        for (final File file : files) {
            try {
                if (Files.probeContentType(file.toPath()).contains("video")) {
                    ml.addMedia(file.getAbsolutePath(), new String[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
            }
        }
        this.listPlayer.setMode(MediaListPlayerMode.LOOP);
        this.listPlayer.setMediaList(ml);
        this.listPlayer.setMediaPlayer(this.mediaPlayer);
        this.listPlayer.play();
    }

    public synchronized void setLoket(final String name) {
    }

    private void init() {
        final Konfigurasi konfig = new Konfigurasi();
        final Konfigurasi k = new Konfigurasi();
        final File file = new File("video");
        Label_0058:
        {
            if (file.exists()) {
                if (file.isDirectory()) {
                    break Label_0058;
                }
            }
            try {
                file.mkdir();
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }
        mediaFactory = new MediaPlayerFactory();
        final Canvas canvas = new Canvas();
        canvas.setBackground(Color.BLACK);
        final CanvasVideoSurface surface = this.mediaFactory.newVideoSurface(canvas);
        (this.mediaPlayer = this.mediaFactory.newEmbeddedMediaPlayer()).setVideoSurface(surface);
        this.mediaPlayer.addMediaPlayerEventListener(this);
        this.mediaPlayer.setVolume(konfig.getVolume());
        this.jPanel2.add(canvas);
        try {
            final Thread t = new Thread(() -> {
                try {
                    FolderWatcher fw = new FolderWatcher(file.toPath(), true);
                    fw.addEventListener(this);
                    fw.processEvents();
                } catch (Exception e2) {
                    e2.printStackTrace(System.err);
                }
                return;
            });
            t.start();
        } catch (Exception e3) {
            e3.printStackTrace(System.err);
        }
        this.loadMarquee();
        this.initAntrean();
        this.loadData();
    }

    public void setTextMarquee(final String text) {
        if (this.marquee != null) {
            this.marquee.stopRequest();
            this.jPanel1.removeAll();
        }
        this.marquee = new MyMarquee(text);
        this.jPanel1.add(this.marquee, "Center");
        this.jPanel1.revalidate();
        this.jPanel1.repaint();
    }

    public void loadMarquee() {
        this.marquee = new MyMarquee("Selamat Datang Di Kecamatan Bergas");
        this.jPanel1.add(this.marquee, "Center");
        this.jPanel1.repaint();
    }

    private void initKey() {
        final InputMap videoSetting = this.getRootPane().getInputMap(2);
        videoSetting.put(KeyStroke.getKeyStroke(84, 128), "setting");
        this.getRootPane().getActionMap().put("setting", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final JDialog dlg = new JDialog(VideoMain.this, true);
                dlg.getContentPane().setLayout(new BoxLayout(dlg.getContentPane(), 1));
                final PanelSettingVideo panelSettingVideo = new PanelSettingVideo(VideoMain.this.mediaPlayer);
                dlg.getContentPane().add(panelSettingVideo);
                dlg.setSize(600, 400);
                dlg.setLocationRelativeTo(null);
                dlg.setVisible(true);
            }
        });
        final InputMap dataSetting = this.getRootPane().getInputMap(2);
        dataSetting.put(KeyStroke.getKeyStroke(119, 0), "data");
        this.getRootPane().getActionMap().put("data", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final Thread t = new Thread() {
                    @Override
                    public void run() {
                        final JXLoginPane.JXLoginFrame frame = new JXLoginPane.JXLoginFrame(new MyLogin().init());
                        SwingUtilities.invokeLater(() -> frame.setVisible(true));
                    }
                };
                t.start();
            }
        });
    }

    public void initAntrean() {
        final LoketRepository lr = Konfig.getInstance().getContext().getBean(LoketRepository.class);
        lr.findAll().forEach(x -> {
            PanelAntrean panel = new PanelAntrean();
            panel.setSize(this.jPanel3.getWidth(), 60);
            panel.setId(x.getKode());
            panel.setBorder(BorderFactory.createTitledBorder(x.getDesk()));
            this.jPanel3.add(panel);
            return;
        });
        this.jPanel3.revalidate();
        this.jPanel3.repaint();
    }

    public void setAntrean(final String antrean, final String loket) {
        for (final Component c : this.jPanel3.getComponents()) {
            if (c instanceof MyPanel && ((MyPanel) c).getId().equals(loket)) {
                ((MyPanel) c).setAntrean(antrean);
            }
        }
    }

    private void showAdmin(final Petugas petugas) {
        final AdminFrame af = new AdminFrame();
        Konfig.getInstance().setFrame(af);
        SwingUtilities.invokeLater(() -> af.setVisible(true));
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jPanel3 = new JPanel();
        this.setDefaultCloseOperation(3);
        this.setUndecorated(true);
        this.jPanel1.setBackground(new Color(19, 15, 15));
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel2.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        this.jPanel2.setLayout(new BoxLayout(this.jPanel2, 2));
        this.jPanel3.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        this.jPanel3.setLayout(new VerticalLayout());
        final GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.jPanel2, -1, 241, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel3, -2, 296, -2).addContainerGap()).addComponent(this.jPanel1, -1, -1, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel2, -1, -1, 32767).addComponent(this.jPanel3, -1, 383, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, 85, -2).addContainerGap()));
        this.pack();
    }

    public static void main(final String[] args) {
        try {
            for (final UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(VideoMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex2) {
            Logger.getLogger(VideoMain.class.getName()).log(Level.SEVERE, null, ex2);
        } catch (IllegalAccessException ex3) {
            Logger.getLogger(VideoMain.class.getName()).log(Level.SEVERE, null, ex3);
        } catch (UnsupportedLookAndFeelException ex4) {
            Logger.getLogger(VideoMain.class.getName()).log(Level.SEVERE, null, ex4);
        }
        EventQueue.invokeLater(() -> {
            VideoMain videoMain = new VideoMain();
            videoMain.setExtendedState(6);
            videoMain.loadMarquee();
            videoMain.setVisible(true);
        });
    }

    @Override
    public void mediaChanged(final MediaPlayer mediaPlayer, final libvlc_media_t media, final String mrl) {
    }

    @Override
    public void opening(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void buffering(final MediaPlayer mediaPlayer, final float newCache) {
    }

    @Override
    public void playing(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void paused(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void stopped(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void forward(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void backward(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void finished(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void timeChanged(final MediaPlayer mediaPlayer, final long newTime) {
    }

    @Override
    public void positionChanged(final MediaPlayer mediaPlayer, final float newPosition) {
    }

    @Override
    public void seekableChanged(final MediaPlayer mediaPlayer, final int newSeekable) {
    }

    @Override
    public void pausableChanged(final MediaPlayer mediaPlayer, final int newPausable) {
    }

    @Override
    public void titleChanged(final MediaPlayer mediaPlayer, final int newTitle) {
    }

    @Override
    public void snapshotTaken(final MediaPlayer mediaPlayer, final String filename) {
    }

    @Override
    public void lengthChanged(final MediaPlayer mediaPlayer, final long newLength) {
    }

    @Override
    public void videoOutput(final MediaPlayer mediaPlayer, final int newCount) {
    }

    @Override
    public void scrambledChanged(final MediaPlayer mediaPlayer, final int newScrambled) {
    }

    @Override
    public void elementaryStreamAdded(final MediaPlayer mediaPlayer, final int type, final int id) {
    }

    @Override
    public void elementaryStreamDeleted(final MediaPlayer mediaPlayer, final int type, final int id) {
    }

    @Override
    public void elementaryStreamSelected(final MediaPlayer mediaPlayer, final int type, final int id) {
    }

    @Override
    public void corked(final MediaPlayer mediaPlayer, final boolean corked) {
    }

    @Override
    public void muted(final MediaPlayer mediaPlayer, final boolean muted) {
    }

    @Override
    public void volumeChanged(final MediaPlayer mediaPlayer, final float volume) {
    }

    @Override
    public void audioDeviceChanged(final MediaPlayer mediaPlayer, final String audioDevice) {
    }

    @Override
    public void chapterChanged(final MediaPlayer mediaPlayer, final int newChapter) {
    }

    @Override
    public void error(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void mediaMetaChanged(final MediaPlayer mediaPlayer, final int metaType) {
    }

    @Override
    public void mediaSubItemAdded(final MediaPlayer mediaPlayer, final libvlc_media_t subItem) {
    }

    @Override
    public void mediaDurationChanged(final MediaPlayer mediaPlayer, final long newDuration) {
    }

    @Override
    public void mediaParsedChanged(final MediaPlayer mediaPlayer, final int newStatus) {
    }

    @Override
    public void mediaFreed(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void mediaStateChanged(final MediaPlayer mediaPlayer, final int newState) {
    }

    @Override
    public void mediaSubItemTreeAdded(final MediaPlayer mediaPlayer, final libvlc_media_t item) {
    }

    @Override
    public void newMedia(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void subItemPlayed(final MediaPlayer mediaPlayer, final int subItemIndex) {
    }

    @Override
    public void subItemFinished(final MediaPlayer mediaPlayer, final int subItemIndex) {
    }

    @Override
    public void endOfSubItems(final MediaPlayer mediaPlayer) {
    }

    @Override
    public void fileAdded(final File file) {
        this.loadData();
    }

    @Override
    public void fileChanged(final File file) {
    }

    @Override
    public void fileRemoved(final String fileName) {
    }

    @Override
    public void notivy() {
        this.loadData();
    }
}
