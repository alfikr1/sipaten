// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.view;

import javax.swing.SwingUtilities;
import com.danang.paten.repository.LayananRepository;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import com.danang.paten.domain.Berkas;
import javax.swing.event.ChangeEvent;
import javax.swing.event.CellEditorListener;
import javax.swing.DefaultCellEditor;
import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;
import com.danang.paten.util.CleanUpText;
import javax.swing.Action;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import java.util.List;
import com.danang.paten.tablemodel.LayananTableModel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import com.danang.paten.componen.MyPopup;
import javax.swing.JPopupMenu;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import com.danang.paten.Konfig;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JSpinner;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import com.danang.paten.tablemodel.SyaratTableModel;
import com.danang.paten.domain.Layanan;
import com.danang.paten.controller.LayananController;
import javax.swing.JPanel;

public class CrudLayanan extends JPanel {

    private final LayananController layananDao;
    private Layanan layanan;
    private SyaratTableModel model;
    private int halaman;
    private int maxRow = 12;
    private int maxPage = 0;
    private Page<Layanan> pg;
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    private JCheckBox jCheckBox1;
    private JCheckBox jCheckBox2;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JScrollPane jScrollPane1;
    private JSpinner jSpinner1;
    private JTable jTable1;
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JTextField jTextField4;

    public CrudLayanan() {
        this(new Layanan());
    }

    public CrudLayanan(final Layanan layanan) {
        this.halaman = 0;
        this.initComponents();
        this.layananDao = Konfig.getInstance().getContext().getBean(LayananController.class);
        this.layanan = layanan;
        this.init(layanan.getKode() == null);
    }

    private void init(final boolean isNew) {
        final KeyStroke ks = KeyStroke.getKeyStroke(127, 0);
        final Action aksiHapusRow = new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int row = CrudLayanan.this.jTable1.getSelectedRow();
                if (row != -1) {
                    CrudLayanan.this.model.hapusBerkas(row);
                }
            }
        };
        this.jTable1.registerKeyboardAction(aksiHapusRow, "hapus", ks, 0);
        this.jButton2.addActionListener(x -> {
            this.layanan = new Layanan();
            this.bersih();
            return;
        });
        this.jButton3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent evt) {
                final JPopupMenu popup = new JPopupMenu();
                final MyPopup pp = new MyPopup();
                pp.getTxtCari().setText(CrudLayanan.this.jTextField4.getText());
                pp.getTxtCari().addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(final KeyEvent evt) {
                        if (evt.getKeyCode() == 10) {
                            CrudLayanan.this.halaman = 0;
                            pp.setHalaman(CrudLayanan.this.halaman);
                            pp.getTableView().setModel(new LayananTableModel(CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText()).getContent()));
                        }
                    }
                });
                final List<Layanan> items = CrudLayanan.this.layananDao.getData(new PageRequest(CrudLayanan.this.halaman, 12)).getContent();
                pp.getTableView().setModel(new LayananTableModel(items));
                pp.getTableView().addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(final MouseEvent evt) {
                        if (evt.getClickCount() == 2) {
                            CrudLayanan.this.jTextField4.setText(((LayananTableModel) pp.getTableView().getModel()).getItem(pp.getTableView().getSelectedRow()).getKode());
                            popup.setVisible(false);
                        }
                    }
                });
                pp.btnFirst().addActionListener(x -> {
                    halaman = 0;
                    pp.setHalaman(halaman);
                    pp.getTableView().setModel(new LayananTableModel(CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText()).getContent()));
                });

                pp.btnLast().addActionListener((ActionEvent x1) -> {
                    pg = CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText());
                    CrudLayanan.this.halaman = pg.getTotalPages();
                    pp.setHalaman(CrudLayanan.this.halaman);
                    pp.getTableView().setModel(new LayananTableModel(CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText()).getContent()));
                });
                pp.btnNext().addActionListener(x -> {
                    pg = CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText());
                    maxPage = pg.getTotalPages();
                    if (halaman + 1 > maxPage) {
                        CrudLayanan.this.halaman = maxPage;
                    } else {
                        ++CrudLayanan.this.halaman;
                    }
                    pp.setHalaman(CrudLayanan.this.halaman);
                    pp.getTableView().setModel(new LayananTableModel(CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText()).getContent()));
                });
                pp.btnPref().addActionListener(x -> {
                    halaman -= 1;
                    if (halaman < 0) {
                        halaman = 0;
                    }
                    pp.setHalaman(CrudLayanan.this.halaman);
                    pp.getTableView().setModel(new LayananTableModel(CrudLayanan.this.getAllLayanan(pp.getTxtCari().getText()).getContent()));
                    return;
                });
                popup.add(pp);
                popup.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        });
        this.bersih();
        if (!isNew) {
            if (this.layanan.getParent() != null) {
                final String kode = this.layanan.getKode();
                final String[] kd = StringUtils.split(kode, ".");
                this.jTextField1.setText(kd[1]);
            } else {
                this.jTextField1.setText(this.layanan.getKode());
            }
            this.jSpinner1.setValue(this.layanan.getMulaiSeri());
            this.jTextField2.setText(this.layanan.getNamaLayanan());
            this.jTextField3.setText(this.layanan.getDisposisi());
            if (this.layanan.getParent() != null) {
                this.jTextField4.setText(this.layanan.getParent().getKode());
            }
            this.jCheckBox2.setSelected(this.layanan.isMasaBerlaku());
            this.jCheckBox1.setSelected(this.layanan.isLangsungTetapkan());
            this.model = new SyaratTableModel(this.layanan.getSyarat());
            this.jTable1.setModel(this.model);
        }
        this.initTable();
    }

    public JButton getBtnSimpan() {
        return this.jButton1;
    }

    public Layanan getLayanan() {
        if (!this.jTextField4.getText().equals("")) {
            this.layanan.setParent(this.layananDao.findByKode(this.jTextField4.getText()));
            this.layanan.setKode(this.jTextField4.getText() + "." + this.jTextField1.getText());
        } else {
            this.layanan.setKode(this.jTextField1.getText());
        }
        this.layanan.setMasaBerlaku(this.jCheckBox2.isSelected());
        this.layanan.setNamaLayanan(this.jTextField2.getText());
        this.layanan.setDisposisi(this.jTextField3.getText());
        this.layanan.setMulaiSeri(Integer.valueOf(String.valueOf(this.jSpinner1.getValue())));
        this.layanan.setLangsungTetapkan(this.jCheckBox1.isSelected());
        this.layanan.setSyarat(this.model.getItems());
        return this.layanan;
    }

    private Page<Layanan> getAllLayanan(final String cari) {
        if (cari.equals("")) {
            return this.layananDao.getData(new PageRequest(this.halaman, 12));
        }
        return this.layananDao.cariData(cari, new PageRequest(this.halaman, 12));
    }

    public void bersih() {
        CleanUpText.clean(this);
        this.jSpinner1.setValue(0);
        this.model = new SyaratTableModel();
        this.jTable1.setModel(this.model);
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jTextField1 = new JTextField();
        this.jTextField2 = new JTextField();
        this.jTextField3 = new JTextField();
        this.jTextField4 = new JTextField();
        this.jButton3 = new JButton();
        this.jLabel5 = new JLabel();
        this.jSpinner1 = new JSpinner();
        this.jCheckBox1 = new JCheckBox();
        this.jCheckBox2 = new JCheckBox();
        this.jLabel1.setText("Kode");
        this.jLabel2.setText("Layanan");
        this.jLabel3.setText("Disposisi");
        this.jLabel4.setText("Parent");
        this.jTable1.setModel(new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this.jScrollPane1.setViewportView(this.jTable1);
        this.jButton1.setText("Simpan");
        this.jButton2.setText("Reset");
        this.jTextField1.setText("jTextField1");
        this.jTextField2.setText("jTextField2");
        this.jTextField3.setText("jTextField3");
        this.jTextField4.setText("jTextField4");
        this.jButton3.setText("...");
        this.jLabel5.setText("Start Serial");
        this.jSpinner1.setModel(new SpinnerNumberModel(0, 0, null, 1));
        this.jCheckBox1.setText("Langsung Tetapkan");
        this.jCheckBox2.setText("Masa Berlaku");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 528, 32767).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jButton1, -2, 93, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton2, -2, 98, -2)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.jLabel2).addComponent(this.jLabel3).addComponent(this.jLabel4).addComponent(this.jLabel5)).addGap(32, 32, 32).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jTextField1, -2, 177, -2).addComponent(this.jTextField2, -2, 297, -2).addComponent(this.jTextField3, -2, 191, -2).addGroup(layout.createSequentialGroup().addComponent(this.jTextField4, -2, 127, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jCheckBox2)).addGroup(layout.createSequentialGroup().addComponent(this.jSpinner1, -2, 83, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jCheckBox1))))).addGap(0, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.jTextField1, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.jTextField2, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.jTextField3, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this.jTextField4, -2, -1, -2).addComponent(this.jButton3).addComponent(this.jCheckBox2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this.jSpinner1, -2, -1, -2).addComponent(this.jCheckBox1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -2, 173, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jButton1).addComponent(this.jButton2)).addContainerGap()));
    }

    private void initTable() {
        final DefaultCellEditor dokEdit = new DefaultCellEditor(new JTextField());
        dokEdit.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(final ChangeEvent e) {
                final int row = CrudLayanan.this.jTable1.getSelectedRow();
                Berkas b = CrudLayanan.this.model.getItem(row);
                if (b == null) {
                    b = new Berkas();
                }
                b.setDokumen(dokEdit.getCellEditorValue().toString());
                CrudLayanan.this.model.cekItem();
            }

            @Override
            public void editingCanceled(final ChangeEvent e) {
            }
        });
        final DefaultCellEditor optEdit = new DefaultCellEditor(new JCheckBox());
        optEdit.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(final ChangeEvent e) {
                final int row = CrudLayanan.this.jTable1.getSelectedRow();
                Berkas b = CrudLayanan.this.model.getItem(row);
                if (b == null) {
                    b = new Berkas();
                }
                b.setOptional(((JCheckBox) optEdit.getComponent()).isSelected());
                CrudLayanan.this.model.fireTableRowsUpdated(row, row);
            }

            @Override
            public void editingCanceled(final ChangeEvent e) {
            }
        });
        this.jTable1.getColumnModel().getColumn(0).setCellEditor(dokEdit);
        this.jTable1.getColumnModel().getColumn(1).setCellEditor(optEdit);
    }

    public static void main(final String[] args) {
        Konfig.getInstance();
        final JFrame f = new JFrame();
        f.setSize(600, 400);
        f.setDefaultCloseOperation(3);
        f.getContentPane().setLayout(new BorderLayout());
        final Layanan x = Konfig.getInstance().getContext().getBean(LayananRepository.class).findOne("501");
        f.getContentPane().add(new CrudLayanan());
        f.setLocationRelativeTo(null);
        SwingUtilities.invokeLater(() -> f.setVisible(true));
    }
}
