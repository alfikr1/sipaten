// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view.controller;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.CrudRepository;
import java.awt.Window;
import com.danang.paten.view.CrudLoket;
import java.awt.LayoutManager;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JComponent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.table.TableModel;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import com.danang.paten.Konfig;
import com.danang.paten.tablemodel.LoketTableModel;
import com.danang.paten.domain.Loket;
import org.springframework.data.domain.Page;
import com.danang.paten.view.PanelUmum;
import com.danang.paten.service.LoketService;
import com.danang.paten.repository.LoketRepository;

public class ViewLoketController
{
    private final LoketRepository loketDao;
    private final LoketService service;
    private PanelUmum panel;
    private int halaman;
    private Page<Loket> page;
    private LoketTableModel model;
    
    public ViewLoketController() {
        this.halaman = 0;
        this.loketDao = Konfig.getInstance().getContext().getBean(LoketRepository.class);
        this.service = Konfig.getInstance().getContext().getBean(LoketService.class);
    }
    
    public void init() {
        this.panel = new PanelUmum("Loket");
        this.panel.getTxtCari().setText("");
        this.panel.getTxtCari().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(final KeyEvent evt) {
                if (evt.getKeyCode() == 10) {
                    ViewLoketController.this.refresh();
                }
            }
        });
        this.model = new LoketTableModel();
        this.panel.getTableView().setModel(this.model);
        this.panel.getButtonTambah().addActionListener(x -> this.tambah());
        this.panel.getTableView().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent evt) {
                if (evt.getClickCount() == 2 && ViewLoketController.this.panel.getTableView().getSelectedRow() != -1) {
                    ViewLoketController.this.edit();
                }
            }
        });
        this.panel.getTableView().registerKeyboardAction(new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLoketController.this.refresh();
            }
        }, "refresh", KeyStroke.getKeyStroke(82, 128), 0);
        this.panel.getTableView().registerKeyboardAction(new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLoketController.this.delete();
            }
        }, "delete", KeyStroke.getKeyStroke(127, 0), 0);
        this.panel.getInputMap(2).put(KeyStroke.getKeyStroke(82, 128), "refresh");
        this.panel.getActionMap().put("refresh", new AbstractAction() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ViewLoketController.this.refresh();
            }
        });
        this.refresh();
        Konfig.getInstance().getFrame().setKontent(this.panel);
    }
    
    private void delete() {
        final int row = this.panel.getTableView().getSelectedRow();
        if (row == -1) {
            return;
        }
        final int ret = JOptionPane.showConfirmDialog(this.panel, "hapus data ini", "delete", 2, 3);
        if (ret == 0) {
            loketDao.delete(this.model.getItem(row));
            this.refresh();
        }
    }
    
    private void refresh() {
        this.model = new LoketTableModel();
        this.panel.getTableView().setModel(this.model);
        if (this.panel.getTxtCari().getText().equals("")) {
            this.page = (this.loketDao).findAll(new PageRequest(this.halaman, 20));
            System.out.println("isi " + this.page.getContent().size());
        }
        else {
            this.page = this.service.cariLoket(this.panel.getTxtCari().getText(), new PageRequest(this.halaman, 20));
        }
        this.loadData();
        this.pagination();
    }
    
    private void loadData() {
        final Callable cal = () -> {
            this.page.getContent().stream().forEach(x -> this.model.add(x));
            return null;
        };
        final FutureTask ft = new FutureTask(cal);
        final ExecutorService exe = Executors.newFixedThreadPool(1);
        exe.execute(ft);
    }
    
    private void pagination() {
        this.panel.getPagination().removeAll();
        for (int i = 0; i < this.page.getTotalPages(); ++i) {
            final JButton btn = new JButton("" + (i + 1));
            btn.addActionListener(x -> {
                this.halaman = Integer.parseInt(btn.getText()) - 1;
                this.refresh();
                return;
            });
            this.panel.getPagination().add(btn);
        }
        this.panel.repaint();
        this.panel.revalidate();
    }
    
    private void tambah() {
        final JDialog dlg = new JDialog();
        dlg.setSize(600, 400);
        dlg.setLayout(new BoxLayout(dlg.getContentPane(), 0));
        final CrudLoket cl = new CrudLoket();
        cl.getBtnSimpan().addActionListener(x -> {
            Loket l = cl.getLoket();
            this.loketDao.save(l);
            dlg.dispose();
            this.refresh();
        });
        dlg.add(cl);
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
        this.refresh();
    }
    
    private void edit() {
        final JDialog dlg = new JDialog();
        dlg.setSize(600, 400);
        dlg.setLayout(new BoxLayout(dlg.getContentPane(), 0));
        final int row = this.panel.getTableView().getSelectedRow();
        if (row == -1) {
            return;
        }
        final CrudLoket cl = new CrudLoket(this.model.getItem(row));
        cl.getBtnSimpan().addActionListener(x -> {
            Loket l = cl.getLoket();
            this.loketDao.save(l);
            dlg.dispose();
            this.refresh();
        });
        dlg.add(cl);
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
        this.refresh();
    }
}
