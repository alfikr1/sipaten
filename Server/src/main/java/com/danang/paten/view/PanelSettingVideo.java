// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import javax.swing.LayoutStyle;
import javax.swing.GroupLayout;
import com.danang.paten.konfigurasi.Konfigurasi;
import javax.swing.JTextArea;
import javax.swing.JSlider;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import javax.swing.JPanel;

public class PanelSettingVideo extends JPanel
{
    private final EmbeddedMediaPlayer mediaPlayer;
    private JButton jButton1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JScrollPane jScrollPane1;
    private JSlider jSlider1;
    private JTextArea jTextArea1;
    
    public PanelSettingVideo(final EmbeddedMediaPlayer mediaPlayer) {
        this.initComponents();
        this.mediaPlayer = mediaPlayer;
        this.init();
    }
    
    private void init() {
        final Konfigurasi konfig = new Konfigurasi();
        this.jSlider1.setValue(konfig.getVolume());
        this.jSlider1.addChangeListener(e -> this.mediaPlayer.setVolume(this.jSlider1.getValue()));
        this.jTextArea1.setText(konfig.getRunningText());
        this.jButton1.addActionListener(x -> {
            konfig.setRunning(this.jTextArea1.getText());
            konfig.setVolume(this.jSlider1.getValue());
        });
    }
    
    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jSlider1 = new JSlider();
        this.jLabel2 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTextArea1 = new JTextArea();
        this.jButton1 = new JButton();
        this.jLabel1.setText("Volume");
        this.jSlider1.setMaximum(200);
        this.jLabel2.setText("Marque Text");
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setRows(5);
        this.jScrollPane1.setViewportView(this.jTextArea1);
        this.jButton1.setText("Simpan");
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSlider1, -2, 298, -2)).addComponent(this.jLabel2)).addGap(0, 20, 32767)).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGap(0, 0, 32767).addComponent(this.jButton1, -2, 98, -2))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.jSlider1, -2, -1, -2)).addGap(21, 21, 21).addComponent(this.jLabel2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 159, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton1).addContainerGap()));
    }
}
