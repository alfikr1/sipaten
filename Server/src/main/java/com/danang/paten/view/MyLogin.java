// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.view;

import javax.swing.SwingUtilities;
import org.jdesktop.swingx.auth.LoginEvent;
import org.jdesktop.swingx.auth.LoginListener;
import org.jdesktop.swingx.auth.LoginService;
import com.danang.paten.Konfig;
import com.danang.paten.domain.Petugas;
import com.danang.paten.controller.PetugasController;
import org.jdesktop.swingx.JXLoginPane;

public class MyLogin
{
    private JXLoginPane panel;
    private PetugasController pDao;
    private Petugas petugas;
    
    public MyLogin() {
        this.pDao = Konfig.getInstance().getContext().getBean(PetugasController.class);
    }
    
    public JXLoginPane init() {
        final LoginService service = new LoginService() {
            @Override
            public boolean authenticate(final String name, final char[] password, final String server) throws Exception {
                MyLogin.this.petugas = MyLogin.this.pDao.login(name, new String(password));
                return MyLogin.this.petugas != null && MyLogin.this.petugas.isAdministrator();
            }
        };
        service.addLoginListener(new LoginListener() {
            @Override
            public void loginFailed(final LoginEvent source) {
            }
            
            @Override
            public void loginStarted(final LoginEvent source) {
            }
            
            @Override
            public void loginCanceled(final LoginEvent source) {
            }
            
            @Override
            public void loginSucceeded(final LoginEvent source) {
                final AdminFrame af = new AdminFrame();
                Konfig.getInstance().setPetugas(MyLogin.this.petugas);
                Konfig.getInstance().setFrame(af);
                SwingUtilities.invokeLater(() -> af.setVisible(true));
            }
        });
        return this.panel = new JXLoginPane(service);
    }
}
