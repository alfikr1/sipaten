// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import org.joda.time.DateTime;
import java.util.Date;
import com.danang.paten.domain.Token;
import org.mindrot.jbcrypt.BCrypt;
import java.util.HashMap;
import java.util.Map;
import com.danang.paten.domain.Petugas;
import com.danang.paten.Konfig;
import com.google.gson.JsonObject;
import com.danang.paten.domain.MessageFormat;
import com.danang.paten.repository.TokenRepository;
import com.danang.paten.service.PetugasService;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.PetugasRepository;
import java.text.SimpleDateFormat;
import org.springframework.stereotype.Controller;

@Controller
public class LoginController {

    @Autowired
    private PetugasRepository userDao;
    @Autowired
    private PetugasService userService;
    @Autowired
    private TokenRepository tokenDao;

    public MessageFormat handle(final MessageFormat mf) {
        final MessageFormat res = new MessageFormat();
        final JsonObject json = Konfig.getInstance().getGson().fromJson(mf.getMsg(), JsonObject.class);
        final String username = json.get("username").getAsString();
        final String password = json.get("password").getAsString();
        if (username != null && password != null) {
            final Petugas p = this.userService.login(username, password);
            if (p != null) {
                Token token = new Token();
                token.setPetugas(p);
                token.setCreateDate(new Date());
                token.setToken(BCrypt.gensalt(16));
                DateTime dt = new DateTime();
                token.setExpiredDate(dt.plusDays(1).toDate());
                tokenDao.save(token);
                p.setLastLogin(new Date());
                Map<String, Object> map = new HashMap<>();
                map.put("token", token.getToken());
                map.put("nama", p.getNama());
                res.setCommand("Response");
                res.setError("");
                res.setMsg(Konfig.getInstance().getGson().toJson(map));
                return res;
            }
            res.setError("username/password salah");
        } else {
            res.setError("data tidak valid");
        }
        return res;
    }

    public Map<String, Object> getToken(final String username, final String password, final String ip) {
        final Map<String, Object> map = new HashMap<>();
        try {
            final Petugas p = this.userService.login(username, password);
            if (p != null) {
                p.setIsLogged(true);
                final String token = BCrypt.gensalt(16);
                final Token token2 = new Token();
                token2.setCreateDate(new Date());
                DateTime dt = new DateTime();
                token2.setToken(token);
                token2.setExpiredDate(dt.plusDays(1).toDate());
                token2.setPetugas(p);
                Token tr = tokenDao.save(token2);
                map.put("token", tr);
                //map.put("layanan", tr.getPetugas().getRoles());
                map.put("error", "");
            } else {
                map.put("error", "username / password not match");
            }
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public boolean validasiToken(String tok) {
        final Token token = this.tokenDao.findOne(tok);
        if (token != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy hh:mm:ss");
            System.out.println(sdf.format(token.getExpiredDate()));
            return !new Date().after(token.getExpiredDate());
        } else {
            return false;
        }
    }
}
