// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.google.gson.Gson;
import com.danang.paten.Konfig;
import com.danang.paten.domain.MessageFormat;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.danang.paten.domain.KartuKendaliSuratMasuk;
import com.danang.paten.repository.KendaliSuratMasukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.SuratService;
import java.util.Arrays;
import org.springframework.stereotype.Controller;

@Controller
public class SuratMasukController {

    @Autowired
    private SuratService service;
    @Autowired
    KendaliSuratMasukRepository suratDao;

    public KartuKendaliSuratMasuk simpanSuratMasuk(final KartuKendaliSuratMasuk kms) {
        kms.setTanggalTerima(new Date());
        return this.service.simpanSuratMasuk(kms);
    }

    public Map<String, Object> updateKartuKendali(final KartuKendaliSuratMasuk km) {
        Map<String, Object> map = new HashMap<>();
        if (km.getStatusSurat().equals("DIDISPOSISIKAN")) {
            map.put("error", "data tidak boleh diupdate lagi");
            return map;
        }
        try {
            KartuKendaliSuratMasuk obj = service.updateSuratMasuk(km);
            map.put("status", "terupdate");
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }
    public KartuKendaliSuratMasuk temukanSatu(String id){
        return suratDao.findOne(id);
    }
    public Map<String, Object> deleteKartuKendali(String id) {
        Map<String, Object> map = new HashMap<>();
        KartuKendaliSuratMasuk kmk = suratDao.findOne(id);
        if (kmk == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        if (kmk.getStatusSurat().equals("DIDISPOSISIKAN")) {
            map.put("error", "data tidak boleh dihapus karena telah didisposisikan");
            return map;
        }
        try {
            suratDao.delete(id);
            map.put("error", "");
            map.put("status", "Data terhapus");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> simpanDisposisiSurat(String surat, String disposisi, String catatan, String tugas) {
        Map<String, Object> map = new HashMap<>();
        List<String> lst = Arrays.asList(disposisi.split("#"));
        KartuKendaliSuratMasuk kmk = service.simpanDisposisiSuratMasuk(tugas, lst, new Date(), surat, disposisi, catatan);
        if (kmk != null) {
            map.put("error", "");
            map.put("data", kmk);
            return map;
        }
        map.put("error", "No data qualified");
        return map;
    }

    public Map<String, Object> report(final Date awal, final Date akhir) {
        final Map<String, Object> map = new HashMap<>();
        map.put("data", service.findMasuk(awal, akhir));
        map.put("error", "");
        return map;
    }

    public List<KartuKendaliSuratMasuk> getAwal() {
        return this.service.findFirst();
    }

    public MessageFormat handler(final MessageFormat mf) {
        final Gson gson = Konfig.getInstance().getGson();
        final MessageFormat res = new MessageFormat();
        final String command = mf.getCommand();
        switch (command) {
            case "baru": {
                final KartuKendaliSuratMasuk km = gson.fromJson(mf.getMsg(), KartuKendaliSuratMasuk.class);
            }
            case "all": {
            }
        }
        return res;
    }

    public Map<String, Object> disposisiSuratMasuk(String kode, String... disposisi) {
        Map<String, Object> map = new HashMap<>();

        return map;
    }

    public Map<String, Object> findSuratMasuk(final String cari, final Pageable pageable) {
        Map<String, Object> map = new HashMap<>();
        Page<KartuKendaliSuratMasuk> page = this.service.findSuratMasuk(cari, pageable);
        map.put("konten", page.getContent());
        map.put("element", page.getTotalElements());
        map.put("total", page.getTotalPages());
        return map;
    }

    public Map<String, Object> findSurat(final String cari, final Date awal, final Date akhir, final Pageable pageable) {
        final Map<String, Object> map = new HashMap<>();
        Page<KartuKendaliSuratMasuk> page;
        if (cari == null) {
            page = service.findSuratByDate(awal, akhir, pageable);
        } else {
            page = service.findMasukTanggal(cari, awal, akhir, pageable);
        }
        System.out.println(page.getContent().size());
        map.put("data", page.getContent());
        map.put("totalPage", page.getTotalPages());
        map.put("size", page.getTotalElements());
        map.put("error", "");
        return map;
    }
}
