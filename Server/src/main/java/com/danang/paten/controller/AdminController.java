/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.controller;

import com.danang.paten.domain.Kecamatan;
import com.danang.paten.domain.Loket;
import com.danang.paten.domain.Petugas;
import com.danang.paten.repository.KecamatanRepository;
import com.danang.paten.repository.LoketRepository;
import com.danang.paten.repository.PetugasRepository;
import com.danang.paten.service.LoketService;
import com.danang.paten.service.PetugasService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;

/**
 *
 * @author danang
 */
@Controller
public class AdminController {

    //kecamatan setting
    private @Autowired
    KecamatanRepository kecamatanDao;
    private @Autowired
    LoketRepository loketDao;
    private @Autowired
    LoketService loketService;
    private @Autowired
    PetugasService petugasService;
    private @Autowired
    PetugasRepository petugasDao;

    public Kecamatan getKecamatan() {
        List<Kecamatan> items = kecamatanDao.findAll();
        Kecamatan result;
        if (items.isEmpty()) {
            result = new Kecamatan();
        } else {
            result = items.get(0);
        }
        return result;
    }

    public Map<String,Object> simpanKecamatan(Kecamatan kecamatan) {
        Map<String,Object> result = new HashMap<>();
        try {
            kecamatanDao.deleteAll();
            kecamatanDao.save(kecamatan);
            result.put("error", "");
        } catch (Exception e) {
            result.put("error", e.getMessage());
        }
        return result;
    }

    public Loket simpanLoket(Loket loket) {
        return loketDao.save(loket);
    }

    public Loket updateLoket(Loket loket, String id) {
        Loket x = loketDao.findOne(id);
        if (x == null) {
            throw new IllegalStateException("Loket id tidak ditemukan");
        }
        loket.setKode(x.getKode());
        return loketDao.save(loket);
    }

    public void hapusLoket(String id) {
        Loket x = loketDao.findOne(id);
        if (x == null) {
            throw new IllegalStateException("Loket id tidak ditemukan");
        }
        loketDao.delete(x);
    }

    public Map<String, Object> findAllLoket(String cari, Pageable p) {
        Map<String, Object> result = new HashMap<>();
        Page<Loket> pg;
        if (cari.equals("")) {
            pg = loketDao.findAll(p);
        } else {
            pg = loketService.cariLoket(cari, p);
        }
        result.put("konten", pg.getContent());
        result.put("total", pg.getTotalPages());
        result.put("size", pg.getSize());
        result.put("error", "");
        return result;
    }

    public Map<String, Object> findAllPetugas(String cari, Pageable p) {
        Map<String, Object> result = new HashMap<>();
        Page<Petugas> pg;
        if (cari.equals("")) {
            pg = petugasDao.findAll(p);
        } else {
            pg = petugasService.cariData(cari, p);
        }
        result.put("konten", pg.getContent());
        result.put("total", pg.getTotalPages());
        result.put("size", pg.getSize());
        result.put("error", "");
        return result;
    }

    public Petugas simpanPetugas(Petugas p) {
        return petugasDao.save(p);
    }
    public void hapusPetugas(String s){
        Petugas x = petugasDao.findOne(s);
        if (x == null) {
            throw new IllegalStateException("Petugas id tidak ditemukan");
        }
        petugasDao.delete(s);
    }

    public Petugas updatePetugas(Petugas p, String s) {
        Petugas x = petugasDao.findOne(s);
        if (x == null) {
            throw new IllegalStateException("Petugas id tidak ditemukan");
        }
        p.setKode(x.getKode());
        return petugasDao.save(p);
    }

}
