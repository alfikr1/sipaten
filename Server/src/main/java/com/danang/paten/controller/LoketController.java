// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.controller;

import com.danang.paten.domain.Loket;
import com.danang.paten.repository.LoketRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;

@Controller
public class LoketController
{
    private @Autowired LoketRepository loketDao;
    public Map<String,Object> getAllLoket(){
        Map<String,Object> map = new HashMap<>();
        loketDao.findAll().stream().forEach((x)->{
            map.put(x.getKode(), x.getDesk());
        });
        return map;
    }
    public List<Loket> findAll(){
        return loketDao.findAll();
    }
    public Page<Loket> findAllData(String cari,Pageable p){
        return loketDao.findByKodeLikeOrDeskLike(cari, p);
    }
    public Page<Loket> findAllData(Pageable p){
        return loketDao.findAll(p);
    }
    public Loket simpan(Loket loket){
        Loket x = loketDao.findOne(loket.getKode());
        if(x!=null){
            throw new IllegalStateException("Loket telah ada");
        }        
        return loketDao.save(loket);
    }
    public void delete(String kode){
        loketDao.delete(kode);
    }
    public Loket updateLoket(Loket l){
        Loket x = loketDao.findOne(l.getKode());
        if(x==null){
            throw new IllegalStateException("Loket tidak ditemukan");
        }
        l.setKode(x.getKode());
        return loketDao.save(l);
    }
    
}
