package com.danang.paten.controller;

import java.util.ArrayList;
import com.danang.paten.domain.Berkas;
import java.util.HashMap;
import java.util.Map;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import com.danang.paten.domain.Layanan;
import com.danang.paten.domain.Roles;
import com.danang.paten.repository.CounterRepository;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.PetugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.LayananService;
import java.util.Iterator;
import org.springframework.stereotype.Controller;

@Controller
public class LayananController {

    @Autowired
    private LayananService service;
    @Autowired
    private LayananRepository layananDao;
    @Autowired
    private CounterRepository counterDao;
    private @Autowired
    PetugasRepository userDao;

    public Layanan findByKode(final String kode) {
        return this.layananDao.findOne(kode);
    }

    public List<Layanan> getSemua() {
        return (this.layananDao).findAll();
    }
    public List<Map> findSemuaLayanan(){
        return service.getAntrianLayanan();
    }

    public Page<Layanan> getData(Pageable pageabl) {
        if (pageabl == null) {
            pageabl = new PageRequest(0, 20);
        }
        return (this.layananDao).findAll(pageabl);
    }

    public Page<Layanan> cariData(final String cari, final Pageable pageable) {
        return service.cariData(cari, pageable);
    }

    public void updateLayanan(final Layanan layanan) {
        final Layanan x = this.service.findById(layanan.getKode());
        if (x == null) {
            throw new IllegalStateException("Data layanan dengan kode " + layanan.getKode() + " tidak ditemukan");
        }
        layanan.setKode(x.getKode());
        this.service.simpan(layanan);
    }

    public Map<String, Object> getAllLayanan() {
        final Map<String, Object> map = new HashMap<>();
        try {
            List<Layanan> layanan = this.service.getAllLayanan();
            map.put("error", "");
            map.put("konten", layanan);
        } catch (Exception e) {
            map.put("error", e.getCause());
        }
        return map;
    }

    public Map<String, Object> buatLayanan(final String kode, final String nama, 
            final String parent, final List<Berkas> items, 
            final boolean langsung, final boolean masaberlaku, final int mulai) {
        final Map<String, Object> map = new HashMap<>();
        final Layanan l = new Layanan();
        l.setNamaLayanan(nama);
        l.setSyarat(items);
        Layanan paren = null;
        if (parent != null) {
            paren = this.layananDao.findOne(parent);
            if (paren != null) {
                l.setParent(paren);
            }
        }
        l.setLangsungTetapkan(langsung);
        l.setMasaBerlaku(masaberlaku);
        l.setMulaiSeri(mulai);
        l.setKode((paren != null) ? (paren.getKode() + "." + kode) : kode);
        try {
            layananDao.save(l);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public void hapusLayanan(String id) {
        Layanan l = this.layananDao.findOne(id);
        if (l != null) {
            userDao.findAll().stream().forEach((x) -> {
                List<Roles> items = x.getRoles();
                Iterator itr=items.iterator();
                while(itr.hasNext()){
                    Roles r = (Roles) itr.next();
                    if(r.getKode().equals(id)){
                        itr.remove();
                    }
                }
                x.setRoles(items);
                userDao.save(x);
                
            });
            layananDao.delete(l);
        }
    }

    public List<Layanan> findByParent(final String kode) {
        Layanan l = this.layananDao.findOne(kode);
        if (l != null) {
            return this.layananDao.findByParent(l);
        }
        return new ArrayList<>();
    }
}
