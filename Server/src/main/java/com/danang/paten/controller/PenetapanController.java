// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.Berkas;
import com.danang.paten.domain.PenetapanBerkas;
import com.danang.paten.form.IMB;
import com.danang.paten.form.IUM;
import com.danang.paten.form.DomisiliUsaha;
import com.danang.paten.form.DispensasiNikah;
import com.danang.paten.form.RekomendasiPenelitian;
import com.danang.paten.form.HO;
import java.util.Date;
import com.danang.paten.enumerasi.StatusPekerjaan;
import java.util.List;
import com.danang.paten.domain.TransaksiLayanan;
import org.springframework.data.domain.Page;
import com.google.gson.Gson;
import com.danang.paten.form.DokumenBerkas;
import java.util.ArrayList;
import spark.utils.StringUtils;
import com.danang.paten.domain.Token;
import java.util.HashMap;
import com.danang.paten.Konfig;
import com.danang.paten.konfigurasi.Konfigurasi;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import com.danang.paten.repository.TokenRepository;
import com.danang.paten.service.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.TransaksiLayananRepository;
import com.danang.paten.util.KirimPesan;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

@Controller
public class PenetapanController {

    @Autowired
    private TransaksiLayananRepository trxDao;
    @Autowired
    private TransaksiService service;
    @Autowired
    private TokenRepository tokenDao;
    private final Logger log =Logger.getLogger(PenetapanController.class);
    
    public Map<String, Object> cariPenetapan(final String cari, final String keypair, final Pageable pageable) {
        final Gson gson = Konfig.getInstance().getGson();
        final Map<String, Object> map = new HashMap<String, Object>();
        final Token token = this.tokenDao.findOne(keypair);
        if (token == null || token.getPetugas() == null) {
            map.put("error", "data request tidak valid");
            return map;
        }
        Page<TransaksiLayanan> page;
        if (StringUtils.isEmpty(cari)) {
            page = this.service.findAllPenetapan(pageable);
        } else {
            page = this.service.cariPenetapanData(cari, pageable);
        }
        final List<DokumenBerkas> items = new ArrayList<>();
        map.put("totalPage", page.getTotalPages());
        map.put("ukuran", page.getTotalElements());
        page.getContent().stream().forEach(x -> {
            DokumenBerkas db = new DokumenBerkas();
            if (x.getLayanan() != null) {
                db.setLayanan(x.getLayanan().getNamaLayanan());
            }
            db.setNomor(x.getNomor());
            if (x.getPenduduk() != null) {
                db.setPemohon(x.getPenduduk().getNama());
            }
            db.setTanggalRegistrasi(x.getTerimaBerkas().getTanggalTerima());
            db.setStatusPekerjaan(x.getStatusPekerjaan().toString());
            items.add(db);
        });
        map.put("error", "");
        map.put("data", items);
        return map;
    }

    public Map<String, Object> kembaliPeriksa(final String nomor) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        tl.setStatusPekerjaan(StatusPekerjaan.Pemberkasan);
        this.trxDao.save(tl);
        map.put("error", "");
        return map;
    }
    
    public Map<String, Object> simpanPenetapanHO(final Date ketetapan, final Date awal, final Date akhir, final String nomor, final String token) {
        final Map<String, Object> map = new HashMap<>();
        TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            throw new IllegalStateException("Petugas penetapan tidak boleh kosong");
        }
        final TransaksiLayanan x = this.service.simpanPenetapanHO(ketetapan, tok.getPetugas(), tl, awal, akhir);
        HO ho = Konfig.getInstance().getGson().fromJson(x.getKonten(), HO.class);
        map.put("data", ho);
        map.put("error", "");
        //kirim pesan
        DataPenduduk dp = tl.getTerimaBerkas().getPemohon();
        Konfigurasi k = new Konfigurasi();
        if (!dp.getNomorHp().equals("") && k.isReadySms()) {
            String pesan = k.getTemplate().replace("{LAYANAN}", "Izin Gangguan");
            kirimSMS(dp.getNomorHp(), pesan);
        }
        return map;
    }

    private void kirimSMS(String nomo, String pesan) {
        KirimPesan kp = new KirimPesan(nomo, pesan);
        Thread t = new Thread(kp);
        t.start();
    }

    public Map<String, Object> simpanPenetapanPenelitian(final Date tanggal, final String nomor, final String token) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            throw new IllegalStateException("Petugas penetapan tidak boleh kosong");
        }
        final TransaksiLayanan x = this.service.simpanPenetapanPenelitian(tanggal, tok.getPetugas(), tl);
        final RekomendasiPenelitian tr = Konfig.getInstance().getGson().fromJson(x.getKonten(), RekomendasiPenelitian.class);
        map.put("data", tr);
        map.put("error", "");
        return map;
    }

    public Map<String, Object> simpanPenetapanDispensasiMenikah(final Date tanggal, final String nomor, final String token) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            throw new IllegalStateException("Petugas penetapan tidak boleh kosong");
        }
        final TransaksiLayanan x = this.service.simpanPenetapanDispensasiMenikah(tanggal, tok.getPetugas(), tl);
        final DispensasiNikah dn = Konfig.getInstance().getGson().fromJson(x.getKonten(), DispensasiNikah.class);
        map.put("data", dn);
        map.put("error", "");
        return map;
    }

    public Map<String, Object> simpanPenetapanDomisiliUsaha(final Date ketetapan, final Date berlaku, final Date selesai, final String token, final String nomor) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            map.put("error", "Petugas penetapan tidak boleh kosong");
            return map;
        }
        final TransaksiLayanan x = this.service.simpanPenetapanDomisiliUsaha(ketetapan, berlaku, selesai, tl, tok.getPetugas());
        final DomisiliUsaha usaha = Konfig.getInstance().getGson().fromJson(x.getKonten(), DomisiliUsaha.class);
        map.put("data", usaha);
        map.put("error", "");
        //kirim pesan
        DataPenduduk dp = tl.getTerimaBerkas().getPemohon();
        Konfigurasi k = new Konfigurasi();
        if (!dp.getNomorHp().equals("") && k.isReadySms()) {
            String pesan = k.getTemplate().replace("{LAYANAN}", "Surat Keterangan Domisili Usaha");
            kirimSMS(dp.getNomorHp(), pesan);
        }
        return map;
    }

    public Map<String, Object> simpanPenetapanIUM(final Date ketetapan, final String token, final String nomor, final Date berlaku, final Date akhir) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            throw new IllegalStateException("Petugas penetapan tidak boleh kosong");
        }
        final TransaksiLayanan x = this.service.simpanPenetapanIUM(ketetapan, tok.getPetugas(), tl, berlaku, akhir);
        if (!x.getStatusPekerjaan().equals(StatusPekerjaan.Pemeriksaan)) {
            final IUM ium = Konfig.getInstance().getGson().fromJson(x.getKonten(), IUM.class);
            map.put("data", ium);
            map.put("suratDesa", x.getTerimaBerkas().getNomorDesa());
            map.put("tanggalDesa", x.getTerimaBerkas().getTglSuratDesa());
        }
        map.put("error", "");
        //kirim pesan
        DataPenduduk dp = tl.getTerimaBerkas().getPemohon();
        Konfigurasi k = new Konfigurasi();
        if (!dp.getNomorHp().equals("") && k.isReadySms()) {

            String pesan = k.getTemplate().replace("{LAYANAN}", "Izin Usaha Mikro");
            kirimSMS(dp.getNomorHp(), pesan);
        }
        return map;
    }

    public Map<String, Object> simpanPenetapanIMB(final Date selesai, final Date mulai, final String nomor, final String token) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            throw new IllegalStateException("Petugas penetapan tidak boleh kosong");
        }
        final TransaksiLayanan x = this.service.simpanPenetapanIMB(selesai, mulai, tok.getPetugas(), tl);
        if (x.getStatusPekerjaan().equals(StatusPekerjaan.Pemeriksaan)) {
            map.put("status", "berhasil dikembalikan");
        } else {
            final IMB imb = Konfig.getInstance().getGson().fromJson(x.getKonten(), IMB.class);
            map.put("data", imb);
        }
        map.put("error", "");
        //kirim pesan
        DataPenduduk dp = tl.getTerimaBerkas().getPemohon();
        Konfigurasi k = new Konfigurasi();
        if (!dp.getNomorHp().equals("") && k.isReadySms()) {

            String pesan = k.getTemplate().replace("{LAYANAN}", "IMB");
            kirimSMS(dp.getNomorHp(), pesan);
        }
        return map;
    }
    public Map<String,Object> simpanPenetapanSPPL(String nomorBerkas,String token){
        Map<String,Object> map = new HashMap<>();
        if(nomorBerkas==null){
            map.put("error", "data nomor berkas tidak valid");
            return map;
        }
        TransaksiLayanan tl = this.trxDao.findOne(nomorBerkas);
        if(tl==null){
             map.put("error", "maaf data transaksi ini tidak ditemukan silahkan registrasikan data anda terlebih dahulu");
            return map;
        }
        Token tkn=tokenDao.findOne(token);
        if(tkn==null){
            map.put("error", "silahkan relogin");
            return map;
        }
        try {
            TransaksiLayanan x=service.simpanPenetapanSPPL(tkn, nomorBerkas);
            map.put("data", x);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", "failed to save sppl");
            return map;
        }
        return map;
    }
    public Map<String, Object> simpanPenetapan(final String nomorBerkas, final String command, final String token) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomorBerkas);
        if (tl == null) {
            map.put("error", "maaf data transaksi ini tidak ditemukan silahkan registrasikan data anda terlebih dahulu");
            return map;
        }
        final Token toke = this.tokenDao.findOne(token);
        if (toke == null) {
            map.put("error", "silahkan relogin");
            return map;
        }
        if (command.equalsIgnoreCase("tetapkan")) {
            final TransaksiLayanan trx = this.service.simpanPenetapanUmum(new Date(), tl, toke.getPetugas());
            if (trx.getPenetapanBerkas() != null) {
                map.put("ketetapan", trx.getPenetapanBerkas().getNomor());
                map.put("error", "");
            } else {
                map.put("error", "data tidak bisa disimpan. periksa kembali");
            }
            return map;
        }
        return this.kembaliPeriksa(nomorBerkas);
    }

    public Map<String, Object> simpanPenetapan(final String nomorBerkas, final String token, final Date masaberlaku, final Date mulaiBerlaku, final List<String> items) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomorBerkas);
        if (tl == null) {
            throw new IllegalStateException("maaf data transaksi ini tidak ditemukan silahkan registrasikan data anda terlebih dahulu");
        }
        final Token toke = this.tokenDao.findOne(token);
        if (toke == null) {
            throw new IllegalStateException("Silahkan relogin ");
        }
        final PenetapanBerkas p = new PenetapanBerkas();
        p.setPetugas(toke.getPetugas());
        p.setItems(new ArrayList<>());
        items.stream().forEach(x -> {
            Berkas b = new Berkas();
            b.setDokumen(x);
            b.setOptional(false);
            b.setValid(true);
            p.getItems().add(b);
        });
        p.setTanggal(new Date());
        p.setValidasi(true);
        tl.setPenetapanBerkas(p);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        return map;
    }

    private Map<String, Object> handleReport(final TransaksiLayanan tl) {
        final Map<String, Object> map = new HashMap<>();
        final String kode = tl.getLayanan().getKode();
        switch (kode) {
            case "503": {
                this.isiHO(tl, map);
                break;
            }
            case "536": {
                this.isiDomisiliUsaha(tl, map);
                break;
            }
            case "648": {
                this.isiIMB(tl, map);
                break;
            }
            case "518": {
                this.isiIUM(tl, map);
                break;
            }
        }
        return map;
    }

    public void isiIUM(final TransaksiLayanan tl, final Map<String, Object> map) {
        final IUM ium = Konfig.getInstance().getGson().fromJson(tl.getKonten(), IUM.class);
        map.put("namaPenduduk", ium.getDp().getNama());
        map.put("nik", ium.getDp().getNik());
        map.put("alamatPemohon", this.getAlamatPenduduk(ium.getDp()));
        map.put("namaUsaha", ium.getNamaUsaha());
        map.put("bentukUsaha", ium.getBentukUsaha());
        map.put("npwp", ium.getNpwp());
        map.put("kegiatanUsaha", ium.getKegiatanUsaha());
        map.put("saranaUsaha", ium.getSaranaUsaha());
        map.put("alamatusaha", ium.getAlamatUsaha());
        map.put("jumlahModal", ium.getJumlahModal());
        map.put("nomorDaftar", tl.getNomor());
        map.put("tanggalKetetapan", tl.getTanggalKetetapan());
        map.put("camat", ium.getCamat());
        map.put("nipcamat", ium.getNipCamat());
    }

    public void isiIMB(final TransaksiLayanan tl, final Map<String, Object> map) {
        final IMB im = Konfig.getInstance().getGson().fromJson(tl.getKonten(), IMB.class);
        map.put("namaPemohon", im.getPemohon().getNama());
        map.put("pemohonAlamat", this.getAlamatPenduduk(im.getPemohon()));
        map.put("namaIjin", im.getIjin().getNama());
        map.put("alamatIjin", this.getAlamatPenduduk(im.getIjin()));
        map.put("statusTanah", im.getStatusTanah());
        map.put("nomor", im.getNomorHM());
        map.put("persil", im.getNomorPersil());
        map.put("atasNama", im.getAtasNamaTanah());
        map.put("jalan", im.getJalan());
        map.put("nomor", tl.getNomorKetetapan());
        map.put("luasTanah", im.getLuasTanah());
        map.put("luasBangunan", im.getLuasBangunan());
        map.put("kelasTanah", im.getKelas());
        map.put("desa", im.getKelurahan());
        map.put("pondasi", im.getPondasi());
        map.put("kerangkaUtama", im.getKerangka());
        map.put("dinding", im.getDinding());
        map.put("kerangkaAtap", im.getKerangkaAtap());
        map.put("fungsiBangunan", im.getGunaBangunan());
        map.put("pondasiTerdepan", im.getJarakJalan());
        map.put("gsp", im.getGsp());
        map.put("persenLantai", im.getPersenLantai());
        map.put("persenBangunan", im.getPersenBangunan());
        map.put("retribusi", im.getRetribusi());
        map.put("berlakuMulai", im.getBerlakuMulai());
        map.put("berlakuSampai", im.getBerlakuSampai());
    }

    public void isiHO(final TransaksiLayanan tl, final Map<String, Object> map) {
        final HO ho = Konfig.getInstance().getGson().fromJson(tl.getKonten(), HO.class);
        map.put("mulaiberlaku", tl.getBerlakuSurat());
        map.put("akhirberlaku", tl.getExpiredSurat());
        map.put("nomor", tl.getNomorKetetapan());
        map.put("namaijin", ho.getIjin().getNama());
        map.put("alamatIjin", this.getAlamatPenduduk(ho.getIjin()));
        map.put("namaUsaha", ho.getNamaPerusahaan());
        map.put("jenisUsaha", ho.getJenisUsaha());
        map.put("luas", ho.getLuas());
        map.put("lokasi", ho.getLokasi());
        map.put("namaPemohon", ho.getPemohon().getNama());
        map.put("alamatPemohon", this.getAlamatPenduduk(ho.getPemohon()));
        map.put("retribusi", ho.getRetribusi());
        map.put("tanggalKetetapan", tl.getTanggalKetetapan());
    }

    public void isiDomisiliUsaha(final TransaksiLayanan tl, final Map<String, Object> map) {
        final Gson gson = Konfig.getInstance().getGson();
        final DomisiliUsaha dm = gson.fromJson(tl.getKonten(), DomisiliUsaha.class);
        map.put("nomor", tl.getNomorKetetapan());
        map.put("namaPengusaha", dm.getPenduduk().getNama());
        map.put("nik", dm.getPenduduk().getNik());
        map.put("alamatPengusaha", this.getAlamatPenduduk(dm.getPenduduk()));
        map.put("namaPerusahaan", dm.getNamaPerusahaan());
        map.put("jenisUsaha", dm.getJenisUsaha());
        map.put("alamatUsaha", dm.getAlamatUsaha());
        map.put("karyawan", dm.getJumlahKaryawan());
        map.put("rekomendasi", dm.getRekomendasi());
        map.put("tanggalBerlaku", dm.getTanggalBerlaku());
        map.put("tanggalselesai", dm.getTanggalHabisBerlaku());
        map.put("tanggalKetetapan", tl.getTanggalKetetapan());
        map.put("camat", dm.getCamat());
        map.put("jabatan", dm.getJabatanCamat());
        map.put("nip", dm.getNipCamat());
    }

    public String getAlamatPenduduk(final DataPenduduk dp) {
        final StringBuilder sb = new StringBuilder();
        sb.append(dp.getLingk()).append(" ").append(dp.getRt()).append("/");
        sb.append(dp.getRw()).append(" ").append(dp.getDesa());
        return sb.toString();
    }
}
