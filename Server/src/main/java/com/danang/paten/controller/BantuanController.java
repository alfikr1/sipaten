// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.controller;

import org.springframework.data.domain.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.RekomendasiBantuan;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import com.danang.paten.repository.PendudukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.ProposalService;
import org.springframework.stereotype.Controller;

@Controller
public class BantuanController
{
    @Autowired
    private ProposalService service;
    @Autowired
    private PendudukRepository pendudukDao;
    
    public Map<String, Object> simpanBantuan(final Date tanggal, final String nik, final String jenisBantuan, final int jumlah, final String tujuan, final String keterangan) {
        final Map<String, Object> map = new HashMap<String, Object>();
        final RekomendasiBantuan rb = new RekomendasiBantuan();
        rb.setTanggal(tanggal);
        rb.setDp(this.pendudukDao.findOne(nik));
        rb.setJenisBantuan(jenisBantuan);
        rb.setJumlah(jumlah);
        rb.setTujuanProposal(tujuan);
        rb.setKeterangan(keterangan);
        final RekomendasiBantuan x = this.service.simpanBantuanDao(rb);
        map.put("data", x);
        map.put("error", "");
        return map;
    }
    
    public Map<String, Object> findBantuan(final String cari, final Pageable p) {
        final Map<String, Object> map = new HashMap<String, Object>();
        Page<RekomendasiBantuan> rk;
        if (StringUtils.isEmpty(cari)) {
            rk = this.service.findAll(p);
        }
        else {
            rk = this.service.findBantuans(cari, p);
        }
        map.put("data", rk.getContent());
        map.put("total", rk.getTotalPages());
        map.put("size", rk.getTotalElements());
        return map;
    }
}
