// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import org.springframework.data.domain.Page;
import com.danang.paten.domain.Token;
import java.util.HashMap;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import org.mindrot.jbcrypt.BCrypt;
import com.danang.paten.domain.Petugas;
import com.danang.paten.repository.TokenRepository;
import com.danang.paten.service.PetugasService;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.PetugasRepository;
import org.springframework.stereotype.Controller;

@Controller
public class PetugasController {

    @Autowired
    private PetugasRepository petugasDao;
    @Autowired
    private PetugasService service;
    @Autowired
    private TokenRepository tokenDao;

    public Petugas login(final String kode, final String password) {
        final Petugas p = this.petugasDao.findByUser(kode);
        if (p == null) {
            return null;
        }
        if (!BCrypt.checkpw(password, p.getPsw())) {
            return null;
        }
        return p;
    }

    public Map<String, Object> findAll(final String cari, final String keypair, final Pageable pageable) {
        final Map<String, Object> map = new HashMap<>();
        final Token tken = this.tokenDao.findOne(keypair);
        if (tken == null || tken.getPetugas() == null) {
            map.put("error", "Data request tidak valid");
            return map;
        }
        Page<Petugas> page;
        if (cari.equals("")) {
            page = (this.petugasDao).findAll(pageable);
        } else {
            page = this.service.cariData(cari, pageable);
        }
        map.put("data", page.getContent());
        map.put("totalPage", page.getTotalPages());
        map.put("error", "");
        return map;
    }

    public Petugas update(final Petugas p) {
        final Petugas x = this.petugasDao.findOne(p.getKode());
        if (x == null) {
            throw new IllegalStateException("staf dengan kode " + p.getKode() + " tidak ditemukan");
        }
        p.setKode(x.getKode());
        return this.petugasDao.save(p);
    }

    public Petugas findOne(final String nip, final String keypair) {
        final Token token = this.tokenDao.findOne(keypair);
        if (token == null || token.getPetugas() == null) {
            return null;
        }
        return this.petugasDao.findOne(nip);
    }

    public void hapusData(final String id) {
        final Petugas p = this.petugasDao.findOne(id);
        if(p==null){
            throw new IllegalStateException("kode petugas tidak ditemukan");
        }
        petugasDao.delete(p);
    }

    public Petugas save(final Petugas p) {
        final Petugas x = this.petugasDao.findOne(p.getKode());
        if (x != null) {
            throw new IllegalStateException("Kode petugas telah digunakan, silahkan periksa data anda kembali");
        }
        p.setSalt(BCrypt.gensalt());
        p.setPsw(BCrypt.hashpw(p.getPsw(), p.getSalt()));
        return this.petugasDao.save(p);
    }
}
