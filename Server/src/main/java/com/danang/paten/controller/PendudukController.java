// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import com.danang.paten.domain.Token;
import org.springframework.data.domain.Page;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.Gson;
import com.danang.paten.domain.DataPenduduk;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import com.danang.paten.Konfig;
import com.danang.paten.domain.MessageFormat;
import com.danang.paten.repository.TokenRepository;
import com.danang.paten.service.PendudukService;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.PendudukRepository;
import org.springframework.stereotype.Controller;

@Controller
public class PendudukController {

    @Autowired
    private PendudukRepository dpDao;
    @Autowired
    private PendudukService service;
    @Autowired
    private TokenRepository tokenDao;
    private @Autowired LoginController lc;

    public void hapus(final String nik) {
        this.service.hapusData(nik);
    }

    public MessageFormat handlePenduduk(final MessageFormat mf) {        
        final Gson gson = Konfig.getInstance().getGson();
        final MessageFormat res = new MessageFormat();
        
        final String method = mf.getMethod();
        switch (method) {
            case "All": {
                final String[] a = mf.getMsg().split("#");
                final int size = Integer.parseInt(a[0]);
                final int page = Integer.parseInt(a[1]);
                res.setValue(this.findAllDataPenduduk(new PageRequest(page, size)));
                res.setCommand("reply");
                break;
            }
            case "Find": {
                final String[] a = mf.getMsg().split("#");
                if (a.length != 3) {
                    res.setError("Data tidak valid");
                    return res;
                }
                final int size = Integer.parseInt(a[0]);
                final int page = Integer.parseInt(a[1]);
                final String cari = a[2];
                res.setCommand("reply");
                break;
            }
            case "Update": {
                final DataPenduduk dp = gson.fromJson(mf.getMsg(), DataPenduduk.class);
                try {
                    this.updatePenduduk(dp);
                } catch (Exception e) {
                    res.setError(e.getMessage());
                }
                break;
            }
            case "Add": {
                final DataPenduduk ad = gson.fromJson(mf.getMsg(), DataPenduduk.class);
                try {
                    this.simpanPenduduk(ad);
                } catch (Exception e2) {
                    res.setError(e2.getMessage());
                }
                break;
            }
            case "Delete": {
                final String id = mf.getMsg();
                final DataPenduduk d = this.dpDao.findOne(id);
                if (d != null) {
                    dpDao.delete(d);
                    break;
                }
                res.setError("data tidak ditemukan");
                break;
            }
            case "FindOne": {
                final DataPenduduk x = this.dpDao.findOne(mf.getMsg());
                if (x != null) {
                    res.setValue(x);
                    break;
                }
                res.setError("data tidak ditemukan");
                break;
            }
        }
        return res;
    }

    public Map<String, Object> simpanPenduduk(final DataPenduduk dp) {
        final Map<String, Object> map = new HashMap<>();
        dp.setDesa(dp.getDesa().toUpperCase());
        dp.setKecamatan(dp.getKecamatan().toUpperCase());
        final DataPenduduk x = this.dpDao.findOne(dp.getNik());
        if (x != null) {
            throw new IllegalStateException("nik sudah ada");
        }
        this.dpDao.save(dp);
        map.put("error", "");
        return map;
    }

    public Map<String, Object> updatePenduduk(final DataPenduduk dp) {
        final Map<String, Object> map = new HashMap<>();
        final DataPenduduk x = dpDao.findOne(dp.getNik());
        if (x == null) {
            map.put("error", "Data tidak ditemukan");
            return map;
        }
        dp.setNik(x.getNik());
        this.dpDao.save(dp);
        map.put("error", "");
        return map;
    }

    public List<DataPenduduk> findAll() {
        return (this.dpDao).findAll();
    }

    public Map<String, Object> findAllDataPenduduk(final Pageable pageable) {
        final Map<String, Object> map = new HashMap<>();
        final Page<DataPenduduk> dp = (this.dpDao).findAll(pageable);
        map.put("data", dp.getContent());
        map.put("element", dp.getTotalElements());
        map.put("page", dp.getTotalPages());
        return map;
    }

    public Map<String, Object> findDataPenduduk(final String cari, final String tk, final Pageable pageable) {
        final Map<String, Object> map = new HashMap<>();
        final Token token = this.tokenDao.findOne(tk);
        if (token == null || token.getPetugas() == null) {
            map.put("error", "Please login first");
            return map;
        }
        final Page<DataPenduduk> dp = this.service.cariData(cari, pageable);
        map.put("data", dp.getContent());
        map.put("element", dp.getTotalElements());
        map.put("page", dp.getTotalPages());
        map.put("error", "");
        return map;
    }

    public DataPenduduk findOne(final String nik, final String tk) {
        final Map<String, Object> map = new HashMap<>();
        final Token token = this.tokenDao.findOne(tk);
        if (token == null || token.getPetugas() == null) {
            throw new IllegalStateException("silahkan login terlebih dahulu");
        }
        return this.dpDao.findOne(nik);
    }
}
