// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import com.hazelcast.core.IAtomicLong;
import java.util.concurrent.BlockingQueue;
import com.danang.paten.Konfig;
import com.danang.paten.domain.Antrian;
import java.util.concurrent.TimeUnit;
import com.hazelcast.core.IMap;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.LoketRepository;
import com.danang.paten.service.AntrianService;
import java.util.Date;
import org.springframework.stereotype.Controller;

@Controller
public class AntrianController {

    @Autowired
    private LoketRepository loketDao;
    @Autowired
    private AntrianService serviceAntrian;
    public void initInstance() {
        this.loketDao.findAll().forEach(x -> {
            Config config = new Config();
            config.setInstanceName(x.getKode());
            HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> instance.shutdown()));
        });
    }
    public void simpanAntrian(String jenis,String nomor,int loket,Date tanggal,String filename){
        Antrian antrian = new Antrian();
        antrian.setFoto(filename);
        antrian.setJenis(jenis);
        antrian.setAntrian(nomor);
        antrian.setLoket(loket);
        antrian.setTanggal(tanggal);
        serviceAntrian.simpanData(antrian);
    }
    

    public Object getNextAntrean(final String kode, final long antre) throws InterruptedException {
        final HazelcastInstance instance = Hazelcast.getHazelcastInstanceByName(kode);
        final IMap<Long, Object> im = instance.getMap(kode);
        return im.get(antre);
    }

    public void setNextAntrean(final String kode, final Object obj) {
        final HazelcastInstance instance = Hazelcast.getHazelcastInstanceByName(kode);
        final IMap<Long, Object> im = instance.getMap(kode);
        im.put(this.regNext(kode), obj);
    }

    public long getNext(final String kode) throws InterruptedException {
        HazelcastInstance instance = Hazelcast.getHazelcastInstanceByName(kode);
        BlockingQueue<Long> bq = instance.getQueue(kode);
        final Long nomor = bq.poll(2L, TimeUnit.SECONDS);
        Konfig.getInstance().getVideoMain().setAntrean(String.valueOf(nomor), kode);
        return nomor;
    }

    public long regNext(final String kode) {
        final HazelcastInstance instance = Hazelcast.getHazelcastInstanceByName(kode);
        final IAtomicLong antrean = instance.getAtomicLong(kode);
        final BlockingQueue<Long> bq = instance.getQueue(kode);
        final long nomor = antrean.incrementAndGet();
        bq.add(nomor);
        return nomor;
    }
}
