/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.controller;

import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.ftplet.UserManager;

/**
 *
 * @author danang
 */
public class FtpUserManager implements UserManager {

    @Override
    public User getUserByName(String string) throws FtpException {
        if(string.equalsIgnoreCase("admin")){
            return new FtpUser();
        }
        return null;
    }

    @Override
    public String[] getAllUserNames() throws FtpException {
        FtpUser fu = new FtpUser();
        String result[]={"admin"};
        return result;
    }

    @Override
    public void delete(String string) throws FtpException {
        
    }

    @Override
    public void save(User user) throws FtpException {
    }

    @Override
    public boolean doesExist(String string) throws FtpException {
        return string.equalsIgnoreCase("admin");
    }

    @Override
    public User authenticate(Authentication a) throws AuthenticationFailedException {
        return new FtpUser();
    }

    @Override
    public String getAdminName() throws FtpException {
        return "admin";
    }

    @Override
    public boolean isAdmin(String string) throws FtpException {
        return string.equalsIgnoreCase("admin");
    }
    
}
