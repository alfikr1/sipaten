// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.controller;

import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.danang.paten.domain.Kecamatan;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.KecamatanRepository;
import org.springframework.stereotype.Controller;

@Controller
public class SerialController
{
    @Autowired
    private KecamatanRepository kecamatanDao;
    
    public boolean isSerialValid() {
        final Kecamatan kec = this.kecamatanDao.findOne("001");
        if (kec == null || kec.getSerial() == null) {
            return false;
        }
        final String serial = kec.getSerial();
        return true;
    }
    
    public boolean isLinux() {
        final String os = System.getProperty("os.name");
        return os.contains("Linux") || os.equalsIgnoreCase("Linux");
    }
    
    private String getDiskSerial() throws IOException {
        final String command = "udevadm info --query=all --name=/dev/sda | grep ID_SERIAL";
        final Process process = Runtime.getRuntime().exec(command);
        final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String s;
        while ((s = reader.readLine()) != null) {
            if (s.contains("ID_SERIAL_SHORT")) {
                return s;
            }
        }
        return null;
    }
}
