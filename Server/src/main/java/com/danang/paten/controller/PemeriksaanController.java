// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import com.danang.paten.enumerasi.StatusPekerjaan;
import com.danang.paten.domain.PeriksaBerkas;
import com.danang.paten.form.HO;
import com.danang.paten.form.IUM;
import com.danang.paten.form.IMB;
import com.danang.paten.form.DomisiliUsaha;
import com.danang.paten.form.RekomendasiPenelitian;
import com.danang.paten.form.DispensasiNikah;
import java.util.Date;
import com.danang.paten.domain.Layanan;
import com.danang.paten.domain.Petugas;
import org.springframework.data.domain.Page;
import com.google.gson.Gson;
import com.danang.paten.form.DokumenBerkas;
import spark.utils.StringUtils;
import com.danang.paten.domain.Token;
import com.danang.paten.Konfig;
import org.springframework.data.domain.Pageable;
import java.util.ArrayList;
import com.danang.paten.domain.BerkasPeriksa;
import com.danang.paten.domain.Berkas;
import java.util.List;
import com.danang.paten.domain.TerimaBerkas;
import com.danang.paten.domain.TransaksiLayanan;
import java.util.HashMap;
import java.util.Map;
import com.danang.paten.domain.MessageFormat;
import com.danang.paten.domain.Roles;
import com.danang.paten.form.SPPL;
import com.danang.paten.repository.PendudukRepository;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.PetugasRepository;
import com.danang.paten.repository.TokenRepository;
import com.danang.paten.repository.TransaksiLayananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.TransaksiService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

@Controller
public class PemeriksaanController {

    @Autowired
    private TransaksiService service;
    @Autowired
    private TransaksiLayananRepository trxDao;
    @Autowired
    private TokenRepository tokenDao;
    @Autowired
    private LayananRepository layananDao;
    @Autowired
    private PendudukRepository pDao;
    private @Autowired
    PetugasRepository petugasDao;
    private final Logger log = Logger.getLogger(PemeriksaanController.class);

    public MessageFormat handle(final MessageFormat mf) {
        final MessageFormat res = new MessageFormat();
        return res;
    }

    public Map<String, Object> getPemeriksaan(String nomor) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "no data retreive");
            return map;
        }
        final TerimaBerkas tb = tl.getTerimaBerkas();
        map.put("ijin", tb.getIjin());
        map.put("pengaju", tb.getPemohon());
        map.put("nomor terima", tb.getNomor());
        map.put("nomorDesa", tb.getNomorDesa());
        map.put("tanggalDesa", tb.getTglSuratDesa());
        map.put("items", this.getBerkasPeriksa(tb.getBerkas()));
        map.put("layanan", tl.getLayanan());
        map.put("error", "");
        return map;
    }

    private List<BerkasPeriksa> getBerkasPeriksa(final List<Berkas> tb) {
        final List<BerkasPeriksa> items = new ArrayList<>();
        tb.stream().map(b -> {
            BerkasPeriksa pb2 = new BerkasPeriksa();
            pb2.setBerkas(b);
            return pb2;
        }).map(pb -> {
            pb.setValid(false);
            return pb;
        }).map(pb -> {
            pb.setCatatan("");
            return pb;
        }).forEach(pb -> items.add(pb));
        return items;
    }

    public Map<String, Object> cariPeriksa(final String cari, final String petugas, final Pageable pageable) {
        final Gson gson = Konfig.getInstance().getGson();
        System.out.println("cari data " + cari);
        final Token token = this.tokenDao.findOne(petugas);
        final Map<String, Object> map = new HashMap<>();
        log.debug("Cari Periksa " + cari);
        if (token == null) {
            map.put("error", "Please login first/restart the apps");
            return map;
        }
        Page<TransaksiLayanan> page;
        if (StringUtils.isEmpty(cari)) {
            page = this.service.findallPeriksa(pageable);
        } else {
            page = this.service.cariPeriksaData(cari, pageable);
        }
        //log.debug("result : "+page.getContent());
        final List<DokumenBerkas> items = new ArrayList<>();
        map.put("totalPage", page.getTotalPages());
        map.put("ukuran", page.getTotalElements());
        Token toket = tokenDao.findOne(petugas);
        Petugas p = petugasDao.findOne(toket.getPetugas().getKode());
        System.out.println(p.getKode());
        page.getContent().stream().forEach(x -> {
            if (x.getLayanan().getKode().equals("503/REKOM")) {
                System.out.println("marai error " + x.getNomor());
            }
            if (this.isboleh(x.getLayanan(), p)) {
                DokumenBerkas db = new DokumenBerkas();
                if (x.getLayanan() != null) {
                    db.setLayanan(x.getLayanan().getNamaLayanan());
                }
                db.setNomor(x.getNomor());
                if (x.getPenduduk() != null) {
                    db.setPemohon(x.getPenduduk().getNama());
                }
                db.setTanggalRegistrasi(x.getTerimaBerkas().getTanggalTerima());
                items.add(db);
            }
        });
        map.put("error", "");
        map.put("data", items);
        return map;
    }

    private boolean isboleh(final Layanan x, final Petugas p) {
        Gson gs = new Gson();
        for (Roles r : p.getRoles()) {
            try {
                if (r.getLayanan().getKode().equals(x.getKode())) {
                    return true;
                }
            } catch (Exception e) {
                System.out.println(gs.toJson(r));
                log.debug("error " + r);
            }
        }
        return false;
        /*try {
            boolean result= p.getRoles().stream().anyMatch(r -> r.getLayanan().getKode().equals(x.getKode()));
            System.out.println(x.getKode() +" "+result);
            return result;
        } catch (Exception e) {
            return false;
        }*/
    }

    public Map<String, Object> simpanDispensasiMenikah(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items, final String pria, final String wanita, final Date tglNikah, final String tempat) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token toke = this.tokenDao.findOne(token);
        if (toke == null) {
            throw new IllegalStateException("Petugas pemeriksa tidak boleh kosong");
        }
        final Petugas petugas = toke.getPetugas();
        final DispensasiNikah dn = new DispensasiNikah();
        dn.setPria(this.pDao.findOne(pria));
        dn.setWanita(this.pDao.findOne(wanita));
        dn.setTempat(tempat);
        dn.setTanggalNikah(tglNikah);
        try {
            sendPeriksaMQ(nomor);
            this.service.simpanPeriksaDispensasiNikah(dn, nomor, petugas, lanjut, catatan, items);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> simpanPenelitian(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items, final RekomendasiPenelitian rp) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token toke = this.tokenDao.findOne(token);
        if (toke == null) {
            throw new IllegalStateException("Petugas pemeriksa tidak boleh kosong");
        }
        final Petugas petugas = toke.getPetugas();
        try {
            sendPeriksaMQ(nomor);
            service.simpanPeriksaPenelitan(items, tl, catatan, lanjut, rp, petugas);
            map.put("error", "");
        } catch (Exception ex) {
            map.put("error", ex.getMessage());
        }

        return map;
    }

    public Map<String, Object> simpanDomisiliUsaha(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items, final DomisiliUsaha du) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Token toke = this.tokenDao.findOne(token);
        if (toke == null) {
            map.put("error", "Petugas pemeriksa tidak boleh kosong");
            return map;
        }
        final Petugas petugas = toke.getPetugas();
        try {
            sendPeriksaMQ(nomor);
            this.service.simpanPeriksaDomisiliUsaha(du, nomor, petugas, lanjut, catatan, items);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> simpanIMB(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items, final IMB imb) {
        final Map<String, Object> map = new HashMap<>();
        final Token toke = this.tokenDao.findOne(token);
        final Petugas petugas = toke.getPetugas();
        try {
            final TransaksiLayanan tl = this.service.simpanPeriksaIMB(imb, nomor, petugas, lanjut, catatan, items);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> simpanSPPL(String token, String regNomor, SPPL sppl, List<BerkasPeriksa> items, boolean lanjut, String catatan) {
        final Map<String, Object> map = new HashMap<>();
        Token reg = tokenDao.findOne(token);
        if (reg == null) {
            map.put("error", "Token tidak valid");
            return map;
        }
        Petugas p = reg.getPetugas();
        try {
            TransaksiLayanan tl = service.simpanPeriksaSppl(sppl, regNomor, p, lanjut, items, catatan);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> simpanIUM(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items, final IUM ium) {
        final Map<String, Object> map = new HashMap<>();
        final Token toke = this.tokenDao.findOne(token);
        final Petugas petugas = toke.getPetugas();
        if (ium.getJumlahModal() > 50000000) {
            map.put("error", "Data tidak bisa diproses! Modal melebihi ketetapan");
            return map;
        }
        try {
            final TransaksiLayanan tl = this.service.simpanPeriksaIUM(ium, nomor, petugas, lanjut, catatan, items);
            sendPeriksaMQ(nomor);
            map.put("data", tl);
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> simpanHO(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items, final String index, final String namaPerusahaan, final String alamatPerusahaan, final String jenisUsaha, final double retribusi, final double luas, final double bangunan, final double investasi) {
        final Map<String, Object> map = new HashMap<>();
        final Petugas petugas = this.tokenDao.findOne(token).getPetugas();
        if (petugas == null) {
            map.put("error", "please Login");
            return map;
        }
        final HO ho = new HO();
        ho.setCatatan(catatan);
        ho.setIndexGangguan(index);
        ho.setInvestasi(investasi);
        ho.setJenisUsaha(jenisUsaha);
        ho.setLokasi(alamatPerusahaan);
        ho.setLuas(luas);
        ho.setLuasBangunan(bangunan);
        ho.setNamaPerusahaan(namaPerusahaan);
        ho.setPemeriksa(petugas);
        ho.setRetribusi(retribusi);
        ho.setTanggalPemeriksaan(new Date());
        try {
            final TransaksiLayanan x = this.service.simpanPeriksaHO(ho, nomor, petugas, lanjut, catatan, items);
            map.put("error", "");
            sendPeriksaMQ(nomor);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, String> simpanPeriksa(final Date tanggal, final String catatan, final boolean lanjut, final String nomor, final String token, final List<BerkasPeriksa> items) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        final Map<String, String> map = new HashMap<>();
        if (tl == null) {
            map.put("error", "data tidak ditemukan");
            return map;
        }
        final Petugas petugas = this.tokenDao.findOne(token).getPetugas();
        if (petugas == null) {
            map.put("error", "please Login");
            return map;
        }
        map.put("error", "");
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setTanggal(tanggal);
        pb.setCatatan(catatan);
        pb.setLanjut(lanjut);
        pb.setPetugas(petugas);
        pb.setPeriksa(items);
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        tl.setPeriksaBerkas(pb);
        final PeriksaBerkas x = this.service.simpanPeriksaBerkas(tl);
        sendPeriksaMQ(tl.getNomor());
        return map;
    }

    public void sendPeriksaMQ(String kode) {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    Konfig.getInstance().getPenetapanChannel().basicPublish("", "PENETAPAN", null, kode.getBytes());
                    
                } catch (Exception e) {
                }
            }
        };
        t.start();
    }
}
