/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.ftpserver.command.impl.MD5;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.AuthorizationRequest;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.usermanager.impl.WritePermission;

/**
 *
 * @author danang
 */
public class FtpUser implements User {

    @Override
    public String getName() {
        return "admin";
    }

    @Override
    public String getPassword() {
        return String.valueOf(MD5.encodeHex("admin".getBytes()));
    }

    @Override
    public List<Authority> getAuthorities() {
        List<Authority> items = new ArrayList<>();
        items.add(new WritePermission());
        items.add(new org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission(1, 1));
        items.add(new org.apache.ftpserver.usermanager.impl.TransferRatePermission(4098, 4098));
        return items;
    }

    @Override
    public List<Authority> getAuthorities(Class<? extends Authority> type) {
        List<Authority> items =new ArrayList<>();
        
        return items;
    }

    @Override
    public AuthorizationRequest authorize(AuthorizationRequest ar) {
        return ar;
    }

    @Override
    public int getMaxIdleTime() {
        return 60;
    }

    @Override
    public boolean getEnabled() {
        return true;
    }

    @Override
    public String getHomeDirectory() {
        return "//";
    }
    
}
