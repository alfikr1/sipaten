// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import com.danang.paten.domain.KartuKendaliSuratKeluar;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.SuratService;
import org.springframework.stereotype.Controller;

@Controller
public class SuratKeluarController {

    @Autowired
    private SuratService service;

    public Map<String, Object> simpanSuratKeluar(final KartuKendaliSuratKeluar spk) {
        return service.simpanSuratKeluar(spk);
    }

    public Map<String, Object> updateSuratKeluar(KartuKendaliSuratKeluar spk) {
        Map<String, Object> map = new HashMap<>();
        try {
            KartuKendaliSuratKeluar kkm = service.updateSuratKeluar(spk);
            map.put("error", "");
            map.put("status", "berhasil diupdate");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> report(final Date awal, final Date akhir) {
        Map<String, Object> map = new HashMap<>();
        List<KartuKendaliSuratKeluar> items = this.service.findKeluar(awal, akhir);
        map.put("data", items);
        map.put("error", "");
        return map;
    }

    public Map<String, Object> findCariData(final String cari, final Date awal, final Date akhir, Pageable pageable) {
        if (pageable == null) {
            pageable = new PageRequest(0, 25);
        }
        final Map<String, Object> map = new HashMap<>();
        Page<KartuKendaliSuratKeluar> kkp;
        if (cari.equals("")) {
            kkp = this.service.findKeluarByDate(awal, akhir, pageable);
        } else {
            kkp = this.service.findKeluarTanggal(cari, awal, akhir, pageable);
        }
        map.put("error", "");
        map.put("data", kkp.getContent());
        map.put("totalPage", kkp.getTotalPages());
        map.put("size", kkp.getTotalElements());
        return map;
    }
}
