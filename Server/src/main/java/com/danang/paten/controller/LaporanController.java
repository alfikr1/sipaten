// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import com.danang.paten.domain.MessageFormat;
import com.danang.paten.form.HO;
import com.danang.paten.form.LaporanHO;
import com.danang.paten.form.DomisiliUsaha;
import com.danang.paten.form.LaporanDomisili;
import com.google.gson.Gson;
import com.danang.paten.Konfig;
import com.danang.paten.domain.TransaksiLayanan;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import com.danang.paten.enumerasi.StatusPekerjaan;
import org.springframework.data.mongodb.core.query.Criteria;
import java.util.ArrayList;
import java.util.List;
import com.danang.paten.domain.Kecamatan;
import com.danang.paten.repository.KecamatanRepository;
import com.danang.paten.service.ReportService;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping("/user/report")
public class LaporanController {

    private final String HO_CODE = "503";
    private final String IUM_CODE = "518";
    private final String IMB_CODE = "648";
    private final String DOMISILI_CODE = "536";

    @Autowired
    private MongoTemplate template;
    @Autowired
    private PenetapanController penetapanController;
    @Autowired
    private KecamatanRepository kecamatanDao;
    private @Autowired
    ReportService service;

    @PostMapping("/layanan")
    public Map<String, Object> getLaporanLayanan(@RequestParam(name = "kode") final String kode,
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "ddMMyyyy") @RequestParam(name = "awal") Date awal,
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "ddMMyyyy") @RequestParam(name = "akhir") Date akhir) {
        final Map<String, Object> map = new HashMap<>();
        final List<Kecamatan> kc = (this.kecamatanDao).findAll();
        DateTime ak = new DateTime(akhir);
        if (!kc.isEmpty()) {
            final Kecamatan c = kc.get(0);
            map.put("camat", c.getCamat());
            map.put("jabatan", c.getJabatan());
            map.put("nip", c.getNip());
        } else {
            map.put("error", "mohon untuk mensetting kecamatan di server");
            return map;
        }
        switch (kode) {
            case DOMISILI_CODE: {
                map.put("data", this.getDomisiliItems(awal, ak.plusDays(1).toDate()));
                map.put("error", "");
                return map;
            }
            case IMB_CODE: {
                try {
                    map.put("data", service.getIMBItems(awal, ak.plusDays(1).toDate()));
                    map.put("error", "");
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                    map.put("error", "data tidak valid");
                }
                return map;
            }
            case HO_CODE: {
                map.put("data", service.getHOItems(awal, ak.plusDays(1).toDate()));
                map.put("error", "");
                return map;
            }
            case IUM_CODE: {
                map.put("data", service.getLaporanIUM(awal, ak.plusDays(1).toDate()));
                map.put("error", "");
                return map;
            }
            default: {
                map.put("data", service.getLaporanUmum(awal, ak.plusDays(1).toDate(), kode));
                map.put("error", "");
                return map;
            }
        }
    }

    private Page<LaporanDomisili> getDomisiliItems(final Date awal, final Date akhir, Pageable page) {
        final List<LaporanDomisili> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("536"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        long total = template.count(q, TransaksiLayanan.class);
        q.with(page);
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        final Gson gson = Konfig.getInstance().getGson();
        tls.stream().forEach(tl -> {
            DomisiliUsaha du = gson.fromJson(tl.getKonten(), DomisiliUsaha.class);
            LaporanDomisili ld = new LaporanDomisili();
            ld.setAlamatPemohon(this.penetapanController.getAlamatPenduduk(du.getPenduduk()));
            ld.setAlamatUsaha(du.getAlamatUsaha());
            ld.setJenisUsaha(du.getJenisUsaha());
            ld.setJumlahKaryawan(du.getJumlahKaryawan());
            ld.setKeterangan(tl.getCatatan());
            ld.setMasaBerlaku(du.getTanggalHabisBerlaku());
            ld.setNamaPemohon(du.getPenduduk().getNama());
            ld.setNamaUsaha(du.getNamaPerusahaan());
            ld.setNomorIjin(tl.getPenetapanBerkas().getNomor());
            ld.setTanggal(tl.getPenetapanBerkas().getTanggal());
            items.add(ld);
        });
        return new PageImpl(items, page, total);
    }

    private List<LaporanDomisili> getDomisiliItems(final Date awal, final Date akhir) {
        final List<LaporanDomisili> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("536"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        final Gson gson = Konfig.getInstance().getGson();
        tls.stream().forEach(tl -> {
            DomisiliUsaha du = gson.fromJson(tl.getKonten(), DomisiliUsaha.class);
            LaporanDomisili ld = new LaporanDomisili();
            ld.setAlamatPemohon(this.penetapanController.getAlamatPenduduk(du.getPenduduk()));
            ld.setAlamatUsaha(du.getAlamatUsaha());
            ld.setJenisUsaha(du.getJenisUsaha());
            ld.setJumlahKaryawan(du.getJumlahKaryawan());
            ld.setKeterangan(tl.getCatatan());
            ld.setMasaBerlaku(du.getTanggalHabisBerlaku());
            ld.setNamaPemohon(du.getPenduduk().getNama());
            ld.setNamaUsaha(du.getNamaPerusahaan());
            ld.setNomorIjin(tl.getPenetapanBerkas().getNomor());
            ld.setTanggal(tl.getPenetapanBerkas().getTanggal());
            items.add(ld);
        });
        return items;
    }

    public Page<LaporanHO> getHOItems(Date awal, Date akhir, Pageable page) {
        List<LaporanHO> items = new ArrayList<>();
        Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("510.4"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        long total = template.count(q, TransaksiLayanan.class);
        q.with(page);
        List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        final Gson gson = Konfig.getInstance().getGson();
        tls.stream().forEach(tl -> {
            LaporanHO lh = new LaporanHO();
            lh.setPemohon(tl.getTerimaBerkas().getIjin().getNama());
            lh.setAlamatPemohon(this.penetapanController.getAlamatPenduduk(tl.getTerimaBerkas().getIjin()));
            HO ho = gson.fromJson(tl.getKonten(), HO.class);
            lh.setNamaUsaha(ho.getNamaPerusahaan());
            lh.setAlamatUsaha(ho.getLokasi());
            lh.setIndexGangguan(ho.getIndexGangguan());
            lh.setJenisUsaha(ho.getJenisUsaha());
            lh.setJumlahInvestasi(ho.getInvestasi());
            lh.setKetetapan(tl.getPenetapanBerkas().getNomor());
            lh.setLuasBangunan(ho.getLuasBangunan());
            lh.setLuasIjinGangguan(ho.getLuas());
            lh.setMulaiBerlaku(ho.getMulaiBerlaku());
            lh.setMasaBerlaku(ho.getAkhirBerlaku());
            lh.setPengajuan(tl.getTerimaBerkas().getTanggalTerima());
            lh.setPeriksa(tl.getPeriksaBerkas().getTanggal());
            lh.setTglKetatapan(tl.getPenetapanBerkas().getTanggal());
            lh.setRetribusi(ho.getRetribusi());
            items.add(lh);
        });
        return new PageImpl(items, page, total);
    }

    public MessageFormat handler(final MessageFormat mf) {
        final MessageFormat res = new MessageFormat();
        final String method = mf.getMethod();
        switch (method) {
            case "LaporanLayanan": {
            }
        }
        return res;
    }
}
