// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.controller;

import com.danang.paten.domain.Petugas;
import com.danang.paten.domain.Token;
import com.danang.paten.domain.TransaksiLayanan;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Date;
import com.danang.paten.domain.MessageFormat;
import com.danang.paten.service.PenyerahanBerkasService;
import com.danang.paten.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.TransaksiLayananRepository;
import org.springframework.stereotype.Controller;

@Controller
public class PenyerahanBerkasController
{
    @Autowired
    private TransaksiLayananRepository trxDao;
    @Autowired
    private TokenRepository tokenDao;
    @Autowired
    private PenyerahanBerkasService service;
    
    public MessageFormat handle(final MessageFormat mf) {
        final MessageFormat res = new MessageFormat();
        mf.getMethod().getClass();
        return res;
    }
    
    public Map<String, Object> simpan(final String reg, final String penerima, final String token, final String alamat, final String hp, final Date tanggal, final List<String> items) {
        final Map<String, Object> map = new HashMap<>();
        final TransaksiLayanan tl = this.trxDao.findOne(reg);
        if (tl == null) {
            map.put("error", "tidak ada data");
            return map;
        }
        final Token tok = this.tokenDao.findOne(token);
        final Petugas p = tok.getPetugas();
        final TransaksiLayanan x = this.service.simpanPenyerahan(penerima, hp, alamat, tanggal, p, tl, items);
        if (x == null) {
            map.put("error", "registrasi penyerahan data gagal");
            return map;
        }
        map.put("error", "");
        return map;
    }
}
