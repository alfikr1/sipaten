// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import org.springframework.data.domain.PageRequest;
import com.danang.paten.Konfig;
import com.danang.paten.domain.MessageFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.domain.Token;
import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.TerimaBerkas;
import java.util.HashMap;
import java.util.Map;
import com.danang.paten.domain.Berkas;
import java.util.List;
import java.util.Date;
import com.danang.paten.repository.TokenRepository;
import com.danang.paten.repository.PendudukRepository;
import com.danang.paten.repository.PetugasRepository;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.TransaksiLayananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.TransaksiService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

@Controller
public class TerimaBerkasController {

    @Autowired
    private TransaksiService service;
    @Autowired
    private TransaksiLayananRepository trxDao;
    @Autowired
    private LayananRepository layananDao;
    @Autowired
    private LoginController lc;
    @Autowired
    private PetugasRepository petugasDao;
    @Autowired
    private PendudukRepository prDao;
    @Autowired
    private TokenRepository tokenDao;
    private Logger logger = Logger.getLogger(TerimaBerkasController.class);

    public Map<String, Object> simpanTerimaBerkas(String pemohon, String pengaju, final String token, final String disposisi, final String layanan, final String suratDesa, final Date tanggal, final List<Berkas> items, final String catatan) {
        System.out.println("layanan " + layanan);
        final Map<String, Object> map = new HashMap<>();
        final TerimaBerkas tb = new TerimaBerkas();
        tb.setLayanan(this.layananDao.findOne(layanan));
        tb.setNamaPengaju(pengaju);
        tb.setDisposisi(disposisi);
        DataPenduduk ijin = prDao.findOne(pemohon);
        if (ijin == null) {
            map.put("error", "Data pemohon tidak ditemukan");
            return map;
        }
        tb.setPemohon(prDao.findOne(pengaju));
        final Token tok = this.tokenDao.findOne(token);
        if (tok == null) {
            map.put("error", "login dulu");
            return map;
        }
        if (tok.getPetugas() == null) {
            map.put("error", "token tidak valid");
            return map;
        }
        if (suratDesa == null || suratDesa.equals("")) {
            map.put("error", "Nomor Surat Desa belum diisi");
            return map;
        }
        tb.setKeterangan(catatan);
        tb.setTanggalTerima(new Date());
        tb.setIjin(this.prDao.findOne(pemohon));
        tb.setNomorDesa(suratDesa);
        tb.setTglSuratDesa(tanggal);
        tb.setBerkas(items);
        tb.setPetugas(tok.getPetugas());
        final TransaksiLayanan tl = this.simpanTerimaBerkas(tb);
        final TerimaBerkas tb2 = tl.getTerimaBerkas();
        map.put("nomor", (tb2.getNomor() != null) ? tb2.getNomor() : "");
        map.put("namaPemohon", (tb2.getPemohon().getNama() != null) ? tb2.getPemohon().getNama() : "");
        map.put("pemohon", (tb2.getIjin().getNik() != null) ? tb2.getIjin().getNik() : "");
        map.put("petugas", (tb2.getPetugas().getNama() != null) ? tb2.getPetugas().getNama() : "");
        map.put("namaIjin", (tb2.getIjin().getNama() != null) ? tb2.getIjin().getNama() : "");
        map.put("data", tb2.getBerkas());
        if (tb.getLayanan().isLangsungTetapkan()) {
            map.put("penetapan", tl.getPenetapanBerkas().getNomor());
        }else{
            sendTerimaBerkas(tb.getNomor());
        }        
        return map;
    }

    private void sendTerimaBerkas(String kode) {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    Konfig.getInstance().getPemeriksaanChannel().basicPublish("", "PEMERIKSAAN", null, kode.getBytes());
                } catch (Exception e) {
                    logger.error("error send data to rabbit mq", e);
                }
            }

        };
        t.start();
    }

    public TransaksiLayanan simpanTerimaBerkas(final TerimaBerkas tb) {
        return this.service.simpanTerimaBerkas(tb);
    }

    public Page<TransaksiLayanan> getAllPeriksa(final Pageable pageable) {
        return this.service.getAllPeriksa(pageable);
    }

    public Page<TransaksiLayanan> findDataPeriksa(final String cari, final Pageable pageable) {
        return this.service.cariData(cari, pageable);
    }

    public MessageFormat handler(final MessageFormat mf) {
        final Gson gson = Konfig.getInstance().getGson();
        final MessageFormat res = new MessageFormat();
        if (!lc.validasiToken(mf.getToken())) {
            res.setError("Token tidak valid");
            return res;
        }
        final String command = mf.getCommand();
        switch (command) {
            case "baru": {
                final TerimaBerkas tb = gson.fromJson(mf.getMsg(), TerimaBerkas.class);
                try {
                    this.service.simpanTerimaBerkas(tb);
                } catch (Exception e2) {
                    res.setError("data registrasi salah,Silahkan ulang kembali");
                }
                break;
            }
            case "cari": {
                final String[] a = mf.getMsg().split("#");
                final int page = Integer.parseInt(a[1]);
                final int size = Integer.parseInt(a[0]);
                final String cari = a[2];
                break;
            }
            case "all": {
                final String[] a = mf.getMsg().split("#");
                final int page = Integer.parseInt(a[1]);
                final int size = Integer.parseInt(a[0]);
                res.setValue(this.service.getBerkasSerahkan(new PageRequest(page, size)));
                break;
            }
            case "cariTanggal": {
                final String[] a = mf.getMsg().split("#");
                final int page = Integer.parseInt(a[1]);
                final int size = Integer.parseInt(a[0]);
                final String cari = a[2];
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                try {
                    final Date awal = sdf.parse(a[3]);
                    sdf.parse(a[4]);
                } catch (Exception e) {
                    res.setError(e.getMessage());
                }
                break;
            }
        }
        return res;
    }
}
