// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.controller;

import java.text.SimpleDateFormat;
import org.springframework.data.domain.PageRequest;
import com.danang.paten.Konfig;
import com.google.gson.JsonObject;
import com.danang.paten.domain.MessageFormat;
import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.TerimaBerkas;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.form.PencarianData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.service.TransaksiService;
import org.springframework.stereotype.Controller;

@Controller
public class TrxController {

    @Autowired
    private TransaksiService service;

    public List<Map> getDashboardCounter() {
        return service.getDashboardTransaksi();
    }

    public Map<String, Object> getDashboardCount() {
        Map<String, Object> map = new HashMap<>();
        map.put("tahun", service.getDashboardTransaksi());
        map.put("bulan", service.getDashboardTransaksiBulanan());
        map.put("minggu", service.getDashboardTransaksiMingguan());
        map.put("harian", service.getDashboardHarian());
        return map;
    }

    public List<Map> getDashboardMingguan() {
        return service.getDashboardTransaksiMingguan();
    }

    public List<Map> getDashboardBulanan() {
        return service.getDashboardTransaksiBulanan();
    }

    public Page<TransaksiLayanan> getKembalikanBerkas(final String cari, final Pageable pageable) {
        if (cari.equals("")) {
            return this.service.temukanDataBelumDikembalikan(pageable);
        }
        return this.service.temukanDataBelumDikembalikan(cari, pageable);
    }

    public TransaksiLayanan findOne(final String nomor) {
        return this.service.findOne(nomor);
    }

    public Map<String, Object> findCariData(final String cari, final Pageable pageable) {
        final Page<TransaksiLayanan> pg = this.service.cariData(cari, pageable);
        final Map<String, Object> map = new HashMap<>();
        map.put("totalPage", pg.getTotalPages());
        map.put("totalSize", pg.getTotalElements());
        map.put("nomor", pg.getNumber());
        map.put("element", pg.getNumberOfElements());
        map.put("data", this.getItems(pg));
        return map;
    }

    public Map<String, Object> findCariData(final String cari, final Date awal, final Date akhir, final Pageable p) {
        final Map<String, Object> map = new HashMap<>();
        final Page<TransaksiLayanan> pg = this.service.cariData(cari, awal, akhir, p);
        map.put("totalPage", pg.getTotalPages());
        map.put("totalSize", pg.getTotalElements());
        map.put("data", this.getItems(pg));
        return map;
    }

    public List<PencarianData> getItems(final Page<TransaksiLayanan> pg) {
        final List<PencarianData> items = new ArrayList<>();
        pg.getContent().stream().forEach(x -> {
            PencarianData pd = new PencarianData();
            pd.setNomorRegistrasi(x.getTerimaBerkas().getNomor());
            pd.setNomorPelayanan(x.getNomor());
            pd.setNamaLayanan(x.getLayanan().getNamaLayanan());
            TerimaBerkas tb = ((x.getTerimaBerkas() != null) ? x.getTerimaBerkas() : null);
            if (tb != null) {
                DataPenduduk pemohon = ((tb.getIjin() != null) ? tb.getIjin() : null);
                if (pemohon != null) {
                    pd.setPemohon(pemohon.getNama());
                }
            }
            pd.setPetugasRegistrasi(x.getTerimaBerkas().getPetugas().getNama());
            pd.setStatusPekerjaan(x.getStatusPekerjaan());
            items.add(pd);
        });
        return items;
    }

    public Map<String, Object> getArsip(String cari, Pageable p) {
        Map<String, Object> map = new HashMap<>();
        try {
            Page<TransaksiLayanan> items = service.temukanDataDitetapkan(cari, p);
            map.put("data", items.getContent());
            map.put("total", items.getTotalPages());
            map.put("halaman", p.getPageNumber());
            map.put("error", "");
        } catch (Exception e) {
            map.put("error", e.getMessage());
        }
        return map;
    }

    public MessageFormat handle(final MessageFormat mf) {
        final MessageFormat res = new MessageFormat();
        final String method = mf.getMethod();
        switch (method) {
            case "ALL": {
                res.setCommand("Replay");
                final JsonObject obj = Konfig.getInstance().getGson().fromJson(mf.getMsg(), JsonObject.class);
                final int page = obj.get("page").getAsInt();
                final int size = obj.get("size").getAsInt();
                final Pageable p = new PageRequest(page, size);
                final String cari = obj.get("cari").getAsString();
                final Page<TransaksiLayanan> pg = this.service.cariData(cari, p);
                final Map<String, Object> map = new HashMap<>();
                map.put("totalPage", pg.getTotalPages());
                map.put("totalSize", pg.getTotalElements());
                map.put("data", this.getItems(pg));
                res.setMsg(Konfig.getInstance().getGson().toJson(map));
                break;
            }
            case "DateAll": {
                res.setCommand("Replay");
                final JsonObject obj = Konfig.getInstance().getGson().fromJson(mf.getMsg(), JsonObject.class);
                final int page = obj.get("page").getAsInt();
                final int size = obj.get("size").getAsInt();
                final Pageable p = new PageRequest(page, size);
                final String cari = obj.get("cari").getAsString();
                final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                final Map<String, Object> map = new HashMap<>();
                try {
                    final Date awal = sdf.parse(obj.get("awal").getAsString());
                    final Date akhir = sdf.parse(obj.get("akhir").getAsString());
                    final Page<TransaksiLayanan> pg = this.service.cariData(cari, awal, akhir, p);
                    System.out.println(pg.getContent().size());
                    map.put("totalPage", pg.getTotalPages());
                    map.put("totalSize", pg.getTotalElements());
                    map.put("data", this.getItems(pg));
                } catch (Exception e) {
                    res.setError("data tidak lengkap");
                }
                res.setMsg(Konfig.getInstance().getGson().toJson(map));
                break;
            }
        }
        return res;
    }
}
