/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author danang
 */
public class GoogleTts {
    private void prepareDirektori(){
        File file = new File("./Sounds");
        if(!file.exists()){
            file.mkdir();
        }
    }

    public GoogleTts() {
        prepareDirektori();
    }
    public void dataLoket(String loket) throws IOException{
        for(int i=1;i<=200;i++){
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("antrian nomor ").append(i).append(" silahkan menuju ke ");
                sb.append(loket);
                ambilData(sb.toString(),loket+" "+" antrian nomor "+i+".mp3");
            } catch (UnsupportedEncodingException | URISyntaxException e) {
                e.printStackTrace(System.err);
            }
        }
    }
    public void testData() throws UnsupportedEncodingException, URISyntaxException, IOException{
        ambilData("silahkan memasuki ruang rekan e k t p","ektp.mp3");
    }
    private void ambilData(String data,String filename) throws UnsupportedEncodingException, URISyntaxException, IOException{
        HttpClient client = HttpClientBuilder.create().setUserAgent("Mozilla").build();
        StringBuilder sb=new StringBuilder();
        sb.append("http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob");
        sb.append("&q=");
        sb.append(URLEncoder.encode(data, "UTF-8"));
        sb.append("&tl=id");        
        HttpGet get = new HttpGet(new URI(sb.toString()));
        HttpResponse response =client.execute(get);
        int code=response.getStatusLine().getStatusCode();
        if(code>=200 && code<300){
            BufferedInputStream bis = new BufferedInputStream(response.getEntity().getContent());
            BufferedOutputStream bos =new BufferedOutputStream(new FileOutputStream(new File("./Sounds/"+filename)));
            int inByte;
            while((inByte=bis.read())!=-1){
                bos.write(inByte);
            }
            bis.close();
            bos.close();
        }
    }
    public static void main(String[] args) throws UnsupportedEncodingException, URISyntaxException, IOException {
        GoogleTts tts=new GoogleTts();
        tts.testData();
        /*File file = new File("./Sounds/Loket3");
        if(!file.exists()){
            file.mkdir();
        }
        tts.dataLoket("loket nomor 3");*/
    }
}
