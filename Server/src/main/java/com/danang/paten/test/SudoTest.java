/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.concurrent.ExecutionException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author danang
 */
public class SudoTest {

    public void kirim() throws IOException, InterruptedException, ExecutionException {
        File cof = new File("gammurc");
        System.out.println(cof.getAbsolutePath() + " " + cof.exists());
        String[] gammu = {"gammu",
            " --sendsms ", " TEXT ", " 08988249780 "};
        String wammu = "gammu --sendsms TEXT 08988249780";
        /*SwingWorker sw = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                String noHP = "08988249780";
                String sms = "hello world";
                Runtime runtime = Runtime.getRuntime();
                String cmd = "gammu --sendsms TEXT " + noHP + " -text \"" + sms + "\"";
                Process p = runtime.exec(cmd);
                String line = "";
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                while (p.isAlive()) {
                    System.out.println("process aktif");
                }
                return "terkirim";
            }

            @Override
            protected void done() {
                try {
                    String data = (String) get();
                    System.out.println("data " + data);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace(System.err);
                }
            }

        };
        sw.execute();*/
        String sms = execCmd(wammu, "Hello world");
        System.out.println(sms);
        //kirimSMS("08988249780", "Hello world");
    }

    public void testBuilder() throws IOException {
        String cmd2 = "gammu --sendsms TEXT 08988249780 -text \"Hello world\"";
        String cmd3[] = {"gammu", " --sendsms", "TEXT 08988249780 -text \"Hello world\" "};
        String cmd4[] = {"/bin/bash", "gammu", " --sendsms TEXT 08988249780", "Hello world"};
        ProcessBuilder pb = new ProcessBuilder("gammu", "--sendsms", "TEXT", "08988249780", "-text", "hello from java");
        pb.redirectError(Redirect.INHERIT);
        pb.redirectOutput(Redirect.INHERIT);
        Process p = pb.start();
        String line = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
    }

    public void eksekusiBatch() throws IOException {
        Runtime runtime = Runtime.getRuntime();
        PrintStream prt = System.out;
        String cmd1 = "/bin/bash " + new File("mbash.sh").getAbsolutePath();
        String cmd2 = "gammu --sendsms TEXT 08988249780 -text \"java\"";
        String cmd3[] = {"/bin/bash", "gammu --sendsms TEXT 08988249780 -text \"Hello world\" "};
        System.out.println("starting execute");
        runtime.traceInstructions(true);
        runtime.traceMethodCalls(true);
        Process p = runtime.exec(cmd2);
        prt = new PrintStream(p.getOutputStream());
        prt.println();
        System.out.println("in wait for process");
        String line = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
    }

    private void kirimSMS(final String noHP, final String sms) {
        SwingWorker<String, String> gammuWorker = new SwingWorker<String, String>() {
            @Override
            protected String doInBackground() throws Exception {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("gammu --sendsms TEXT " + noHP + " -text \"" + sms + "\"");
                return "Pesan Terkirim";
            }

            @Override
            protected void done() {
                try {
                    //Cek apakah proses pengiriman pesan selesai?
                    String pesan = get();
                    JOptionPane.showMessageDialog(null, pesan);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Pesan tidak dikirimkan. Alasan:\n" + ex.getMessage());
                }
            }

        };

        gammuWorker.execute();
    }

    private String execCmd(String cmd, String input) throws java.io.IOException, InterruptedException {

        System.out.println(cmd);
        Process process = Runtime.getRuntime().exec(cmd);
        if (input != null) {
            int i = IOUtils.copy(IOUtils.toInputStream(input), process.getOutputStream());
            process.waitFor();
            // we have to close the outputStream here 
            process.getOutputStream().close();
        }

        // err output
        String errLog = IOUtils.toString(process.getErrorStream());
        if (!errLog.isEmpty()) {
            System.out.println(process.getErrorStream());
            //logger.debug(IOUtils.toString(process.getErrorStream()));
        }

        // std output
        return IOUtils.toString(process.getInputStream());
    }

    public static void main(String[] args) throws Exception {
        JFrame f = new JFrame();
        JButton btn = new JButton("kirim");
        btn.addActionListener((x) -> {
            try {
                new SudoTest().testBuilder();
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        });
        f.getContentPane().add(btn);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(600, 400);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
