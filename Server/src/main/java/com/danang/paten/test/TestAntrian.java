/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import com.danang.paten.Konfig;
import com.danang.paten.util.JsonTransformer;
import com.danang.paten.util.MyStt;
import com.danang.paten.util.PlayAudio;
import com.hazelcast.config.Config;
import com.hazelcast.core.DuplicateInstanceNameException;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import static spark.Spark.put;

/**
 *
 * @author danang
 */
public class TestAntrian {

    public static void main(String[] args) {  
        PlayAudio pa = new PlayAudio();
        pa.start();
        put("/loket/recall", (req, res) -> {
            Map<String, Object> result = new HashMap<>();
            String jenis = req.queryParams("jenis");
            int antrian = Integer.valueOf(req.queryParams("antrian"));
            int loket;
            String th;
            switch (jenis) {
                case "PERIJINAN":
                    loket = 1;
                    th="A";
                    break;
                case "NONPERIJINAN":
                    loket = 2;
                    th="B";
                    break;
                default:
                    loket = 3;
                    th="C";
                    break;
            }           
            System.out.println("request accepted "+jenis +" "+antrian);
            pa.setData(jenis, antrian, loket);
            result.put("error", "");
            return result;
        }, new JsonTransformer());
        put("/test",(req,res)->{
            return req.queryParams();
        },new JsonTransformer());
    }
}
