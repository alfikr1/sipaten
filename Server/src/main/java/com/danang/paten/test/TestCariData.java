/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.PenetapanBerkas;
import com.danang.paten.domain.PeriksaBerkas;
import com.danang.paten.domain.TerimaBerkas;
import com.danang.paten.domain.TransaksiLayanan;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danang
 */
public class TestCariData {

    private MongoCollection collection;
    private MongoDatabase db;

    public TestCariData() throws Exception {
        MongoClient client = new MongoClient("localhost");
        db = client.getDatabase("bergaskec");
        collection = db.getCollection("transaksiLayanan");
        //collection=client.getDB("bergaskec").getCollection("transaksiLayanan");
    }

    public static void main(String[] args) {
        Class kelas = TransaksiLayanan.class;
        final Field[] fds = kelas.getDeclaredFields();
        List<String> items = new ArrayList<>();
        for (Field f : fds) {
            f.setAccessible(true);
            if (f.getType().equals(String.class)) {
                items.add(f.getName());
            }
            if (f.getType().equals(DataPenduduk.class)) {
                searchFieldClass(DataPenduduk.class, f.getName(), items);
            }
            if (f.getType().equals(TerimaBerkas.class)) {
                searchFieldClass(TerimaBerkas.class, f.getName(), items);
            }
            if(f.getType().equals(PeriksaBerkas.class)){
                searchFieldClass(PeriksaBerkas.class, f.getName(), items);
            }
            if(f.getType().equals(PenetapanBerkas.class)){
                searchFieldClass(PenetapanBerkas.class, f.getName(), items);
            }
        }
        items.stream().forEach((x) -> {
            System.out.println(x);
        });
    }

    static void searchFieldClass(Class cl, String a, List<String> items) {
        for (Field f : cl.getDeclaredFields()) {
            if (f.getType().equals(String.class)) {
                items.add(a + "." + f.getName());
            }
        }
    }

}
