/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import com.danang.paten.Konfig;
import com.danang.paten.domain.Petugas;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.repository.PetugasRepository;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;

/**
 *
 * @author danang
 */
public class CrudTest {

    private final MongoTemplate template;
    private final PetugasRepository petugasDao;

    public CrudTest() throws ParseException {
        template = Konfig.getInstance().getContext().getBean(MongoTemplate.class);
        petugasDao = Konfig.getInstance().getContext().getBean(PetugasRepository.class);
        init();
        testuser();
    }

    private void testuser() {
        Petugas p = petugasDao.findByUser("admin");
        System.out.println(p.getKode());
    }

    private void init() {
        ProjectionOperation po = project("nomor")
                .and("terimaBerkas.ijin.nama").as("pemohon")
                .and("layanan.namaLayanan").as("layanan")
                .and("statusPekerjaan").as("statusPekerjaan")
                .and("terimaBerkas.tanggalTerima").as("tanggal")
                .andExpression("terimaBerkas.ijin.desa", "ifnull(terimaBerkas.ijin.desa,'')").as("desa")
                .andExpression("terimaBerkas.ijin.lingk", "ifnull(terimaBerkas.ijin.lingk,''").as("lingk")
                .andExpression("terimaBerkas.ijin.Rt", "ifNull(terimaBerkas.ijin.Rt,''").as("Rt")
                .andExpression("terimaBerkas.ijin.Rw", "ifNull(terimaBerkas.ijin.Rw,''").as("Rw")
                .andExpression("terimaBerkas.ijin.kecamatan", "ifNull(terimaBerkas.ijin.kecamatan,' ')").as("kecamatan");
        Sort sort = new Sort(Sort.Direction.DESC, "tanggal");
        GroupOperation go = group("nomor", "tanggal", "layanan", "pemohon", "desa", "lingk", "Rt", "Rw", "kecamatan", "statusPekerjaan");

        Aggregation agg = newAggregation(po,
                go, sort(sort));
        AggregationResults<Map> result = template.aggregate(agg, TransaksiLayanan.class, Map.class);

        result.getMappedResults().stream().forEach((x) -> {
            System.out.println(x.toString());
        });

    }

    class cek {

        private String nomor;
        private Date tanggal;
        private String pemohon;
        private String layanan;
        private String alamat;
        private String statusPekerjaan;

    }

    public static void main(String[] args) throws ParseException {
        new CrudTest();
    }

}
