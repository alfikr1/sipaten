/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.enumerasi.StatusPekerjaan;
import com.danang.paten.form.LaporanUmum;
import com.danang.paten.konfigurasi.SpringConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Field;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author danang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
public class LaporanTest {

    private @Autowired
    MongoTemplate template;
    Gson gson = new GsonBuilder().setDateFormat("dd MM yyyy").create();

    //@Test
    public void testData() {
        DateTime awal = new DateTime(2017, 1, 1, 0, 0, 0);
        DateTime akhir = new DateTime();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), c, Criteria.where("layanan.kode").is("518"));
        final Query q = new Query(and);
        q.fields().include("penetapanBerkas._id");
        q.fields().include("catatan");
        q.fields().include("terimaBerkas.ijin");
        q.fields().include("penetapanBerkas.tanggal");
        long total = template.count(q, TransaksiLayanan.class);

        template.find(q, TransaksiLayanan.class).stream().forEach((x) -> {
            System.out.println(gson.toJson(x));
        });
        System.out.println("total" + total);
    }

    @Test
    public void testAggregation() {
        DateTime awal = new DateTime(2017, 1, 1, 0, 0, 0);
        DateTime akhir = new DateTime();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), c, Criteria.where("layanan.kode").is("518"));
        Field nomor = Fields.field("nomor", "penetapanBerkas.nomor");
        Field tanggal = Fields.field("tanggal", "penetapanBerkas.tanggal");
        Field penduduk = Fields.field("pemohon", "terimaBerkas.ijin");
        Field keterangan = Fields.field("keterangan", "catatan");
        Field id=Fields.field("_id");
        Fields items = Fields.from(nomor, tanggal, penduduk, keterangan);
        //ProjectionOperation po = Aggregation.project("penetapanBerkas.nomor", "catatan", "terimaBerkas.ijin", "penetapanBerkas.tanggal");
        ProjectionOperation po = Aggregation.project(items);
        //AggregationResults rs = template.aggregate(newAggregation(match(and), po.andExclude("_id")), TransaksiLayanan.class,Map.class);
        AggregationResults<LaporanUmum> rs = template.aggregate(newAggregation(match(and), po.andExclude("_id")), TransaksiLayanan.class,LaporanUmum.class);
        List<LaporanUmum> ls = rs.getMappedResults();
        ls.stream().forEach((x)->{
            System.out.println(gson.toJson(x));
        });
    }
}
