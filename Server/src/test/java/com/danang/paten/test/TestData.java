/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.test;

import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.konfigurasi.SpringConfig;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author danang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class TestData {

    private @Autowired
    MongoTemplate template;

    @Test
    public void dasboard() {
        Date awal = new DateTime(2016, 10, 10, 0, 0, 0).toDate();
        Date akhir = new DateTime(2016, 10, 11, 0, 0, 0).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        GroupOperation grup = group("layanan.kode", "layanan.namaLayanan").count().as("total");
        Gson gson = new Gson();
        Aggregation agg = newAggregation(mat, grup);
        System.out.println(agg.toString());
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        groupResult.getMappedResults().stream().forEach((x) -> {
            System.out.println(gson.toJson(x));
        });
    }
}
