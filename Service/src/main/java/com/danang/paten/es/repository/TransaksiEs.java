/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.es.repository;

import com.danang.paten.domain.TransaksiLayanan;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author danang
 */
@Repository
public interface TransaksiEs extends PagingAndSortingRepository<TransaksiLayanan,String> {
    
}
