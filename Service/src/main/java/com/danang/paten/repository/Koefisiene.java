/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.repository;

import com.danang.paten.domain.KoefisienIMB;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author danang
 */
@Repository
public interface Koefisiene extends PagingAndSortingRepository<KoefisienIMB,Integer> {
    
}
