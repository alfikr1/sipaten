package com.danang.paten.repository;

import com.danang.paten.domain.PeriksaBerkas;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriksaBerkasRepository
  extends MongoRepository<PeriksaBerkas, String>
{}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/PeriksaBerkasRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */