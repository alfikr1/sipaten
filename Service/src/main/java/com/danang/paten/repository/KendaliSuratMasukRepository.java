package com.danang.paten.repository;

import com.danang.paten.domain.KartuKendaliSuratMasuk;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KendaliSuratMasukRepository extends PagingAndSortingRepository<KartuKendaliSuratMasuk, String>
{}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/KendaliSuratMasukRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */