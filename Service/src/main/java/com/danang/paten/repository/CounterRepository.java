package com.danang.paten.repository;

import com.danang.paten.domain.Counter;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface CounterRepository
  extends MongoRepository<Counter, String>
{
    public Counter findByKodeAndYear(String kode,int year);
}
