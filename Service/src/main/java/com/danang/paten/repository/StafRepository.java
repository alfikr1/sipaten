package com.danang.paten.repository;

import com.danang.paten.domain.Staf;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface StafRepository
  extends MongoRepository<Staf, String>
{}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/StafRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */