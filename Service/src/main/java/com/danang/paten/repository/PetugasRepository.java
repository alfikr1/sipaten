package com.danang.paten.repository;

import com.danang.paten.domain.Petugas;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PetugasRepository
  extends MongoRepository<Petugas, String>
{
  public abstract Petugas findByUser(String paramString);
}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/PetugasRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */