package com.danang.paten.repository;

import com.danang.paten.domain.Loket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface LoketRepository
  extends MongoRepository<Loket, String>
{
  public abstract Page<Loket> findByKodeLikeOrDeskLike(String paramString, Pageable paramPageable);
}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/LoketRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */