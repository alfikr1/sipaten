package com.danang.paten.repository;

import com.danang.paten.domain.Layanan;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface LayananRepository
  extends MongoRepository<Layanan, String>
{
  public abstract List<Layanan> findByParent(Layanan paramLayanan);
  
  public abstract Page<Layanan> findByKodeLikeOrNamaLayananLike(String paramString1, String paramString2, Pageable paramPageable);
}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/LayananRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */