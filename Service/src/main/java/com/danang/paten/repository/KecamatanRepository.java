package com.danang.paten.repository;

import com.danang.paten.domain.Kecamatan;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface KecamatanRepository
  extends MongoRepository<Kecamatan, String>
{}
