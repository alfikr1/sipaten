package com.danang.paten.repository;

import com.danang.paten.domain.Layanan;
import com.danang.paten.domain.Roles;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface RolesRepository
  extends MongoRepository<Roles, String>
{
  public abstract Roles findByLayanan(Layanan paramLayanan);
}


/* Location:              /mnt/master/aaaa/ViewController-1.0-SNAPSHOT-jar-with-dependencies.jar!/com/danang/paten/repository/RolesRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */