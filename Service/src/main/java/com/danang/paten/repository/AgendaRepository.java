/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.repository;

import com.danang.paten.domain.Agenda;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danang
 */
@Repository
public interface AgendaRepository extends PagingAndSortingRepository<Agenda, String> {

    Page<Agenda> findByTanggalBetween(Date awal, Date akhir, Pageable pageable);
}
