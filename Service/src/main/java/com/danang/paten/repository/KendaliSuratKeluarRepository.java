package com.danang.paten.repository;

import com.danang.paten.domain.KartuKendaliSuratKeluar;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface KendaliSuratKeluarRepository
  extends MongoRepository<KartuKendaliSuratKeluar, String>
{}
