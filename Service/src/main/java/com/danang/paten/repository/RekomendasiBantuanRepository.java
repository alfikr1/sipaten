package com.danang.paten.repository;

import com.danang.paten.domain.RekomendasiBantuan;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.repository.MongoRepository;

@Repository
public interface RekomendasiBantuanRepository extends MongoRepository<RekomendasiBantuan, String> {
}
