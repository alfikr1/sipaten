// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import com.danang.paten.domain.Counter;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.CounterRepository;
import org.springframework.stereotype.Service;

@Service
public class CounterService
{
    @Autowired
    private CounterRepository counterDao;
    
    public void setSeqAwal(final String kode, final int awal) {
        final Counter c = new Counter();
        c.setKode(kode);
        c.setSeri(awal);
        this.counterDao.save(c);
    }
    
    public int getNextSeq(final String kode) {
        Counter c = this.counterDao.findOne(kode);
        if (c == null) {
            c = new Counter();
            c.setKode(kode);
            c.setSeri(0);
        }
        c.setSeri(c.getSeri() + 1);
        this.counterDao.save(c);
        return c.getSeri();
    }
}
