// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.service;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.Query;
import java.util.regex.Pattern;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.domain.PageRequest;
import com.danang.paten.domain.Loket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class LoketService {

    @Autowired
    private MongoTemplate template;

    public Page<Loket> cariLoket(final String cari, Pageable p) {
        if (p == null) {
            p = new PageRequest(0, 20);
        }
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), Criteria.where("desk").regex(Pattern.compile(cari, 2)));
        final Query q = new Query();
        q.addCriteria(c);
        long total = template.count(q, Loket.class);
        q.with(p);
        return new PageImpl<>(this.template.find(q, Loket.class), p, total);
    }
}
