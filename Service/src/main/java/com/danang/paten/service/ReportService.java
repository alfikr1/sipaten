/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.enumerasi.StatusPekerjaan;
import com.danang.paten.form.DomisiliUsaha;
import com.danang.paten.form.HO;
import com.danang.paten.form.IMB;
import com.danang.paten.form.IUM;
import com.danang.paten.form.LaporanDomisili;
import com.danang.paten.form.LaporanHO;
import com.danang.paten.form.LaporanIMB;
import com.danang.paten.form.LaporanIUM;
import com.danang.paten.form.LaporanUmum;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Field;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author danang
 */
@Service
public class ReportService {

    private Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
    private @Autowired
    MongoTemplate template;

    public List<LaporanUmum> getLaporanUmum(final Date awal, final Date akhir, final String kode) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), c, Criteria.where("layanan.kode").is(kode));
        Field nomor = Fields.field("nomor", "penetapanBerkas.nomor");
        Field tanggal = Fields.field("tanggal", "penetapanBerkas.tanggal");
        Field penduduk = Fields.field("pemohon", "terimaBerkas.ijin");
        Field keterangan = Fields.field("keterangan", "catatan");
        Field id = Fields.field("_id");
        Fields items = Fields.from(nomor, tanggal, penduduk, keterangan);
        ProjectionOperation po = Aggregation.project(items);
        AggregationResults<LaporanUmum> rs = template.aggregate(newAggregation(match(and), po.andExclude("_id")), TransaksiLayanan.class, LaporanUmum.class);
        List<LaporanUmum> ls = rs.getMappedResults();
        return ls;
    }

    public List<LaporanHO> getHOItems(Date awal, Date akhir) {
        List<LaporanHO> items = new ArrayList<>();
        Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("510.4"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        System.out.println(tls.size());
        tls.stream().forEach(tl -> {
            LaporanHO lh = new LaporanHO();
            lh.setPemohon(tl.getTerimaBerkas().getIjin().getNama());
            lh.setAlamatPemohon(getAlamatPenduduk(tl.getTerimaBerkas().getIjin()));
            HO ho = gson.fromJson(tl.getKonten(), HO.class);
            lh.setNamaUsaha(ho.getNamaPerusahaan());
            lh.setAlamatUsaha(ho.getLokasi());
            lh.setIndexGangguan(ho.getIndexGangguan());
            lh.setJenisUsaha(ho.getJenisUsaha());
            lh.setJumlahInvestasi(ho.getInvestasi());
            lh.setKetetapan(tl.getPenetapanBerkas().getNomor());
            lh.setLuasBangunan(ho.getLuasBangunan());
            lh.setLuasIjinGangguan(ho.getLuas());
            lh.setMulaiBerlaku(ho.getMulaiBerlaku());
            lh.setMasaBerlaku(ho.getAkhirBerlaku());
            lh.setPengajuan(tl.getTerimaBerkas().getTanggalTerima());
            lh.setPeriksa(tl.getPeriksaBerkas().getTanggal());
            lh.setTglKetatapan(tl.getPenetapanBerkas().getTanggal());
            lh.setRetribusi(ho.getRetribusi());
            items.add(lh);
        });
        return items;
    }

    public Map<String, Object> getExpiredLayanan() {
        final Map<String, Object> map = new HashMap<>();
        final Criteria c = new Criteria();
        c.andOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("layanan.masaBerlaku").is(Boolean.TRUE), Criteria.where("penetapanBerkas.akhirBerlaku").lte(new Date()));
        final Query q = new Query(c);
        final List<TransaksiLayanan> items = this.template.find(q, TransaksiLayanan.class);
        map.put("data", items);
        map.put("error", "");
        return map;
    }

    public String getAlamatPenduduk(final DataPenduduk dp) {
        if (dp == null) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(dp.getLingk()).append(" ").append("RT :").append(dp.getRt());
        sb.append(" RW :").append(dp.getRw());
        sb.append(" ").append(dp.getDesa());
        return sb.toString();
    }

    public Page<LaporanUmum> getLaporanUmum(final String kode, final Date awal, final Date akhir, Pageable page) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is(kode));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        long total = template.count(q, TransaksiLayanan.class);
        q.with(page);
        List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        List<LaporanUmum> lu = new ArrayList<>();
        tls.stream().forEach(x -> {
            LaporanUmum y = new LaporanUmum();
            y.setNomor(x.getPenetapanBerkas().getNomor());
            y.setKeterangan(x.getCatatan());
            y.setPemohon(x.getTerimaBerkas().getIjin());
            y.setTanggal(x.getPenetapanBerkas().getTanggal());
            lu.add(y);
        });
        return new PageImpl(lu, page, total);
    }

    public Page<LaporanDomisili> getDomisiliItems(final Date awal, final Date akhir, Pageable page) {
        final List<LaporanDomisili> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("536"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        long total = template.count(q, TransaksiLayanan.class);
        q.with(page);
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(tl -> {
            DomisiliUsaha du = gson.fromJson(tl.getKonten(), DomisiliUsaha.class);
            LaporanDomisili ld = new LaporanDomisili();
            ld.setAlamatPemohon(getAlamatPenduduk(du.getPenduduk()));
            ld.setAlamatUsaha(du.getAlamatUsaha());
            ld.setJenisUsaha(du.getJenisUsaha());
            ld.setJumlahKaryawan(du.getJumlahKaryawan());
            ld.setKeterangan(tl.getCatatan());
            ld.setMasaBerlaku(du.getTanggalHabisBerlaku());
            ld.setNamaPemohon(du.getPenduduk().getNama());
            ld.setNamaUsaha(du.getNamaPerusahaan());
            ld.setNomorIjin(tl.getPenetapanBerkas().getNomor());
            ld.setTanggal(tl.getPenetapanBerkas().getTanggal());
            items.add(ld);
        });
        return new PageImpl(items, page, total);
    }

    public List<LaporanDomisili> getDomisiliItems(final Date awal, final Date akhir) {
        final List<LaporanDomisili> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("536"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(tl -> {
            DomisiliUsaha du = gson.fromJson(tl.getKonten(), DomisiliUsaha.class);
            LaporanDomisili ld = new LaporanDomisili();
            ld.setAlamatPemohon(getAlamatPenduduk(du.getPenduduk()));
            ld.setAlamatUsaha(du.getAlamatUsaha());
            ld.setJenisUsaha(du.getJenisUsaha());
            ld.setJumlahKaryawan(du.getJumlahKaryawan());
            ld.setKeterangan(tl.getCatatan());
            ld.setMasaBerlaku(du.getTanggalHabisBerlaku());
            ld.setNamaPemohon(du.getPenduduk().getNama());
            ld.setNamaUsaha(du.getNamaPerusahaan());
            ld.setNomorIjin(tl.getPenetapanBerkas().getNomor());
            ld.setTanggal(tl.getPenetapanBerkas().getTanggal());
            items.add(ld);
        });
        return items;
    }

    public Page<LaporanHO> getHOItems(Date awal, Date akhir, Pageable page) {
        List<LaporanHO> items = new ArrayList<>();
        Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("510.4"));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        long total = template.count(q, TransaksiLayanan.class);
        q.with(page);
        List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(tl -> {
            LaporanHO lh = new LaporanHO();
            lh.setPemohon(tl.getTerimaBerkas().getIjin().getNama());
            lh.setAlamatPemohon(getAlamatPenduduk(tl.getTerimaBerkas().getIjin()));
            HO ho = gson.fromJson(tl.getKonten(), HO.class);
            lh.setNamaUsaha(ho.getNamaPerusahaan());
            lh.setAlamatUsaha(ho.getLokasi());
            lh.setIndexGangguan(ho.getIndexGangguan());
            lh.setJenisUsaha(ho.getJenisUsaha());
            lh.setJumlahInvestasi(ho.getInvestasi());
            lh.setKetetapan(tl.getPenetapanBerkas().getNomor());
            lh.setLuasBangunan(ho.getLuasBangunan());
            lh.setLuasIjinGangguan(ho.getLuas());
            lh.setMulaiBerlaku(ho.getMulaiBerlaku());
            lh.setMasaBerlaku(ho.getAkhirBerlaku());
            lh.setPengajuan(tl.getTerimaBerkas().getTanggalTerima());
            lh.setPeriksa(tl.getPeriksaBerkas().getTanggal());
            lh.setTglKetatapan(tl.getPenetapanBerkas().getTanggal());
            lh.setRetribusi(ho.getRetribusi());
            items.add(lh);
        });
        return new PageImpl(items, page, total);
    }

    public Page<LaporanIMB> getIMBItems(final Date awal, final Date akhir, Pageable page) {
        final List<LaporanIMB> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("536"));
        Query q = new Query();
        q.addCriteria(and);
        long total = template.count(q, TransaksiLayanan.class);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        q.with(page);
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(x -> {
            IMB imb = gson.fromJson(x.getKonten(), IMB.class);
            LaporanIMB laporan = new LaporanIMB();
            laporan.setTanggalIjin(x.getTerimaBerkas().getTanggalTerima());
            StringBuilder sb = new StringBuilder();
            sb.append(imb.getJalan()).append(" ").append(imb.getKelurahan());
            laporan.setAlamat(sb.toString());
            laporan.setAlamatIjin(getAlamatPenduduk(x.getPenduduk()));
            laporan.setLuas(imb.getLuasBangunan());
            laporan.setNamaPemohon(x.getTerimaBerkas().getIjin() != null ? x.getTerimaBerkas().getIjin().getNama() : "");
            laporan.setTanggalKetetapan(x.getPenetapanBerkas().getTanggal());
            laporan.setTanggalPemeriksaan(x.getPeriksaBerkas().getTanggal());
            laporan.setNomorIjin(x.getPenetapanBerkas().getNomor());
            laporan.setPeruntukan(imb.getGunaBangunan());
            laporan.setRetribusi(imb.getRetribusi());
            items.add(laporan);
        });
        System.out.println(items.size());
        return new PageImpl(items, page, total);
    }

    public List<LaporanIMB> getIMBItems(final Date awal, final Date akhir) {
        final List<LaporanIMB> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is("536"));
        Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(x -> {
            IMB imb = gson.fromJson(x.getKonten(), IMB.class);
            LaporanIMB laporan = new LaporanIMB();
            laporan.setTanggalIjin(x.getTerimaBerkas().getTanggalTerima());
            StringBuilder sb = new StringBuilder();
            sb.append(imb.getJalan()).append(" ").append(imb.getKelurahan());
            laporan.setAlamat(sb.toString());
            laporan.setAlamatIjin(getAlamatPenduduk(x.getPenduduk()));
            laporan.setLuas(imb.getLuasBangunan());
            laporan.setNamaPemohon(x.getTerimaBerkas().getIjin() != null ? x.getTerimaBerkas().getIjin().getNama() : "");
            laporan.setTanggalKetetapan(x.getPenetapanBerkas().getTanggal());
            laporan.setTanggalPemeriksaan(x.getPeriksaBerkas().getTanggal());
            laporan.setNomorIjin(x.getPenetapanBerkas().getNomor());
            laporan.setPeruntukan(imb.getGunaBangunan());
            laporan.setRetribusi(imb.getRetribusi());
            items.add(laporan);
        });
        System.out.println(items.size());
        return items;
    }

    public Page<LaporanIUM> getLaporanIUM(final Date awal, final Date akhir, Pageable page) {
        final List<LaporanIUM> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), c, Criteria.where("layanan.kode").is("518"));
        final Query q = new Query(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        long total = template.count(q, TransaksiLayanan.class);
        q.with(page);
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(x -> {
            LaporanIUM ium = new LaporanIUM();
            IUM im = gson.fromJson(x.getKonten(), IUM.class);
            ium.setPemohon(x.getPenduduk());
            ium.setNomor(x.getPenetapanBerkas().getNomor());
            ium.setNomorPeriksa(x.getPeriksaBerkas().getNomor());
            ium.setNomorRegistrasi(x.getTerimaBerkas().getNomor());
            ium.setPenerimaan(x.getTerimaBerkas().getTanggalTerima());
            ium.setPenetapan(x.getPenetapanBerkas().getPetugas().getNama());
            ium.setPetugasPeriksa(x.getPeriksaBerkas().getPetugas().getNama());
            ium.setPetugasTerima(x.getTerimaBerkas().getPetugas().getNama());
            ium.setTanggal(x.getPenetapanBerkas().getTanggal());
            ium.setTanggalPeriksa(x.getPeriksaBerkas().getTanggal());
            ium.setBerlaku(x.getBerlakuSurat());
            ium.setAkhirBerlaku(x.getExpiredSurat());
            ium.setJumlahModal(im.getJumlahModal());
            ium.setAlamatUsaha(im.getAlamatUsaha());
            ium.setBentukUsaha(im.getBentukUsaha());
            ium.setKegiatanUsaha(im.getKegiatanUsaha());
            ium.setNamaUsaha(im.getNamaUsaha());
            items.add(ium);
        });
        return new PageImpl(items, page, total);
    }

    public List<LaporanIUM> getLaporanIUM(final Date awal, final Date akhir) {
        final List<LaporanIUM> items = new ArrayList<>();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), c, Criteria.where("layanan.kode").is("518"));
        final Query q = new Query(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        final List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        tls.stream().forEach(x -> {
            LaporanIUM ium = new LaporanIUM();
            IUM im = gson.fromJson(x.getKonten(), IUM.class);
            ium.setPemohon(x.getPenduduk());
            ium.setNomor(x.getPenetapanBerkas().getNomor());
            ium.setNomorPeriksa(x.getPeriksaBerkas().getNomor());
            ium.setNomorRegistrasi(x.getTerimaBerkas().getNomor());
            ium.setPenerimaan(x.getTerimaBerkas().getTanggalTerima());
            ium.setPenetapan(x.getPenetapanBerkas().getPetugas().getNama());
            ium.setPetugasPeriksa(x.getPeriksaBerkas().getPetugas().getNama());
            ium.setPetugasTerima(x.getTerimaBerkas().getPetugas().getNama());
            ium.setTanggal(x.getPenetapanBerkas().getTanggal());
            ium.setTanggalPeriksa(x.getPeriksaBerkas().getTanggal());
            ium.setBerlaku(x.getBerlakuSurat());
            ium.setAkhirBerlaku(x.getExpiredSurat());
            ium.setJumlahModal(im.getJumlahModal());
            ium.setAlamatUsaha(im.getAlamatUsaha());
            ium.setBentukUsaha(im.getBentukUsaha());
            ium.setKegiatanUsaha(im.getKegiatanUsaha());
            ium.setNamaUsaha(im.getNamaUsaha());
            items.add(ium);
        });
        return items;
    }

    /*private List<LaporanUmum> getLaporanUmum(final String kode, final Date awal, final Date akhir) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Criteria and = new Criteria();
        and.andOperator(c, Criteria.where("penetapanBerkas.tanggal").gte(awal), Criteria.where("penetapanBerkas.tanggal").lte(akhir), Criteria.where("layanan.kode").is(kode));
        final Query q = new Query();
        q.addCriteria(and);
        q.with(new Sort(Sort.Direction.ASC, new String[]{"penetapanBerkas.tanggal"}));
        List<TransaksiLayanan> tls = this.template.find(q, TransaksiLayanan.class);
        List<LaporanUmum> lu = new ArrayList<>();
        tls.stream().forEach(x -> {
            LaporanUmum y = new LaporanUmum();
            y.setNomor(x.getPenetapanBerkas().getNomor());
            y.setKeterangan(x.getCatatan());
            y.setPemohon(x.getTerimaBerkas().getIjin());
            y.setTanggal(x.getPenetapanBerkas().getTanggal());
            lu.add(y);
        });
        return lu;
    }*/
}
