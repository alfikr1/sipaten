/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.Counter;
import com.danang.paten.repository.CounterRepository;
import com.danang.paten.repository.LayananRepository;
import java.util.List;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author danang
 */
@Service
public class UpdateService {

    @Autowired
    private CounterRepository counterDao;
    private @Autowired LayananRepository layananDao;
    public void jalankanCounterChecker(){
        List<Counter> items = counterDao.findAll();
        DateTime dt= new DateTime();
        items.stream().forEach((x)->{
            System.out.println("data "+x.getKode() +" "+x.getYear());
            if(x.getYear()==-1 || x.getYear()==0){
                System.out.println("data update "+x.getKode());
                x.setYear(dt.getYear());
                counterDao.save(x);
            }
        });
    }
    public void jalankanUpdateKodeLayanan(){
        layananDao.findAll().stream().forEach((x)->{
            if(x.getKodeTetapan()==null){
                x.setKodeTetapan(x.getKode());
            }
            layananDao.save(x);
        });
    }
}
