// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.Query;
import java.util.regex.Pattern;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import java.util.HashMap;
import java.util.Map;
import org.mindrot.jbcrypt.BCrypt;
import com.danang.paten.domain.Petugas;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.PetugasRepository;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class PetugasService
{
    @Autowired
    private PetugasRepository petugasDao;
    @Autowired
    private MongoTemplate template;
    
    public Petugas login(final String username, final String password) {
        final Petugas x = this.petugasDao.findByUser(username);
        if (x == null) {
            throw new IllegalStateException("Login gagal username / password salah");
        }
        if (!BCrypt.checkpw(password, x.getPsw())) {
            return null;
        }
        x.setLastLogin(new Date());
        
        return petugasDao.save(x);
    }
    
    public void logout(final String username) {
    }
    
    public Map<String, Object> getDashboardPetugas() {
        final Map<String, Object> map = new HashMap<>();
        final Aggregation agg = Aggregation.newAggregation(Aggregation.group(""));
        return map;
    }
    
    public Page<Petugas> cariData(final String cari, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), Criteria.where("nip").regex(Pattern.compile(cari, 2)), Criteria.where("nama").regex(Pattern.compile(cari, 2)));
        final Query q = new Query();
        q.addCriteria(c);
        q.with(pageable);
        return new PageImpl<Petugas>(this.template.find(q, Petugas.class));
    }
}
