/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.Agenda;
import com.danang.paten.repository.AgendaRepository;
import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author Danang
 */
@Service
public class AgendaService {

    private static final Logger log = Logger.getLogger(AgendaService.class);
    private @Autowired
    AgendaRepository repo;
    private @Autowired
    MongoTemplate template;
    private @Autowired
    CounterService service;

    public Page<Agenda> getAllAgendaKerja(Pageable pageable) {

        return repo.findAll(pageable);
    }

    public Page<Agenda> getAgendaHariIni(Pageable page) {
        Query q = new Query(Criteria.where("tanggal"));
        long total = template.count(q, Agenda.class);
        return new PageImpl(template.find(q, Agenda.class), page, total);
    }

    public void deleteAgenda(String id) {
        Agenda x = repo.findOne(id);
        if (x == null) {
            throw new IllegalStateException("Data tidak ditemukan");
        }
        repo.delete(x);
    }

    public Agenda simpanAgenda(Agenda agenda) {
        agenda.setId(getAgendaCounter());
        agenda.setCreateDate(new Date());
        return agenda;
    }

    public Agenda updateAgenda(Agenda agenda, String id) {
        Agenda x = repo.findOne(id);
        if (x == null) {
            throw new IllegalStateException("Data tidak ditemukan");
        }
        agenda.setId(x.getId());
        return repo.save(agenda);
    }

    public String getAgendaCounter() {
        StringBuilder sb = new StringBuilder();
        int urut = service.getNextSeq("agenda");
        sb.append("RK").append(urut);
        return sb.toString();
    }
}
