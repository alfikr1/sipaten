// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import com.danang.paten.domain.Counter;
import com.danang.paten.domain.Layanan;
import com.danang.paten.domain.Roles;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.CounterRepository;
import org.apache.commons.lang3.StringUtils;

public class RolesService
{
    @Autowired
    private CounterRepository counterDao;
    @Autowired
    private RolesRepository rolesDao;
    @Autowired
    private LayananRepository layananDao;
    
    public void simpanRole(final Roles role) {
        final Layanan l = role.getLayanan();
        final Roles x = this.rolesDao.findByLayanan(l);
        if (x != null) {
            return;
        }
        role.setKode(this.getNextKode());
        this.rolesDao.save(role);
    }
    
    private String getNextKode() {
        Counter c = this.counterDao.findOne("role");
        if (c == null) {
            c = new Counter();
            c.setKode("mrole");
            c.setSeri(0);
        }
        c.setSeri(c.getSeri() + 1);
        this.counterDao.save(c);
        final String result = StringUtils.leftPad(String.valueOf(c.getSeri()), 6, "0");
        return result;
    }
}
