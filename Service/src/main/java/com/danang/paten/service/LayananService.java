// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.service;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.Query;
import java.util.regex.Pattern;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.danang.paten.domain.Layanan;
import java.util.List;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.LayananRepository;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;

@Service
public class LayananService {

    @Autowired
    private LayananRepository layananDao;
    @Autowired
    private MongoTemplate template;

    public List<Layanan> getMainLayanan() {
        final List<Layanan> layanan = this.layananDao.findByParent(null);
        return layanan;
    }

    public List<Layanan> getChildLayanan(final Layanan n) {
        final List<Layanan> layanan = this.layananDao.findByParent(n);
        return layanan;
    }

    public List<Layanan> getAllLayanan() {
        return (this.layananDao).findAll();
    }
    public List<Map> getAntrianLayanan(){        
        Aggregation agg = Aggregation.newAggregation(Layanan.class, 
                project("kode","namaLayanan","syarat"),
                match(Criteria.where("syarat").ne(new ArrayList<>())));
        AggregationResults<Map> mmp=template.aggregate(agg, Layanan.class,Map.class);
        return mmp.getMappedResults();
    }

    public Page<Layanan> cariData(String cari, Pageable pageable) {
        if (pageable == null) {
            pageable = new PageRequest(0, 20, Sort.Direction.ASC, new String[0]);
        }
        if (cari.equals("")) {
            return layananDao.findAll(pageable);
        }
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), Criteria.where("namaLayanan").regex(Pattern.compile(cari, 2)));
        final Query q = new Query();
        q.addCriteria(c);
        long total = template.count(q, Layanan.class);
        q.with(pageable);
        final List<Layanan> items = this.template.find(q, Layanan.class);
        return new PageImpl<>(items, pageable, total);
    }

    public void simpan(final Layanan l) {
        this.layananDao.save(l);
    }

    public Layanan findById(final String kode) {
        return this.layananDao.findOne(kode);
    }
}
