// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.service;

import com.danang.paten.domain.Counter;
import java.util.regex.Pattern;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.domain.Page;
import com.danang.paten.domain.KartuKendaliSuratKeluar;
import java.util.Date;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import com.danang.paten.domain.KartuKendaliSuratMasuk;
import java.util.List;
import com.danang.paten.repository.CounterRepository;
import com.danang.paten.repository.KendaliSuratMasukRepository;
import com.danang.paten.repository.KendaliSuratKeluarRepository;
import java.util.HashMap;
import java.util.Map;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class SuratService {

    @Autowired
    private MongoTemplate template;
    @Autowired
    private KendaliSuratKeluarRepository keluarDao;
    @Autowired
    private KendaliSuratMasukRepository masukDao;
    @Autowired
    private CounterRepository cDao;

    public List<KartuKendaliSuratMasuk> findFirst() {
        final Query q = new Query();
        final Pageable pageable = new PageRequest(0, 25);
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggalSurat"}));
        return this.template.find(q, KartuKendaliSuratMasuk.class);
    }

    public KartuKendaliSuratMasuk simpanDisposisiSuratMasuk(String tugas,List<String> disposisi,Date tanggal, String surat, final String dispo, final String catatan) {
        final KartuKendaliSuratMasuk kmk = this.masukDao.findOne(surat);
        if (kmk == null) {
            throw new IllegalStateException("data tidak ditemukan");
        }
        kmk.setDisposisi(dispo);
        kmk.setCatatanDispo(catatan);
        kmk.setDispo(disposisi);
        kmk.setCatatanDispo(tugas);
        kmk.setStatusSurat("DIDISPOSISIKAN");
        return this.masukDao.save(kmk);
    }

    public KartuKendaliSuratMasuk simpanSuratMasuk(final KartuKendaliSuratMasuk km) {
        DateTime dt=new DateTime();
        km.setId(km.getKode()+"/"+this.getNomor("kms"+dt.getYear())+"/"+getRomawi(dt.getMonthOfYear())+"/"+dt.getYear());
        km.setTanggalTerima(new Date());
        km.setStatusSurat("DITERIMA");
        return this.masukDao.save(km);
    }

    public Map<String,Object> simpanSuratKeluar(final KartuKendaliSuratKeluar km) {
        Map<String,Object> map = new HashMap<>();
        DateTime dt=new DateTime();
        km.setId(km.getKode()+"/"+getNomor("kmk/"+dt.getYear())+getRomawi(dt.getMonthOfYear())+"/"+dt.getYear());
        final StringBuilder sb = new StringBuilder();
        sb.append(km.getKode()).append("/").append(km.getId());
        km.setNomorSurat(sb.toString());
        try {
            keluarDao.save(km);
            map.put("error", "");
            map.put("data", km);
        } catch (Exception e) {
            map.put("error", e);
        }
        return map;
    }

    public KartuKendaliSuratMasuk updateSuratMasuk(final KartuKendaliSuratMasuk km) {
        final KartuKendaliSuratMasuk x = this.masukDao.findOne(km.getId());
        if (x == null) {
            throw new IllegalStateException("data tidak ditemukan, update gagal");
        }
        km.setId(x.getId());
        return this.masukDao.save(km);
    }

    public KartuKendaliSuratKeluar updateSuratKeluar(final KartuKendaliSuratKeluar km) {
        KartuKendaliSuratKeluar x = this.keluarDao.findOne(km.getId());
        if (x == null) {
            throw new IllegalStateException("update kartu keluar gagal, data tidak ditemukan");
        }
        km.setId(x.getId());
        return this.keluarDao.save(km);
    }

    public Page<KartuKendaliSuratMasuk> findAllData(final Pageable pageable) {
        return (this.masukDao).findAll(pageable);
    }

    public Page<KartuKendaliSuratMasuk> findDisposisiMasuk(final Pageable pageable) {
        final Criteria c = new Criteria();
        final Query q = new Query();
        q.addCriteria(Criteria.where("statusSurat").is("DITERIMA"));
        q.with(pageable);
        long total=template.count(q, KartuKendaliSuratMasuk.class);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        return new PageImpl<>(this.template.find(q, KartuKendaliSuratMasuk.class),pageable, total);
    }

    public Page<KartuKendaliSuratMasuk> findSuratByDate(final Date awal, final Date akhir, final Pageable pageable) {
        Criteria c = new Criteria();
        c.orOperator(Criteria.where("tanggalTerima").gte(awal), 
                Criteria.where("tanggalTerima").lte(akhir));
        final Query q = new Query();
        q.addCriteria(c);
        long total=template.count(q, KartuKendaliSuratMasuk.class);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, KartuKendaliSuratMasuk.class),pageable,total);
    }

    public Page<KartuKendaliSuratKeluar> findSuratKeluar(final String cari, final Pageable pageable) {
        final Criteria ac = new Criteria();
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), 
                Criteria.where("ringkas").regex(Pattern.compile(cari, 2)), 
                Criteria.where("pengolah").regex(Pattern.compile(cari, 2)), 
                Criteria.where("kepada").regex(Pattern.compile(cari, 2)));
        final Query q = new Query();
        q.addCriteria(c);
        long total=template.count(q, KartuKendaliSuratKeluar.class);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, KartuKendaliSuratKeluar.class),pageable,total);
    }

    public Page<KartuKendaliSuratMasuk> findMasukTanggal(final String cari, final Date awal, final Date akhir, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), Criteria.where("ringkas").regex(Pattern.compile(cari, 2)), Criteria.where("pengolah").regex(Pattern.compile(cari, 2)), Criteria.where("dari").regex(Pattern.compile(cari, 2)), Criteria.where("kepada").regex(Pattern.compile(cari, 2)));
        final Criteria between = new Criteria();
        between.andOperator(c, Criteria.where("tanggalSurat").lte(akhir), Criteria.where("tanggalSurat").gte(awal));
        final Query q = new Query();
        q.addCriteria(between);
        long total=template.count(q, KartuKendaliSuratMasuk.class);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, KartuKendaliSuratMasuk.class),pageable,total);
    }

    public Page<KartuKendaliSuratMasuk> findSuratMasuk(final String cari, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), Criteria.where("ringkas").regex(Pattern.compile(cari, 2)));
        final Query q = new Query();
        q.addCriteria(c);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, KartuKendaliSuratMasuk.class));
    }

    public List<KartuKendaliSuratMasuk> findMasuk(final Date awal, final Date akhir) {
        final Criteria c = new Criteria();
        c.andOperator(Criteria.where("tanggalSurat").gte(awal), Criteria.where("tanggalSurat").lte(akhir));
        final Query q = new Query(c);        
        return template.find(q, KartuKendaliSuratMasuk.class);
    }

    public List<KartuKendaliSuratKeluar> findKeluar(final Date awal, final Date akhir) {
        final Criteria between = new Criteria();
        between.andOperator(Criteria.where("tanggal").lte(akhir), Criteria.where("tanggal").gte(awal));
        final Query q = new Query();
        q.addCriteria(between);
        return this.template.find(q, KartuKendaliSuratKeluar.class);
    }

    public Page<KartuKendaliSuratKeluar> findKeluarByDate(final Date awal, final Date akhir, final Pageable pageable) {
        final Criteria between = new Criteria();
        between.andOperator(Criteria.where("tanggal").lte(akhir), Criteria.where("tanggal").gte(awal));
        final Query q = new Query();
        q.addCriteria(between);
        long total = this.template.count(q, KartuKendaliSuratKeluar.class);
        q.with(pageable);        
        return new PageImpl<>(this.template.find(q, KartuKendaliSuratKeluar.class), pageable, total);
    }

    public Page<KartuKendaliSuratKeluar> findKeluarTanggal(final String cari, final Date awal, final Date akhir, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("kode").regex(Pattern.compile(cari, 2)), Criteria.where("ringkas").regex(Pattern.compile(cari, 2)), Criteria.where("pengolah").regex(Pattern.compile(cari, 2)), Criteria.where("dari").regex(Pattern.compile(cari, 2)), Criteria.where("kepada").regex(Pattern.compile(cari, 2)));
        final Criteria between = new Criteria();
        between.andOperator(c, Criteria.where("tanggal").lte(akhir), Criteria.where("tanggal").gte(awal));
        final Query q = new Query();
        q.addCriteria(between);
        long total = this.template.count(q, KartuKendaliSuratKeluar.class);
        q.with(pageable);
        return new PageImpl<>(template.find(q, KartuKendaliSuratKeluar.class),pageable,total);
    }

    private int getNomor(final String kode) {
        DateTime dt=new DateTime();
        Counter c = this.cDao.findByKodeAndYear(kode, dt.getYear());
        if (c == null) {
            c = new Counter();
            c.setKode(kode);
            c.setYear(dt.getYear());
            c.setSeri(0);
        }
        
        c.setSeri(c.getSeri() + 1);
        cDao.save(c);
        return c.getSeri();
    }
    private String getRomawi(final int i) {
        switch (i) {
            case 1: {
                return "I";
            }
            case 2: {
                return "II";
            }
            case 3: {
                return "III";
            }
            case 4: {
                return "IV";
            }
            case 5: {
                return "V";
            }
            case 6: {
                return "VI";
            }
            case 7: {
                return "VII";
            }
            case 8: {
                return "VIII";
            }
            case 9: {
                return "IX";
            }
            case 10: {
                return "X";
            }
            case 11: {
                return "XI";
            }
            default: {
                return "XII";
            }
        }
    }
}
