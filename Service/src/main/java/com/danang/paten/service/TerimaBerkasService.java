// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import com.danang.paten.domain.TerimaBerkas;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class TerimaBerkasService
{
    @Autowired
    private MongoTemplate template;
    
    public Page<TerimaBerkas> belumDiperiksa(final Date tanggal1, final Date tanggal2, final String status, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.andOperator(Criteria.where("tanggalTerima").gte(tanggal1), Criteria.where("tanggalTerima").lte(tanggal2));
        final Criteria c2 = new Criteria();
        c2.andOperator(c, Criteria.where("status").is(status));
        final Query q = new Query();
        q.addCriteria(c2);
        q.with(pageable);
        return new PageImpl<TerimaBerkas>(this.template.find(q, TerimaBerkas.class));
    }
    
    public Page<TerimaBerkas> semuaBerkas(final Date tanggal, final Date akhir, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.andOperator(Criteria.where("tanggalTerima").gte(tanggal), Criteria.where("tanggalTerima").lte(akhir));
        final Query q = new Query();
        q.addCriteria(c);
        q.with(pageable);
        return new PageImpl<TerimaBerkas>(this.template.find(q, TerimaBerkas.class));
    }
}
