// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import java.util.regex.Pattern;
import org.springframework.data.mongodb.core.query.Criteria;
import com.danang.paten.domain.DataPenduduk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.danang.paten.repository.PendudukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class PendudukService
{
    @Autowired
    private MongoTemplate template;
    @Autowired
    private PendudukRepository dao;
    
    public Page<DataPenduduk> cariData(final String cari, final Pageable page) {
        if (cari.equals("")) {
            return (this.dao).findAll(page);
        }
        final Criteria c = new Criteria();
        final Pattern pattern = Pattern.compile(cari, 2);
        c.orOperator(Criteria.where("nik").regex(pattern), Criteria.where("nama").regex(pattern), Criteria.where("link").regex(pattern), Criteria.where("desa").regex(pattern));
        final Query q = new Query(c);
        final long total = this.template.count(q, DataPenduduk.class);
        q.with(page);
        return new PageImpl<>(this.template.find(q, DataPenduduk.class), page, total);
    }
    
    public void hapusData(final String nik) {
        final DataPenduduk x = this.dao.findOne(nik);
        if (x == null) {
            throw new IllegalStateException("data dengan nik " + nik + " tidak ditemukan");
        }
        (this.dao).delete(nik);
    }
    
    public DataPenduduk findPendudukByNik(final String nik) {
        return this.dao.findOne(nik);
    }
    
    public DataPenduduk updateDataPenduduk(final DataPenduduk p) {
        final DataPenduduk x = this.dao.findOne(p.getNik());
        if (x == null) {
            throw new IllegalStateException("data dengan nik " + p.getNik() + " tidak ditemukan");
        }
        p.setNik(x.getNik());
        this.dao.save(p);
        return p;
    }
    
    public DataPenduduk buatPenduduk(final DataPenduduk p) {
        final DataPenduduk dp = this.dao.save(p);
        return dp;
    }
}
