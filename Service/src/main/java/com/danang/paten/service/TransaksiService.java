// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.service;

import org.joda.time.DateTime;
import com.danang.paten.domain.Counter;
import org.springframework.data.domain.Sort;
import com.danang.paten.form.IUM;
import com.danang.paten.form.DomisiliUsaha;
import com.danang.paten.domain.Kecamatan;
import com.danang.paten.form.DispensasiNikah;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.danang.paten.domain.Petugas;
import com.danang.paten.form.RekomendasiPenelitian;
import com.danang.paten.domain.PenetapanBerkas;
import com.danang.paten.domain.BerkasPeriksa;
import com.danang.paten.domain.PeriksaBerkas;
import java.util.List;
import java.util.ArrayList;
import com.danang.paten.form.IMB;
import com.danang.paten.domain.TerimaBerkas;
import com.danang.paten.domain.Layanan;
import com.danang.paten.domain.Token;
import com.danang.paten.form.HO;
import java.util.Date;
import java.util.regex.Pattern;
import org.springframework.data.domain.PageImpl;
import com.danang.paten.enumerasi.StatusPekerjaan;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.form.SPPL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.HashMap;
import java.util.Map;
import com.danang.paten.repository.KecamatanRepository;
import com.danang.paten.repository.TransaksiLayananRepository;
import com.danang.paten.repository.CounterRepository;
import com.danang.paten.repository.TerimaBerkasRepository;
import com.danang.paten.repository.LayananRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.stereotype.Service;

@Service
public class TransaksiService {

    @Autowired
    private MongoTemplate template;
    @Autowired
    private LayananRepository layananDao;
    @Autowired
    private TerimaBerkasRepository terimaBerkasDao;
    @Autowired
    private CounterRepository counterDao;
    @Autowired
    private TransaksiLayananRepository trxDao;
    @Autowired
    private PeriksaBerkasService pbService;
    @Autowired
    private PenetapanService penetapanService;
    @Autowired
    private KecamatanRepository kecDao;
    private final Logger log = Logger.getLogger(TransaksiService.class);

    public Map<String, Object> getAllTransaksi() {
        return new HashMap<>();
    }

    public List<Map> getDashboardTransaksi() {
        Criteria cand = new Criteria();
        Date awal = new DateTime(DateTime.now().getYear(), DateTime.now().monthOfYear().getMinimumValue(), DateTime.now().dayOfMonth().getMinimumValue(), 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), DateTime.now().monthOfYear().getMaximumValue(), DateTime.now().dayOfMonth().getMaximumValue(), 0, 0, 0).toDate();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation m = match(cand);
        GroupOperation g = group("layanan.kode", "layanan.namaLayanan");
        Aggregation agg = newAggregation(m, group("layanan.kode", "layanan.namaLayanan").count().as("total"));
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        Gson gson = new Gson();
        groupResult.getMappedResults().stream().forEach((x) -> {
            System.out.println(gson.toJson(x));
        });
        return groupResult.getMappedResults();
    }

    public List<Map> getTimeLineHarian() {
        Date awal = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth(), 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth() + 1, 0, 0, 0).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        ProjectionOperation projet = project("statusPekerjaan").and("terimaBerkas.tanggalTerima").project("hour").as("jam");
        GroupOperation grup = group("layanan.kode", "$hour:$terimaBerkas.tanggalTerima", "layanan.namaLayanan").count().as("total");

        return new ArrayList<>();
    }

    public List<Map> getDashboardHarian() {
        Date awal = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth(), 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth() + 1, 23, 59, 59).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        GroupOperation grup = group("layanan.kode", "layanan.namaLayanan").count().as("total");
        Gson gson = new Gson();
        Aggregation agg = newAggregation(mat, grup);
        System.out.println(agg.toString());
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        return groupResult.getMappedResults();
    }

    public List<Map> getDashboardTransaksiBulanan() {
        Date awal = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().dayOfMonth().getMinimumValue(), 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().dayOfMonth().getMaximumValue(), 0, 0, 0).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        GroupOperation grup = group("layanan.kode", "layanan.namaLayanan").count().as("total");

        Aggregation agg = newAggregation(mat, grup);
        System.out.println(agg.toString());
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        return groupResult.getMappedResults();
    }

    public List<Map> getDashboardTransaksiMingguan() {
        Date awal = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().dayOfWeek().getMinimumValue(), 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().dayOfWeek().getMaximumValue(), 0, 0, 0).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        GroupOperation grup = group("layanan.kode", "layanan.namaLayanan").count().as("total");

        Aggregation agg = newAggregation(mat, grup);
        System.out.println(agg.toString());
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        return groupResult.getMappedResults();
    }

    public Page<TransaksiLayanan> temukanDataBelumDikembalikan(final Pageable pageable) {
        Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan),
                Criteria.where("statusPekerjaan").is(StatusPekerjaan.Dikembalikan));
        Query q = new Query();
        q.addCriteria(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class));
    }

    public TransaksiLayanan findOne(final String nomor) {
        return this.trxDao.findOne(nomor);
    }

    public Page<TransaksiLayanan> temukanDataBelumDikembalikan(final String cari, final Pageable pageable) {
        final Criteria c = new Criteria();
        final Criteria cor = new Criteria();
        cor.orOperator(Criteria.where("nomor").regex(Pattern.compile(cari, 2)), Criteria.where("penduduk.nama").regex(Pattern.compile(cari, 2)), Criteria.where("penduduk.desa").regex(Pattern.compile(cari, 2)), Criteria.where("penduduk.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomor").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.kode").regex(Pattern.compile(cari, 2)));
        c.andOperator(cor, Criteria.where("statusPekerjaan").is(StatusPekerjaan.Dikembalikan));
        final Query q = new Query(c);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public Page<TransaksiLayanan> getDataTransaksi(final String kode, final Date awal, final Date akhir, final Pageable page) {
        final Criteria c = new Criteria();
        c.andOperator(Criteria.where("tanggal").gte(awal), Criteria.where("tanggal").lte(akhir));
        final Criteria w = new Criteria();
        w.andOperator(c, Criteria.where("nomorLayanan").is(kode));
        final Query q = new Query();
        q.addCriteria(w);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(page);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), page, total);
    }

    public void simpanHOTerimaBerkas(HO ho) {
        final TransaksiLayanan tl = new TransaksiLayanan();
        tl.setKonten(this.getStringKonten(ho));
        final Layanan l = this.layananDao.findOne("503");
        if (l == null) {
            throw new IllegalStateException("Layanan tidak ditemukan, silahkan isi data layanan");
        }
        tl.setNomor(this.getRegistrasiNomorLayanan("503"));
        tl.setTanggal(new Date());
        tl.setPenduduk(ho.getPemohon());
        tl.setStatusPekerjaan(StatusPekerjaan.Pemberkasan);
        final TerimaBerkas tb = new TerimaBerkas();
        tb.setBerkas(ho.getItems());
        tb.setIjin(ho.getIjin());
        tb.setPemohon(ho.getPemohon());
        tb.setDisposisi(l.getDisposisi());
        tb.setLayanan(l);
        tb.setNamaPengaju(ho.getPemohon().getNama());
        tb.setPetugas(ho.getRegistrasi());
        tb.setStatus("DITERIMA");
        tb.setTanggalTerima(ho.getTanggalPermohonan());
        TerimaBerkas x = terimaBerkasDao.save(tb);
        tl.setTerimaBerkas(x);
        trxDao.save(tl);
    }

    public void simpanImbTerimaBerkas(final IMB imb) {
        final TransaksiLayanan tl = new TransaksiLayanan();
        tl.setKonten(this.getStringKonten(imb));
        tl.setStatusPekerjaan(StatusPekerjaan.Pemberkasan);
        tl.setNomor(this.getRegistrasiNomorLayanan(""));
        Layanan l = this.layananDao.findOne("");
        if (l == null) {
            l = new Layanan();
            l.setKode("");
            l.setDisposisi("Perijinan");
            l.setMulaiSeri(0);
            l.setSyarat(new ArrayList<>());
            this.layananDao.save(l);
        }
        tl.setLayanan(l);
        tl.setTanggal(new Date());
    }

    public TransaksiLayanan simpanTerimaBerkas(final TerimaBerkas tb) {
        tb.setNomor(this.getRegistrasiNomorLayanan("TB" + tb.getLayanan().getKode()));
        this.terimaBerkasDao.save(tb);
        TransaksiLayanan tl = new TransaksiLayanan();
        String kode = tb.getLayanan().getKode();
        tl.setNomor(this.getRegistrasiNomorLayanan("TL"));
        tl.setTerimaBerkas(tb);
        tl.setPenduduk(tb.getIjin());
        final Layanan l = this.layananDao.findOne(kode);
        tl.setLayanan(l);
        tl.setValidasi(false);
        tl.setStatusPekerjaan(StatusPekerjaan.Pemberkasan);
        if (l.isLangsungTetapkan()) {
            final PeriksaBerkas pb = new PeriksaBerkas();
            pb.setTanggal(new Date());
            pb.setLanjut(true);
            pb.setPetugas(tb.getPetugas());
            pb.setCatatan("");
            pb.setNomor(this.pbService.getNomorPeriksaLayanan(kode));
            final List<BerkasPeriksa> pbs = new ArrayList<>();
            tb.getBerkas().stream().forEach(x -> {
                BerkasPeriksa bp = new BerkasPeriksa();
                bp.setBerkas(x);
                bp.setCatatan("");
                bp.setValid(true);
            });
            tl.setPeriksaBerkas(pb);
            final PenetapanBerkas tetap = new PenetapanBerkas();
            tetap.setPetugas(tb.getPetugas());
            tetap.setTanggal(new Date());
            tetap.setValidasi(true);
            tetap.setItems(new ArrayList<>());
            tetap.setNomor(this.penetapanService.getNomorPenetapanLayanan(kode));
            tl.setPenetapanBerkas(tetap);
            tl.setValidasi(true);
            tl.setDitetapkan(true);
            tl.setCatatan(tb.getKeterangan());
            tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        }
        TransaksiLayanan rpe = trxDao.save(tl);
        log.debug(rpe);
        return rpe;
    }

    public PeriksaBerkas simpanPeriksaBerkas(final TransaksiLayanan trx) {
        final PeriksaBerkas pb = trx.getPeriksaBerkas();
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + trx.getLayanan().getKode()));
        final TransaksiLayanan x = this.trxDao.findOne(trx.getNomor());
        if (x == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        trx.setPeriksaBerkas(pb);
        if (pb.isLanjut()) {
            trx.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            trx.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        trx.setPeriksaDate(pb.getTanggal());
        this.trxDao.save(trx);
        return pb;
    }

    public TransaksiLayanan simpanPeriksaRekomendasiPenelitian(final RekomendasiPenelitian pr, final String nomor, final Petugas ptg, final boolean lanjut, final String catatan, final List<BerkasPeriksa> periksa) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        pr.setDp(tl.getTerimaBerkas().getIjin());
        tl.setKonten(gson.toJson(pr));
        return this.trxDao.save(tl);
    }

    public Map<String, Object> dataArsip(String cari, Pageable p) {
        Map<String, Object> result = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        sb.append("_id,penduduk.nama,terimaBerkas._id,terimaBerkas.petugas.nama,layanan.namaLayanan");
        Criteria c = new Criteria();
        if (cari.isEmpty()) {
            c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai));
        } else {
            Pattern pattern = Pattern.compile(cari, 2);
            Criteria c1 = new Criteria();
            c1.orOperator(Criteria.where("nomor").regex(pattern), Criteria.where("terimaBerkas.nomor").regex(pattern), Criteria.where("terimaBerkas.disposisi").regex(pattern), Criteria.where("terimaBerkas.pemohon.nama").regex(pattern), Criteria.where("terimaBerkas.pemohon.nik").regex(pattern), Criteria.where("terimaBerkas.ijin.nama").regex(pattern), Criteria.where("terimaBerkas.ijin.nik").regex(pattern), Criteria.where("terimaBerkas.nomorDesa").regex(pattern), Criteria.where("layanan.kode").regex(pattern), Criteria.where("layanan.namaLayanan").regex(pattern), Criteria.where("pendaftaran.nama").regex(pattern), Criteria.where("pendaftaran.nik").regex(pattern));
            Criteria c2 = new Criteria();
            c2.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai));
            c.andOperator(c1, c2);
        }
        Aggregation agg = newAggregation(project(Fields.from(
                Fields.field("kode", "nomor"),
                Fields.field("pemohon", "penduduk.nama"),
                Fields.field("terimaBerkas", "terimaBerkas.nomor"),
                Fields.field("petugasTerimaBerkas", "terimaBerkas.petugas.nama"),
                Fields.field("layanan", "layanan.namaLayanan"),
                Fields.field("periksaBerkas", "periksaBerkas.nomor"),
                Fields.field("petugasPeriksaBerkas", "periksaBerkas.petugas.nama")
        )), match(c));
        result.put("data", template.aggregate(agg, TransaksiLayanan.class, Map.class));
        return result;
    }

    public Page<TransaksiLayanan> temukanDataDitetapkan(String cari, Pageable p) {
        Criteria c = new Criteria();
        if (cari.isEmpty()) {
            c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai));
        } else {
            Pattern pattern = Pattern.compile(cari, 2);
            Criteria c1 = new Criteria();
            c1.orOperator(Criteria.where("nomor").regex(pattern), Criteria.where("terimaBerkas.nomor").regex(pattern), Criteria.where("terimaBerkas.disposisi").regex(pattern), Criteria.where("terimaBerkas.pemohon.nama").regex(pattern), Criteria.where("terimaBerkas.pemohon.nik").regex(pattern), Criteria.where("terimaBerkas.ijin.nama").regex(pattern), Criteria.where("terimaBerkas.ijin.nik").regex(pattern), Criteria.where("terimaBerkas.nomorDesa").regex(pattern), Criteria.where("layanan.kode").regex(pattern), Criteria.where("layanan.namaLayanan").regex(pattern), Criteria.where("pendaftaran.nama").regex(pattern), Criteria.where("pendaftaran.nik").regex(pattern));
            Criteria c2 = new Criteria();
            c2.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai));
            c.andOperator(c1, c2);
        }
        Query q = new Query(c);
        long total = template.count(q, TransaksiLayanan.class);
        q.with(p);
        return new PageImpl(template.find(q, TransaksiLayanan.class), p, total);
    }

    public TransaksiLayanan simpanPeriksaPenelitan(final List<BerkasPeriksa> periksa, final TransaksiLayanan tl, final String catatan, final boolean lanjut, final RekomendasiPenelitian rp, final Petugas p) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(p);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        tl.setKonten(catatan);
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        rp.setDp(tl.getTerimaBerkas().getIjin());
        tl.setKonten(gson.toJson(rp));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPeriksaDispensasiNikah(final DispensasiNikah dn, final String nomor, final Petugas ptg, final boolean lanjut, final String catatan, final List<BerkasPeriksa> periksa) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        dn.setNomor(pb.getNomor());
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            dn.setCamat(k.getCamat());
            dn.setNipCamat(k.getNip());
            dn.setJabatanCamat(k.getJabatan());
        }
        tl.setKonten(gson.toJson(dn));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPeriksaDomisiliUsaha(final DomisiliUsaha du, final String nomor, final Petugas ptg, final boolean lanjut, final String catatan, final List<BerkasPeriksa> periksa) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        du.setPenduduk(tl.getTerimaBerkas().getIjin());
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            du.setCamat(k.getCamat());
            du.setNipCamat(k.getNip());
            du.setJabatanCamat(k.getJabatan());
        }
        tl.setKonten(gson.toJson(du));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPeriksaHO(final HO ho, final String nomor, final Petugas petugas, final boolean lanjut, final String catatan, final List<BerkasPeriksa> periksa) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(petugas);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        ho.setPemeriksa(petugas);
        ho.setTanggalPemeriksaan(pb.getTanggal());
        final Kecamatan k = this.getKecamatan();
        ho.setIjin(tl.getTerimaBerkas().getIjin());
        ho.setPemohon(tl.getTerimaBerkas().getPemohon());
        ho.setItems(tl.getTerimaBerkas().getBerkas());
        ho.setRegistrasi(tl.getTerimaBerkas().getPetugas());
        ho.setTanggalPermohonan(tl.getTerimaBerkas().getTanggalTerima());
        if (k != null) {
            ho.setCamat(k.getCamat());
            ho.setNip(k.getNip());
            ho.setJabatanCamat(k.getJabatan());
        }
        tl.setKonten(gson.toJson(ho));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPeriksaIUM(final IUM ium, final String nomor, final Petugas ptg, final boolean lanjut, final String catatan, final List<BerkasPeriksa> periksa) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        final PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        ium.setPetugas(ptg);
        ium.setNomorDaftar(tl.getTerimaBerkas().getNomor());
        ium.setNomorPeriksa(pb.getNomor());
        ium.setRegisterDate(tl.getTerimaBerkas().getTanggalTerima());
        ium.setPeriksa(new Date());
        ium.setDp(tl.getTerimaBerkas().getIjin());
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            ium.setCamat(k.getCamat());
            ium.setNipCamat(k.getNip());
            ium.setJabatanCamat(k.getJabatan());
        }
        tl.setKonten(gson.toJson(ium));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPeriksaSppl(SPPL spp, String nomor, Petugas ptg,
            boolean lanjut, List<BerkasPeriksa> periksa, String catatan) {
        TransaksiLayanan tl = trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        TerimaBerkas tb = tl.getTerimaBerkas();
        spp.setPemohon(tb.getPemohon());
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            spp.setCamat(k.getCamat());
            spp.setPangkat(k.getJabatan());
            spp.setNipCamat(k.getNip());
        }
        Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setPeriksa(periksa);
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        pb.setNomor(getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        tl.setKonten(gson.toJson(spp));
        tl.setPeriksaBerkas(pb);
        return trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanSPPL(Token token, String nomor) {
        TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        PenetapanBerkas pb = new PenetapanBerkas();
        pb.setPetugas(token.getPetugas());
        pb.setNomor(penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        System.out.println(gson.toJson(pb));
        tl.setPenetapanBerkas(pb);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setDitetapkan(true);
        return trxDao.save(tl);
    }

    public TransaksiLayanan simpanPeriksaIMB(final IMB imb, final String nomor, final Petugas ptg, final boolean lanjut, final String catatan, final List<BerkasPeriksa> periksa) {
        final TransaksiLayanan tl = this.trxDao.findOne(nomor);
        if (tl == null) {
            throw new IllegalStateException("Berkas tidak ditemukan");
        }
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Penetapan);
        } else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        PeriksaBerkas pb = new PeriksaBerkas();
        pb.setLanjut(lanjut);
        pb.setCatatan(catatan);
        pb.setNomor(this.getRegistrasiNomorLayanan("PP" + tl.getLayanan().getKode()));
        pb.setPeriksa(periksa);
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        tl.setPeriksaBerkas(pb);
        tl.setPeriksaDate(pb.getTanggal());
        imb.setPemohon(tl.getTerimaBerkas().getPemohon());
        imb.setIjin(tl.getTerimaBerkas().getIjin());
        imb.setKab("Kabupaten Semarang");
        imb.setKecamatan("Bergas");
        imb.setNomorRegistrasi(tl.getTerimaBerkas().getNomor());
        imb.setNomorValidasi(pb.getNomor());
        tl.setKonten(gson.toJson(imb));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanHO(final Date tanggal, final Petugas petugas, final TransaksiLayanan tl, final Date awal, final Date akhir) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final HO ho = gson.fromJson(tl.getKonten(), HO.class);
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            ho.setCamat(k.getCamat());
            ho.setNip(k.getNip());
            ho.setJabatanCamat(k.getJabatan());
        }
        ho.setKetetapan(tanggal);
        ho.setAkhirBerlaku(akhir);
        ho.setMulaiBerlaku(awal);
        final PenetapanBerkas pb = new PenetapanBerkas();
        pb.setNomor(this.penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setPetugas(petugas);
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        pb.setMulaiBerlaku(ho.getMulaiBerlaku());
        pb.setAkhirBerlaku(ho.getAkhirBerlaku());
        ho.setNomor(pb.getNomor());
        ho.setMulaiBerlaku(pb.getMulaiBerlaku());
        ho.setAkhirBerlaku(pb.getAkhirBerlaku());
        tl.setKonten(gson.toJson(ho));
        tl.setPenetapanBerkas(pb);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setDitetapkan(true);
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanPenelitian(final Date tanggal, final Petugas petugas, final TransaksiLayanan tl) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final RekomendasiPenelitian rp = gson.fromJson(tl.getKonten(), RekomendasiPenelitian.class);
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            rp.setCamat(k.getCamat());
            rp.setJabatanCamat(k.getJabatan());
            rp.setNipCamat(k.getNip());
        }
        final PenetapanBerkas pb = new PenetapanBerkas();
        String nomor = this.penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan());
        pb.setNomor(nomor);
        pb.setPetugas(petugas);
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        pb.setMulaiBerlaku(rp.getMulai());
        pb.setAkhirBerlaku(rp.getSelesai());
        rp.setNomor(nomor);
        rp.setNomor(pb.getNomor());
        rp.setDitetapkan(new Date());
        tl.setKonten(gson.toJson(rp));
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setPenetapanBerkas(pb);
        tl.setDitetapkan(true);
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanDispensasiMenikah(final Date tanggal, final Petugas petugas, final TransaksiLayanan tl) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final DispensasiNikah dn = gson.fromJson(tl.getKonten(), DispensasiNikah.class);
        dn.setKetetapan(tanggal);
        final PenetapanBerkas pb = new PenetapanBerkas();
        pb.setNomor(this.penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setPetugas(petugas);
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setPenetapanBerkas(pb);
        tl.setDitetapkan(true);
        dn.setNomor(pb.getNomor());
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            dn.setCamat(k.getCamat());
            dn.setNipCamat(k.getNip());
            dn.setJabatanCamat(k.getJabatan());
        }
        tl.setKonten(gson.toJson(dn));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanDomisiliUsaha(final Date ketetapan, final Date berlaku, final Date selesai, final TransaksiLayanan tl, final Petugas ptg) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final DomisiliUsaha du = gson.fromJson(tl.getKonten(), DomisiliUsaha.class);
        final PenetapanBerkas pb = new PenetapanBerkas();
        pb.setNomor(this.penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setPetugas(ptg);
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        pb.setAkhirBerlaku(selesai);
        pb.setMulaiBerlaku(berlaku);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setPenetapanBerkas(pb);
        du.setNomorKetetapan(pb.getNomor());
        du.setTanggalBerlaku(berlaku);
        du.setTanggalHabisBerlaku(selesai);
        du.setTanggal(ketetapan);
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            du.setCamat(k.getCamat());
            du.setNipCamat(k.getNip());
            du.setJabatanCamat(k.getJabatan());
        }
        tl.setDitetapkan(true);
        tl.setKonten(gson.toJson(du));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanIUM(Date penetapan, Petugas p, TransaksiLayanan tl, final Date berlaku, final Date akhir) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final IUM ium = gson.fromJson(tl.getKonten(), IUM.class);
        final PenetapanBerkas pb = new PenetapanBerkas();
        pb.setNomor(this.penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setPetugas(p);
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        pb.setMulaiBerlaku(berlaku);
        pb.setAkhirBerlaku(akhir);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setPenetapanBerkas(pb);
        tl.setDitetapkan(true);
        ium.setMulaiBerlaku(berlaku);
        ium.setAkhirBerlaku(akhir);
        ium.setDitetapkan(penetapan);
        ium.setNomorKetetapan(pb.getNomor());
        tl.setBerlakuSurat(berlaku);
        tl.setExpiredSurat(akhir);
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            ium.setCamat(k.getCamat());
            ium.setJabatanCamat(k.getJabatan());
            ium.setNipCamat(k.getNip());
        }
        tl.setKonten(gson.toJson(ium));
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanUmum(final Date tanggal, final TransaksiLayanan tl, final Petugas p) {
        PenetapanBerkas pb = new PenetapanBerkas();
        pb.setNomor(penetapanService.getNomorPenetapanLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setPetugas(p);
        pb.setTanggal(tanggal);
        pb.setValidasi(true);
        tl.setDitetapkan(true);
        tl.setPenetapanBerkas(pb);
        return this.trxDao.save(tl);
    }

    public TransaksiLayanan simpanPenetapanIMB(final Date selesai, final Date mulai, final Petugas p, final TransaksiLayanan tl) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final IMB imb = gson.fromJson(tl.getKonten(), IMB.class);
        imb.setBerlakuMulai(mulai);
        imb.setBerlakuSampai(selesai);
        final PenetapanBerkas pb = new PenetapanBerkas();
        pb.setNomor(this.getRegistrasiNomorLayanan(tl.getLayanan().getKodeTetapan()));
        pb.setPetugas(p);
        pb.setTanggal(new Date());
        pb.setValidasi(true);
        pb.setMulaiBerlaku(mulai);
        pb.setAkhirBerlaku(selesai);
        tl.setStatusPekerjaan(StatusPekerjaan.Diserahkan);
        tl.setPenetapanBerkas(pb);
        imb.setKetetapan(pb.getTanggal());
        imb.setNomorKetetapan(pb.getNomor());
        imb.setPetugas(p);
        imb.setTanggalKetetapan(new Date());
        tl.setDitetapkan(true);
        final Kecamatan k = this.getKecamatan();
        if (k != null) {
            imb.setCamat(k.getCamat());
            imb.setNipCamat(k.getNip());
            imb.setJabatanCamat(k.getJabatan());
        }
        tl.setKonten(gson.toJson(imb));
        return this.trxDao.save(tl);
    }

    private Kecamatan getKecamatan() {
        final List<Kecamatan> items = this.kecDao.findAll();
        if (items.size() > 0) {
            return items.get(0);
        }
        return null;
    }

    public Page<TransaksiLayanan> getAll(final Pageable pagealbe) {
        final Query q = new Query();
        q.addCriteria(Criteria.where("statusPekerjaan").ne(StatusPekerjaan.Selesai));
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pagealbe);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pagealbe, total);
    }

    public Page<TransaksiLayanan> getBerkasSerahkan(final Pageable pageable) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Dikembalikan), Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan));
        final Query q = new Query();
        q.addCriteria(c);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        final Sort sort = new Sort(Sort.Direction.DESC, new String[]{"tanggal"});
        q.with(sort);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public List<Map<String, Object>> getLaporanHO(final Date awal, final Date akhir) {
        final List<Map<String, Object>> map = new ArrayList<Map<String, Object>>();
        final Criteria tgl = new Criteria();
        tgl.andOperator(Criteria.where("tanggal"));
        return map;
    }

    public Page<TransaksiLayanan> findAllPenetapan(final Pageable pageable) {
        final Query q = new Query();
        q.addCriteria(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Penetapan));
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class));
    }

    public Page<TransaksiLayanan> findallPeriksa(final Pageable pageable) {
        final Query q = new Query();
        q.addCriteria(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Pemberkasan));
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public Page<TransaksiLayanan> cariPenetapanData(final String cari, final Pageable pageable) {
        final Pattern pattern = Pattern.compile(cari, 2);
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("nomor").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomor").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.disposisi").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.pemohon.nama").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.pemohon.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.ijin.nama").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.ijin.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomorDesa").regex(Pattern.compile(cari, 2)), Criteria.where("layanan.kode").regex(Pattern.compile(cari, 2)), Criteria.where("layanan.namaLayanan").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.nama").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.nik").regex(Pattern.compile(cari, 2)), Criteria.where("periksaBerkas.nomor").regex(Pattern.compile(cari, 2)), Criteria.where("periksaBerkas.catatan").regex(Pattern.compile(cari, 2)), Criteria.where("periksaBerkas.lanjut").regex(pattern));
        final Criteria c2 = new Criteria();
        c2.andOperator(c, Criteria.where("statusPekerjaan").is(StatusPekerjaan.Penetapan));
        final Query q = new Query();
        q.addCriteria(c2);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public Page<TransaksiLayanan> cariPeriksaData(final String cari, final Pageable pageable) {
        log.debug("cari data " + cari);
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("nomor").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomor").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.disposisi").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.pemohon.nama").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.pemohon.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.ijin.nama").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.ijin.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomorDesa").regex(Pattern.compile(cari, 2)), Criteria.where("layanan.kode").regex(Pattern.compile(cari, 2)), Criteria.where("layanan.namaLayanan").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.nama").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.nik").regex(Pattern.compile(cari, 2)));
        final Criteria c2 = new Criteria();
        c2.andOperator(c, Criteria.where("statusPekerjaan").is(StatusPekerjaan.Pemberkasan));
        final Query q = new Query();
        q.addCriteria(c2);
        final long total = this.template.count(q, TransaksiLayanan.class);
        log.debug("total " + total);
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public Page<TransaksiLayanan> cariData(final String cari, final Pageable pageable) {
        final Criteria c = new Criteria();
        final Pattern pattern = Pattern.compile(cari, 2);
        c.orOperator(Criteria.where("nomor").regex(pattern), Criteria.where("terimaBerkas.nomor").regex(pattern), Criteria.where("terimaBerkas.disposisi").regex(pattern), Criteria.where("terimaBerkas.pemohon.nama").regex(pattern), Criteria.where("terimaBerkas.pemohon.nik").regex(pattern), Criteria.where("terimaBerkas.ijin.nama").regex(pattern), Criteria.where("terimaBerkas.ijin.nik").regex(pattern), Criteria.where("terimaBerkas.nomorDesa").regex(pattern), Criteria.where("layanan.kode").regex(pattern), Criteria.where("layanan.namaLayanan").regex(pattern), Criteria.where("pendaftaran.nama").regex(pattern), Criteria.where("pendaftaran.nik").regex(pattern));
        final Query q = new Query(c);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        final List<TransaksiLayanan> items = this.template.find(q, TransaksiLayanan.class);
        return new PageImpl<>(items, pageable, total);
    }

    public Page<TransaksiLayanan> cariData(final String cari, final Date awal, final Date akhir, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.orOperator(Criteria.where("nomor").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomor").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.disposisi").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.pemohon.nama").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.pemohon.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.ijin.nama").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.ijin.nik").regex(Pattern.compile(cari, 2)), Criteria.where("terimaBerkas.nomorDesa").regex(Pattern.compile(cari, 2)), Criteria.where("layanan.kode").regex(Pattern.compile(cari, 2)), Criteria.where("layanan.namaLayanan").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.nama").regex(Pattern.compile(cari, 2)), Criteria.where("pendaftaran.nik").regex(Pattern.compile(cari, 2)));
        final Criteria oc = new Criteria();
        oc.andOperator(Criteria.where("tanggal").gte(awal), Criteria.where("tanggal").lte(akhir), c);

        final Query q = new Query();
        q.addCriteria(oc);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        q.with(new Sort(Sort.Direction.DESC, new String[]{"tanggal"}));
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public Page<TransaksiLayanan> getAllPeriksa(final Pageable pageable) {
        final Query q = new Query();
        q.addCriteria(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Pemberkasan));
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public Page<TransaksiLayanan> getAllPeriksa(final Date awal, final Date akhir, final Pageable pageable) {
        final Criteria c = new Criteria();
        c.andOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Pemberkasan), Criteria.where("tanggal").lte(awal), Criteria.where("tanggal").gte(akhir));
        final Query q = new Query(c);
        final long total = this.template.count(q, TransaksiLayanan.class);
        q.with(pageable);
        return new PageImpl<>(this.template.find(q, TransaksiLayanan.class), pageable, total);
    }

    public String getLayananStatus(String kode) {
        Criteria c = new Criteria();
        return "";
    }

    public String getRegistrasiNomorLayanan(final String kode) {
        DateTime ddm = new DateTime();
        Counter counter = this.counterDao.findByKodeAndYear(kode, ddm.getYear());
        if (counter == null) {
            counter = new Counter();
            counter.setKode(kode);
            counter.setYear(ddm.getYear());
            counter.setSeri(0);
        }
        counter.setSeri(counter.getSeri() + 1);
        this.counterDao.save(counter);
        final StringBuilder sb = new StringBuilder();
        sb.append(kode).append("/");
        sb.append(counter.getSeri()).append("/");
        final DateTime dt = new DateTime();
        sb.append(dt.getYear());
        return sb.toString();
    }

    private String getStringKonten(final Object model) {
        final Gson gson = new GsonBuilder().setDateFormat("dd MM yyyy").create();
        return gson.toJson(model);
    }
}
