// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.service;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import java.util.regex.Pattern;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.danang.paten.domain.RekomendasiBantuan;
import com.danang.paten.domain.Counter;
import org.springframework.data.mongodb.core.MongoTemplate;
import com.danang.paten.repository.RekomendasiBantuanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.CounterRepository;
import org.springframework.stereotype.Service;

@Service
public class ProposalService {

    @Autowired
    private CounterRepository counterDao;
    @Autowired
    private RekomendasiBantuanRepository bantuanDao;
    @Autowired
    private MongoTemplate template;

    private String getNomor() {
        Counter c = this.counterDao.findOne("bantuan");
        if (c == null) {
            c = new Counter();
            c.setKode("bantuan");
            c.setSeri(0);
        }
        c.setSeri(c.getSeri() + 1);
        this.counterDao.save(c);
        return String.valueOf(c.getSeri());
    }

    public RekomendasiBantuan simpanBantuanDao(final RekomendasiBantuan rb) {
        rb.setNomor(this.getNomor());
        return this.bantuanDao.save(rb);
    }

    public Page<RekomendasiBantuan> findBantuans(final String cari, final Pageable p) {
        final Pattern pattern = Pattern.compile(cari, 2);
        final Criteria or = new Criteria();
        or.orOperator(Criteria.where("nomor").regex(pattern), Criteria.where("dp.nik").regex(pattern), Criteria.where("dp.nama").regex(pattern), Criteria.where("dp.desa").regex(pattern), Criteria.where("jenisBantuan").regex(pattern), Criteria.where("tujuanProposal").regex(pattern), Criteria.where("keterangan").regex(pattern));
        final Query q = new Query(or);
        q.with(p);
        return new PageImpl<RekomendasiBantuan>(this.template.find(q, RekomendasiBantuan.class));
    }

    public RekomendasiBantuan temukan(final String kode) {
        return this.bantuanDao.findOne(kode);
    }

    public void deleteBantuan(final String kode) {
        final RekomendasiBantuan rb = this.bantuanDao.findOne(kode);
        if (rb == null) {
            throw new IllegalStateException("kode tidak ditemukan");
        }
        (this.bantuanDao).delete(rb);
    }

    public void updateBantuan(final RekomendasiBantuan rb) {
        final RekomendasiBantuan x = this.bantuanDao.findOne(rb.getNomor());
        if (rb == null) {
            throw new IllegalStateException("kode tidak ditemukan");
        }
        rb.setNomor(x.getNomor());
        this.bantuanDao.save(rb);
    }

    public Page<RekomendasiBantuan> findAll(final Pageable p) {
        return (this.bantuanDao).findAll(p);
    }
}
