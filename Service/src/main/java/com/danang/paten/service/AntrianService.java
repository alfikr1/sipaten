/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.Antrian;
import com.danang.paten.repository.AntrianRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author danang
 */
@Service
public class AntrianService {

    private @Autowired
    AntrianRepository dao;
    private @Autowired
    CounterService counterService;
    private @Autowired
    MongoTemplate template;

    public void simpanData(Antrian antrian) {
        String id = counterService.getNextSeq("antrian") + "/" + Calendar.getInstance().get(Calendar.YEAR);
        antrian.setId(id);
        dao.save(antrian);
    }

    public void hapusData(String id) {
        Antrian x = dao.findOne(id);
        if (x == null) {
            throw new IllegalStateException("data tidak ditemukan");
        }
        dao.delete(x);
    }

    public Page<Antrian> getAllByDateJenis(String jenis, int loket, Date awal, Date akhir, Pageable pageable) {
        Criteria c = new Criteria();
        if (jenis.isEmpty() && loket < 0) {
            c.andOperator(Criteria.where("tanggal").gte(awal), Criteria.where("tanggal").lte(akhir));
        } else if (!jenis.isEmpty() && loket < 0) {
            Criteria js = Criteria.where("jenis").is(jenis);
            c.andOperator(js, Criteria.where("tanggal").gte(awal), Criteria.where("tanggal").lte(akhir));
        } else {
            c.andOperator(Criteria.where("jenis").is(jenis), Criteria.where("loket").is(loket), Criteria.where("tanggal").gte(awal), Criteria.where("tanggal").lte(akhir));
        }
        Query q = new Query();
        q.addCriteria(c);
        long total = template.count(q, Antrian.class);
        q.with(pageable);
        List<Antrian> items = template.find(q, Antrian.class);
        return new PageImpl(items, pageable, total);
    }

}
