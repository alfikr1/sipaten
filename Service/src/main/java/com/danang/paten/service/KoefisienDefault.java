/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.KoefisienIMB;
import com.danang.paten.repository.Koefisiene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author danang
 */
@Service
public class KoefisienDefault {
    private @Autowired CounterService counter;
    private @Autowired Koefisiene kof;
    
    
    
    private void buatDefault(){
        KoefisienIMB ki1 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"KOTA RANGKING I","KOEFISIEN KOTA / DAERAH",1.00);
        KoefisienIMB ki2 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"KOTA RANGKING II","KOEFISIEN KOTA / DAERAH",0.90);
        KoefisienIMB ki3 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"KOTA RANGKING III","KOEFISIEN KOTA / DAERAH",0.80);
        KoefisienIMB ki4 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"KAWASAN KHUSUS","KOEFISIEN KOTA / DAERAH",0.80);
        KoefisienIMB ki5 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"PEDESAAN","KOEFISIEN KOTA / DAERAH",0.50);
        KoefisienIMB ki6 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Permanen dengan dinding batu bata dan /atau kayu kelas satu, konstruksi beton baja",
                "KOEFISIEN KELAS BANGUNAN",1.00);
        KoefisienIMB ki7 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Permanen dengan dinding batu bata biasa",
                "KOEFISIEN KELAS BANGUNAN",0.75);
        KoefisienIMB ki8 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Semi permanen / kotangan",
                "KOEFISIEN KELAS BANGUNAN",0.50);
        KoefisienIMB ki9 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Temporer dengan dinding bambu / papan dan lain-lain",
                "KOEFISIEN KELAS BANGUNAN",0.20);        
        KoefisienIMB ki10 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan Orang Pribadi",
                "KOEFISIEN STATUS BANGUNAN",1.00);
        KoefisienIMB ki11 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan Badan",
                "KOEFISIEN STATUS BANGUNAN",1.30);
        KoefisienIMB ki12 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan dengan luas s/d 100m2",
                "KOEFISIEN LUAS BANGUNAN",0.75);
        KoefisienIMB ki13 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan dengan luas 101 s/d 250m2",
                "KOEFISIEN LUAS BANGUNAN",1.00);
        KoefisienIMB ki14 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan dengan luas 251 s/d 500m2",
                "KOEFISIEN LUAS BANGUNAN",1.25);
        KoefisienIMB ki15 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan dengan luas 501 s/d 1.000m2",
                "KOEFISIEN LUAS BANGUNAN",1.50);
        KoefisienIMB ki16 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan dengan luas > 1.000m2",
                "KOEFISIEN LUAS BANGUNAN",1.75);
        KoefisienIMB ki17 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan di Pinggir Jalan Arteri",
                "KOEFISIEN FUNGSI JALAN",1.20);
        KoefisienIMB ki18 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan di Pinggir Jalan Kolektor",
                "KOEFISIEN FUNGSI JALAN",1.10);
        KoefisienIMB ki19 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan di Pinggir Jalan Lokal",
                "KOEFISIEN FUNGSI JALAN",1.00);
        KoefisienIMB ki20 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan di Pinggir Jalan Lingkungan",
                "KOEFISIEN FUNGSI JALAN",0.60);
        KoefisienIMB ki21 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan di Pinggir Jalan Tol",
                "KOEFISIEN FUNGSI JALAN",2.40);
        KoefisienIMB ki22 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan perdagangan dan jasa",
                "KOEFISIEN GUNA BANGUNAN",2.50);
        KoefisienIMB ki23 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan perindustrian",
                "KOEFISIEN GUNA BANGUNAN",1.50);
        KoefisienIMB ki24 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan perumahan",
                "KOEFISIEN GUNA BANGUNAN",1.00);
        KoefisienIMB ki25 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan kelembagaan",
                "KOEFISIEN GUNA BANGUNAN",0.80);
        KoefisienIMB ki26 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan umum",
                "KOEFISIEN GUNA BANGUNAN",0.60);
        KoefisienIMB ki27 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan pendidikan",
                "KOEFISIEN GUNA BANGUNAN",0.60);
        KoefisienIMB ki28 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan khusus",
                "KOEFISIEN GUNA BANGUNAN",0.60);
        KoefisienIMB ki29 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan campuran",
                "KOEFISIEN GUNA BANGUNAN",2.00);
        KoefisienIMB ki30 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan sosial",
                "KOEFISIEN GUNA BANGUNAN",0.40);
        KoefisienIMB ki31 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan lain-lain",
                "KOEFISIEN GUNA BANGUNAN",0.80);
        KoefisienIMB ki32 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan 1 lantai",
                "KOEFISIEN TINGKAT BANGUNAN",1.00);
        KoefisienIMB ki33 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan 2 lantai",
                "KOEFISIEN TINGKAT BANGUNAN",1.20);
        KoefisienIMB ki34 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan 3 lantai",
                "KOEFISIEN TINGKAT BANGUNAN",1.50);
        KoefisienIMB ki35 = new KoefisienIMB(counter.getNextSeq("KofIMB"),"Bangunan 4 lantai",
                "KOEFISIEN TINGKAT BANGUNAN",2.00);
        
        kof.save(ki1);
        kof.save(ki2);
        kof.save(ki3);
        kof.save(ki4);
        kof.save(ki5);
        kof.save(ki6);
        kof.save(ki7);
        kof.save(ki8);
        kof.save(ki9);
        kof.save(ki10);
        kof.save(ki11);
        kof.save(ki12);
        kof.save(ki13);
        kof.save(ki14);
        kof.save(ki15);
        kof.save(ki16);
        kof.save(ki17);
        kof.save(ki18);
        kof.save(ki19);
        kof.save(ki20);
        kof.save(ki21);
        kof.save(ki22);
        kof.save(ki23);
        kof.save(ki24);
        kof.save(ki25);
        kof.save(ki26);
        kof.save(ki27);
        kof.save(ki28);
        kof.save(ki29);
        kof.save(ki30);
        kof.save(ki31);
        kof.save(ki32);
        kof.save(ki33);
        kof.save(ki34);
        kof.save(ki35);
    }
}
