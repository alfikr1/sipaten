/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.Artikel;
import com.danang.paten.domain.Layanan;
import com.danang.paten.repository.ArtikelRepository;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author danang
 */
@Service
public class ArtikelService {

    private @Autowired
    ArtikelRepository artikelRepository;
    private @Autowired MongoTemplate template;

    public Artikel simpan(Artikel artikel) {
        artikel.setId(artikel.getTitle() + new SimpleDateFormat("ddMMyyyhhmmss").format(new Date()));
        return artikelRepository.save(artikel);
    }

    public void update(Artikel artikel) {
        String kode = artikel.getId();
        Artikel x = artikelRepository.findOne(kode);
        if (x == null) {
            throw new IllegalStateException("Data update tidak ditemukan");
        }
        artikel.setId(x.getId());
        artikelRepository.save(artikel);
    }
    public Page<Artikel> findArtikel(Pageable page){
        return artikelRepository.findAll(page);
    }
    public Page<Artikel> findArtikelPublish(Pageable page){
        Query q = new Query(Criteria.where("publish").is(true));
        long total = template.count(q, Artikel.class);
        q.with(page);
        q.with(new Sort(Sort.Direction.DESC,"tanggal"));
        return new PageImpl(template.find(q, Layanan.class),page,total);
    }
}
