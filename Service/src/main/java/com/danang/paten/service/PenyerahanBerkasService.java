// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import org.joda.time.DateTime;
import com.danang.paten.domain.Counter;
import com.danang.paten.enumerasi.StatusPekerjaan;
import java.util.List;
import com.danang.paten.domain.Petugas;
import java.util.Date;
import com.danang.paten.domain.PenyerahanBerkas;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.repository.TransaksiLayananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.CounterRepository;
import org.springframework.stereotype.Service;

@Service
public class PenyerahanBerkasService
{
    @Autowired
    private CounterRepository cDao;
    @Autowired
    private TransaksiLayananRepository trxDao;
    
    public PenyerahanBerkas simpan(final TransaksiLayanan tl) {
        final PenyerahanBerkas pb = tl.getPenyerahanBerkas();
        pb.setNomor(this.getSeqNomor("PR" + tl.getLayanan().getKode()));
        return this.trxDao.save(tl).getPenyerahanBerkas();
    }
    
    public TransaksiLayanan simpanPenyerahan(final String penerima, final String hp, final String alamat, final Date tanggal, final Petugas p, final TransaksiLayanan tl, final List<String> items) {
        final PenyerahanBerkas pb = new PenyerahanBerkas();
        pb.setTanggal(tanggal);
        pb.setAlamat(alamat);
        pb.setHp(hp);
        pb.setItems(items);
        pb.setNomor("PR" + tl.getLayanan().getKode());
        pb.setPenerima(penerima);
        tl.setTanggalPenyerahan(tanggal);
        tl.setPenyerahanBerkas(pb);
        tl.setStatusPekerjaan(StatusPekerjaan.Selesai);
        return this.trxDao.save(tl);
    }
    
    private String getSeqNomor(final String kode) {
        final Counter c = this.cDao.findOne(kode);
        if (c == null) {
            c.setKode(kode);
            c.setSeri(0);
        }
        c.setSeri(c.getSeri() + 1);
        this.cDao.save(c);
        final StringBuilder sb = new StringBuilder();
        sb.append(kode).append("/");
        sb.append(c.getSeri()).append("/");
        sb.append(new DateTime().getYear());
        return sb.toString();
    }
}
