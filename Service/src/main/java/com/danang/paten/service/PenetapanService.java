// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import org.joda.time.DateTime;
import com.danang.paten.domain.Counter;
import com.danang.paten.domain.Layanan;
import com.danang.paten.repository.LayananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.danang.paten.repository.CounterRepository;
import org.springframework.stereotype.Service;

@Service
public class PenetapanService
{
    @Autowired
    private CounterRepository counterDao;
    @Autowired
    private LayananRepository layananDao;
    
    public String getNomorPenetapanLayanan(final String kode) {
        final Layanan l = this.layananDao.findOne(kode);
        DateTime dt = new DateTime();
        Counter c = this.counterDao.findByKodeAndYear(kode, dt.getYear());
        if (c == null) {
            c = new Counter();
            c.setKode(kode);
            c.setSeri(l.getMulaiSeri());
        }
        c.setSeri(c.getSeri() + 1);
        counterDao.save(c);
        StringBuilder builder = new StringBuilder();
        builder.append(kode).append("/").append(c.getSeri()).append("/").append(getRomawi(dt.getMonthOfYear())).append("/").append(dt.getYear());
        return builder.toString();
    }
    
    private String getRomawi(final int i) {
        switch (i) {
            case 1: {
                return "I";
            }
            case 2: {
                return "II";
            }
            case 3: {
                return "III";
            }
            case 4: {
                return "IV";
            }
            case 5: {
                return "V";
            }
            case 6: {
                return "VI";
            }
            case 7: {
                return "VII";
            }
            case 8: {
                return "VIII";
            }
            case 9: {
                return "IX";
            }
            case 10: {
                return "X";
            }
            case 11: {
                return "XI";
            }
            default: {
                return "XII";
            }
        }
    }
    
    public String getRegistrasiNomorLayanan(final String kode) {
        Counter counter = this.counterDao.findOne(kode);
        if (counter == null) {
            final Layanan l = this.layananDao.findOne(kode);
            counter = new Counter();
            counter.setKode(kode);
            if (l != null) {
                counter.setSeri(l.getMulaiSeri());
            }
            else {
                counter.setSeri(0);
            }
        }
        counter.setSeri(counter.getSeri() + 1);
        this.counterDao.save(counter);
        final StringBuilder sb = new StringBuilder();
        sb.append(kode).append("/");
        sb.append(counter.getSeri()).append("/");
        final DateTime dt = new DateTime();
        sb.append(dt.getYear());
        return sb.toString();
    }
}
