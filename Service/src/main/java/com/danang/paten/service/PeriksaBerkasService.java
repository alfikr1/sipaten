// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.service;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import java.util.HashMap;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import org.joda.time.DateTime;
import com.danang.paten.domain.Counter;
import com.google.gson.Gson;
import com.danang.paten.enumerasi.StatusPekerjaan;
import java.util.Date;
import com.danang.paten.domain.PeriksaBerkas;
import com.google.gson.GsonBuilder;
import com.danang.paten.form.HO;
import com.danang.paten.domain.Petugas;
import com.danang.paten.domain.TerimaBerkas;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.repository.LayananRepository;
import com.danang.paten.repository.CounterRepository;
import com.danang.paten.repository.TransaksiLayananRepository;
import com.danang.paten.repository.PeriksaBerkasRepository;
import com.danang.paten.repository.TerimaBerkasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class PeriksaBerkasService
{
    @Autowired
    private MongoTemplate template;
    @Autowired
    private TerimaBerkasRepository terimaBerkasDao;
    @Autowired
    private PeriksaBerkasRepository periksaDao;
    @Autowired
    private TransaksiLayananRepository trxDao;
    @Autowired
    private CounterRepository counterDao;
    @Autowired
    private LayananRepository layananDao;
    
    public void simpanPeriksa(final TransaksiLayanan trx) {
        final TerimaBerkas tb = trx.getTerimaBerkas();
        final TerimaBerkas x = this.terimaBerkasDao.findOne(tb.getNomor());
        if (x == null) {
            throw new IllegalStateException("berkas tidak ditemukan");
        }
        tb.setStatus("DIPERIKSA");
        tb.setNomor(x.getNomor());
        this.terimaBerkasDao.save(tb);
        this.trxDao.save(trx);
    }
    
    public TransaksiLayanan simpanPeriksaHO(final TransaksiLayanan tl, final boolean lanjut, final Petugas petugas, final String catatan, final HO ho) {
        final Gson gson = new GsonBuilder().setDateFormat("ddMMyyyy hh:mm:ss").create();
        final TerimaBerkas terimaBerkas = tl.getTerimaBerkas();
        final PeriksaBerkas periksa = new PeriksaBerkas();
        periksa.setNomor(this.getNomorPeriksaLayanan(terimaBerkas.getLayanan().getKode()));
        periksa.setTanggal(new Date());
        periksa.setLanjut(lanjut);
        periksa.setPetugas(petugas);
        periksa.setCatatan(catatan);
        final PeriksaBerkas x = this.periksaDao.save(periksa);
        tl.setPeriksaBerkas(x);
        if (lanjut) {
            tl.setStatusPekerjaan(StatusPekerjaan.Pemeriksaan);
        }
        else {
            tl.setStatusPekerjaan(StatusPekerjaan.Dikembalikan);
        }
        ho.setRegistrasi(terimaBerkas.getPetugas());
        ho.setIjin(terimaBerkas.getIjin());
        ho.setPemohon(terimaBerkas.getPemohon());
        ho.setPemeriksa(petugas);
        ho.setTanggalPemeriksaan(new Date());
        ho.setTanggalPermohonan(terimaBerkas.getTanggalTerima());
        tl.setKonten(gson.toJson(ho));
        return this.trxDao.save(tl);
    }
    
    public String getNomorPeriksaLayanan(final String kode) {
        Counter c = this.counterDao.findOne(kode);
        if (c == null) {
            c = new Counter();
            c.setKode(kode);
            c.setSeri(0);
        }
        c.setSeri(c.getSeri() + 1);
        final StringBuilder builder = new StringBuilder();
        final DateTime dt = new DateTime();
        builder.append(kode).append("/").append(c.getSeri()).append("/").append(dt.getYear());
        return builder.toString();
    }
    
    public Map<String, Object> getAllPeriksa(final Pageable page) {
        final Map<String, Object> map = new HashMap<String, Object>();
        final Query q = new Query();
        q.addCriteria(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Pemberkasan));
        q.with(page);
        final Sort sort = new Sort(Sort.Direction.ASC, new String[0]);
        q.with(sort);
        final List<TransaksiLayanan> items = this.template.find(q, TransaksiLayanan.class);
        map.put("berkas", items);
        map.put("status", 1);
        map.put("date", new Date());
        return map;
    }
}
