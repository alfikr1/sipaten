/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.TransaksiLayanan;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

/**
 *
 * @author danang
 */
@Service
public class CariService {

    private @Autowired
    MongoTemplate template;

    public Page<TransaksiLayanan> cariDataLayanan(Map<String, Object> param) {
        Criteria c = new Criteria();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        if (param.get("awal") != null) {
            try {
                Date awal = sdf.parse(param.get("awal").toString());
                c.andOperator(Criteria.where("tanggal").gte(awal));
            } catch (Exception e) {
            }
        }
        if (param.get("akhir") != null) {
            try {
                Date akhir = sdf.parse(param.get("akhir").toString());
                c.andOperator(Criteria.where("tanggal").lte(akhir));
            } catch (Exception e) {
            }

        }
        if (param.get("keyword") != null) {
            Criteria paramkey = new Criteria();
            paramkey.orOperator(Criteria.where("").regex(Pattern.compile("", Pattern.CASE_INSENSITIVE)),
                    Criteria.where("").regex(Pattern.compile("", Pattern.CASE_INSENSITIVE)),
                    Criteria.where("").regex(Pattern.compile("", Pattern.CASE_INSENSITIVE)),
                    Criteria.where("").regex(Pattern.compile("", Pattern.CASE_INSENSITIVE)));
        }
        if (param.get("desa") != null) {
            String desa = (String) param.get("desa");
            if (desa.equalsIgnoreCase("ALL")) {
                
            } else {
                c.andOperator(Criteria.where("").is(""));
            }
        }
        if (param.get("usia") != null) {
            c.andOperator(Criteria.where("").lte(""));
        }
        return null;
    }
}
