// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.util;

import org.springframework.data.annotation.Id;
import org.springframework.data.mapping.model.MappingException;
import org.springframework.data.mongodb.core.mapping.DBRef;
import java.lang.reflect.Field;
import org.springframework.util.ReflectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;

public class CascadingMongoEventListener extends AbstractMongoEventListener
{
    @Autowired
    private MongoOperations mongoOperation;
    
    public void onBeforeConver(final Object source) {
        ReflectionUtils.doWithFields(source.getClass(), (final Field field) -> {
            ReflectionUtils.makeAccessible(field);
            if (field.isAnnotationPresent(DBRef.class) && field.isAnnotationPresent(CascadeSave.class)) {
                final Object fieldValue = field.get(source);
                final DbRefFieldCallback callback = new DbRefFieldCallback();
                ReflectionUtils.doWithFields(fieldValue.getClass(), callback);
                if (!callback.isIdFound()) {
                    throw new MappingException("Cannot perform cascade save on child object whithoud id set");
                }
                CascadingMongoEventListener.this.mongoOperation.save(fieldValue);
            }
        });
    }
    
    private static class DbRefFieldCallback implements ReflectionUtils.FieldCallback
    {
        private boolean idFound;
        
        @Override
        public void doWith(final Field field) throws IllegalArgumentException, IllegalAccessException {
            ReflectionUtils.makeAccessible(field);
            if (field.isAnnotationPresent(Id.class)) {
                this.idFound = true;
            }
        }
        
        public boolean isIdFound() {
            return this.idFound;
        }
    }
}