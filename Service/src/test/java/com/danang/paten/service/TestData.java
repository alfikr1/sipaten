/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.Counter;
import com.danang.paten.konfigurasi.SpringConfig;
import com.danang.paten.repository.CounterRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Danang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class TestData {
    private @Autowired TransaksiService service;
    private @Autowired CounterRepository cos;
    private @Autowired UpdateService us;
    
    public void buatTransaksi(){
        Counter c = cos.findByKodeAndYear("KKM", 2017);
        if(c==null){
            c=new Counter();
            c.setKode("KKM");
            c.setYear(2017);
            c.setSeri(0);
        }
        c.setSeri(c.getSeri()+1);
        cos.save(c);
        System.out.println(c.getSeri()+1);
    }
    @Test
    public void testUpdate(){
        us.jalankanCounterChecker();
    }
}
