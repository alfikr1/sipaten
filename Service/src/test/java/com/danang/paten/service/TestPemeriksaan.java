/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.konfigurasi.SpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author danang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class TestPemeriksaan {

    private @Autowired
    TransaksiService service;

    @Test
    public void cariData() throws InterruptedException {
        Page<TransaksiLayanan> page = service.findallPeriksa(new PageRequest(0,25));
        for(int i=0;i<page.getTotalPages();i++){
            Page<TransaksiLayanan> page1=service.findallPeriksa(new PageRequest(i,25));
            page1.getContent().stream().forEach((x)->{
                if(x.getTerimaBerkas().getNomor().equals("TB474.2.2/9/2016")){
                    System.out.println("ketemu "+x.getTerimaBerkas().getNomor());
                }
            });
        }
    }
}
