/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.service;

import com.danang.paten.domain.KartuKendaliSuratMasuk;
import com.danang.paten.domain.TransaksiLayanan;
import com.danang.paten.enumerasi.StatusPekerjaan;
import com.danang.paten.konfigurasi.SpringConfig;
import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.bson.Document;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author danang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class CrudTest {

    private @Autowired
    MongoTemplate template;
    private @Autowired
    MongoClient client;
    private @Autowired LayananService service;

    //@Test
    public void testSuratMasuk() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        Date awal = sdf.parse("01042016");
        Date akhir = sdf.parse("30072016");
        Criteria c = new Criteria();
        c.orOperator(Criteria.where("tanggalSurat").gte(awal), Criteria.where("tanggalSurat").lte(akhir));
        final Query q = new Query();
        q.addCriteria(c);
        long total = template.count(q, KartuKendaliSuratMasuk.class);
        System.out.println(total);
    }
    @Test
    public void testDataMap(){
        Gson gson = new Gson();
        String stt=gson.toJson(service.getAntrianLayanan());
        System.out.println(stt);
    }

    //@Test
    public void testCounter() {
        Aggregation agg = newAggregation(match(Criteria.where("penetapanBerkas").ne(null)), group("layanan.kode", "layanan.namaLayanan").count().as("total"));
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        groupResult.forEach((x) -> {
            System.out.println(x.toString());
        });
    }

    //@Test
    public void testCriteria() {
        Criteria c = new Criteria();
        Map<String, Object> param = getParam();
        MongoDatabase db = client.getDatabase("bergaskec");
        MongoCollection<Document> trx = db.getCollection("transaksiLayanan");
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        if (param.get("awal") != null) {
            try {
                Date awal = sdf.parse(param.get("awal").toString());
                
            } catch (Exception e) {
            }
        }
        if (param.get("akhir") != null) {
            try {
                Date akhir = sdf.parse(param.get("akhir").toString());
                
            } catch (Exception e) {
            }

        }
        if (param.get("keyword") != null) {
            String keyword = (String) param.get("keyword");
            Criteria kkey = new Criteria();
            kkey.orOperator(Criteria.where("nomor").regex(Pattern.compile(keyword, Pattern.CASE_INSENSITIVE)),
                    Criteria.where("terimaBerkas.nomor").regex(Pattern.compile(keyword, Pattern.CASE_INSENSITIVE)),
                    Criteria.where("terimaBerkas.ijin.nama").regex(Pattern.compile(keyword, Pattern.CASE_INSENSITIVE)),
                    Criteria.where("terimaBerkas.ijin.nik").regex(Pattern.compile(keyword, Pattern.CASE_INSENSITIVE)),
                    Criteria.where("terimaBerkas.ijin.lingk").regex(Pattern.compile(keyword, Pattern.CASE_INSENSITIVE)));
        }
        if (param.get("desa") != null) {
            Criteria cdesa = Criteria.where("terimaBerkas.ijin.desa").is(param.get("desa"));
        }
        if (param.get("usia") != null) {
            String kodeUsia = (String) param.get("usia");
            if (param.get("usian") != null) {
                int usia = (int) param.get("usian");
                DateTime dateTime = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth(), 0, 0);
                DateTime dt;
                if (kodeUsia.equalsIgnoreCase(">")) {
                    dt = dateTime.minusYears(usia);
                } else {
                    dt = dateTime.plusYears(usia);
                }
                Criteria cusia = Criteria.where("terimaBerkas.ijin.tanggalLahir").lte(dt.toDate());
            }

        }
        if (param.get("layanan") != null) {
            Criteria.where("layanan.kode").is(param.get("layanan"));
        }
        if (param.get("statusPekerjaan") != null) {
            Criteria.where("statusPekerjaan").is(param.get("statusPekerjaan"));
        }
        if (param.get("jenisKelamin") != null) {
            Criteria.where("terimaBerkas.ijin.jenisKelamin").is(param.get("jenisKelamin"));
        }
        Document document=new Document();
        document.put("", "");
        trx.find(document);
    }

    private Map<String, Object> getParam() {
        Map<String, Object> map = new HashMap<>();
        map.put("desa", "ALL");
        map.put("awal", "01012016");
        map.put("akhir", "31122016");
        return map;
    }

    public void timeseries() {
        
        MongoDatabase db = client.getDatabase("bergaskec");
        MongoCollection<Document> trx = db.getCollection("transaksiLayanan");
        Date awal = new DateTime(DateTime.now().getYear(), 8, 8, 1, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), 8, 8, 23, 59, 0).toDate();
        ProjectionOperation po = project("$layanan.kode", "$layanan.namaLayanan")
                .andExpression("$terimaBerkas.tanggalTerima").extractHour().as("jam");
        Criteria c = new Criteria();
        c.andOperator(Criteria.where("terimaBerkas.tanggalTerima").gte(awal), Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation ma = match(c);
        GroupOperation go = group("$layanan.kode", "$layanan.namaLayanan", "$terimaBerkas.tanggalTerima")
                .count().as("jumlah");
        ProjectionOperation po2 = project("layanan.kode")
                .andInclude("layanan.namaLayanan", "jumlah");
        AggregationResults<Document> da = template.aggregate(newAggregation(ma, go), TransaksiLayanan.class, Document.class);
        System.out.println("jumlah item " + da.getMappedResults().size());
        Gson gson = new Gson();
        da.iterator().forEachRemaining((x) -> {
            System.out.println(gson.toJson(x));
        });
    }
    //@Test
    public void testData(){
        System.out.println("test data date start");
        Date awal=new DateTime(2016,5,1,0,0,0).toDate();
        Date akhir=new DateTime(2016,5,30,0,0,0).toDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS\'Z\'");
        Criteria cand = new Criteria();
        cand.andOperator(
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        Query q = new Query(cand);
        q.limit(5);
        List<TransaksiLayanan> tls=template.find(q, TransaksiLayanan.class);
        tls.stream().forEach((x)->{System.out.println(x.getNomor());});
    }
    //@Test
    public void testSeries(){
        System.out.println("time series test start");
        
        //Date awal = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth(), 0, 0, 0).toDate();
        //Date akhir = new DateTime(DateTime.now().getYear(), DateTime.now().getMonthOfYear(), DateTime.now().getDayOfMonth(), 23, 59, 59).toDate();
        
        Date awal=new DateTime(2016,05,1,0,0,0).toDate();
        Date akhir=new DateTime(2016,05,30,0,0,0).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        GroupOperation grup = group("layanan.kode").addToSet("layanan.namaLayanan").as("namaLayanan").count().as("total");

        Aggregation agg = newAggregation(mat,grup);
        System.out.println("agg : "+agg.toString());
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        System.out.println("ukuran result "+groupResult.getMappedResults().size());
        Gson gson = new Gson();
        groupResult.getMappedResults().stream().forEach((x)->{
            System.out.println(gson.toJson(x));
        });
    }
    public void testNative() {
        MongoDatabase db = client.getDatabase("bergaskec");
        MongoCollection<Document> trx = db.getCollection("transaksiLayanan");
        Date awal = new DateTime(DateTime.now().getYear(), 8, 8, 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), 8, 8, 23, 59, 0).toDate();
        List<Document> kl = new ArrayList<>();

        Map<String, Object> kriteria = new HashMap<>();
        kriteria.put("$ne", null);
        Map<String, Object> pb = new HashMap<>();
        pb.put("penetapanBerkas", kriteria);
        Map<String, Object> mat = new HashMap<>();
        mat.put("$match", pb);
        Map<String, Object> gj = new HashMap<>();
        gj.put("kode", "$layanan.kode");
        Map<String, Object> gaj = new HashMap<>();
        gaj.put("$hour", "terimaBerkas.tanggalTerima");
        gj.put("jam", gaj);
        gj.put("nama", "$layanan.namaLayanan");
        Document po = new Document("$project",
                new Document());
        Map<String, Object> id = new HashMap<>();
        id.put("kod", "$layanan.kode");
        id.put("jam", "");
        Map mp = new HashMap<>();
        mp.put("$hour", "$terimaBerkas.tanggalTerima");
        Document dc = new Document("$group",
                new Document("_id",
                        new Document("id", "$_id")
                        .append("kode", "$layanan._id")
                        .append("jam", new Document(mp))
                        .append("nl", "$layanan.namaLayanan"))
                .append("jumlah", new Document("$sum", 1)));
        kl.add(dc);
        Iterator itr = trx.aggregate(kl).iterator();
        while (itr.hasNext()) {
            System.out.println((Document) itr.next());
        }
    }

    //@Test
    public void testByDate() {
        System.out.println("test date");
        Date awal = new DateTime(DateTime.now().getYear(), 8, 8, 0, 0, 0).toDate();
        Date akhir = new DateTime(DateTime.now().getYear(), 8, 8, 23, 59, 0).toDate();
        Criteria cand = new Criteria();
        cand.andOperator(Criteria.where("penetapanBerkas").ne(null),
                Criteria.where("terimaBerkas.tanggalTerima").gte(awal),
                Criteria.where("terimaBerkas.tanggalTerima").lte(akhir));
        MatchOperation mat = match(cand);
        UnwindOperation uw = unwind("terimaBerkas");
        ProjectionOperation pj = project("layanan.kode")
                .and("terimaBerkas.tanggalTerima").extractHour().as("jam")
                .and("layanan.namaLayanan").as("nl");
        GroupOperation grup = group("layanan.kode", "$hour:'$terimaBerkas.tanggalTerima'", "layanan.namaLayanan").count().as("total");
        Aggregation agg = newAggregation(mat, grup);
        System.out.println(agg.toString());
        AggregationResults<Map> groupResult = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        System.out.println("size : " + groupResult.getMappedResults().size());
    }

    //@Test
    public void testExpired() {
        Criteria c1 = new Criteria();
        c1.orOperator(Criteria.where("statusPekerjaan").is(StatusPekerjaan.Diserahkan),
                Criteria.where("statusPekerjaan").is(StatusPekerjaan.Selesai));
        Criteria c = new Criteria();
        c1.andOperator(c1, Criteria.where("layanan.masaBerlaku").is(true),
                Criteria.where("penetapanBerkas.mulaiBerlaku").ne(null),
                Criteria.where("penetapanBerkas.akhirBerlaku").ne(null));
        MatchOperation mt = match(c);
        GroupOperation go = group("nomor", "layanan.namaLayanan", "terimaBerkas.tanggalTerima", "penetapanBerkas.mulaiBerlaku", "penetapanBerkas.akhirBerlaku");
        Aggregation agg = newAggregation(mt, go);
        AggregationResults<Map> gs = template.aggregate(agg, TransaksiLayanan.class, Map.class);
        gs.forEach((x) -> {
            System.out.println(x.toString());
        });
    }

    class Hasil {

        private String kode;
        private long total;

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

    }

}
