// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

public class Transaksi
{
    private String kode;
    private String token;
    private String konten;
    private String method;
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public String getToken() {
        return this.token;
    }
    
    public void setToken(final String token) {
        this.token = token;
    }
    
    public String getKonten() {
        return this.konten;
    }
    
    public void setKonten(final String konten) {
        this.konten = konten;
    }
    
    public String getMethod() {
        return this.method;
    }
    
    public void setMethod(final String method) {
        this.method = method;
    }
}
