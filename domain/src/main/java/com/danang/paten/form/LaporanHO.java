// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;

public class LaporanHO
{
    private String pemohon;
    private String alamatPemohon;
    private String namaUsaha;
    private String alamatUsaha;
    private String jenisUsaha;
    private double jumlahInvestasi;
    private Date pengajuan;
    private Date periksa;
    private double luasBangunan;
    private double luasIjinGangguan;
    private String indexGangguan;
    private double retribusi;
    private String ketetapan;
    private Date tglKetatapan;
    private Date mulaiBerlaku;
    private Date masaBerlaku;
    private String keterangan;
    private String nomorPeriksa;
    private String nomorKetetapan;

    public String getNomorPeriksa() {
        return nomorPeriksa;
    }

    public void setNomorPeriksa(String nomorPeriksa) {
        this.nomorPeriksa = nomorPeriksa;
    }

    public String getNomorKetetapan() {
        return nomorKetetapan;
    }

    public void setNomorKetetapan(String nomorKetetapan) {
        this.nomorKetetapan = nomorKetetapan;
    }
    
    
    public Date getMulaiBerlaku() {
        return this.mulaiBerlaku;
    }
    
    public void setMulaiBerlaku(final Date mulaiBerlaku) {
        this.mulaiBerlaku = mulaiBerlaku;
    }
    
    public Date getTglKetatapan() {
        return this.tglKetatapan;
    }
    
    public void setTglKetatapan(final Date tglKetatapan) {
        this.tglKetatapan = tglKetatapan;
    }
    
    public String getPemohon() {
        return this.pemohon;
    }
    
    public void setPemohon(final String pemohon) {
        this.pemohon = pemohon;
    }
    
    public String getAlamatPemohon() {
        return this.alamatPemohon;
    }
    
    public void setAlamatPemohon(final String alamatPemohon) {
        this.alamatPemohon = alamatPemohon;
    }
    
    public String getNamaUsaha() {
        return this.namaUsaha;
    }
    
    public void setNamaUsaha(final String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }
    
    public String getAlamatUsaha() {
        return this.alamatUsaha;
    }
    
    public void setAlamatUsaha(final String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }
    
    public String getJenisUsaha() {
        return this.jenisUsaha;
    }
    
    public void setJenisUsaha(final String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }
    
    public double getJumlahInvestasi() {
        return this.jumlahInvestasi;
    }
    
    public void setJumlahInvestasi(final double jumlahInvestasi) {
        this.jumlahInvestasi = jumlahInvestasi;
    }
    
    public Date getPengajuan() {
        return this.pengajuan;
    }
    
    public void setPengajuan(final Date pengajuan) {
        this.pengajuan = pengajuan;
    }
    
    public Date getPeriksa() {
        return this.periksa;
    }
    
    public void setPeriksa(final Date periksa) {
        this.periksa = periksa;
    }
    
    public double getLuasBangunan() {
        return this.luasBangunan;
    }
    
    public void setLuasBangunan(final double luasBangunan) {
        this.luasBangunan = luasBangunan;
    }
    
    public double getLuasIjinGangguan() {
        return this.luasIjinGangguan;
    }
    
    public void setLuasIjinGangguan(final double luasIjinGangguan) {
        this.luasIjinGangguan = luasIjinGangguan;
    }
    
    public String getIndexGangguan() {
        return this.indexGangguan;
    }
    
    public void setIndexGangguan(final String indexGangguan) {
        this.indexGangguan = indexGangguan;
    }
    
    public double getRetribusi() {
        return this.retribusi;
    }
    
    public void setRetribusi(final double retribusi) {
        this.retribusi = retribusi;
    }
    
    public String getKetetapan() {
        return this.ketetapan;
    }
    
    public void setKetetapan(final String ketetapan) {
        this.ketetapan = ketetapan;
    }
    
    public Date getMasaBerlaku() {
        return this.masaBerlaku;
    }
    
    public void setMasaBerlaku(final Date masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
}
