// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;
import com.danang.paten.domain.DataPenduduk;

public class RekomendasiPenelitian
{
    private DataPenduduk dp;
    private String judulPenelitian;
    private String lokasiPenelitian;
    private String bidangPenelitian;
    private String penanggungJawab;
    private Date mulai;
    private Date selesai;
    private String camat;
    private String nipCamat;
    private String jabatanCamat;
    private Date ditetapkan;
    private Date diajukan;
    private String nomor;
    private String rekomendasi;
    private Date suratRekomendasi;
    private String nomorSurat;
    
    public String getNipCamat() {
        return this.nipCamat;
    }
    
    public void setNipCamat(final String nipCamat) {
        this.nipCamat = nipCamat;
    }
    
    public String getJabatanCamat() {
        return this.jabatanCamat;
    }
    
    public void setJabatanCamat(final String jabatanCamat) {
        this.jabatanCamat = jabatanCamat;
    }
    
    public String getPenanggungJawab() {
        return this.penanggungJawab;
    }
    
    public void setPenanggungJawab(final String penanggungJawab) {
        this.penanggungJawab = penanggungJawab;
    }
    
    public Date getSuratRekomendasi() {
        return this.suratRekomendasi;
    }
    
    public void setSuratRekomendasi(final Date suratRekomendasi) {
        this.suratRekomendasi = suratRekomendasi;
    }
    
    public String getNomorSurat() {
        return this.nomorSurat;
    }
    
    public void setNomorSurat(final String nomorSurat) {
        this.nomorSurat = nomorSurat;
    }
    
    public DataPenduduk getDp() {
        return this.dp;
    }
    
    public void setDp(final DataPenduduk dp) {
        this.dp = dp;
    }
    
    public String getJudulPenelitian() {
        return this.judulPenelitian;
    }
    
    public void setJudulPenelitian(final String judulPenelitian) {
        this.judulPenelitian = judulPenelitian;
    }
    
    public String getLokasiPenelitian() {
        return this.lokasiPenelitian;
    }
    
    public void setLokasiPenelitian(final String lokasiPenelitian) {
        this.lokasiPenelitian = lokasiPenelitian;
    }
    
    public String getBidangPenelitian() {
        return this.bidangPenelitian;
    }
    
    public void setBidangPenelitian(final String bidangPenelitian) {
        this.bidangPenelitian = bidangPenelitian;
    }
    
    public Date getMulai() {
        return this.mulai;
    }
    
    public void setMulai(final Date mulai) {
        this.mulai = mulai;
    }
    
    public Date getSelesai() {
        return this.selesai;
    }
    
    public void setSelesai(final Date selesai) {
        this.selesai = selesai;
    }
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public Date getDitetapkan() {
        return this.ditetapkan;
    }
    
    public void setDitetapkan(final Date ditetapkan) {
        this.ditetapkan = ditetapkan;
    }
    
    public Date getDiajukan() {
        return this.diajukan;
    }
    
    public void setDiajukan(final Date diajukan) {
        this.diajukan = diajukan;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public String getRekomendasi() {
        return this.rekomendasi;
    }
    
    public void setRekomendasi(final String rekomendasi) {
        this.rekomendasi = rekomendasi;
    }
}
