// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.ArrayList;
import com.danang.paten.domain.Berkas;
import java.util.List;
import com.danang.paten.domain.Petugas;
import java.util.Date;
import com.danang.paten.domain.DataPenduduk;

public class HO
{
    private String nomor;
    private DataPenduduk pemohon;
    private DataPenduduk ijin;
    private String namaPerusahaan;
    private String jenisUsaha;
    private double luas;
    private String lokasi;
    private Date mulaiBerlaku;
    private Date akhirBerlaku;
    private double retribusi;
    private String indexGangguan;
    private Date tanggalPemeriksaan;
    private Date tanggalPermohonan;
    private Date tanggalAmbil;
    private double investasi;
    private double luasBangunan;
    private Petugas pemeriksa;
    private Petugas registrasi;
    private Date ketetapan;
    private String catatan;
    private int statusProgres;
    private String camat;
    private String jabatanCamat;
    private String nip;
    private List<Berkas> items;
    
    public HO() {
        this.items = new ArrayList<Berkas>();
    }
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public String getJabatanCamat() {
        return this.jabatanCamat;
    }
    
    public void setJabatanCamat(final String jabatanCamat) {
        this.jabatanCamat = jabatanCamat;
    }
    
    public String getNip() {
        return this.nip;
    }
    
    public void setNip(final String nip) {
        this.nip = nip;
    }
    
    public double getLuasBangunan() {
        return this.luasBangunan;
    }
    
    public void setLuasBangunan(final double luasBangunan) {
        this.luasBangunan = luasBangunan;
    }
    
    public Date getTanggalAmbil() {
        return this.tanggalAmbil;
    }
    
    public void setTanggalAmbil(final Date tanggalAmbil) {
        this.tanggalAmbil = tanggalAmbil;
    }
    
    public List<Berkas> getItems() {
        return this.items;
    }
    
    public void setItems(final List<Berkas> items) {
        this.items = items;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
    
    public int getStatusProgres() {
        return this.statusProgres;
    }
    
    public void setStatusProgres(final int statusProgres) {
        this.statusProgres = statusProgres;
    }
    
    public Petugas getPemeriksa() {
        return this.pemeriksa;
    }
    
    public void setPemeriksa(final Petugas pemeriksa) {
        this.pemeriksa = pemeriksa;
    }
    
    public Petugas getRegistrasi() {
        return this.registrasi;
    }
    
    public void setRegistrasi(final Petugas registrasi) {
        this.registrasi = registrasi;
    }
    
    public Date getKetetapan() {
        return this.ketetapan;
    }
    
    public void setKetetapan(final Date ketetapan) {
        this.ketetapan = ketetapan;
    }
    
    public String getIndexGangguan() {
        return this.indexGangguan;
    }
    
    public void setIndexGangguan(final String indexGangguan) {
        this.indexGangguan = indexGangguan;
    }
    
    public Date getTanggalPemeriksaan() {
        return this.tanggalPemeriksaan;
    }
    
    public void setTanggalPemeriksaan(final Date tanggalPemeriksaan) {
        this.tanggalPemeriksaan = tanggalPemeriksaan;
    }
    
    public Date getTanggalPermohonan() {
        return this.tanggalPermohonan;
    }
    
    public void setTanggalPermohonan(final Date tanggalPermohonan) {
        this.tanggalPermohonan = tanggalPermohonan;
    }
    
    public double getInvestasi() {
        return this.investasi;
    }
    
    public void setInvestasi(final double investasi) {
        this.investasi = investasi;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public DataPenduduk getPemohon() {
        return this.pemohon;
    }
    
    public void setPemohon(final DataPenduduk pemohon) {
        this.pemohon = pemohon;
    }
    
    public DataPenduduk getIjin() {
        return this.ijin;
    }
    
    public void setIjin(final DataPenduduk ijin) {
        this.ijin = ijin;
    }
    
    public String getNamaPerusahaan() {
        return this.namaPerusahaan;
    }
    
    public void setNamaPerusahaan(final String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }
    
    public String getJenisUsaha() {
        return this.jenisUsaha;
    }
    
    public void setJenisUsaha(final String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }
    
    public double getLuas() {
        return this.luas;
    }
    
    public void setLuas(final double luas) {
        this.luas = luas;
    }
    
    public String getLokasi() {
        return this.lokasi;
    }
    
    public void setLokasi(final String lokasi) {
        this.lokasi = lokasi;
    }
    
    public Date getMulaiBerlaku() {
        return this.mulaiBerlaku;
    }
    
    public void setMulaiBerlaku(final Date mulaiBerlaku) {
        this.mulaiBerlaku = mulaiBerlaku;
    }
    
    public Date getAkhirBerlaku() {
        return this.akhirBerlaku;
    }
    
    public void setAkhirBerlaku(final Date akhirBerlaku) {
        this.akhirBerlaku = akhirBerlaku;
    }
    
    public double getRetribusi() {
        return this.retribusi;
    }
    
    public void setRetribusi(final double retribusi) {
        this.retribusi = retribusi;
    }
    
    @Override
    public String toString() {
        return "HO{nomor=" + this.nomor + ", pemohon=" + this.pemohon + ", ijin=" + this.ijin + ", namaPerusahaan=" + this.namaPerusahaan + ", jenisUsaha=" + this.jenisUsaha + ", luas=" + this.luas + ", lokasi=" + this.lokasi + ", mulaiBerlaku=" + this.mulaiBerlaku + ", akhirBerlaku=" + this.akhirBerlaku + ", retribusi=" + this.retribusi + ", indexGangguan=" + this.indexGangguan + ", tanggalPemeriksaan=" + this.tanggalPemeriksaan + ", tanggalPermohonan=" + this.tanggalPermohonan + ", tanggalAmbil=" + this.tanggalAmbil + ", investasi=" + this.investasi + ", luasBangunan=" + this.luasBangunan + ", pemeriksa=" + this.pemeriksa + ", registrasi=" + this.registrasi + ", ketetapan=" + this.ketetapan + ", catatan=" + this.catatan + ", statusProgres=" + this.statusProgres + ", camat=" + this.camat + ", jabatanCamat=" + this.jabatanCamat + ", nip=" + this.nip + ", items=" + this.items + '}';
    }
}
