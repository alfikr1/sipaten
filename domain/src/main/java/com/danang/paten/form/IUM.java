// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.domain.Petugas;
import java.util.Date;

public class IUM
{
    private String nomor;
    private Date registerDate;
    private Petugas petugas;
    private DataPenduduk dp;
    private String namaUsaha;
    private String bentukUsaha;
    private String npwp;
    private String kegiatanUsaha;
    private String saranaUsaha;
    private String alamatUsaha;
    private double jumlahModal;
    private String nomorDaftar;
    private String nomorPeriksa;
    private String nomorKetetapan;
    private Date periksa;
    private Date ditetapkan;
    private Date mulaiBerlaku;
    private Date akhirBerlaku;
    private String foto;
    private String camat;
    private String nipCamat;
    private String jabatanCamat;
    
    public Date getMulaiBerlaku() {
        return this.mulaiBerlaku;
    }
    
    public void setMulaiBerlaku(final Date mulaiBerlaku) {
        this.mulaiBerlaku = mulaiBerlaku;
    }
    
    public Date getAkhirBerlaku() {
        return this.akhirBerlaku;
    }
    
    public void setAkhirBerlaku(final Date akhirBerlaku) {
        this.akhirBerlaku = akhirBerlaku;
    }
    
    public String getJabatanCamat() {
        return this.jabatanCamat;
    }
    
    public void setJabatanCamat(final String jabatanCamat) {
        this.jabatanCamat = jabatanCamat;
    }
    
    public String getNipCamat() {
        return this.nipCamat;
    }
    
    public void setNipCamat(final String nipCamat) {
        this.nipCamat = nipCamat;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public String getFoto() {
        return this.foto;
    }
    
    public void setFoto(final String foto) {
        this.foto = foto;
    }
    
    public Date getRegisterDate() {
        return this.registerDate;
    }
    
    public void setRegisterDate(final Date registerDate) {
        this.registerDate = registerDate;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public DataPenduduk getDp() {
        return this.dp;
    }
    
    public void setDp(final DataPenduduk dp) {
        this.dp = dp;
    }
    
    public String getNamaUsaha() {
        return this.namaUsaha;
    }
    
    public void setNamaUsaha(final String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }
    
    public String getKegiatanUsaha() {
        return this.kegiatanUsaha;
    }
    
    public void setKegiatanUsaha(final String kegiatanUsaha) {
        this.kegiatanUsaha = kegiatanUsaha;
    }
    
    public String getBentukUsaha() {
        return this.bentukUsaha;
    }
    
    public void setBentukUsaha(final String bentukUsaha) {
        this.bentukUsaha = bentukUsaha;
    }
    
    public String getNpwp() {
        return this.npwp;
    }
    
    public void setNpwp(final String npwp) {
        this.npwp = npwp;
    }
    
    public String getSaranaUsaha() {
        return this.saranaUsaha;
    }
    
    public void setSaranaUsaha(final String saranaUsaha) {
        this.saranaUsaha = saranaUsaha;
    }
    
    public String getAlamatUsaha() {
        return this.alamatUsaha;
    }
    
    public void setAlamatUsaha(final String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }
    
    public double getJumlahModal() {
        return this.jumlahModal;
    }
    
    public void setJumlahModal(final double jumlahModal) {
        this.jumlahModal = jumlahModal;
    }
    
    public String getNomorDaftar() {
        return this.nomorDaftar;
    }
    
    public void setNomorDaftar(final String nomorDaftar) {
        this.nomorDaftar = nomorDaftar;
    }
    
    public String getNomorPeriksa() {
        return this.nomorPeriksa;
    }
    
    public void setNomorPeriksa(final String nomorPeriksa) {
        this.nomorPeriksa = nomorPeriksa;
    }
    
    public String getNomorKetetapan() {
        return this.nomorKetetapan;
    }
    
    public void setNomorKetetapan(final String nomorKetetapan) {
        this.nomorKetetapan = nomorKetetapan;
    }
    
    public Date getPeriksa() {
        return this.periksa;
    }
    
    public void setPeriksa(final Date periksa) {
        this.periksa = periksa;
    }
    
    public Date getDitetapkan() {
        return this.ditetapkan;
    }
    
    public void setDitetapkan(final Date ditetapkan) {
        this.ditetapkan = ditetapkan;
    }
}
