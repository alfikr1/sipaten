// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;
import com.danang.paten.domain.DataPenduduk;

public class DomisiliUsaha
{
    private DataPenduduk penduduk;
    private String namaPerusahaan;
    private String jenisUsaha;
    private double jumlahKaryawan;
    private String alamatUsaha;
    private String rekomendasi;
    private String nomorRekomendasi;
    private Date tanggalRekomendasi;
    private Date tanggalBerlaku;
    private Date tanggalHabisBerlaku;
    private Date tanggal;
    private String camat;
    private String jabatanCamat;
    private String nipCamat;
    private String nomorKetetapan;
    
    public String getNomorKetetapan() {
        return this.nomorKetetapan;
    }
    
    public void setNomorKetetapan(final String nomorKetetapan) {
        this.nomorKetetapan = nomorKetetapan;
    }
    
    public String getJabatanCamat() {
        return this.jabatanCamat;
    }
    
    public void setJabatanCamat(final String jabatanCamat) {
        this.jabatanCamat = jabatanCamat;
    }
    
    public String getNipCamat() {
        return this.nipCamat;
    }
    
    public void setNipCamat(final String nipCamat) {
        this.nipCamat = nipCamat;
    }
    
    public DataPenduduk getPenduduk() {
        return this.penduduk;
    }
    
    public void setPenduduk(final DataPenduduk penduduk) {
        this.penduduk = penduduk;
    }
    
    public String getNamaPerusahaan() {
        return this.namaPerusahaan;
    }
    
    public void setNamaPerusahaan(final String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }
    
    public String getJenisUsaha() {
        return this.jenisUsaha;
    }
    
    public void setJenisUsaha(final String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }
    
    public double getJumlahKaryawan() {
        return this.jumlahKaryawan;
    }
    
    public void setJumlahKaryawan(final double jumlahKaryawan) {
        this.jumlahKaryawan = jumlahKaryawan;
    }
    
    public String getAlamatUsaha() {
        return this.alamatUsaha;
    }
    
    public void setAlamatUsaha(final String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }
    
    public String getRekomendasi() {
        return this.rekomendasi;
    }
    
    public void setRekomendasi(final String rekomendasi) {
        this.rekomendasi = rekomendasi;
    }
    
    public String getNomorRekomendasi() {
        return this.nomorRekomendasi;
    }
    
    public void setNomorRekomendasi(final String nomorRekomendasi) {
        this.nomorRekomendasi = nomorRekomendasi;
    }
    
    public Date getTanggalRekomendasi() {
        return this.tanggalRekomendasi;
    }
    
    public void setTanggalRekomendasi(final Date tanggalRekomendasi) {
        this.tanggalRekomendasi = tanggalRekomendasi;
    }
    
    public Date getTanggalBerlaku() {
        return this.tanggalBerlaku;
    }
    
    public void setTanggalBerlaku(final Date tanggalBerlaku) {
        this.tanggalBerlaku = tanggalBerlaku;
    }
    
    public Date getTanggalHabisBerlaku() {
        return this.tanggalHabisBerlaku;
    }
    
    public void setTanggalHabisBerlaku(final Date tanggalHabisBerlaku) {
        this.tanggalHabisBerlaku = tanggalHabisBerlaku;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
}
