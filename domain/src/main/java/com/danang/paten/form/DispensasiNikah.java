// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;
import com.danang.paten.domain.DataPenduduk;

public class DispensasiNikah
{
    private String nomor;
    private DataPenduduk pria;
    private DataPenduduk wanita;
    private Date tanggalNikah;
    private String tempat;
    private String camat;
    private Date ketetapan;
    private String nipCamat;
    private String jabatanCamat;
    
    public String getNipCamat() {
        return this.nipCamat;
    }
    
    public void setNipCamat(final String nipCamat) {
        this.nipCamat = nipCamat;
    }
    
    public String getJabatanCamat() {
        return this.jabatanCamat;
    }
    
    public void setJabatanCamat(final String jabatanCamat) {
        this.jabatanCamat = jabatanCamat;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public DataPenduduk getPria() {
        return this.pria;
    }
    
    public void setPria(final DataPenduduk pria) {
        this.pria = pria;
    }
    
    public DataPenduduk getWanita() {
        return this.wanita;
    }
    
    public void setWanita(final DataPenduduk wanita) {
        this.wanita = wanita;
    }
    
    public Date getTanggalNikah() {
        return this.tanggalNikah;
    }
    
    public void setTanggalNikah(final Date tanggalNikah) {
        this.tanggalNikah = tanggalNikah;
    }
    
    public String getTempat() {
        return this.tempat;
    }
    
    public void setTempat(final String tempat) {
        this.tempat = tempat;
    }
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public Date getKetetapan() {
        return this.ketetapan;
    }
    
    public void setKetetapan(final Date ketetapan) {
        this.ketetapan = ketetapan;
    }
}
