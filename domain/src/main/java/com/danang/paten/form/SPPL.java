/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.form;

import com.danang.paten.domain.DataPenduduk;
import java.util.Date;

/**
 *
 * @author danang
 */
public class SPPL {

    private String nomorSuratPermohonan;
    private Date tanggalPermohonan;
    private Date suratPernyataan;
    private DataPenduduk pemohon;
    private String jabatan;
    private String telp;
    private String usaha;
    private String jenisUsaha;
    private String alamatUsaha;
    private String kapasitas;
    private String camat;
    private String nipCamat;
    private String pangkat;
    private double modal;
    private double luasUsaha;

    public String getCamat() {
        return camat;
    }

    public void setCamat(String camat) {
        this.camat = camat;
    }

    public String getNipCamat() {
        return nipCamat;
    }

    public void setNipCamat(String nipCamat) {
        this.nipCamat = nipCamat;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }
    
    public String getNomorSuratPermohonan() {
        return nomorSuratPermohonan;
    }

    public void setNomorSuratPermohonan(String nomorSuratPermohonan) {
        this.nomorSuratPermohonan = nomorSuratPermohonan;
    }

    public Date getTanggalPermohonan() {
        return tanggalPermohonan;
    }

    public void setTanggalPermohonan(Date tanggalPermohonan) {
        this.tanggalPermohonan = tanggalPermohonan;
    }

    public Date getSuratPernyataan() {
        return suratPernyataan;
    }

    public void setSuratPernyataan(Date suratPernyataan) {
        this.suratPernyataan = suratPernyataan;
    }

    public DataPenduduk getPemohon() {
        return pemohon;
    }

    public void setPemohon(DataPenduduk pemohon) {
        this.pemohon = pemohon;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getUsaha() {
        return usaha;
    }

    public void setUsaha(String usaha) {
        this.usaha = usaha;
    }

    public String getJenisUsaha() {
        return jenisUsaha;
    }

    public void setJenisUsaha(String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }

    public String getAlamatUsaha() {
        return alamatUsaha;
    }

    public void setAlamatUsaha(String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public double getModal() {
        return modal;
    }

    public void setModal(double modal) {
        this.modal = modal;
    }

    public double getLuasUsaha() {
        return luasUsaha;
    }

    public void setLuasUsaha(double luasUsaha) {
        this.luasUsaha = luasUsaha;
    }

    

}
