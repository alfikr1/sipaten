// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;

public class DokumenBerkas
{
    private String nomor;
    private Date tanggalRegistrasi;
    private String layanan;
    private String statusPekerjaan;
    private String pemohon;
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Date getTanggalRegistrasi() {
        return this.tanggalRegistrasi;
    }
    
    public void setTanggalRegistrasi(final Date tanggalRegistrasi) {
        this.tanggalRegistrasi = tanggalRegistrasi;
    }
    
    public String getLayanan() {
        return this.layanan;
    }
    
    public void setLayanan(final String layanan) {
        this.layanan = layanan;
    }
    
    public String getStatusPekerjaan() {
        return this.statusPekerjaan;
    }
    
    public void setStatusPekerjaan(final String statusPekerjaan) {
        this.statusPekerjaan = statusPekerjaan;
    }
    
    public String getPemohon() {
        return this.pemohon;
    }
    
    public void setPemohon(final String pemohon) {
        this.pemohon = pemohon;
    }
}
