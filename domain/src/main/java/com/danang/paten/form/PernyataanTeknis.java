/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.form;

/**
 *
 * @author danang
 */
public class PernyataanTeknis {
    private String nama;
    private String jabatan;
    private String alamat;
    private int jumlahLantai;
    private String pondasi;
    private String kerangkaUtama;
    private String dinding;
    private String lantai;
    private String kerangkatap;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getJumlahLantai() {
        return jumlahLantai;
    }

    public void setJumlahLantai(int jumlahLantai) {
        this.jumlahLantai = jumlahLantai;
    }

    public String getPondasi() {
        return pondasi;
    }

    public void setPondasi(String pondasi) {
        this.pondasi = pondasi;
    }

    public String getKerangkaUtama() {
        return kerangkaUtama;
    }

    public void setKerangkaUtama(String kerangkaUtama) {
        this.kerangkaUtama = kerangkaUtama;
    }

    public String getDinding() {
        return dinding;
    }

    public void setDinding(String dinding) {
        this.dinding = dinding;
    }

    public String getLantai() {
        return lantai;
    }

    public void setLantai(String lantai) {
        this.lantai = lantai;
    }

    public String getKerangkatap() {
        return kerangkatap;
    }

    public void setKerangkatap(String kerangkatap) {
        this.kerangkatap = kerangkatap;
    }
    
}
