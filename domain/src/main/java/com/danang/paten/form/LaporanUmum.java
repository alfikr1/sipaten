// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.form;

import com.danang.paten.domain.DataPenduduk;
import java.util.Comparator;
import java.util.Date;

public class LaporanUmum {
    private String nomor;
    private Date tanggal;
    private DataPenduduk pemohon;
    private String keterangan;

    public String getNomor() {
        return this.nomor;
    }

    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }

    public Date getTanggal() {
        return this.tanggal;
    }

    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }

    public DataPenduduk getPemohon() {
        return this.pemohon;
    }

    public void setPemohon(final DataPenduduk pemohon) {
        this.pemohon = pemohon;
    }

    public String getKeterangan() {
        return this.keterangan;
    }

    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
    public static Comparator<LaporanUmum> LaporanUmumComparator = new Comparator<LaporanUmum>() {
        @Override
        public int compare(LaporanUmum o1, LaporanUmum o2) {
            String d1 = o1.getPemohon() != null ? o1.getPemohon().getDesa() : "";
            String d2 = o2.getPemohon() != null ? o2.getPemohon().getDesa() : "";
            return d1.compareTo(d2);
        }
    };
}
