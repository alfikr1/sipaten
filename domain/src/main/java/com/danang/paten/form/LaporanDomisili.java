// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;

public class LaporanDomisili
{
    private String nomorIjin;
    private Date tanggal;
    private String namaPemohon;
    private String alamatPemohon;
    private String namaUsaha;
    private String alamatUsaha;
    private String jenisUsaha;
    private double jumlahKaryawan;
    private Date masaBerlaku;
    private String keterangan;
    
    public String getNomorIjin() {
        return this.nomorIjin;
    }
    
    public void setNomorIjin(final String nomorIjin) {
        this.nomorIjin = nomorIjin;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public String getNamaPemohon() {
        return this.namaPemohon;
    }
    
    public void setNamaPemohon(final String namaPemohon) {
        this.namaPemohon = namaPemohon;
    }
    
    public String getAlamatPemohon() {
        return this.alamatPemohon;
    }
    
    public void setAlamatPemohon(final String alamatPemohon) {
        this.alamatPemohon = alamatPemohon;
    }
    
    public String getNamaUsaha() {
        return this.namaUsaha;
    }
    
    public void setNamaUsaha(final String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }
    
    public String getAlamatUsaha() {
        return this.alamatUsaha;
    }
    
    public void setAlamatUsaha(final String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }
    
    public String getJenisUsaha() {
        return this.jenisUsaha;
    }
    
    public void setJenisUsaha(final String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }
    
    public double getJumlahKaryawan() {
        return this.jumlahKaryawan;
    }
    
    public void setJumlahKaryawan(final double jumlahKaryawan) {
        this.jumlahKaryawan = jumlahKaryawan;
    }
    
    public Date getMasaBerlaku() {
        return this.masaBerlaku;
    }
    
    public void setMasaBerlaku(final Date masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
}
