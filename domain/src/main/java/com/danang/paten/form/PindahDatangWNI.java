// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.ArrayList;
import com.danang.paten.domain.Berkas;
import java.util.List;
import com.danang.paten.domain.Petugas;

public class PindahDatangWNI
{
    private String nomor;
    private PindahDatangAsal asal;
    private PindahDatangTujuan tujuan;
    private Petugas petugas;
    private String nomorRegistrasi;
    private String nomorCek;
    private String nomorPenyerahan;
    private List<Berkas> berkas;
    private String flag;
    
    public PindahDatangWNI() {
        this.berkas = new ArrayList<Berkas>();
    }
    
    public String getFlag() {
        return this.flag;
    }
    
    public void setFlag(final String flag) {
        this.flag = flag;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public PindahDatangAsal getAsal() {
        return this.asal;
    }
    
    public void setAsal(final PindahDatangAsal asal) {
        this.asal = asal;
    }
    
    public PindahDatangTujuan getTujuan() {
        return this.tujuan;
    }
    
    public void setTujuan(final PindahDatangTujuan tujuan) {
        this.tujuan = tujuan;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public String getNomorRegistrasi() {
        return this.nomorRegistrasi;
    }
    
    public void setNomorRegistrasi(final String nomorRegistrasi) {
        this.nomorRegistrasi = nomorRegistrasi;
    }
    
    public String getNomorCek() {
        return this.nomorCek;
    }
    
    public void setNomorCek(final String nomorCek) {
        this.nomorCek = nomorCek;
    }
    
    public String getNomorPenyerahan() {
        return this.nomorPenyerahan;
    }
    
    public void setNomorPenyerahan(final String nomorPenyerahan) {
        this.nomorPenyerahan = nomorPenyerahan;
    }
    
    public List<Berkas> getBerkas() {
        return this.berkas;
    }
    
    public void setBerkas(final List<Berkas> berkas) {
        this.berkas = berkas;
    }
}
