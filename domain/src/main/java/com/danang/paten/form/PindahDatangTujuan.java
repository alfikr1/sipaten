// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import com.danang.paten.domain.DataPenduduk;
import com.danang.paten.enumerasi.StatusNomorKK;

public class PindahDatangTujuan
{
    private StatusNomorKK sttKK;
    private String nomorKK;
    private DataPenduduk kk;
    private Date tanggalDatang;
    private String jalan;
    private String rt;
    private String rw;
    private String dusun;
    private String desa;
    private String kecamatan;
    private String kab;
    private String prov;
    private List<DataPenduduk> childKK;
    
    public PindahDatangTujuan() {
        this.childKK = new ArrayList<DataPenduduk>();
    }
    
    public StatusNomorKK getSttKK() {
        return this.sttKK;
    }
    
    public void setSttKK(final StatusNomorKK sttKK) {
        this.sttKK = sttKK;
    }
    
    public String getNomorKK() {
        return this.nomorKK;
    }
    
    public void setNomorKK(final String nomorKK) {
        this.nomorKK = nomorKK;
    }
    
    public DataPenduduk getKk() {
        return this.kk;
    }
    
    public void setKk(final DataPenduduk kk) {
        this.kk = kk;
    }
    
    public Date getTanggalDatang() {
        return this.tanggalDatang;
    }
    
    public void setTanggalDatang(final Date tanggalDatang) {
        this.tanggalDatang = tanggalDatang;
    }
    
    public String getJalan() {
        return this.jalan;
    }
    
    public void setJalan(final String jalan) {
        this.jalan = jalan;
    }
    
    public String getRt() {
        return this.rt;
    }
    
    public void setRt(final String rt) {
        this.rt = rt;
    }
    
    public String getRw() {
        return this.rw;
    }
    
    public void setRw(final String rw) {
        this.rw = rw;
    }
    
    public String getDusun() {
        return this.dusun;
    }
    
    public void setDusun(final String dusun) {
        this.dusun = dusun;
    }
    
    public String getDesa() {
        return this.desa;
    }
    
    public void setDesa(final String desa) {
        this.desa = desa;
    }
    
    public String getKecamatan() {
        return this.kecamatan;
    }
    
    public void setKecamatan(final String kecamatan) {
        this.kecamatan = kecamatan;
    }
    
    public String getKab() {
        return this.kab;
    }
    
    public void setKab(final String kab) {
        this.kab = kab;
    }
    
    public String getProv() {
        return this.prov;
    }
    
    public void setProv(final String prov) {
        this.prov = prov;
    }
    
    public List<DataPenduduk> getChildKK() {
        return this.childKK;
    }
    
    public void setChildKK(final List<DataPenduduk> childKK) {
        this.childKK = childKK;
    }
}
