// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import com.danang.paten.domain.DataPenduduk;
import java.util.Date;

public class LaporanIUM
{
    private String nomor;
    private Date tanggal;
    private DataPenduduk pemohon;
    private String namaUsaha;
    private String bentukUsaha;
    private String npwp;
    private String kegiatanUsaha;
    private String saranaUsaha;
    private String alamatUsaha;
    private double jumlahModal;
    private Date penerimaan;
    private String nomorRegistrasi;
    private String nomorPeriksa;
    private Date tanggalPeriksa;
    private String petugasTerima;
    private String petugasPeriksa;
    private String penetapan;
    private Date berlaku;
    private Date akhirBerlaku;

    public String getNamaUsaha() {
        return namaUsaha;
    }

    public void setNamaUsaha(String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }

    public String getBentukUsaha() {
        return bentukUsaha;
    }

    public void setBentukUsaha(String bentukUsaha) {
        this.bentukUsaha = bentukUsaha;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getKegiatanUsaha() {
        return kegiatanUsaha;
    }

    public void setKegiatanUsaha(String kegiatanUsaha) {
        this.kegiatanUsaha = kegiatanUsaha;
    }

    public String getSaranaUsaha() {
        return saranaUsaha;
    }

    public void setSaranaUsaha(String saranaUsaha) {
        this.saranaUsaha = saranaUsaha;
    }

    public String getAlamatUsaha() {
        return alamatUsaha;
    }

    public void setAlamatUsaha(String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }

    public double getJumlahModal() {
        return jumlahModal;
    }

    public void setJumlahModal(double jumlahModal) {
        this.jumlahModal = jumlahModal;
    }
    
    public Date getBerlaku() {
        return this.berlaku;
    }
    
    public void setBerlaku(final Date berlaku) {
        this.berlaku = berlaku;
    }
    
    public Date getAkhirBerlaku() {
        return this.akhirBerlaku;
    }
    
    public void setAkhirBerlaku(final Date akhirBerlaku) {
        this.akhirBerlaku = akhirBerlaku;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public DataPenduduk getPemohon() {
        return this.pemohon;
    }
    
    public void setPemohon(final DataPenduduk pemohon) {
        this.pemohon = pemohon;
    }
    
    public Date getPenerimaan() {
        return this.penerimaan;
    }
    
    public void setPenerimaan(final Date penerimaan) {
        this.penerimaan = penerimaan;
    }
    
    public String getNomorRegistrasi() {
        return this.nomorRegistrasi;
    }
    
    public void setNomorRegistrasi(final String nomorRegistrasi) {
        this.nomorRegistrasi = nomorRegistrasi;
    }
    
    public String getNomorPeriksa() {
        return this.nomorPeriksa;
    }
    
    public void setNomorPeriksa(final String nomorPeriksa) {
        this.nomorPeriksa = nomorPeriksa;
    }
    
    public Date getTanggalPeriksa() {
        return this.tanggalPeriksa;
    }
    
    public void setTanggalPeriksa(final Date tanggalPeriksa) {
        this.tanggalPeriksa = tanggalPeriksa;
    }
    
    public String getPetugasTerima() {
        return this.petugasTerima;
    }
    
    public void setPetugasTerima(final String petugasTerima) {
        this.petugasTerima = petugasTerima;
    }
    
    public String getPetugasPeriksa() {
        return this.petugasPeriksa;
    }
    
    public void setPetugasPeriksa(final String petugasPeriksa) {
        this.petugasPeriksa = petugasPeriksa;
    }
    
    public String getPenetapan() {
        return this.penetapan;
    }
    
    public void setPenetapan(final String penetapan) {
        this.penetapan = penetapan;
    }
}
