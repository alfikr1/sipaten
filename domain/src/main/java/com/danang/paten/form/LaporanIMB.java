// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import java.util.Date;

public class LaporanIMB
{
    private int nomor;
    private String namaPemohon;
    private String alamat;
    private String alamatIjin;
    private String peruntukan;
    private double luas;
    private double retribusi;
    private String nomorIjin;
    private Date tanggalIjin;
    private Date tanggalKetetapan;
    private Date tanggalPemeriksaan;
    private String keterangan;

    public Date getTanggalKetetapan() {
        return tanggalKetetapan;
    }

    public void setTanggalKetetapan(Date tanggalKetetapan) {
        this.tanggalKetetapan = tanggalKetetapan;
    }

    public Date getTanggalPemeriksaan() {
        return tanggalPemeriksaan;
    }

    public void setTanggalPemeriksaan(Date tanggalPemeriksaan) {
        this.tanggalPemeriksaan = tanggalPemeriksaan;
    }
    
    public int getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final int nomor) {
        this.nomor = nomor;
    }
    
    public String getNamaPemohon() {
        return this.namaPemohon;
    }
    
    public void setNamaPemohon(final String namaPemohon) {
        this.namaPemohon = namaPemohon;
    }
    
    public String getAlamat() {
        return this.alamat;
    }
    
    public void setAlamat(final String alamat) {
        this.alamat = alamat;
    }
    
    public String getAlamatIjin() {
        return this.alamatIjin;
    }
    
    public void setAlamatIjin(final String alamatIjin) {
        this.alamatIjin = alamatIjin;
    }
    
    public String getPeruntukan() {
        return this.peruntukan;
    }
    
    public void setPeruntukan(final String peruntukan) {
        this.peruntukan = peruntukan;
    }
    
    public double getLuas() {
        return this.luas;
    }
    
    public void setLuas(final double luas) {
        this.luas = luas;
    }
    
    public double getRetribusi() {
        return this.retribusi;
    }
    
    public void setRetribusi(final double retribusi) {
        this.retribusi = retribusi;
    }
    
    public String getNomorIjin() {
        return this.nomorIjin;
    }
    
    public void setNomorIjin(final String nomorIjin) {
        this.nomorIjin = nomorIjin;
    }
    
    public Date getTanggalIjin() {
        return this.tanggalIjin;
    }
    
    public void setTanggalIjin(final Date tanggalIjin) {
        this.tanggalIjin = tanggalIjin;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
}
