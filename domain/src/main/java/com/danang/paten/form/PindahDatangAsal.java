// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import com.danang.paten.domain.DataPenduduk;

public class PindahDatangAsal
{
    private String nomorKK;
    private String kepalaKeluarga;
    private String lingk;
    private String rt;
    private String rw;
    private String dusun;
    private String desa;
    private String kab;
    private String kec;
    private String prov;
    private DataPenduduk dp;
    private String kodePos;
    private String telp;
    private String email;
    
    public String getNomorKK() {
        return this.nomorKK;
    }
    
    public void setNomorKK(final String nomorKK) {
        this.nomorKK = nomorKK;
    }
    
    public String getKepalaKeluarga() {
        return this.kepalaKeluarga;
    }
    
    public void setKepalaKeluarga(final String kepalaKeluarga) {
        this.kepalaKeluarga = kepalaKeluarga;
    }
    
    public String getLingk() {
        return this.lingk;
    }
    
    public void setLingk(final String lingk) {
        this.lingk = lingk;
    }
    
    public String getRt() {
        return this.rt;
    }
    
    public void setRt(final String rt) {
        this.rt = rt;
    }
    
    public String getRw() {
        return this.rw;
    }
    
    public void setRw(final String rw) {
        this.rw = rw;
    }
    
    public String getDusun() {
        return this.dusun;
    }
    
    public void setDusun(final String dusun) {
        this.dusun = dusun;
    }
    
    public String getDesa() {
        return this.desa;
    }
    
    public void setDesa(final String desa) {
        this.desa = desa;
    }
    
    public String getKab() {
        return this.kab;
    }
    
    public void setKab(final String kab) {
        this.kab = kab;
    }
    
    public String getKec() {
        return this.kec;
    }
    
    public void setKec(final String kec) {
        this.kec = kec;
    }
    
    public String getProv() {
        return this.prov;
    }
    
    public void setProv(final String prov) {
        this.prov = prov;
    }
    
    public DataPenduduk getDp() {
        return this.dp;
    }
    
    public void setDp(final DataPenduduk dp) {
        this.dp = dp;
    }
    
    public String getKodePos() {
        return this.kodePos;
    }
    
    public void setKodePos(final String kodePos) {
        this.kodePos = kodePos;
    }
    
    public String getTelp() {
        return this.telp;
    }
    
    public void setTelp(final String telp) {
        this.telp = telp;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
}
