// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import com.danang.paten.enumerasi.StatusPekerjaan;

public class PencarianData
{
    private String nomorRegistrasi;
    private String petugasRegistrasi;
    private String namaLayanan;
    private String pemohon;
    private String nomorPelayanan;
    private StatusPekerjaan statusPekerjaan;
    
    public String getNomorRegistrasi() {
        return this.nomorRegistrasi;
    }
    
    public void setNomorRegistrasi(final String nomorRegistrasi) {
        this.nomorRegistrasi = nomorRegistrasi;
    }
    
    public String getPetugasRegistrasi() {
        return this.petugasRegistrasi;
    }
    
    public void setPetugasRegistrasi(final String petugasRegistrasi) {
        this.petugasRegistrasi = petugasRegistrasi;
    }
    
    public String getNamaLayanan() {
        return this.namaLayanan;
    }
    
    public void setNamaLayanan(final String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }
    
    public String getPemohon() {
        return this.pemohon;
    }
    
    public void setPemohon(final String pemohon) {
        this.pemohon = pemohon;
    }
    
    public String getNomorPelayanan() {
        return this.nomorPelayanan;
    }
    
    public void setNomorPelayanan(final String nomorPelayanan) {
        this.nomorPelayanan = nomorPelayanan;
    }
    
    public StatusPekerjaan getStatusPekerjaan() {
        return this.statusPekerjaan;
    }
    
    public void setStatusPekerjaan(final StatusPekerjaan statusPekerjaan) {
        this.statusPekerjaan = statusPekerjaan;
    }
}
