// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.form;

import com.danang.paten.domain.BeritaAcaraPemeriksaan;
import com.danang.paten.domain.Petugas;
import java.util.Date;
import com.danang.paten.domain.DataPenduduk;

public class IMB
{
    private String nomor;
    private String nomorRegistrasi;
    private String nomorValidasi;
    private String nomorKetetapan;
    private String nomorPengembalianBerkas;
    private DataPenduduk pemohon;
    private DataPenduduk ijin;
    private String statusTanah;
    private String nomorPersil;
    private String nomorHM;
    private String kelas;
    private double luasTanah;
    private String atasNamaTanah;
    private String jalan;
    private String lingk;
    private String kelurahan;
    private String kecamatan;
    private String kab;
    private String pondasi;
    private String kerangka;
    private String dinding;
    private String lantai;
    private String kerangkaAtap;
    private String atap;
    private String gunaBangunan;
    private Date tanggalRegistrasi;
    private String denah;
    private String skala;
    private String saluran;
    private double luasBangunan;
    private double retribusi;
    private Date berlakuMulai;
    private Date berlakuSampai;
    private Date ketetapan;
    private Petugas petugas;
    private String gambarTampak;
    private Date ambilBerkas;
    private String camat;
    private String nipCamat;
    private String jabatanCamat;
    private String rabat;
    private double jarakJalan;
    private double gsp;
    private double persenLantai;
    private double persenBangunan;
    private Date tanggalKetetapan;
    private double garisSepadanBangunan;
    private PernyataanTeknis pernyataanTeknis;
    private BeritaAcaraPemeriksaan pemeriksaan;
    private double koefisienKota;
    private double koefisienKelasJalan;
    private double koefisiengunaBangunan;
    private double koefisienKelasBangunan;
    private double koefisienstatusBangunan;
    private double koefisienTingkatBangunan;
    private double koefisienLuasLantai;
    private double constTanah;
    private double constPagar;
    private double constRabat;
    private double constSaluran;
    private String koefisienKotaText;
    private String koefisienKelasJalanText;
    private String koefisienKelasBangunanText;
    private String koefisienStatusBangunanText;
    private String koefisienTingkatBangunanText;
    private String koefisienGunaBangunanText;
    private String koefisienLuasLantaiText;
    private boolean cetakUndangan;
    private boolean inputBeritaAcara;
    private String keputusanIjin;
    private String alasan;
    private String saran;

    public String getKeputusanIjin() {
        return keputusanIjin;
    }

    public void setKeputusanIjin(String keputusanIjin) {
        this.keputusanIjin = keputusanIjin;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public String getSaran() {
        return saran;
    }

    public void setSaran(String saran) {
        this.saran = saran;
    }
    

    public boolean isCetakUndangan() {
        return cetakUndangan;
    }

    public void setCetakUndangan(boolean cetakUndangan) {
        this.cetakUndangan = cetakUndangan;
    }

    public boolean isInputBeritaAcara() {
        return inputBeritaAcara;
    }

    public void setInputBeritaAcara(boolean inputBeritaAcara) {
        this.inputBeritaAcara = inputBeritaAcara;
    }
    

    public String getKoefisienGunaBangunanText() {
        return koefisienGunaBangunanText;
    }

    public void setKoefisienGunaBangunanText(String koefisienGunaBangunanText) {
        this.koefisienGunaBangunanText = koefisienGunaBangunanText;
    }
    
    public String getKoefisienKotaText() {
        return koefisienKotaText;
    }

    public void setKoefisienKotaText(String koefisienKotaText) {
        this.koefisienKotaText = koefisienKotaText;
    }

    public String getKoefisienKelasJalanText() {
        return koefisienKelasJalanText;
    }

    public void setKoefisienKelasJalanText(String koefisienKelasJalanText) {
        this.koefisienKelasJalanText = koefisienKelasJalanText;
    }

    public String getKoefisienKelasBangunanText() {
        return koefisienKelasBangunanText;
    }

    public void setKoefisienKelasBangunanText(String koefisienKelasBangunanText) {
        this.koefisienKelasBangunanText = koefisienKelasBangunanText;
    }

    public String getKoefisienStatusBangunanText() {
        return koefisienStatusBangunanText;
    }

    public void setKoefisienStatusBangunanText(String koefisienStatusBangunanText) {
        this.koefisienStatusBangunanText = koefisienStatusBangunanText;
    }

    public String getKoefisienTingkatBangunanText() {
        return koefisienTingkatBangunanText;
    }

    public void setKoefisienTingkatBangunanText(String koefisienTingkatBangunanText) {
        this.koefisienTingkatBangunanText = koefisienTingkatBangunanText;
    }

    public String getKoefisienLuasLantaiText() {
        return koefisienLuasLantaiText;
    }

    public void setKoefisienLuasLantaiText(String koefisienLuasLantaiText) {
        this.koefisienLuasLantaiText = koefisienLuasLantaiText;
    }
    

    public double getKoefisienKota() {
        return koefisienKota;
    }

    public void setKoefisienKota(double koefisienKota) {
        this.koefisienKota = koefisienKota;
    }

    public double getKoefisienKelasJalan() {
        return koefisienKelasJalan;
    }

    public void setKoefisienKelasJalan(double koefisienKelasJalan) {
        this.koefisienKelasJalan = koefisienKelasJalan;
    }

    public double getKoefisiengunaBangunan() {
        return koefisiengunaBangunan;
    }

    public void setKoefisiengunaBangunan(double koefisiengunaBangunan) {
        this.koefisiengunaBangunan = koefisiengunaBangunan;
    }

    public double getKoefisienKelasBangunan() {
        return koefisienKelasBangunan;
    }

    public void setKoefisienKelasBangunan(double koefisienKelasBangunan) {
        this.koefisienKelasBangunan = koefisienKelasBangunan;
    }

    public double getKoefisienstatusBangunan() {
        return koefisienstatusBangunan;
    }

    public void setKoefisienstatusBangunan(double koefisienstatusBangunan) {
        this.koefisienstatusBangunan = koefisienstatusBangunan;
    }

    public double getKoefisienTingkatBangunan() {
        return koefisienTingkatBangunan;
    }

    public void setKoefisienTingkatBangunan(double koefisienTingkatBangunan) {
        this.koefisienTingkatBangunan = koefisienTingkatBangunan;
    }

    public double getKoefisienLuasLantai() {
        return koefisienLuasLantai;
    }

    public void setKoefisienLuasLantai(double koefisienLuasLantai) {
        this.koefisienLuasLantai = koefisienLuasLantai;
    }

    
    public double getConstTanah() {
        return constTanah;
    }

    public void setConstTanah(double constTanah) {
        this.constTanah = constTanah;
    }

    public double getConstPagar() {
        return constPagar;
    }

    public void setConstPagar(double constPagar) {
        this.constPagar = constPagar;
    }

    public double getConstRabat() {
        return constRabat;
    }

    public void setConstRabat(double constRabat) {
        this.constRabat = constRabat;
    }

    public double getConstSaluran() {
        return constSaluran;
    }

    public void setConstSaluran(double constSaluran) {
        this.constSaluran = constSaluran;
    }
    

    public double getGarisSepadanBangunan() {
        return garisSepadanBangunan;
    }

    public void setGarisSepadanBangunan(double garisSepadanBangunan) {
        this.garisSepadanBangunan = garisSepadanBangunan;
    }
    
    

    public PernyataanTeknis getPernyataanTeknis() {
        return pernyataanTeknis;
    }

    public void setPernyataanTeknis(PernyataanTeknis pernyataanTeknis) {
        this.pernyataanTeknis = pernyataanTeknis;
    }

    public BeritaAcaraPemeriksaan getPemeriksaan() {
        return pemeriksaan;
    }

    public void setPemeriksaan(BeritaAcaraPemeriksaan pemeriksaan) {
        this.pemeriksaan = pemeriksaan;
    }
    
    public Date getTanggalKetetapan() {
        return this.tanggalKetetapan;
    }
    
    public void setTanggalKetetapan(final Date tanggalKetetapan) {
        this.tanggalKetetapan = tanggalKetetapan;
    }
    
    public String getNipCamat() {
        return this.nipCamat;
    }
    
    public void setNipCamat(final String nipCamat) {
        this.nipCamat = nipCamat;
    }
    
    public String getJabatanCamat() {
        return this.jabatanCamat;
    }
    
    public void setJabatanCamat(final String jabatanCamat) {
        this.jabatanCamat = jabatanCamat;
    }
    
    public String getRabat() {
        return this.rabat;
    }
    
    public void setRabat(final String rabat) {
        this.rabat = rabat;
    }
    
    public double getJarakJalan() {
        return this.jarakJalan;
    }
    
    public void setJarakJalan(final double jarakJalan) {
        this.jarakJalan = jarakJalan;
    }
    
    public double getGsp() {
        return this.gsp;
    }
    
    public void setGsp(final double gsp) {
        this.gsp = gsp;
    }
    
    public double getPersenLantai() {
        return this.persenLantai;
    }
    
    public void setPersenLantai(final double persenLantai) {
        this.persenLantai = persenLantai;
    }
    
    public double getPersenBangunan() {
        return this.persenBangunan;
    }
    
    public void setPersenBangunan(final double persenBangunan) {
        this.persenBangunan = persenBangunan;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public String getNomorRegistrasi() {
        return this.nomorRegistrasi;
    }
    
    public void setNomorRegistrasi(final String nomorRegistrasi) {
        this.nomorRegistrasi = nomorRegistrasi;
    }
    
    public String getNomorValidasi() {
        return this.nomorValidasi;
    }
    
    public void setNomorValidasi(final String nomorValidasi) {
        this.nomorValidasi = nomorValidasi;
    }
    
    public String getNomorKetetapan() {
        return this.nomorKetetapan;
    }
    
    public void setNomorKetetapan(final String nomorKetetapan) {
        this.nomorKetetapan = nomorKetetapan;
    }
    
    public String getNomorPengembalianBerkas() {
        return this.nomorPengembalianBerkas;
    }
    
    public void setNomorPengembalianBerkas(final String nomorPengembalianBerkas) {
        this.nomorPengembalianBerkas = nomorPengembalianBerkas;
    }
    
    public Date getAmbilBerkas() {
        return this.ambilBerkas;
    }
    
    public void setAmbilBerkas(final Date ambilBerkas) {
        this.ambilBerkas = ambilBerkas;
    }
    
    public String getGambarTampak() {
        return this.gambarTampak;
    }
    
    public void setGambarTampak(final String gambarTampak) {
        this.gambarTampak = gambarTampak;
    }
    
    public DataPenduduk getPemohon() {
        return this.pemohon;
    }
    
    public void setPemohon(final DataPenduduk pemohon) {
        this.pemohon = pemohon;
    }
    
    public DataPenduduk getIjin() {
        return this.ijin;
    }
    
    public void setIjin(final DataPenduduk ijin) {
        this.ijin = ijin;
    }
    
    public String getStatusTanah() {
        return this.statusTanah;
    }
    
    public void setStatusTanah(final String statusTanah) {
        this.statusTanah = statusTanah;
    }
    
    public String getNomorPersil() {
        return this.nomorPersil;
    }
    
    public void setNomorPersil(final String nomorPersil) {
        this.nomorPersil = nomorPersil;
    }
    
    public String getNomorHM() {
        return this.nomorHM;
    }
    
    public void setNomorHM(final String nomorHM) {
        this.nomorHM = nomorHM;
    }
    
    public String getKelas() {
        return this.kelas;
    }
    
    public void setKelas(final String kelas) {
        this.kelas = kelas;
    }
    
    public double getLuasTanah() {
        return this.luasTanah;
    }
    
    public void setLuasTanah(final double luasTanah) {
        this.luasTanah = luasTanah;
    }
    
    public String getAtasNamaTanah() {
        return this.atasNamaTanah;
    }
    
    public void setAtasNamaTanah(final String atasNamaTanah) {
        this.atasNamaTanah = atasNamaTanah;
    }
    
    public String getJalan() {
        return this.jalan;
    }
    
    public void setJalan(final String jalan) {
        this.jalan = jalan;
    }
    
    public String getLingk() {
        return this.lingk;
    }
    
    public void setLingk(final String lingk) {
        this.lingk = lingk;
    }
    
    public String getKelurahan() {
        return this.kelurahan;
    }
    
    public void setKelurahan(final String kelurahan) {
        this.kelurahan = kelurahan;
    }
    
    public String getKecamatan() {
        return this.kecamatan;
    }
    
    public void setKecamatan(final String kecamatan) {
        this.kecamatan = kecamatan;
    }
    
    public String getKab() {
        return this.kab;
    }
    
    public void setKab(final String kab) {
        this.kab = kab;
    }
    
    public String getPondasi() {
        return this.pondasi;
    }
    
    public void setPondasi(final String pondasi) {
        this.pondasi = pondasi;
    }
    
    public String getKerangka() {
        return this.kerangka;
    }
    
    public void setKerangka(final String kerangka) {
        this.kerangka = kerangka;
    }
    
    public String getDinding() {
        return this.dinding;
    }
    
    public void setDinding(final String dinding) {
        this.dinding = dinding;
    }
    
    public String getLantai() {
        return this.lantai;
    }
    
    public void setLantai(final String lantai) {
        this.lantai = lantai;
    }
    
    public String getKerangkaAtap() {
        return this.kerangkaAtap;
    }
    
    public void setKerangkaAtap(final String kerangkaAtap) {
        this.kerangkaAtap = kerangkaAtap;
    }
    
    public String getAtap() {
        return this.atap;
    }
    
    public void setAtap(final String atap) {
        this.atap = atap;
    }
    
    public String getGunaBangunan() {
        return this.gunaBangunan;
    }
    
    public void setGunaBangunan(final String gunaBangunan) {
        this.gunaBangunan = gunaBangunan;
    }
    
    public Date getTanggalRegistrasi() {
        return this.tanggalRegistrasi;
    }
    
    public void setTanggalRegistrasi(final Date tanggalRegistrasi) {
        this.tanggalRegistrasi = tanggalRegistrasi;
    }
    
    public String getDenah() {
        return this.denah;
    }
    
    public void setDenah(final String denah) {
        this.denah = denah;
    }
    
    public String getSkala() {
        return this.skala;
    }
    
    public void setSkala(final String skala) {
        this.skala = skala;
    }
    
    public String getSaluran() {
        return this.saluran;
    }
    
    public void setSaluran(final String saluran) {
        this.saluran = saluran;
    }
    
    public double getLuasBangunan() {
        return this.luasBangunan;
    }
    
    public void setLuasBangunan(final double luasBangunan) {
        this.luasBangunan = luasBangunan;
    }
    
    public double getRetribusi() {
        return this.retribusi;
    }
    
    public void setRetribusi(final double retribusi) {
        this.retribusi = retribusi;
    }
    
    public Date getBerlakuMulai() {
        return this.berlakuMulai;
    }
    
    public void setBerlakuMulai(final Date berlakuMulai) {
        this.berlakuMulai = berlakuMulai;
    }
    
    public Date getBerlakuSampai() {
        return this.berlakuSampai;
    }
    
    public void setBerlakuSampai(final Date berlakuSampai) {
        this.berlakuSampai = berlakuSampai;
    }
    
    public Date getKetetapan() {
        return this.ketetapan;
    }
    
    public void setKetetapan(final Date ketetapan) {
        this.ketetapan = ketetapan;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
}
