// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.enumerasi;

public enum StatusPekerjaan
{
    Pemeriksaan, 
    Pemberkasan, 
    Selesai, 
    SelesaiDenganSyarat, 
    Dikembalikan, 
    Penetapan, 
    Diserahkan;
}
