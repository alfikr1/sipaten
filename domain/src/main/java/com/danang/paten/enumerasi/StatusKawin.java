// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.enumerasi;

public enum StatusKawin
{
    BELUMKAWIN(0), 
    KAWIN(1), 
    DUDA(2), 
    JANDA(3);
    
    private final int value;
    
    private StatusKawin(final int val) {
        this.value = val;
    }
}
