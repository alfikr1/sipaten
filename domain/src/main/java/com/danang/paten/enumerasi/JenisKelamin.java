// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.enumerasi;

public enum JenisKelamin
{
    Pria(0), 
    Perempuan(1);
    
    private final int value;
    
    private JenisKelamin(final int val) {
        this.value = val;
    }
}
