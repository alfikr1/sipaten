// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.enumerasi;

public enum StatusNomorKK
{
    NUMPANGKK(1), 
    KKBARU(2), 
    KKTETAP(3);
    
    private final int val;
    
    private StatusNomorKK(final int v) {
        this.val = v;
    }
}
