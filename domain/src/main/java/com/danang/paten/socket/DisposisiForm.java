/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.socket;

import java.util.Date;

/**
 *
 * @author Danang
 */
public class DisposisiForm {
    private String nomorSurat;
    private Date tanggalDispo;
    private String isiDispo;

    public String getNomorSurat() {
        return nomorSurat;
    }

    public void setNomorSurat(String nomorSurat) {
        this.nomorSurat = nomorSurat;
    }

    public Date getTanggalDispo() {
        return tanggalDispo;
    }

    public void setTanggalDispo(Date tanggalDispo) {
        this.tanggalDispo = tanggalDispo;
    }

    public String getIsiDispo() {
        return isiDispo;
    }

    public void setIsiDispo(String isiDispo) {
        this.isiDispo = isiDispo;
    }
    
}
