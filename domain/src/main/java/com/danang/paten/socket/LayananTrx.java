/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.socket;

import java.util.Date;

/**
 *
 * @author Danang
 */
public class LayananTrx {

    private String nomorDaftar;
    private Date tanggalDaftar;
    private String namaPemohon;
    private String alamatPemohon;
    private String statusTransaksi;

    public String getNomorDaftar() {
        return nomorDaftar;
    }

    public void setNomorDaftar(String nomorDaftar) {
        this.nomorDaftar = nomorDaftar;
    }

    public Date getTanggalDaftar() {
        return tanggalDaftar;
    }

    public void setTanggalDaftar(Date tanggalDaftar) {
        this.tanggalDaftar = tanggalDaftar;
    }

    public String getNamaPemohon() {
        return namaPemohon;
    }

    public void setNamaPemohon(String namaPemohon) {
        this.namaPemohon = namaPemohon;
    }

    public String getAlamatPemohon() {
        return alamatPemohon;
    }

    public void setAlamatPemohon(String alamatPemohon) {
        this.alamatPemohon = alamatPemohon;
    }

    public String getStatusTransaksi() {
        return statusTransaksi;
    }

    public void setStatusTransaksi(String statusTransaksi) {
        this.statusTransaksi = statusTransaksi;
    }

}
