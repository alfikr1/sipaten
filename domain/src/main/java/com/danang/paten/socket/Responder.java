/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

/**
 *
 * @author danang
 */
public class Responder {

    private String serverSentence;
    private final Socket connectionSocket;
    private final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public Responder(Socket connectionSocket) {
        this.connectionSocket = connectionSocket;
    }
    
    synchronized public boolean responderMethod(Socket connectionSocket) {
        try {
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            String clientSentence = inFromClient.readLine();
            if (clientSentence == null || clientSentence.equals("EXIT")) {
                return false;
            }
            System.out.println("client : " + clientSentence);
            serverSentence=br.readLine()+"\n";
            outToClient.writeBytes(serverSentence);
            return true;
        } catch (SocketException e) {
            System.out.println("Disconnected");
            return false;
        }catch(Exception e){
            e.printStackTrace(System.err);
            return false;
        }
    }
}
