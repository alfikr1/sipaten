// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PenetapanBerkas
{
    @Id
    private String nomor;
    private Date tanggal;
    private Petugas petugas;
    private Date mulaiBerlaku;
    private Date akhirBerlaku;
    private boolean validasi;
    private List<Berkas> items;
    
    public PenetapanBerkas() {
        this.items = new ArrayList<Berkas>();
    }
    
    public List<Berkas> getItems() {
        return this.items;
    }
    
    public Date getMulaiBerlaku() {
        return this.mulaiBerlaku;
    }
    
    public void setMulaiBerlaku(final Date mulaiBerlaku) {
        this.mulaiBerlaku = mulaiBerlaku;
    }
    
    public Date getAkhirBerlaku() {
        return this.akhirBerlaku;
    }
    
    public void setAkhirBerlaku(final Date akhirBerlaku) {
        this.akhirBerlaku = akhirBerlaku;
    }
    
    public void setItems(final List<Berkas> items) {
        this.items = items;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public boolean isValidasi() {
        return this.validasi;
    }
    
    public void setValidasi(final boolean validasi) {
        this.validasi = validasi;
    }
}
