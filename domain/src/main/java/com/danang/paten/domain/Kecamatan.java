// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Kecamatan implements Serializable
{
    @Id
    private String kodeKecamatan;
    private String namaKecamatan;
    private String kodeKabupaten;
    private String namaKabupaten;
    private String alamat;
    private String alamat2;
    private String telp;
    private String kodepos;
    private String serial;
    private String camat;
    private String jabatan;
    private String nip;
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public String getJabatan() {
        return this.jabatan;
    }
    
    public void setJabatan(final String jabatan) {
        this.jabatan = jabatan;
    }
    
    public String getNip() {
        return this.nip;
    }
    
    public void setNip(final String nip) {
        this.nip = nip;
    }
    
    public String getSerial() {
        return this.serial;
    }
    
    public void setSerial(final String serial) {
        this.serial = serial;
    }
    
    public String getAlamat() {
        return this.alamat;
    }
    
    public void setAlamat(final String alamat) {
        this.alamat = alamat;
    }
    
    public String getAlamat2() {
        return this.alamat2;
    }
    
    public void setAlamat2(final String alamat2) {
        this.alamat2 = alamat2;
    }
    
    public String getTelp() {
        return this.telp;
    }
    
    public void setTelp(final String telp) {
        this.telp = telp;
    }
    
    public String getKodepos() {
        return this.kodepos;
    }
    
    public void setKodepos(final String kodepos) {
        this.kodepos = kodepos;
    }
    
    public String getKodeKecamatan() {
        return this.kodeKecamatan;
    }
    
    public void setKodeKecamatan(final String kodeKecamatan) {
        this.kodeKecamatan = kodeKecamatan;
    }
    
    public String getNamaKecamatan() {
        return this.namaKecamatan;
    }
    
    public void setNamaKecamatan(final String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }
    
    public String getKodeKabupaten() {
        return this.kodeKabupaten;
    }
    
    public void setKodeKabupaten(final String kodeKabupaten) {
        this.kodeKabupaten = kodeKabupaten;
    }
    
    public String getNamaKabupaten() {
        return this.namaKabupaten;
    }
    
    public void setNamaKabupaten(final String namaKabupaten) {
        this.namaKabupaten = namaKabupaten;
    }
    
    @Override
    public String toString() {
        return "Kecamatan{kodeKecamatan=" + this.kodeKecamatan + ", namaKecamatan=" + this.namaKecamatan + ", kodeKabupaten=" + this.kodeKabupaten + ", namaKabupaten=" + this.namaKabupaten + ", alamat=" + this.alamat + ", alamat2=" + this.alamat2 + ", telp=" + this.telp + ", kodepos=" + this.kodepos + ", serial=" + this.serial + ", camat=" + this.camat + ", jabatan=" + this.jabatan + ", nip=" + this.nip + '}';
    }
}
