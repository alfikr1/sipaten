// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.List;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class KartuKendaliSuratMasuk implements Serializable
{
    @Id
    private String id;
    private String kode;
    private String idx;
    private String IsiRingkas;
    private String dari;
    private String kepada;
    private Date tanggalTerima;
    private Date tanggalSurat;
    private String nomorSurat;
    private String pengolah;
    private Date tanggalDitentukan;
    private String lampiran;
    private String tandaTerima;
    private String catatan;
    private String sifat;
    private String catatanDispo;
    private String disposisi;
    private boolean isDispose;
    private List<String> dispo;
    private String statusSurat;
    private Date tanggalDisposisi;
    private DisposisiSuratMasuk dsm;

    public Date getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }
    
    public DisposisiSuratMasuk getDsm() {
        return this.dsm;
    }
    
    public void setDsm(final DisposisiSuratMasuk dsm) {
        this.dsm = dsm;
    }
    
    public boolean isIsDispose() {
        return this.isDispose;
    }
    
    public void setIsDispose(final boolean isDispose) {
        this.isDispose = isDispose;
    }
    
    public Date getTanggalDisposisi() {
        return this.tanggalDisposisi;
    }
    
    public void setTanggalDisposisi(final Date tanggalDisposisi) {
        this.tanggalDisposisi = tanggalDisposisi;
    }
    
    public String getCatatanDispo() {
        return this.catatanDispo;
    }
    
    public void setCatatanDispo(final String catatanDispo) {
        this.catatanDispo = catatanDispo;
    }
    
    public String getDisposisi() {
        return this.disposisi;
    }
    
    public void setDisposisi(final String disposisi) {
        this.disposisi = disposisi;
    }
    
    public String getStatusSurat() {
        return this.statusSurat;
    }
    
    public void setStatusSurat(final String statusSurat) {
        this.statusSurat = statusSurat;
    }
    
    public String getKepada() {
        return this.kepada;
    }
    
    public void setKepada(final String kepada) {
        this.kepada = kepada;
    }
    
    public String getSifat() {
        return this.sifat;
    }
    
    public void setSifat(final String sifat) {
        this.sifat = sifat;
    }
    
    public List<String> getDispo() {
        return this.dispo;
    }
    
    public void setDispo(final List<String> dispo) {
        this.dispo = dispo;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public String getIdx() {
        return this.idx;
    }
    
    public void setIdx(final String idx) {
        this.idx = idx;
    }
    
    public String getIsiRingkas() {
        return this.IsiRingkas;
    }
    
    public void setIsiRingkas(final String IsiRingkas) {
        this.IsiRingkas = IsiRingkas;
    }
    
    public String getDari() {
        return this.dari;
    }
    
    public void setDari(final String dari) {
        this.dari = dari;
    }
    
    public Date getTanggalSurat() {
        return this.tanggalSurat;
    }
    
    public void setTanggalSurat(final Date tanggalSurat) {
        this.tanggalSurat = tanggalSurat;
    }
    
    public String getNomorSurat() {
        return this.nomorSurat;
    }
    
    public void setNomorSurat(final String nomorSurat) {
        this.nomorSurat = nomorSurat;
    }
    
    public String getPengolah() {
        return this.pengolah;
    }
    
    public void setPengolah(final String pengolah) {
        this.pengolah = pengolah;
    }
    
    public Date getTanggalDitentukan() {
        return this.tanggalDitentukan;
    }
    
    public void setTanggalDitentukan(final Date tanggalDitentukan) {
        this.tanggalDitentukan = tanggalDitentukan;
    }
    
    public String getLampiran() {
        return this.lampiran;
    }
    
    public void setLampiran(final String lampiran) {
        this.lampiran = lampiran;
    }
    
    public String getTandaTerima() {
        return this.tandaTerima;
    }
    
    public void setTandaTerima(final String tandaTerima) {
        this.tandaTerima = tandaTerima;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
}
