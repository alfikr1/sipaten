// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class LiburNasional
{
    @Id
    private int id;
    private String keterangan;
    private int tanggal;
    private int bulan;
    private int tahun;
    private boolean tiapTahun;
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
    
    public int getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final int tanggal) {
        this.tanggal = tanggal;
    }
    
    public int getBulan() {
        return this.bulan;
    }
    
    public void setBulan(final int bulan) {
        this.bulan = bulan;
    }
    
    public int getTahun() {
        return this.tahun;
    }
    
    public void setTahun(final int tahun) {
        this.tahun = tahun;
    }
    
    public boolean isTiapTahun() {
        return this.tiapTahun;
    }
    
    public void setTiapTahun(final boolean tiapTahun) {
        this.tiapTahun = tiapTahun;
    }
}
