// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Document
public class Token implements Serializable
{
    @Id
    private String token;
    @DBRef
    private Petugas petugas;
    private Date createDate;
    private Date expiredDate;
    
    public String getToken() {
        return this.token;
    }
    
    public void setToken(final String token) {
        this.token = token;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(final Date createDate) {
        this.createDate = createDate;
    }
    
    public Date getExpiredDate() {
        return this.expiredDate;
    }
    
    public void setExpiredDate(final Date expiredDate) {
        this.expiredDate = expiredDate;
    }
}
