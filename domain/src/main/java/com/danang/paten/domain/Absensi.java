// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Absensi
{
    @Id
    private int id;
    private Petugas petugas;
    private Date masuk;
    private Date pulang;
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public Date getMasuk() {
        return this.masuk;
    }
    
    public void setMasuk(final Date masuk) {
        this.masuk = masuk;
    }
    
    public Date getPulang() {
        return this.pulang;
    }
    
    public void setPulang(final Date pulang) {
        this.pulang = pulang;
    }
}
