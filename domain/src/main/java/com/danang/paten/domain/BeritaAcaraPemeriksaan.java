/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danang.paten.domain;

import com.danang.paten.enumerasi.LetakSaluran;
import com.danang.paten.enumerasi.StatusKondisiTanah;
import com.danang.paten.enumerasi.StrukturSaluran;
import com.danang.paten.enumerasi.Topografi;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author danang
 */
public class BeritaAcaraPemeriksaan {
    private Date tanggal;
    private List<Map<String,String>> pemeriksaLapangan;
    private String lokasiBangunan;
    private String desa;
    private String kelasJalan;
    private String strukturJalan;
    private String lebarBadanJalan;
    private String lebarDamija;
    private String kondisiTanah;
    private Topografi topografi;
    private StatusKondisiTanah statusKondisiTanah;
    private String batasUtara;
    private String batasSelatan;
    private String batasBarat;
    private String batasTimur;
    private LetakSaluran letakSaluran;
    private double saluranLebar;
    private double saluranDalam;
    private StrukturSaluran strukturSaluran;
    private double GSB;
    private double GSP;
    private double KDB;
    private double KLB;
    private String pembuanganHujan;
    private String lain;

    public Date getTanggal() {
        return tanggal;
    }

    public double getSaluranLebar() {
        return saluranLebar;
    }

    public void setSaluranLebar(double saluranLebar) {
        this.saluranLebar = saluranLebar;
    }

    public double getSaluranDalam() {
        return saluranDalam;
    }

    public void setSaluranDalam(double saluranDalam) {
        this.saluranDalam = saluranDalam;
    }
    
    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public List<Map<String, String>> getPemeriksaLapangan() {
        return pemeriksaLapangan;
    }

    public void setPemeriksaLapangan(List<Map<String, String>> pemeriksaLapangan) {
        this.pemeriksaLapangan = pemeriksaLapangan;
    }

    public String getLokasiBangunan() {
        return lokasiBangunan;
    }

    public void setLokasiBangunan(String lokasiBangunan) {
        this.lokasiBangunan = lokasiBangunan;
    }

    public String getDesa() {
        return desa;
    }

    public void setDesa(String desa) {
        this.desa = desa;
    }

    public String getKelasJalan() {
        return kelasJalan;
    }

    public void setKelasJalan(String kelasJalan) {
        this.kelasJalan = kelasJalan;
    }

    public String getStrukturJalan() {
        return strukturJalan;
    }

    public void setStrukturJalan(String strukturJalan) {
        this.strukturJalan = strukturJalan;
    }

    public String getLebarBadanJalan() {
        return lebarBadanJalan;
    }

    public void setLebarBadanJalan(String lebarBadanJalan) {
        this.lebarBadanJalan = lebarBadanJalan;
    }

    public String getLebarDamija() {
        return lebarDamija;
    }

    public void setLebarDamija(String lebarDamija) {
        this.lebarDamija = lebarDamija;
    }

    public String getKondisiTanah() {
        return kondisiTanah;
    }

    public void setKondisiTanah(String kondisiTanah) {
        this.kondisiTanah = kondisiTanah;
    }

    public Topografi getTopografi() {
        return topografi;
    }

    public void setTopografi(Topografi topografi) {
        this.topografi = topografi;
    }

    public StatusKondisiTanah getStatusKondisiTanah() {
        return statusKondisiTanah;
    }

    public void setStatusKondisiTanah(StatusKondisiTanah statusKondisiTanah) {
        this.statusKondisiTanah = statusKondisiTanah;
    }

    public String getBatasUtara() {
        return batasUtara;
    }

    public void setBatasUtara(String batasUtara) {
        this.batasUtara = batasUtara;
    }

    public String getBatasSelatan() {
        return batasSelatan;
    }

    public void setBatasSelatan(String batasSelatan) {
        this.batasSelatan = batasSelatan;
    }

    public String getBatasBarat() {
        return batasBarat;
    }

    public void setBatasBarat(String batasBarat) {
        this.batasBarat = batasBarat;
    }

    public String getBatasTimur() {
        return batasTimur;
    }

    public void setBatasTimur(String batasTimur) {
        this.batasTimur = batasTimur;
    }

    public LetakSaluran getLetakSaluran() {
        return letakSaluran;
    }

    public void setLetakSaluran(LetakSaluran letakSaluran) {
        this.letakSaluran = letakSaluran;
    }

    public StrukturSaluran getStrukturSaluran() {
        return strukturSaluran;
    }

    public void setStrukturSaluran(StrukturSaluran strukturSaluran) {
        this.strukturSaluran = strukturSaluran;
    }

    public double getGSB() {
        return GSB;
    }

    public void setGSB(double GSB) {
        this.GSB = GSB;
    }

    public double getGSP() {
        return GSP;
    }

    public void setGSP(double GSP) {
        this.GSP = GSP;
    }

    public double getKDB() {
        return KDB;
    }

    public void setKDB(double KDB) {
        this.KDB = KDB;
    }

    public double getKLB() {
        return KLB;
    }

    public void setKLB(double KLB) {
        this.KLB = KLB;
    }

    public String getPembuanganHujan() {
        return pembuanganHujan;
    }

    public void setPembuanganHujan(String pembuanganHujan) {
        this.pembuanganHujan = pembuanganHujan;
    }

    public String getLain() {
        return lain;
    }

    public void setLain(String lain) {
        this.lain = lain;
    }
    
}
