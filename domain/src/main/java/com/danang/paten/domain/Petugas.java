// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Document
public class Petugas implements Serializable
{
    @Id
    private String kode;
    //@TextIndexed
    private String nip;
    private String user;
    private String psw;
    private String salt;
    private boolean isLogged;
    private String nama;
    private Date lastLogin;
    //@DBRef
    private Staf staf;
    private boolean administrator;
    @DBRef
    private List<Roles> roles;
    @DBRef
    private List<Agenda> agendas;

    public List<Agenda> getAgendas() {
        return agendas;
    }

    public void setAgendas(List<Agenda> agendas) {
        this.agendas = agendas;
    }
    
    
    public Petugas() {
        this.roles = new ArrayList<>();
    }
    
    public boolean isAdministrator() {
        return this.administrator;
    }
    
    public void setAdministrator(final boolean administrator) {
        this.administrator = administrator;
    }
    
    public List<Roles> getRoles() {
        return this.roles;
    }
    
    public void setRoles(final List<Roles> roles) {
        this.roles = roles;
    }
    
    public String getNip() {
        return this.nip;
    }
    
    public void setNip(final String nip) {
        this.nip = nip;
    }
    
    public Staf getStaf() {
        return this.staf;
    }
    
    public void setStaf(final Staf staf) {
        this.staf = staf;
    }
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public String getUser() {
        return this.user;
    }
    
    public void setUser(final String user) {
        this.user = user;
    }
    
    public String getPsw() {
        return this.psw;
    }
    
    public void setPsw(final String psw) {
        this.psw = psw;
    }
    
    public String getSalt() {
        return this.salt;
    }
    
    public void setSalt(final String salt) {
        this.salt = salt;
    }
    
    public boolean isIsLogged() {
        return this.isLogged;
    }
    
    public void setIsLogged(final boolean isLogged) {
        this.isLogged = isLogged;
    }
    
    public String getNama() {
        return this.nama;
    }
    
    public void setNama(final String nama) {
        this.nama = nama;
    }
    
    public Date getLastLogin() {
        return this.lastLogin;
    }
    
    public void setLastLogin(final Date lastLogin) {
        this.lastLogin = lastLogin;
    }
}
