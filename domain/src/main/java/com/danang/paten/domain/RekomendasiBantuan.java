// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class RekomendasiBantuan implements Serializable
{
    @Id
    private String nomor;
    private Date tanggal;
    private DataPenduduk dp;
    private String jenisBantuan;
    private int jumlah;
    private String tujuanProposal;
    private String keterangan;
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public DataPenduduk getDp() {
        return this.dp;
    }
    
    public void setDp(final DataPenduduk dp) {
        this.dp = dp;
    }
    
    public String getJenisBantuan() {
        return this.jenisBantuan;
    }
    
    public void setJenisBantuan(final String jenisBantuan) {
        this.jenisBantuan = jenisBantuan;
    }
    
    public int getJumlah() {
        return this.jumlah;
    }
    
    public void setJumlah(final int jumlah) {
        this.jumlah = jumlah;
    }
    
    public String getTujuanProposal() {
        return this.tujuanProposal;
    }
    
    public void setTujuanProposal(final String tujuanProposal) {
        this.tujuanProposal = tujuanProposal;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
}
