// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

public class BerkasPeriksa
{
    private Berkas berkas;
    private String catatan;
    private boolean valid;
    
    public Berkas getBerkas() {
        return this.berkas;
    }
    
    public void setBerkas(final Berkas berkas) {
        this.berkas = berkas;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
    
    public boolean isValid() {
        return this.valid;
    }
    
    public void setValid(final boolean valid) {
        this.valid = valid;
    }
}
