// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Attendance implements Serializable
{
    @Id
    private int id;
    private Staf staf;
    private Date masuk;
    private Date pulang;
    private Date createdDate;
    private Date updateDate;
    private String keterangan;
    private String catatan;
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public Staf getStaf() {
        return this.staf;
    }
    
    public void setStaf(final Staf staf) {
        this.staf = staf;
    }
    
    public Date getMasuk() {
        return this.masuk;
    }
    
    public void setMasuk(final Date masuk) {
        this.masuk = masuk;
    }
    
    public Date getPulang() {
        return this.pulang;
    }
    
    public void setPulang(final Date pulang) {
        this.pulang = pulang;
    }
    
    public Date getCreatedDate() {
        return this.createdDate;
    }
    
    public void setCreatedDate(final Date createdDate) {
        this.createdDate = createdDate;
    }
    
    public Date getUpdateDate() {
        return this.updateDate;
    }
    
    public void setUpdateDate(final Date updateDate) {
        this.updateDate = updateDate;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
}
