// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PenyerahanBerkas
{
    private Date tanggal;
    @Id
    private String nomor;
    //@DBRef
    private Petugas petugas;
    private String penerima;
    private String alamat;
    private String hp;
    private List<String> items;
    
    public PenyerahanBerkas() {
        this.items = new ArrayList<String>();
    }
    
    public String getPenerima() {
        return this.penerima;
    }
    
    public void setPenerima(final String penerima) {
        this.penerima = penerima;
    }
    
    public String getAlamat() {
        return this.alamat;
    }
    
    public void setAlamat(final String alamat) {
        this.alamat = alamat;
    }
    
    public String getHp() {
        return this.hp;
    }
    
    public void setHp(final String hp) {
        this.hp = hp;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public List<String> getItems() {
        return this.items;
    }
    
    public void setItems(final List<String> items) {
        this.items = items;
    }
}
