// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import com.danang.paten.enumerasi.StatusWarga;
import java.util.Date;
import com.danang.paten.enumerasi.StatusKawin;
import com.danang.paten.enumerasi.JenisKelamin;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class DataPenduduk implements Serializable
{
    @Id
    private String nik;
    //@TextIndexed
    private String nama;
    private String Rt;
    private String Rw;
    //@TextIndexed
    private String lingk;
    private String desa;
    private String kecamatan;
    private String kabupaten;
    private String provinsi;
    private JenisKelamin jenisKelamin;
    private String pekerjaan;
    private String agama;
    private StatusKawin kawin;
    private Date tanggalLahir;
    private String tempatLahir;
    private String nomorHp;
    private String telp;
    private String kodePos;
    private String email;
    private StatusWarga wargaNegara;
    
    public String getProvinsi() {
        return this.provinsi;
    }
    
    public void setProvinsi(final String provinsi) {
        this.provinsi = provinsi;
    }
    
    public String getKecamatan() {
        return this.kecamatan;
    }
    
    public void setKecamatan(final String kecamatan) {
        this.kecamatan = kecamatan;
    }
    
    public String getKabupaten() {
        return this.kabupaten;
    }
    
    public void setKabupaten(final String kabupaten) {
        this.kabupaten = kabupaten;
    }
    
    public StatusWarga getWargaNegara() {
        return this.wargaNegara;
    }
    
    public void setWargaNegara(final StatusWarga wargaNegara) {
        this.wargaNegara = wargaNegara;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public String getKodePos() {
        return this.kodePos;
    }
    
    public void setKodePos(final String kodePos) {
        this.kodePos = kodePos;
    }
    
    public String getNomorHp() {
        return this.nomorHp;
    }
    
    public void setNomorHp(final String nomorHp) {
        this.nomorHp = nomorHp;
    }
    
    public String getTelp() {
        return this.telp;
    }
    
    public void setTelp(final String telp) {
        this.telp = telp;
    }
    
    public String getNik() {
        return this.nik;
    }
    
    public void setNik(final String nik) {
        this.nik = nik;
    }
    
    public String getNama() {
        return this.nama;
    }
    
    public void setNama(final String nama) {
        this.nama = nama;
    }
    
    public String getRt() {
        return this.Rt;
    }
    
    public void setRt(final String Rt) {
        this.Rt = Rt;
    }
    
    public String getRw() {
        return this.Rw;
    }
    
    public void setRw(final String Rw) {
        this.Rw = Rw;
    }
    
    public String getLingk() {
        return this.lingk;
    }
    
    public void setLingk(final String lingk) {
        this.lingk = lingk;
    }
    
    public String getDesa() {
        return this.desa;
    }
    
    public void setDesa(final String desa) {
        this.desa = desa;
    }
    
    public JenisKelamin getJenisKelamin() {
        return this.jenisKelamin;
    }
    
    public void setJenisKelamin(final JenisKelamin jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }
    
    public String getPekerjaan() {
        return this.pekerjaan;
    }
    
    public void setPekerjaan(final String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }
    
    public String getAgama() {
        return this.agama;
    }
    
    public void setAgama(final String agama) {
        this.agama = agama;
    }
    
    public StatusKawin getKawin() {
        return this.kawin;
    }
    
    public void setKawin(final StatusKawin kawin) {
        this.kawin = kawin;
    }
    
    public Date getTanggalLahir() {
        return this.tanggalLahir;
    }
    
    public void setTanggalLahir(final Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getTempatLahir() {
        return this.tempatLahir;
    }
    
    public void setTempatLahir(final String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }
}
