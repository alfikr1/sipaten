// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class TerimaBerkas implements Serializable {

    @Id
    private String nomor;
    private Date tanggalTerima;
    //@DBRef
    private Petugas petugas;
    private String disposisi;
    //@DBRef
    private Layanan layanan;
    private List<Berkas> berkas;
    private String namaPengaju;
    //@DBRef
    private DataPenduduk pemohon;
    //@DBRef
    private DataPenduduk ijin;
    private String status;
    private String nomorDesa;
    private Date tglSuratDesa;
    //@TextIndexed
    private String keterangan;

    public TerimaBerkas() {
        this.berkas = new ArrayList<Berkas>();
    }

    public String getKeterangan() {
        return this.keterangan;
    }

    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }

    public Date getTglSuratDesa() {
        return this.tglSuratDesa;
    }

    public void setTglSuratDesa(final Date tglSuratDesa) {
        this.tglSuratDesa = tglSuratDesa;
    }

    public String getNomorDesa() {
        return this.nomorDesa;
    }

    public void setNomorDesa(final String nomorDesa) {
        this.nomorDesa = nomorDesa;
    }

    public DataPenduduk getPemohon() {
        return this.pemohon;
    }

    public void setPemohon(final DataPenduduk pemohon) {
        this.pemohon = pemohon;
    }

    public DataPenduduk getIjin() {
        return this.ijin;
    }

    public void setIjin(final DataPenduduk ijin) {
        this.ijin = ijin;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getNamaPengaju() {
        return this.namaPengaju;
    }

    public void setNamaPengaju(final String namaPengaju) {
        this.namaPengaju = namaPengaju;
    }

    public String getNomor() {
        return this.nomor;
    }

    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }

    public Date getTanggalTerima() {
        return this.tanggalTerima;
    }

    public void setTanggalTerima(final Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public Petugas getPetugas() {
        return this.petugas;
    }

    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }

    public String getDisposisi() {
        return this.disposisi;
    }

    public void setDisposisi(final String disposisi) {
        this.disposisi = disposisi;
    }

    public Layanan getLayanan() {
        return this.layanan;
    }

    public void setLayanan(final Layanan layanan) {
        this.layanan = layanan;
    }

    public List<Berkas> getBerkas() {
        return this.berkas;
    }

    public void setBerkas(final List<Berkas> berkas) {
        this.berkas = berkas;
    }
}
