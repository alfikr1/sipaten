
package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Staf implements Serializable
{
    @Id
    private String nip;
    private String nama;
    private Date tanggalLahir;
    private String alamat;
    private String Jabatan;
    private String noHp;
    
    public String getNip() {
        return this.nip;
    }
    
    public void setNip(final String nip) {
        this.nip = nip;
    }
    
    public String getNama() {
        return this.nama;
    }
    
    public void setNama(final String nama) {
        this.nama = nama;
    }
    
    public Date getTanggalLahir() {
        return this.tanggalLahir;
    }
    
    public void setTanggalLahir(final Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getAlamat() {
        return this.alamat;
    }
    
    public void setAlamat(final String alamat) {
        this.alamat = alamat;
    }
    
    public String getJabatan() {
        return this.Jabatan;
    }
    
    public void setJabatan(final String Jabatan) {
        this.Jabatan = Jabatan;
    }
    
    public String getNoHp() {
        return this.noHp;
    }
    
    public void setNoHp(final String noHp) {
        this.noHp = noHp;
    }
}
