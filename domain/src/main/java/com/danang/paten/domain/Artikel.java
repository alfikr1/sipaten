// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Artikel
{
    @Id
    private String id;
    private String body;
    private String permalink;
    private String author;
    private String title;
    private List<String> tags;
    private List<Komentar> comments;
    private Date tanggal;
    private boolean publish;
    private boolean utama;

    public boolean isPublish() {
        return publish;
    }

    public void setPublish(boolean publish) {
        this.publish = publish;
    }
    
    public boolean isUtama() {
        return utama;
    }

    public void setUtama(boolean utama) {
        this.utama = utama;
    }
    
    
    public Artikel() {
        this.tags = new ArrayList<String>();
        this.comments = new ArrayList<Komentar>();
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getBody() {
        return this.body;
    }
    
    public void setBody(final String body) {
        this.body = body;
    }
    
    public String getPermalink() {
        return this.permalink;
    }
    
    public void setPermalink(final String permalink) {
        this.permalink = permalink;
    }
    
    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(final String author) {
        this.author = author;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public List<String> getTags() {
        return this.tags;
    }
    
    public void setTags(final List<String> tags) {
        this.tags = tags;
    }
    
    public List<Komentar> getComments() {
        return this.comments;
    }
    
    public void setComments(final List<Komentar> comments) {
        this.comments = comments;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
}
