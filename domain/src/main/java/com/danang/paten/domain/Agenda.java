// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Agenda
{
    @Id
    private String id;
    private Date tanggal;
    private String keterangan;
    private boolean selesai;
    private Date createDate;
    private Petugas petugas;

    public Petugas getPetugas() {
        return petugas;
    }

    public void setPetugas(Petugas petugas) {
        this.petugas = petugas;
    }
    

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public String getKeterangan() {
        return this.keterangan;
    }
    
    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }
    
    public boolean isSelesai() {
        return this.selesai;
    }
    
    public void setSelesai(final boolean selesai) {
        this.selesai = selesai;
    }
}
