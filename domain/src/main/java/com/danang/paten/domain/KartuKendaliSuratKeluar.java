// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.Date;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class KartuKendaliSuratKeluar implements Serializable
{
    @Id
    private String id;
    
    private String nomorSurat;
    @TextIndexed
    private String kode;
    @TextIndexed
    private String idx;
    @TextIndexed
    private String ringkas;
    @TextIndexed
    private String kepada;
    @TextIndexed
    private String dari;
    @TextIndexed
    private String pengolah;
    private Date tanggal;
    @TextIndexed
    private String lampiran;
    @TextIndexed
    private String catatan;
    private Date tnggalKirim;
    private Date tanggalTerima;
    private String privasi;
    private String prioritas;
    
    public String getNomorSurat() {
        return this.nomorSurat;
    }
    
    public void setNomorSurat(final String nomorSurat) {
        this.nomorSurat = nomorSurat;
    }
    
    public String getDari() {
        return this.dari;
    }
    
    public void setDari(final String dari) {
        this.dari = dari;
    }
    
    public String getPrioritas() {
        return this.prioritas;
    }
    
    public void setPrioritas(final String prioritas) {
        this.prioritas = prioritas;
    }
    
    public String getPrivasi() {
        return this.privasi;
    }
    
    public void setPrivasi(final String privasi) {
        this.privasi = privasi;
    }
    
    public Date getTnggalKirim() {
        return this.tnggalKirim;
    }
    
    public void setTnggalKirim(final Date tnggalKirim) {
        this.tnggalKirim = tnggalKirim;
    }
    
    public Date getTanggalTerima() {
        return this.tanggalTerima;
    }
    
    public void setTanggalTerima(final Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public String getIdx() {
        return this.idx;
    }
    
    public void setIdx(final String idx) {
        this.idx = idx;
    }
    
    public String getRingkas() {
        return this.ringkas;
    }
    
    public void setRingkas(final String ringkas) {
        this.ringkas = ringkas;
    }
    
    public String getKepada() {
        return this.kepada;
    }
    
    public void setKepada(final String kepada) {
        this.kepada = kepada;
    }
    
    public String getPengolah() {
        return this.pengolah;
    }
    
    public void setPengolah(final String pengolah) {
        this.pengolah = pengolah;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public String getLampiran() {
        return this.lampiran;
    }
    
    public void setLampiran(final String lampiran) {
        this.lampiran = lampiran;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
}
