// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

public class Berkas
{
    private String dokumen;
    private boolean optional;
    private boolean valid;
    
    public boolean isValid() {
        return this.valid;
    }
    
    public void setValid(final boolean valid) {
        this.valid = valid;
    }
    
    public String getDokumen() {
        return this.dokumen;
    }
    
    public void setDokumen(final String dokumen) {
        this.dokumen = dokumen;
    }
    
    public boolean isOptional() {
        return this.optional;
    }
    
    public void setOptional(final boolean optional) {
        this.optional = optional;
    }
}
