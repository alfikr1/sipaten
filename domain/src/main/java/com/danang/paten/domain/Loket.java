// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.ArrayList;
import org.springframework.data.mongodb.core.mapping.DBRef;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Loket implements Serializable
{
    @Id
    private String kode;
    private String desk;
    private boolean buka;
    @DBRef
    private List<Layanan> layanans;
    
    public Loket() {
        this.layanans = new ArrayList<Layanan>();
    }
    
    public boolean isBuka() {
        return this.buka;
    }
    
    public void setBuka(final boolean buka) {
        this.buka = buka;
    }
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public String getDesk() {
        return this.desk;
    }
    
    public void setDesk(final String desk) {
        this.desk = desk;
    }
    
    public List<Layanan> getLayanans() {
        return this.layanans;
    }
    
    public void setLayanans(final List<Layanan> layanans) {
        this.layanans = layanans;
    }
}
