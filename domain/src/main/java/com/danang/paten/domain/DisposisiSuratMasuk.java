// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.List;

public class DisposisiSuratMasuk
{
    private List<String> diteruskan;
    private List<String> tugas;
    private String sifat;
    private String catatan;
    private Petugas petugas;
    private String camat;
    
    public String getCamat() {
        return this.camat;
    }
    
    public void setCamat(final String camat) {
        this.camat = camat;
    }
    
    public List<String> getDiteruskan() {
        return this.diteruskan;
    }
    
    public void setDiteruskan(final List<String> diteruskan) {
        this.diteruskan = diteruskan;
    }
    
    public List<String> getTugas() {
        return this.tugas;
    }
    
    public void setTugas(final List<String> tugas) {
        this.tugas = tugas;
    }
    
    public String getSifat() {
        return this.sifat;
    }
    
    public void setSifat(final String sifat) {
        this.sifat = sifat;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
}
