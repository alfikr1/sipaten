// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Counter implements Serializable
{
    @Id
    private String kode;
    private int seri;
    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public int getSeri() {
        return this.seri;
    }
    
    public void setSeri(final int seri) {
        this.seri = seri;
    }
}
