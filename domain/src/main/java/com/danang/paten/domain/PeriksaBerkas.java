// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.List;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class PeriksaBerkas implements Serializable
{
    @Id
    private String nomor;
    private Date tanggal;
    private Petugas petugas;
    private boolean lanjut;
    private String catatan;
    private List<BerkasPeriksa> periksa;
    
    public List<BerkasPeriksa> getPeriksa() {
        return this.periksa;
    }
    
    public void setPeriksa(final List<BerkasPeriksa> periksa) {
        this.periksa = periksa;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public Petugas getPetugas() {
        return this.petugas;
    }
    
    public void setPetugas(final Petugas petugas) {
        this.petugas = petugas;
    }
    
    public boolean isLanjut() {
        return this.lanjut;
    }
    
    public void setLanjut(final boolean lanjut) {
        this.lanjut = lanjut;
    }
}
