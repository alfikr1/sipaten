// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import com.danang.paten.enumerasi.StatusPekerjaan;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class TransaksiLayanan implements Serializable
{
    @Id
    private String nomor;
    private Date tanggal;
    //@DBRef
    private DataPenduduk penduduk;
    //@DBRef
    private TerimaBerkas terimaBerkas;
    //@DBRef
    private PeriksaBerkas periksaBerkas;
    //@DBRef
    private PenetapanBerkas penetapanBerkas;
    //@DBRef
    private PenyerahanBerkas penyerahanBerkas;
    private StatusPekerjaan statusPekerjaan;
    private Layanan layanan;
    private Date tanggalKetetapan;
    private Date tanggalTerbit;
    private boolean validasi;
    private Date tanggalPenyerahan;
    private String catatan;
    private String catatanPengiriman;
    private String konten;
    private Date berlakuSurat;
    private Date expiredSurat;
    private Date periksaDate;
    private String nomorKetetapan;
    private boolean ditetapkan;
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Nomor").append(" : ").append(this.nomor).append("\n");
        sb.append("Tanggal Masuk").append(" : ").append(this.terimaBerkas.getTanggalTerima()).append("\n");
        sb.append("Penerima").append(" : ").append(this.terimaBerkas.getPetugas().getNama()).append("\n");
        sb.append("Pemohon").append(" : ").append(this.penduduk.getNama());
        return sb.toString();
    }
    
    public boolean isDitetapkan() {
        return this.ditetapkan;
    }
    
    public void setDitetapkan(final boolean ditetapkan) {
        this.ditetapkan = ditetapkan;
    }
    
    public String getNomorKetetapan() {
        return this.nomorKetetapan;
    }
    
    public void setNomorKetetapan(final String nomorKetetapan) {
        this.nomorKetetapan = nomorKetetapan;
    }
    
    public PenyerahanBerkas getPenyerahanBerkas() {
        return this.penyerahanBerkas;
    }
    
    public void setPenyerahanBerkas(final PenyerahanBerkas penyerahanBerkas) {
        this.penyerahanBerkas = penyerahanBerkas;
    }
    
    public PenetapanBerkas getPenetapanBerkas() {
        return this.penetapanBerkas;
    }
    
    public void setPenetapanBerkas(final PenetapanBerkas penetapanBerkas) {
        this.penetapanBerkas = penetapanBerkas;
    }
    
    public Date getPeriksaDate() {
        return this.periksaDate;
    }
    
    public void setPeriksaDate(final Date periksaDate) {
        this.periksaDate = periksaDate;
    }
    
    public Date getBerlakuSurat() {
        return this.berlakuSurat;
    }
    
    public void setBerlakuSurat(final Date berlakuSurat) {
        this.berlakuSurat = berlakuSurat;
    }
    
    public Date getExpiredSurat() {
        return this.expiredSurat;
    }
    
    public void setExpiredSurat(final Date expiredSurat) {
        this.expiredSurat = expiredSurat;
    }
    
    public String getKonten() {
        return this.konten;
    }
    
    public void setKonten(final String konten) {
        this.konten = konten;
    }
    
    public String getCatatan() {
        return this.catatan;
    }
    
    public void setCatatan(final String catatan) {
        this.catatan = catatan;
    }
    
    public String getCatatanPengiriman() {
        return this.catatanPengiriman;
    }
    
    public void setCatatanPengiriman(final String catatanPengiriman) {
        this.catatanPengiriman = catatanPengiriman;
    }
    
    public String getNomor() {
        return this.nomor;
    }
    
    public void setNomor(final String nomor) {
        this.nomor = nomor;
    }
    
    public Date getTanggal() {
        return this.tanggal;
    }
    
    public void setTanggal(final Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public DataPenduduk getPenduduk() {
        return this.penduduk;
    }
    
    public void setPenduduk(final DataPenduduk penduduk) {
        this.penduduk = penduduk;
    }
    
    public TerimaBerkas getTerimaBerkas() {
        return this.terimaBerkas;
    }
    
    public void setTerimaBerkas(final TerimaBerkas terimaBerkas) {
        this.terimaBerkas = terimaBerkas;
    }
    
    public PeriksaBerkas getPeriksaBerkas() {
        return this.periksaBerkas;
    }
    
    public void setPeriksaBerkas(final PeriksaBerkas periksaBerkas) {
        this.periksaBerkas = periksaBerkas;
    }
    
    public StatusPekerjaan getStatusPekerjaan() {
        return this.statusPekerjaan;
    }
    
    public void setStatusPekerjaan(final StatusPekerjaan statusPekerjaan) {
        this.statusPekerjaan = statusPekerjaan;
    }
    
    public Layanan getLayanan() {
        return this.layanan;
    }
    
    public void setLayanan(final Layanan layanan) {
        this.layanan = layanan;
    }
    
    public Date getTanggalKetetapan() {
        return this.tanggalKetetapan;
    }
    
    public void setTanggalKetetapan(final Date tanggalKetetapan) {
        this.tanggalKetetapan = tanggalKetetapan;
    }
    
    public Date getTanggalTerbit() {
        return this.tanggalTerbit;
    }
    
    public void setTanggalTerbit(final Date tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }
    
    public boolean isValidasi() {
        return this.validasi;
    }
    
    public void setValidasi(final boolean validasi) {
        this.validasi = validasi;
    }
    
    public Date getTanggalPenyerahan() {
        return this.tanggalPenyerahan;
    }
    
    public void setTanggalPenyerahan(final Date tanggalPenyerahan) {
        this.tanggalPenyerahan = tanggalPenyerahan;
    }
}
