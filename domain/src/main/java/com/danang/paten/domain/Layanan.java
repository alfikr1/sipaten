// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Layanan implements Serializable
{
    @Id
    private String kode;
    private String namaLayanan;
    private String disposisi;
    private int mulaiSeri;
    private boolean masaBerlaku;
    private String kodeTetapan;
    @DBRef
    private Layanan parent;
    private boolean langsungTetapkan;
    private List<Berkas> syarat;

    public String getKodeTetapan() {
        return kodeTetapan;
    }

    public void setKodeTetapan(String kodeTetapan) {
        this.kodeTetapan = kodeTetapan;
    }
    
    public Layanan() {
        this.syarat = new ArrayList<Berkas>();
    }
    
    public boolean isMasaBerlaku() {
        return this.masaBerlaku;
    }
    
    public void setMasaBerlaku(final boolean masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }
    
    public boolean isLangsungTetapkan() {
        return this.langsungTetapkan;
    }
    
    public void setLangsungTetapkan(final boolean langsungTetapkan) {
        this.langsungTetapkan = langsungTetapkan;
    }
    
    public Layanan getParent() {
        return this.parent;
    }
    
    public void setParent(final Layanan parent) {
        this.parent = parent;
    }
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public String getNamaLayanan() {
        return this.namaLayanan;
    }
    
    public void setNamaLayanan(final String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }
    
    public String getDisposisi() {
        return this.disposisi;
    }
    
    public void setDisposisi(final String disposisi) {
        this.disposisi = disposisi;
    }
    
    public int getMulaiSeri() {
        return this.mulaiSeri;
    }
    
    public void setMulaiSeri(final int mulaiSeri) {
        this.mulaiSeri = mulaiSeri;
    }
    
    public List<Berkas> getSyarat() {
        return this.syarat;
    }
    
    public void setSyarat(final List<Berkas> syarat) {
        this.syarat = syarat;
    }
    
    @Override
    public String toString() {
        return "Layanan{kode=" + this.kode + ", namaLayanan=" + this.namaLayanan + ", disposisi=" + this.disposisi + ", mulaiSeri=" + this.mulaiSeri + ", parent=" + this.parent + ", syarat=" + this.syarat + '}';
    }
}
