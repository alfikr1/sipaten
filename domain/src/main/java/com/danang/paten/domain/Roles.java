// 
// Decompiled by Procyon v0.5.30
// 

package com.danang.paten.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document
public class Roles implements Serializable
{
    @Id
    private String kode;
    private String deskripsi;
    @DBRef
    private Layanan layanan;
    private boolean baca;
    private boolean tulis;
    private boolean hapus;
    
    public String getKode() {
        return this.kode;
    }
    
    public void setKode(final String kode) {
        this.kode = kode;
    }
    
    public Layanan getLayanan() {
        return this.layanan;
    }
    
    public void setLayanan(final Layanan layanan) {
        this.layanan = layanan;
    }
    
    public boolean isBaca() {
        return this.baca;
    }
    
    public void setBaca(final boolean baca) {
        this.baca = baca;
    }
    
    public boolean isTulis() {
        return this.tulis;
    }
    
    public void setTulis(final boolean tulis) {
        this.tulis = tulis;
    }
    
    public boolean isHapus() {
        return this.hapus;
    }
    
    public void setHapus(final boolean hapus) {
        this.hapus = hapus;
    }
    
    public String getDeskripsi() {
        return this.deskripsi;
    }
    
    public void setDeskripsi(final String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
