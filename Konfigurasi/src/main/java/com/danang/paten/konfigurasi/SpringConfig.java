package com.danang.paten.konfigurasi;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.Arrays;
import org.elasticsearch.client.transport.TransportClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories({"com.danang.paten.repository"})
//@EnableElasticsearchRepositories({"com.danang.paten.es.repository"})
@ComponentScan({"com.danang.paten.service", "com.danang.paten.controller"})
public class SpringConfig {

    @Bean
    public MongoCredential mongoCredential() {
        Konfigurasi k = new Konfigurasi();
        MongoCredential mc = MongoCredential.createCredential(k.getDbusername(), k.getDbname(), k.getDbpassword().toCharArray());
        //new UserCredentialsConnectionFactoryAdapter();
        return mc;
    }

    @Bean
    public ServerAddress serverAddress() {
        Konfigurasi k = new Konfigurasi();
        return new ServerAddress(k.getDbhost(), k.getDbport());
    }

    @Bean
    public MongoClient mongo()
            throws UnknownHostException, MalformedURLException {
        Konfigurasi k = new Konfigurasi();
        String uri = "mongodb://" + k.getDbhost() + ":" + k.getDbport();
        //return new MongoClient(new MongoClientURI(uri));
        if(k.getDbusername().isEmpty()){
            return new MongoClient(new MongoClientURI(uri));
        }
        return new MongoClient(serverAddress(), Arrays.asList(mongoCredential()));
    }

    @Bean
    public SimpleMongoDbFactory mongoDbFactory()
            throws UnknownHostException, MalformedURLException {
        Konfigurasi k = new Konfigurasi();
        return new SimpleMongoDbFactory(mongo(), k.getDbname());
        //return new SimpleMongoDbFactory(mongo());
    }

    @Bean
    public MongoTemplate mongoTemplate()
            throws UnknownHostException, MalformedURLException {
        return new MongoTemplate(mongoDbFactory());
    }

    public TransportClient transportClient() {

        TransportClient tc = new TransportClient();

        return tc;
    }

    /*public NodeBuilder nodeBuilder() {
        NodeBuilder nodeBuilder = new NodeBuilder();
        nodeBuilder.clusterName("paten").client(true);
        ImmutableSettings.Builder setting = nodeBuilder.getSettings();
        setting.put("id", "client");
        setting.put("local", true);
        nodeBuilder.settings(setting.build());
        return nodeBuilder;
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchTemplate(nodeBuilder().local(true).node().client());
    }*/
}
