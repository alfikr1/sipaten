// 
// Decompiled by Procyon v0.5.30
// 
package com.danang.paten.konfigurasi;

import java.io.File;
import org.apache.log4j.Logger;
import org.ini4j.Ini;

public class Konfigurasi {

    private Ini ini;

    private final Logger log = Logger.getLogger(Konfigurasi.class);

    public String getDbhost() {
        String host = ini.get("DATABASE", "HOST");
        if (host == null) {
            setDbhost("localhost");
            return "localhost";
        }
        return host;
    }

    public void setDbhost(String dbhost) {
        try {
            ini.put("DATABASE", "HOST", dbhost);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public int getDbport() {
        String port = ini.get("DATABASE", "PORT");
        if (port == null) {
            setDbport(27017);
            return 27017;
        }
        return Integer.valueOf(port);
    }

    public void setDbport(int dbport) {
        try {
            ini.put("DATABASE", "PORT", dbport);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public String getDbusername() {
        String user = ini.get("DATABASE", "USERNAME");
        if (user == null) {
            setDbusername("admin");
            return "admin";
        }
        return user;
    }

    public void setDbusername(String dbusername) {
        try {
            ini.put("DATABASE", "USERNAME", dbusername);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public String getDbpassword() {
        String passw = ini.get("DATABASE", "PASSWORD");
        if (passw == null) {
            setDbpassword("admin");
            return "admin";
        }
        return passw;
    }

    public void setDbpassword(String dbpassword) {
        try {
            ini.put("DATABASE", "PASSWORD", dbpassword);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public String getDbname() {
        String dbname = ini.get("DATABASE", "DBNAME");
        if (dbname == null) {
            setDbname("bergaskec");
            return "bergaskec";
        }
        return dbname;
    }

    public void setDbname(String dbname) {
        try {
            ini.put("DATABASE", "DBNAME", dbname);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public Konfigurasi() {
        final File file = new File("Konfig.ini");
        if (!file.exists()) {
            try {
                file.createNewFile();
                this.defaultKonfig();
            } catch (Exception e) {
                log.error(e);
            }
        }
        try {
            this.ini = new Ini(file);
        } catch (Exception e) {
            log.error(e);
        }
    }

    public String getConnectionModel() {
        String connection = ini.get("SMS", "CONNECTION");
        if (connection == null) {
            setConnection("at115200");
            return "at115200";
        }
        return connection;
    }

    public void setConnection(String connection) {
        try {
            ini.put("SMS", "CONNECTION", connection);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public String getComModem() {
        String getComModem = ini.get("SMS", "MODEM NAME");
        return getComModem;
    }

    public String getComPORT() {
        return ini.get("SMS", "PORT");

    }

    public int getBaudRate() {
        return 115200;
    }

    public String getSMSCenter() {
        return ini.get("SMS", "SMS CENTER");
    }

    public String getMerkModem() {
        return ini.get("SMS", "MERK");

    }

    public String getSeriModem() {
        return ini.get("SMS", "SERI MODEM");
    }

    public String getPin() {
        return ini.get("SMS", "PIN");
    }

    public boolean isReadySms() {
        return ini.get("SMS", "AKTIF").equals("TRUE");
    }

    public void setSMSReady(boolean aktif) {
        try {
            ini.put("SMS", "AKTIF", "TRUE");
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public void setPort(int port) {
        try {
            ini.put("SERVER", "PORT", port);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public int getPort() {
        String port = ini.get("SERVER", "PORT");
        if (port == null) {
            defaultKonfig();
        }
        port = ini.get("SERVER", "PORT");
        return Integer.parseInt(port);
    }

    public String getTemplate() {
        String template = ini.get("SMS", "TEMPLATE");
        if (template == null) {
            template = "Berkas {LAYANAN} anda telah dapat diambil. Terima Kasih";
            setTemplate(template);
        }
        return template;
    }

    public void setTemplate(String template) {
        try {
            ini.put("SMS", "TEMPLATE", template);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    private void defaultKonfig() {
        try {
            ini = new Ini(new File("Konfig.ini"));
            ini.put("VIDEO", "VOLUME", 50);
            ini.put("VIDEO", "TEXT", "Hello world \nHello running text \nHello marque");
            ini.put("GLOBAL", "VIDEOPATH", "");
            ini.put("SERVER", "PORT", 4567);
            ini.put("DATABASE", "HOST", "127.0.0.1");
            ini.put("DATABASE", "PORT", 27017);
            ini.put("DATABASE","DBNAME","bergaskec");
            ini.put("DATABASE", "USERNAME","bergas");
            ini.put("DATABASE", "PASSWORD", "bergasceria");
            ini.put("SMS", "PORT", "");
            ini.put("SMS", "MODEM NAME", "");
            ini.put("SMS", "MERK", "HUAWEI");
            ini.put("SMS", "SERI MODEM", "");
            ini.put("SMS", "PIN", "0000");
            ini.put("SMS", "SMS CENTER", "");
            ini.put("SMS", "CONNECTION", "at115200");
            ini.put("SMS", "AKTIF", "FALSE");
            ini.put("SMS", "TEMPLATE", "");
            this.ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public void setVideoPath(String str) {
        try {
            ini.put("GLOBAL", "VIDEOPATH", str);
            ini.store();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public String getVideoPath() {
        return this.ini.get("GLOBAL", "VIDEOPATH");
    }

    public int getVolume() {
        return Integer.valueOf(this.ini.get("VIDEO", "VOLUME"));
    }

    public String getRunningText() {
        return this.ini.get("VIDEO", "TEXT");
    }

    public void setRunning(String txt) {
        try {
            this.ini.put("VIDEO", "TEXT", txt);
            this.ini.store();
        } catch (Exception ex) {
        }
    }

    public void setVolume(int vol) {
        try {
            this.ini.put("VIDEO", "VOLUME", vol);
            this.ini.store();
        } catch (Exception ex) {
        }
    }

    public static void main(String[] args) {
        Konfigurasi k = new Konfigurasi();
        System.out.println(k.getDbport());
        System.out.println(k.getDbpassword());
        System.out.println(k.getDbusername());
        System.out.println(k.getDbname());
    }
}
